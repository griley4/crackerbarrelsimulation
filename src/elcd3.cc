#include "stdafx.h"
#include "elcd3.h"
#include "unistd.h"

int indexmirror;
double zmirror;

// Output global variables:
int Nsubel,Nsp[Ngroup+1],indexgroup[Nmax+1];
double element[Nmax+1][2][6];
double common[100];
double Cgauss[Nmax+1][Nmax+2];
double z0a[Ngroup+1][Nspmax+1];

// Filename for source coefficients
char elsourcefile[200];
char inputelectrode[200];

////////////////////////////////////////////////////////

void elcd2(int scale,double power,double *z0min,double *z0max,double
           *delz0,char *inputelectrodesuffix)
{
  strcpy( elsourcefile,"elsource_");
  strncat( elsourcefile, inputelectrodesuffix, 184);
  strcpy( inputelectrode,"inputelectrodes_");
  strncat( inputelectrode, inputelectrodesuffix, 184);
//
  input_electrodes(scale,power,inputelectrode);
  chargedensities();
  input_sourcepoints(z0min,z0max,delz0);
  elsource(elsourcefile);
//
  return;
}

////////////////////////////////////////////////////////

// Electrode parameters (geometry and potential):

void input_electrodes(int scale,double power,char *inputelectrodeName)
{
// This function computes the geometrical parameters of the
// electrode subelements.
  double zA[Nmax],rA[Nmax],zB[Nmax],rB[Nmax],U[Nmax],L;
  int num[Nmax],istart,Nelectrode,igroupelectrode[Nmax],l;
  int igroupelectrodemax;
  FILE *fp;

// 1. step: input from file inputelectrode
  fp=fopen(inputelectrodeName,"r");
  if (!fp)
  {
      G4cout<<"Message from function input_electrodes:" << G4endl;
      G4cout<<"Cannot open the electrode input file " << inputelectrodeName << "!" << G4endl;
      G4cout<<"Program running is stopped !!! " << G4endl;
      exit(0);
  }
  fscanf(fp,"%i",&Nelectrode);
  if( Nelectrode>Nmax-1)
  {
      G4cout<<"Message from function input_electrodes:" << G4endl;
      G4cout<<" Nelectrode>Nmax-1 !!!" << G4endl;
      G4cout<<"Computation is stopped !!! " << G4endl;
      exit(0);
  }
  igroupelectrodemax=0;
  for(l=1;l<=Nelectrode;l++)
  {
    fscanf(fp,"%i %le %le %le %le %le %i",&igroupelectrode[l],&zA[l],&rA[l],&zB[l],&rB[l],&U[l],&num[l]);
    if(igroupelectrode[l]>igroupelectrodemax)
      igroupelectrodemax=igroupelectrode[l];
    L=sqrt((zA[l]-zB[l])*(zA[l]-zB[l])+(rA[l]-rB[l])*(rA[l]-rB[l]));
    if(L<1.e-12 || rA[l]<0. || rB[l]<0. || num[l]<1)
    {
      G4cout<<"Message from function input_electrodes:" << G4endl;
      G4cout<<"Wrong input data for electrodes !!!" << G4endl;
      G4cout<<"Computation is stopped !!! " << G4endl;
      exit(0);
    }
    if( igroupelectrode[l]<1 || igroupelectrode[l]>Ngroup)
    {
      G4cout<<"Message from function input_electrodes:" << G4endl;
      G4cout<<" igroupelectrode[l]<1  or  igroupelectrode[l]>Ngroup !!!" << G4endl;
      G4cout<<"Computation is  stopped !!! " << G4endl;
      exit(0);
    }
  }
  if( igroupelectrodemax != Ngroup)
  {
      G4cout<<"Message from function input_electrodes:" << G4endl;
      G4cout<<" igroupelectrodemax should be equal to Ngroup !!!" << G4endl;
      G4cout<<"Computation is stopped !!! " << G4endl;
      exit(0);
  }
  fclose(fp);
// 2. step: calculation of independent electrode subelement parameters
  istart=1;
  Nsubel=0;
  for(l=1;l<=Nelectrode;l++)
  {
    inputAB(igroupelectrode[l],zA[l],rA[l],zB[l],rB[l],U[l],num[l]*scale,istart,power);
    istart+=2*num[l]*scale;
    Nsubel+=2*num[l]*scale;
  }
// 3. step: Test of the Nmax>=Nsubel  conditions:
  if(Nsubel>Nmax)
  {
    G4cout<<"Message from function input_electrodes:" << G4endl;
    G4cout<<"Nsubel>Nmax !!! Computation is stopped !!! " << G4endl;
    exit(0);
  }
// 4. step: calculation of all electrode parameters by mirror
//   symmetry transformation
  if(indexmirror==1)
    mirrorsymm();
  return;
}

////////////////////////////////////////////////////////

void inputAB(int igroup,double zA,double rA,double zB,double rB,double U,
             int num,int istart,double power)
// This function computes subelements for a long conical full electrode
// with endpoints A and B (for sm=0 index).
// igroup: electrode group index
// zA, rA: z and r coordinates of endpoint A
// zB, rB: z and r coordinates of endpoint B
// U: potential of the electrode
// num: number of subelements within the electrode is 2*num
// istart: the first subelement has index i=istart,
//         the last one has index i=istart+2*num-1
// power: for  power>1 the subelements are smaller at the endpoints A and B
//        (if the field is higher at the endpoints, one should use power>1
//        (2,3 or 4), in order to get better accuracy ;
//        power=1: the subelements have equal lengths
{
  int i,k;
  double knum;
  for(k=1;k<=num;k++)
  {
    i=istart+k-1;
    knum=fabs((k-1.)/(1.*num));
    if(i>Nmax)
    {
      G4cout<<"Message from subroutine inputAB: "
		<<"electrode subelement index i is larger than Nmax"
		<<"Computation is  stopped !!! " << G4endl;
      exit(0);
    }
    element[i][0][1]=zA+pow(knum,power)*(zB-zA)/2.;
    element[i][0][2]=rA+pow(knum,power)*(rB-rA)/2.;
    knum=k/(1.*num);
    element[i][0][3]=zA+pow(knum,power)*(zB-zA)/2.;
    element[i][0][4]=rA+pow(knum,power)*(rB-rA)/2.;
    element[i][0][5]=U;
    indexgroup[i]=igroup;
  }
  for(k=1;k<=num;k++)
  {
    i=istart+num+k-1;
    if(i>Nmax)
    {
      G4cout<<"Message from subroutine inputAB: "
		<<"electrode subelement index i is larger than Nmax. "
		<<"Computation is  stopped !!! "<<G4endl;
      exit(0);
    }
    knum=fabs((k-1.)/(1.*num));
    element[i][0][1]=zB+pow(knum,power)*(zA-zB)/2.;
    element[i][0][2]=rB+pow(knum,power)*(rA-rB)/2.;
    knum=k/(1.*num);
    element[i][0][3]=zB+pow(knum,power)*(zA-zB)/2.;
    element[i][0][4]=rB+pow(knum,power)*(rA-rB)/2.;
    element[i][0][5]=U;
    indexgroup[i]=igroup;
  }
  return;
}

////////////////////////////////////////////////////////////////////////

void mirrorsymm()
{
// This function computes electrode parameters by
// mirror symmetry transformation
// (computation of element[i][1][j] from element[i][0][j])
  int i,imax;
// 
  if (true)
  {
	  G4cout << "Message from Stefan in subroutine mirrorsym: "
		  << "We should never be here!" << G4endl;
	  exit(1);
  }
  imax = Nsubel;
  for(i=1;i<=imax;i++)
  {
    element[i][1][0]=element[i][0][0];
    element[i][1][2]=element[i][0][2];
    element[i][1][4]=element[i][0][4];
    element[i][1][5]=element[i][0][5];
    element[i][1][1]=2.*zmirror-element[i][0][1];
    element[i][1][3]=2.*zmirror-element[i][0][3];
  }
  return;
}

////////////////////////////////////////////////////////

void chargedensities()
{
// This function computes the charge densities of all independent
// subelements.
  int N,i,j,sm;
  double U[Nmax+1],sigma[Nmax+1];
  if (Nsubel > Nmax)
  {
	  G4cout << "Message from Stefan in subroutine chargedensities: "
		  << "Nsubel too large" << G4endl;
	  exit(-1);
  }
  N = Nsubel;
// 1. step: all charge densities are given the value 1.:
  for(i=1;i<=N;i++)
    for(sm=0;sm<=indexmirror;sm++)
      element[i][sm][0]=1.;
// 2. step: the potential values are defined:
  for(i=1;i<=N;i++)
    U[i]=element[i][0][5];
// 3. step: the Coulomb integral matrix element values are computed:
  for(i=1;i<=N;i++)
    for(j=1;j<=N;j++)
      Cgauss[i][j]=Coulomb(i,j);
// 4. step: solution of the linear alg. eq. system:
  for(i=1;i<=N;i++)
    Cgauss[i][N+1]=U[i];

//Tong: gauss is a unit in Geant4, so I changed its name...
//gauss(N,sigma);
Gauss(N,sigma);

// 5. step: charge density calculation:
  for(i=1;i<=N;i++)
    for(sm=0;sm<=indexmirror;sm++)
      element[i][sm][0]=sigma[i];
// 6. step: output to file element.dat
  elementwrite();
  return;
}

////////////////////////////////////////////////////

void elementwrite()
{
// This function writes the charge density, geometry and potential parameters
// of the independent subelements into the data file element.dat
  int N;
  FILE *fp;
  int i,j;
  if ( access( "element.dat", R_OK ) == -1 ) { //Leave it alone if we can read it
    do {
      fp=fopen("element.dat","w");
    } while (fp==NULL && !sleep(5)); //Stevens: make sure file was opened
    
    fprintf(fp,"%9i %9i %9i %22.14e \n",Nsubel,Ngroup,indexmirror,zmirror);
    N=Nsubel;
    for(i=1;i<=N;i++)
      {
	fprintf(fp,"%9i  \n",indexgroup[i]);
	for(j=0;j<=5;j++)
	  fprintf(fp,"%22.14e  \n",element[i][0][j]);
      }
    fclose(fp);
  }
  else G4cout << "Using old element.dat" << G4endl;
  return;
}

////////////////////////////////////////////////////

void elementread()
{
// This function reads the charge density, geometry and potential parameters
// of the independent subelements from the data file element.dat.
// Then it computes the parameters of all subelements
//   (by mirror symmetry transform.)
  int N,Ngroupx,Nsubel_test;
  FILE *fp;
  int i,j;
// 1. step: input from file 'element.dat'
  do {
      fp=fopen("element.dat","r");
  } while (fp==NULL && !sleep(5)); //Stevens: make sure file was opened
  fscanf(fp, "%i %i %i %le", &Nsubel_test, &Ngroupx, &indexmirror, &zmirror);
  if(Ngroupx!=Ngroup)
  {
      G4cout<<"Message from subroutine elementread: "
		<<"Ngroup value from file 'element.dat': "<< Ngroupx << " is different "
	    <<"from Ngroup value of #define: " << Ngroup << G4endl
		<<"Computation is  stopped !!! " << G4endl;
      exit(1);
  }
  if (Nsubel > Nmax)
  {
	  G4cout << "Message from Stefan in subroutine elementread: "
		  << "Nsubel too large" << G4endl;
	  exit(-1);
  }
  if (Ngroupx > Ngroup)
  {
	  G4cout << "Message from Stefan in subroutine elementread: "
		  << "Ngroupx too large" << G4endl;
	  exit(-1);
  }
  if (Nsubel_test != Nsubel)
  {
	  G4cout << "Message from Stefan in subroutine elementread: "
		  << "Nsubel is wrong" << G4endl;
	  exit(-1);
  }
  N = Nsubel;
  for(i=1;i<=N;i++)
  {
    fscanf(fp,"%i",&indexgroup[i]);
    for(j=0;j<=5;j++)
      fscanf(fp,"%le",&element[i][0][j]);
  }
  fclose(fp);
// 2. step: mirror symmetry transformations
  if(indexmirror==1)
    mirrorsymm();
  return;
}

//////////////////////////////////////////////////////

// Electric potential computation:

// Potential computation for axisymmetric conical electrodes

double Phielement(int i,int sm,double *P)
{
// Computes the electric potential in a point P,
// due to the axisymmetric subelement with main index i and
// mirror symmetry index sm
  const double eps0=8.854188e-12;
  const double ln4=1.386294361119890;
  double Phi,sigma,za,ra,zb,rb,L,Da,Db,u[3],z,r,D;
  double q,pp,a,b,pmin,pmax,h;
  int n;
  z=P[3];
  r=sqrt(P[1]*P[1]+P[2]*P[2]);
  common[1]=z;
  common[2]=r;
  za=common[3]=element[i][sm][1];
  ra=common[4]=element[i][sm][2];
  zb=common[5]=element[i][sm][3];
  rb=common[6]=element[i][sm][4];
  sigma=element[i][sm][0];
// L: distance between points (za,ra) and (zb,rb)
// Da: distance between points (za,ra) and (z,r)
// Db: distance between points (zb,rb) and (z,r)
  L=common[7]=sqrt((za-zb)*(za-zb)+(ra-rb)*(ra-rb));
  Da=sqrt((za-z)*(za-z)+(ra-r)*(ra-r));
  Db=sqrt((zb-z)*(zb-z)+(rb-r)*(rb-r));
  D=fabs(Da+Db-L)/L;
  if(D>=5.e-2)
    q=quad(funRKS,0.,L,INTNUM);
  else if(D>=5.e-3 && D<5.e-2)
    q=quad(funRKS,0.,L,100);
  else if(D>=5.e-4 && D<5.e-3)
    q=quad(funRKS,0.,L,500);
  else
  {
    u[1]=(zb-za)/L; u[2]=(rb-ra)/L;
    pp=(z-za)*u[1]+(r-ra)*u[2];
    q=0.;
    if(pp<L)
    {
      pmax=L;
      pmin=pp;
      if(pp<0.)
        pmin=0.;
      b=pmax;
      a=pmin+(b-pmin)*0.3;
      for(n=1;n<=35;n++)
      {
        q+=quad(funRKS,a,b,50);
        if(fabs(a-pmin)/L<1.e-8) break;
        b=a;
        a=pmin+(b-pmin)*0.3;
      }
      h=fabs(a-pmin);
      q+=h*(ln4+log(2.*r+1.e-12)+1.-log(h))/2.;
    }
    if(pp>0.)
    {
      pmax=pp;
      pmin=0.;
      if(pp>L)
        pmax=L;
      b=pmin;
      a=pmax-(pmax-b)*0.3;
      for(n=1;n<=35;n++)
      {
        q+=quad(funRKS,b,a,50);
        if(fabs(pmax-a)/L<1.e-8) break;
        b=a;
        a=pmax-(pmax-b)*0.3;
      }
      h=fabs(pmax-a);
      q+=h*(ln4+log(2.*r+1.e-12)+1.-log(h))/2.;
    }
  }
  Phi=sigma/(M_PI*eps0)*q;
  return Phi;
}

//////////////////////////////////////////////////////

double funRKS(double p)
{
  double z,r,za,zb,ra,rb,L,Z,R,K,eta,dr,dz,sumr,S;
  z=common[1];
  r=common[2];
  za=common[3];
  ra=common[4];
  zb=common[5];
  rb=common[6];
  L=common[7];
  Z=za+p/L*(zb-za);
  R=ra+p/L*(rb-ra);
  dz=z-Z;
  dr=r-R;
  sumr=R+r;
  eta=(dr*dr+dz*dz)/(sumr*sumr+dz*dz);
  K=Kelliptic(eta);
  S=sqrt(sumr*sumr+dz*dz);
  return R*K/S;
}

//////////////////////////////////////////////////////

// Coulomb integral matrix elements:

double Coulomb(int i,int j)
{
// This function computes the potential on the independent
// subelement with main index i due to the subelement with main index j
  double Phi,za,ra,zb,rb,Z,R,P[4];
// We define first the field point P:
  za=element[i][0][1];
  ra=element[i][0][2];
  zb=element[i][0][3];
  rb=element[i][0][4];
  Z=(za+zb)/2.; R=(ra+rb)/2.;
  P[1]=R; P[2]=0.; P[3]=Z;
// Now we compute the potential:
  if(indexmirror==0)
    Phi=Phielement(j,0,P);
  else
    Phi=Phielement(j,0,P)+Phielement(j,1,P);
  return Phi;
}

////////////////////////////////////////////////////

// Electric potential due to all electrode subelements:

double Phitot(double *P)
{
//   Computes the electric potential from all subelements
//   in the space point P; P[1], P[2] and P[3]
//   are the x, y and z components of the point, P[0] is not
//   employed.
  double Phi;
  int igroup;
//
  Phi=0.;
  for(igroup=1;igroup<=Ngroup;igroup++)
    Phi+=Phigroup(igroup,P);
  return Phi;
}

////////////////////////////////////////////////////

// Electric potential due to the subelements of group igroup:

double Phigroup(int igroup,double *P)
{
//   Computes the electric potential from  subelements of group igroup
//   in the space point P; P[1], P[2] and P[3]
//   are the x, y and z components of the point, P[0] is not
//   employed.
  double Phi;
  int i,sm;
  static int iff=0;
//
// Input from file element.dat:
  if(iff==0)
  {
    elementread();
    iff=1;
  }
//
  Phi=0.;
  for(i=1;i<=Nsubel;i++)
    for(sm=0;sm<=indexmirror;sm++)
      if(indexgroup[i]==igroup)
        Phi+=Phielement(i,sm,P);
  return Phi;
}

/////////////////////////////////////////////////////

// Electric field due to all subelements:

void Elfield(double eps,double *P,double *E,double *Phi)
{
// This function computes in a point P[4] the electric field
// components E[1], E[2], E[3] and the potential Phi,
// due to all subelements.
// The electric field is computed by
// numerical differentiation. For this purpose, we need the number
// eps: this is a distance that is much smaller than the characteristic
// dimension of the problem. For the KATRIN main spectrometer
// one could use eps=1.e-6 meter value.
// The distance of the field point P and an electrode surface should
// be not smaller than the eps value.
  int k,j;
  double Pplus[4],Pminus[4]; 
  *Phi=Phitot(P);
  for(k=1;k<=3;k++)
  {
    for(j=1;j<=3;j++)
      Pplus[j]=Pminus[j]=P[j];
    Pplus[k]=P[k]+eps;
    Pminus[k]=P[k]-eps;
    E[k]=-(Phitot(Pplus)-Phitot(Pminus))/(2.*eps);
  }
  return;
}


/////////////////////////////////////////////////////

// Electric field due to the subelements of group igroup:

void Elfieldgroup(int igroup,double eps,double *P,double *E,double *Phi)
{
// This function computes in a point P[4] the electric field
// components E[1], E[2], E[3] and the potential Phi,
// due to the subelements of group igroup,
// using the function Phigroup. The electric field is computed by
// numerical differentiation. For this purpose, we need the number
// eps: this is a distance that is much smaller than the characteristic
// dimension of the problem. For the KATRIN main spectrometer
// one could use eps=1.e-6 meter value.
// The distance of the field point P and an electrode surface should
// be not smaller than the eps value.
  int k,j;
  double Pplus[4],Pminus[4]; 
  *Phi=Phigroup(igroup,P);
  for(k=1;k<=3;k++)
  {
    for(j=1;j<=3;j++)
      Pplus[j]=Pminus[j]=P[j];
    Pplus[k]=P[k]+eps;
    Pminus[k]=P[k]-eps;
    E[k]=-(Phigroup(igroup,Pplus)-Phigroup(igroup,Pminus))/(2.*eps);
  }
  return;
}

//////////////////////////////////////////////////////

// Source coefficient calculation:

void input_sourcepoints(double *z0min,double *z0max,double *delz0)
{
// The z0a[igroup][k] source points are defined here.
// igroup: electrode group index  (1,...,Ngroup)
// k: index of the source points (1,...,Nsp[igroup])
// We use here the simplest case: equidistant source point distribution.
// Input  parameters:
//   z0min[igroup],z0max[igroup]: minimal and maximal z 
//      values of the source points for electrode group igroup.
//   delz0[igroup]: distance between neighbouring source points
//             for electrode group igroup.
// Output:
//   z values of source points, z0a[igroup][k],
//     igroup=1,...,Ngroup,   k=1,...,Nsp[igroup]
//   Nsp[igroup]: number of source points for electrode group igroup.
// SI units: meter !
  int k,igroup;
//
  for(igroup=1;igroup<=Ngroup;igroup++)
  {
    Nsp[igroup]=(int)((z0max[igroup]-z0min[igroup])/delz0[igroup]+1.1);
    for(k=1;k<=Nsp[igroup];k++)
      z0a[igroup][k]=z0min[igroup]+delz0[igroup]*((double)k-1.);
//
    if(Nsp[igroup]>Nspmax)
    {
      G4cout<<"Message from subroutine input_sourcepoints: "
		<<"Nsp[igroup]>Nspmax . "
		<<"Computation is  stopped !!! " << G4endl;
      exit(0);
    }
  }
  return;
}

//////////////////////////////////////////////////////

void elsource(  char *inputelectrodeName)
// This subroutine computes the electric field source constants Phin[n]
//  (n=0,...,nmax), at the axis source points 
//  z0a[igroup][k] (igroup=1,...,Ngroup; k=1,...,Nsp[igroup]).
// It needs the global integers Nsubel, nmax and Nsp[Ngroup+1], and the
//  electrode subelement parameters element[Nmax+1][2][6].
//  Nsp[igroup]: number of z0a source points for electrode group igroup.
//  nmax: maximal index of the source constants (maximum of n).
//  nmax is given by  #define command, Nsp[igroup] by global vector.
// Output into data file elsource.dat:
//   nmax, Ngroup, Nsp[igroup] (igroup=1,...,Ngroup),
//   and k number-groups;
//    numbers in number-group k:
//              igroup, k, z0a, rocen, Phin[n] (n=0,...,nmax)
{
  double Phin[nmax+1],P[nmax+1];
// Indices: i: electrode; k: source points; m: numerical integration;
//          n: source constants, sm: mirror symmetry,
//          igroup: electrode group index
  int N,i,k,m,n,sm,igroup;
  const int M=30;
  const double w9[10]={ 0.2803440531305107e0,0.1648702325837748e1,
                        -0.2027449845679092e0,0.2797927414021179e1,
                        -0.9761199294532843e0,0.2556499393738999e1,
                         0.1451083002645404e0,0.1311227127425048e1,
                          0.9324249063051143e0,0.1006631393298060e1};
  static double w[31];
  static double c1[nmax+1],c2[nmax+1];
  static int iff=0;
  double rocen,ro,za,zb,ra,rb,Z,R,L,c,s,xm,x,sigma,del;
  double ro0,u0,rc,rcn,Rper;
  const double eps0=8.854188e-12;
  FILE *fp;
//
  N=Nsubel;
  if(iff==0)
  {
// Initialization of the integration weight factors:
    for(m=0;m<=9;m++)
      w[m]=w9[m];
    for(m=10;m<=M-10;m++)
      w[m]=1.;
    for(m=M-9;m<=M;m++)
      w[m]=w9[M-m];
// Initialization of c1,c2 vectors:
    for(n=2;n<=nmax;n++)
    {
      c1[n]=(2.*n-1.)/(1.*n);
      c2[n]=(n-1.)/(1.*n);
    }
// Input from file element.dat:
    elementread();
//
    iff=1;
  }
//
// Output to file elsource.dat:
  bool write_elsource= ((fp=fopen( inputelectrodeName, "r")) == NULL);
  if (!write_elsource) {
     fclose( fp);
     G4cout << "Use old elsource file "<<inputelectrodeName<<G4endl;
	return;
  }

  G4cout << "Write new elsource file "<<inputelectrodeName<<G4endl;
  if ((fp=fopen( inputelectrodeName, "w")) == NULL) {
      G4cout << "I/O Error with elsource file "<<inputelectrodeName<<G4endl;
      exit(-1);
  }

  fprintf(fp,"%9i %9i \n",nmax,Ngroup);
  for(igroup=1;igroup<=Ngroup;igroup++)
    fprintf(fp,"%9i  \n",Nsp[igroup]);
// Electrode group loop:
  for(igroup=1;igroup<=Ngroup;igroup++)
// Source point loop:
    for(k=1;k<=Nsp[igroup];k++)
    {
// Calc. of rocen = minimum distance of the axis point z0a from the
//                  electrodes of group igroup
      rocen=1.e20;
      for(i=1;i<=N;i++)
      {
        if(indexgroup[i]==igroup)
          for(sm=0;sm<=indexmirror;sm++)
          {
            za=element[i][sm][1]; ra=element[i][sm][2];
            zb=element[i][sm][3]; rb=element[i][sm][4];
            Z=(za+zb)/2.-z0a[igroup][k]; R=(ra+rb)/2.;
            L=sqrt((za-zb)*(za-zb)+(ra-rb)*(ra-rb)); xm=L/2.;
            c=(zb-za)/L; s=(rb-ra)/L;
            x=-Z*c-R*s;
            if(x>=xm)
              ro=sqrt((Z+xm*c)*(Z+xm*c)+(R+xm*s)*(R+xm*s));
            else if(x<=-xm)
              ro=sqrt((Z-xm*c)*(Z-xm*c)+(R-xm*s)*(R-xm*s));
            else
              ro=sqrt((Z+x*c)*(Z+x*c)+(R+x*s)*(R+x*s));
            if(ro<rocen) rocen=ro;
          }
      }
      if(rocen<1.e-10)
      {
        G4cout<<"Message from subroutine elsource: "
		<<"rocen is zero !!! "
		<<"A source point should not be on an electrode surface !!! "
		<<"Computation is stopped !!! " << G4endl;
        exit(0);
      }
// Phin[n] calculation
// Initialization of Phin[n]:
      for(n=0;n<=nmax;n++)
        Phin[n]=0.;
// Electrode subelement loop:
      for(i=1;i<=N;i++)
      {
        if(indexgroup[i]==igroup)
        for(sm=0;sm<=indexmirror;sm++)
        {
          za=element[i][sm][1]; ra=element[i][sm][2];
          zb=element[i][sm][3]; rb=element[i][sm][4];
          L=sqrt((za-zb)*(za-zb)+(ra-rb)*(ra-rb)); xm=L/2.;
          sigma=element[i][sm][0];
          del=L/M;
// Integration loop:
          for(m=0;m<=M;m++)
          {
            x=-xm+del*m;
            Z=(za+zb)/2.+x*(zb-za)/L;
            R=(ra+rb)/2.+x*(rb-ra)/L;
            ro0=sqrt(R*R+(Z-z0a[igroup][k])*(Z-z0a[igroup][k]));
	    Rper=R/ro0;
	    u0=(Z-z0a[igroup][k])/ro0;
            P[0]=1.;
            P[1]=u0;
	    rc=rcn=rocen/ro0;
            c=sigma/(2.*eps0)*w[m]*del*Rper;
            Phin[0]+=c*P[0];
            Phin[1]+=c*rcn*P[1];
// Legendre polynomial expansion loop:
            for(n=2;n<=nmax;n++)
	    {
              rcn*=rc;
              P[n]=c1[n]*u0*P[n-1]-c2[n]*P[n-2];
              Phin[n]+=c*rcn*P[n];
	      if(rcn<1.e-17) break;
	    }
          } // m-loop
        } // sm-loop
      } // i-loop
// Output to file elsource.dat (igroup, k, z0a, rocen, Phin[n])
      fprintf(fp,"%9i %9i  \n",igroup,k);
      fprintf(fp,"%22.14e %22.14e \n",z0a[igroup][k],rocen);
      for(n=0;n<=nmax;n++)
        fprintf(fp,"%32.14e  \n",Phin[n]);
    }    
  fclose(fp);
  return;
}

/////////////////////////////////////////////////////
//////////////////////////////////////////////////////

//////////////////////////////////////////////////////


double Kelliptic(double eta)
{
// Chebyshev approximation for complete elliptic integral K
//   (W.J.Cody, Math. of Comp. 19 (1965) 105).
// The variable eta should be between 0 and 1.
// The usual variable used is k;  eta=1-k*k.
  const double ln4=1.386294361119890;
  const double a[10]={9.657359028085625e-2,3.088514627130518e-2,
                       1.493801353268716e-2,8.789801874555064e-3,
                       6.179627446053317e-3,6.847909282624505e-3,
                       9.848929322176893e-3,8.003003980649985e-3,
                       2.296634898396958e-3,1.393087857006646e-4};
  const double b[10]={1.249999999999080e-1,7.031249973903835e-2,
                       4.882804190686239e-2,3.737773975862360e-2,
                       3.012484901289893e-2,2.393191332311079e-2,
                       1.553094163197720e-2,5.973904299155429e-3,
                       9.215546349632498e-4,2.970028096655561e-5};
  double etan,suma,sumb;
  int n;
  if(eta < -1.e-13 || eta > 1.+1.e-13)
  {
    G4cout<<"Message from function Kelliptic: "
		<<"the variable eta is smaller than 0 or larger than 1 !!! "
		<<"Computation is stopped !!! " << G4endl;
    exit(0);
  }
  suma=sumb=0.;
  etan=eta;
  for(n=0;n<=9;n++)
  {
    suma+=a[n]*etan;
    sumb+=b[n]*etan;
    etan*=eta;
  }
  return ln4+suma-(1./2.+sumb)*log(fabs(eta)+1.e-19);
}

////////////////////////////////////////////////////////

double quad(double (*f)(double),double a,double b,int n)
// Numerical integration routine:
//   quad=integral of function f from a to b, with n+1 function value
//   evaluations. Equidistant points, closed form: f(a) and f(b) are
//   also needed.
//
{
  int i;
  double deln,sum;
  const double w5[6]={0.3187500000000000e+00,0.1376388888888889e+01,
                       0.6555555555555556e+00,0.1212500000000000e+01,
                       0.9256944444444445e+00,0.1011111111111111e+01};
  const double w9[10]={ 0.2803440531305107e0,0.1648702325837748e1,
                        -0.2027449845679092e0,0.2797927414021179e1,
                        -0.9761199294532843e0,0.2556499393738999e1,
                         0.1451083002645404e0,0.1311227127425048e1,
                         0.9324249063051143e0,0.1006631393298060e1};
  if(n<12) n=12;          // n too small
  deln=(b-a)/n;
  sum=0.;
  if(n<20)
  {
    for(i=0;i<=5;i++) sum+=w5[i]*f(a+deln*i);
    for(i=6;i<=n-6;i++) sum+=f(a+deln*i);
    for(i=n-5;i<=n;i++) sum+=w5[n-i]*f(a+deln*i);
  }
  else
  {
    for(i=0;i<=9;i++) sum+=w9[i]*f(a+deln*i);
    for(i=10;i<=n-10;i++) sum+=f(a+deln*i);
    for(i=n-9;i<=n;i++) sum+=w9[n-i]*f(a+deln*i);
  }
  return sum*deln;
}

///////////////////////////////////////////////////////////

//Tong: gauss is a unit in Geant4, so I changed its name...
//void Gauss(int n,double *x)
void Gauss(int n,double *x)
{
//   This subroutine computes the linear algebraic equation
//   Ax=b, where A is n * n matrix, b and x are n vectors, n is
//   a positive integer.
//     A[i][j]=Cgauss[i][j],  i=1,...,n; j=1,...,n
//     b[i]=Cgauss[i][n+1],   i=1,...,n,
//        where Cgauss is a global matrix.
//   We need: n>=2 !
  //extern double Cgauss[Nmax+1][Nmax+2];
  double stock,sum;
  int i,j,imin,imax,imin1;
//
  if (n > Nmax)
  {
	  G4cout << "Message from Stefan in subroutine Gauss: "
		  << "n too large" << G4endl;
	  exit(-1);
  }
  
  for(imin=2;imin<=n;imin++)
  {
    imin1=imin-1;
    imax=imin1;
    for(i=imin;i<=n;i++)
      if(fabs(Cgauss[i][imin1])>fabs(Cgauss[imin1][imin1]))
        imax=i;
    for(j=1;j<=n+1;j++)
    { stock=Cgauss[imin1][j];
      Cgauss[imin1][j]=Cgauss[imax][j];
      Cgauss[imax][j]=stock; }
    for(i=imin;i<=n;i++)
      for(j=imin;j<=n+1;j++)
        Cgauss[i][j]=Cgauss[i][j]-Cgauss[i][imin1]/
	   Cgauss[imin1][imin1]*Cgauss[imin1][j];
  }
  x[n]=1./Cgauss[n][n]*Cgauss[n][n+1];
  for(i=n-1;i>=1;i--)
  { sum=0.;
    for(j=i+1;j<=n;j++)
      sum+=Cgauss[i][j]*x[j];
    x[i]=1./Cgauss[i][i]*(Cgauss[i][n+1]-sum); }
  return;
}



///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////



/*

   *************************************************************************
   * C program package elcd2.c for axysymmetric electric field calculation *
   *************************************************************************

This is a BEM computation for a fully axisymmetric electrode system
(without wires). The input system contains axisymmetric conical
electrodes. The user can divide the electrode system into electrode groups. The number of the electrode groups Ngroup should be given by
#define command. For each electrode group a separate system of source
points should be given. For example, for Ngroup=2:
   z0min[igroup], z0max[igroup] : smallest and largest source point z
      values for electrode group with index igroup  (igroup=1,2)
   delz0[igroup] :  distance between neighbouring source points 
        for electrode group with index igroup  (igroup=1,2)
The program computes source coefficients for the source points 
in each electrode group separately. For many field points, it can
happen that it is possible to find for each electrode group some source
point where the zonal harmonic (Legendre polynomial) expansion is
convergent. If for a given electrode group no appropriate source point
can be found, then it is possible to compute the potential and electric
field for this given electrode group by using elliptic integrals.
 
The electric field calculation consists of the following main steps:

1. Writing the electrode input file.
-------------------------------------

   First line of the electrode input file: Nelectrode = number of electrodes.
   Then we have to write Nelectrode number of lines.
   Each line contains the following conical electrode parameters:
      
   igroup     zA     rA     zB     rB     U     num
   
   where:  igroup is the electrode index  (igroup=1,...,Ngroup)
           zA: axial coordinate of end A of the conical electrode
           rA: radial coordinate of end A of the conical electrode
           zB: axial coordinate of end B of the conical electrode
           rB: radial coordinate of end B of the conical electrode
           U: potential of the conical electrode
	   num: discretization parameter (integer)
  SI units have to be used:
      zA,zB,rA,rB in meter,  U in Volt

2. Global and local input parameters:
-------------------------------------

  The user has to define the following integers by #define command:

   Ngroup:  number of electrode groups
   Nmax:    this integer should be larger than the number of independent
            electrode subelements
   nmax:    number of terms in the Legendre polynomial expansion 
   INTNUM:  discretization number for numerical integration
   Nspmax:  this integer should be larger than the maximal number of
            source points in each electrode group
	    
In addition, the following input parameters should be given by the user:
    indexmirror:  if this is zero, no mirror symmetry is used;
                  if it is 1, a mirror symmetry exists in the electrode
		  system; the symmetry plane is then at z=zmirror
		  (the latter should be also given by the user)	
      indexmirror and zmirror are global variables (declared in elcd2.c)
    scale:  integer scale factor for electrode discretization
    power:  double factor for electrode discretization
            (power=2. should be usually suitable)
    string 'inputelectrode' : name of the electrode input file
Source points:
   The user has to define also for each electrode group the smallest
   and largest source point, and the distance between 2 adjacent source
   points:  z0min[igroup], z0max[igroup], delz0[igroup]  (in meter).
   These parameters should be chosen so that the fast Legendre polynomial
   expansion method should be possible to employ in a large space region.   
    
3. Calling elcd2
----------------

After these parameter definitions the user has to call the function elcd2.
This function carries out the following actions:
   a, Reads the geometry and potentials of the electrode system
      from the electrode input file, and discretizes the electrodes into
      conical subelements
      (through the function 'input_electrodes').
   b, Function 'chargedensities' computes the charge densities of the
      electrode subelements. All the subelement parameters (geometry,
      potential, charge density) are then written into the output file
      'element.dat'.   
   c, Function 'input_sourcepoints' computes the source points for all
      electrode groups.
   d, Function 'elsource' computes the source coefficients in all source
      points. They are then written into the output file
      'elsource.dat'.
   
4. Potential and field calculation
----------------------------------

Using the file 'elcd2.c', the potential in an arbitrary point P
(with Descartes coordinates P[1], P[2], P[3]) can be computed by calling
function 'Phitot' /potential = Phitot(P)/. This calculation can be done in a
separate run, after elcd2 has already been called. Namely, calling Phitot the file 'element.dat', which contains all the necessary information about the electrode subelements, is automatically read into the memory. The electric field can be calculated by calling the function 'Elfield' (this is based on numerical differentiation).

Nevertheless, the functions Phitot and Elfield in file 'elcd2.c' are served mainly for test calculation purposes. For the potential and field calculations during a trajectory simulation the file 'fieldelcd2.c' should be used.



*********************************

Electrode subelement parameters :

  double element[Nmax+1][2][6];

Number of independent conical subelements:   Nsubel
Main index of independent conical subelements: i (i=1,...,Nsubel)
Mirror symmetry index of conical subelements: sm (sm=0,indexmirror)
(f.e.: sm=0 for electrodes left from mirror plane, sm=1 for
  electrodes right from mirror plane)
 indexmirror=0:  no mirror symmetry
 indexmirror=1:  with mirror symmetry
 element[i][sm][0]: charge density of conical subelement
 element[i][sm][1]: za value of conical subelement
 element[i][sm][2]: ra value of conical subelement
 element[i][sm][3]: zb value of conical subelement
 element[i][sm][4]: rb value of conical subelement
 element[i][sm][5]: potential U_i of conical subelement
  SI units have to be used:
      za,zb,ra,rb in meter,  U_i in Volt


*/
