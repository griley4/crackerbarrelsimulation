///////////////////////////////////////////////////////////////
//Name: NabSDHit.cc
//Description: Hit class for NabSensitiveDetector
///////////////////////////////////////////////////////////////

//largely taken over from geant4 examples

#include "NabSDHit.hh"

#include "G4UnitsTable.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"

G4Allocator<NabSDHit> NabSDHitAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

NabSDHit::NabSDHit() {}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

NabSDHit::~NabSDHit() {}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

NabSDHit::NabSDHit(const NabSDHit& right)
  : G4VHit()
{
  trackID       = right.trackID;
  edep          = right.edep;
  pos           = right.pos;
  name          = right.name;
  hitTimeGlobal = right.hitTimeGlobal;
  hitTimeLocal  = right.hitTimeLocal;
  parentID      = right.parentID;
  processName   = right.processName;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

const NabSDHit& NabSDHit::operator=(const NabSDHit& right)
{
  trackID       = right.trackID;
  edep          = right.edep;
  pos           = right.pos;
  name          = right.name;
  hitTimeGlobal = right.hitTimeGlobal;
  hitTimeLocal  = right.hitTimeLocal;
  parentID      = right.parentID;
  processName   = right.processName;
  return *this;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

G4int NabSDHit::operator==(const NabSDHit& right) const
{
  return (this==&right) ? 1 : 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

void NabSDHit::Draw()
{
  G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
  if(pVVisManager)
  {
    G4Circle circle(pos);
    circle.SetScreenSize(2.);
    circle.SetFillStyle(G4Circle::filled);
    G4Colour colour(1.,0.,0.);
    G4VisAttributes attribs(colour);
    circle.SetVisAttributes(attribs);
    pVVisManager->Draw(circle);
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void NabSDHit::Print()
{
  G4cout << "  trackID: " << trackID 
	 << "  name: " << name
         << "  energy deposit: " << G4BestUnit(edep,"Energy")
	 << "  time: " << G4BestUnit(hitTimeGlobal, "Time")
	 << "  time form creation: " <<  G4BestUnit(hitTimeLocal, "Time")
	 << G4endl;
  
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
