///////////////////////////////////////////////////////////////
//Name: NabDeadNeutron.cc
//Description: A neutron with 0 lifetime to simulate a decay event
///////////////////////////////////////////////////////////////
//Identical to regular neutron in every single way except for the lifetime which is 0 s


#include "NabDeadNeutron.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include "G4ParticleTable.hh"

#include "G4NeutronBetaDecayChannel.hh"
#include "G4DecayTable.hh"

// ######################################################################
// ###                    NAB DEAD NEUTRON                            ###
// ######################################################################

NabDeadNeutron* NabDeadNeutron::theInstance = 0;

NabDeadNeutron* NabDeadNeutron::Definition()
{
  if (theInstance !=0) return theInstance;
  const G4String name = "nabNeutron";
  // search in particle table]
  G4ParticleTable* pTable = G4ParticleTable::GetParticleTable();
  G4Ions* anInstance =  reinterpret_cast<G4Ions*>(pTable->FindParticle(name));
  if (anInstance ==0)
  {
  // create particle
  //
  //    Arguments for constructor are as follows
  //               name             mass          width         charge
  //             2*spin           parity  C-conjugation
  //          2*Isospin       2*Isospin3       G-parity
  //               type    lepton number  baryon number   PDG encoding
  //             stable         lifetime    decay table
  //             shortlived      subType    anti_encoding
  // use constants in CLHEP
  // static const double  neutron_mass_c2 = 939.56563 * MeV;

    anInstance = new G4Ions(
                 name, neutron_mass_c2, 7.467e-28*GeV,         0.0, 
		    1,              +1,             0,          
		    1,              -1,             0,             
	     "baryon",               0,            +1,        2112,
		false,        0*second,          NULL,
		false,       "nucleon",             0,
                  0.0 
              );
    // Magnetic Moment
    G4double mN = eplus*hbar_Planck/2./(proton_mass_c2 /c_squared);
    anInstance->SetPDGMagneticMoment( -1.9130427 * mN);
    //create Decay Table 
    G4DecayTable* table = new G4DecayTable();
    // create a decay channel
    G4VDecayChannel* mode = new G4NeutronBetaDecayChannel("neutron",1.00);
    table->Insert(mode);
    anInstance->SetDecayTable(table);
    
  }
  theInstance = reinterpret_cast<NabDeadNeutron*>(anInstance);
  return theInstance;
}

NabDeadNeutron*  NabDeadNeutron::NeutronDefinition()
{
  return Definition();
}

NabDeadNeutron*  NabDeadNeutron::Neutron()
{
  return Definition();
}

