
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "NabTrackingAction.hh"

#include "CBDetectorConstruction.hh"
#include "NabRunAction.hh"
#include "NabEventAction.hh"

#include "G4Track.hh"
#include "G4VProcess.hh"
#include "G4TrackStatus.hh"
//#include "TTree.h"


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

NabTrackingAction::NabTrackingAction(CBDetectorConstruction* DET,NabRunAction* RA,NabEventAction* EA)
:detector(DET), runaction(RA), eventaction(EA)
{ }
 
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void NabTrackingAction::PreUserTrackingAction(const G4Track* aTrack )
{
  /*
  if(aTrack->GetCreatorProcess()->GetProcessName() == "Decay" && 
     aTrack->GetParticleDefinition()->GetParticleName() == "anti_nu_e") 
    {
      return fKill;
    }
  */

  //all void and obsolete unless neutron decay is used to initialize the event

  /*
  if(aTrack->GetParentID() == 0 && aTrack->GetTrackID() == 1)
    //picks the first particle to get position of the decay
    {
      G4ThreeVector pos = aTrack->GetVertexPosition();
      eventaction->Setx0decay(pos.x());
      eventaction->Sety0decay(pos.y());
      eventaction->Setz0decay(pos.z());
    }

   if((aTrack->GetParticleDefinition()->GetParticleName() == "e-")
     &&
     ( (aTrack->GetParentID() == 0)
       ||
       (aTrack->GetParentID() == 1 && aTrack->GetCreatorProcess()->GetProcessName() == "Decay")
       )
     )
    {
      G4ThreeVector P0 = aTrack->GetVertexMomentumDirection();
      eventaction->SeteP0x(P0.x());
      eventaction->SeteP0y(P0.y());
      eventaction->SeteP0z(P0.z());
      eventaction->SeteE0(aTrack->GetVertexKineticEnergy());

      //aTrack->SetTrackStatus(fStopAndKill);
    }

  if((aTrack->GetParticleDefinition()->GetParticleName() == "proton")
     &&
     ( (aTrack->GetParentID() == 0)
       ||
       (aTrack->GetParentID() == 1 && aTrack->GetCreatorProcess()->GetProcessName() == "Decay")
       )
     )
    {
      G4ThreeVector P0 = aTrack->GetVertexMomentumDirection();
      eventaction->SetpP0x(P0.x());
      eventaction->SetpP0y(P0.y());
      eventaction->SetpP0z(P0.z());
      eventaction->SetpE0(aTrack->GetVertexKineticEnergy());

    }

  if((aTrack->GetParticleDefinition()->GetParticleName() == "anti_nu_e")
     &&
     ( (aTrack->GetParentID() == 0)
       ||
       (aTrack->GetParentID() == 1 && aTrack->GetCreatorProcess()->GetProcessName() == "Decay")
       )
     )
    {
      G4ThreeVector P0 = aTrack->GetVertexMomentumDirection();
      eventaction->SetvP0x(P0.x());
      eventaction->SetvP0y(P0.y());
      eventaction->SetvP0z(P0.z());
      eventaction->SetvE0(aTrack->GetVertexKineticEnergy());

    }
    
  */
 

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void NabTrackingAction::PostUserTrackingAction(const G4Track* aTrack)
{

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

