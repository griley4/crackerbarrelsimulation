///////////////////////////////////////////////////////////////
//Name: NabSensitiveDetector.cc
//Description: Used to flag volumes as sensitive in NabDetectorConstruction
///////////////////////////////////////////////////////////////


#include "NabSensitiveDetector.hh"
#include "NabSDHit.hh"
#include "NabEventAction.hh"

#include "G4SDManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4Track.hh"
#include "G4VProcess.hh"
#include "G4ParticleTable.hh"
#include "G4RunManager.hh"


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....


NabSensitiveDetector::NabSensitiveDetector(G4String name,G4String colName)
  :G4VSensitiveDetector(name)
{//constructor: name SD and its collection
 //necessary to avoid ambigous collectrion names
  G4String HCname;
  collectionName.insert(HCname=colName);
  HCID = -1;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

//destructor
NabSensitiveDetector::~NabSensitiveDetector(){ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

void NabSensitiveDetector::Initialize(G4HCofThisEvent* HCE)
{//intialize the hits collection
  trackerCollection = new NabSDHitsCollection (SensitiveDetectorName,collectionName[0]);
  if(HCID<0)
    { HCID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]); }
  HCE->AddHitsCollection( HCID, trackerCollection );
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....


G4bool NabSensitiveDetector::ProcessHits(G4Step* aStep,G4TouchableHistory* ROgeom)
{//adds hits to the hits collection
  G4double edep = aStep->GetTotalEnergyDeposit();

  if(edep==0.) return false; 
  //only hits that deposit energy in some form are counted
  //hits collection saves important information on this step.
 
  NabSDHit* newHit = new NabSDHit();

  //get various variables from step
  newHit->SetTrackID    (aStep->GetTrack()->GetTrackID());
  newHit->SetEdep       (edep);
  newHit->SetPos        (aStep->GetPostStepPoint()->GetPosition());
  //newHit->SetName       (aStep->GetTrack()->GetParticleDefinition()->GetParticleName());
  newHit->SetTimeGlobal (aStep->GetTrack()->GetGlobalTime());
  newHit->SetTimeLocal  (aStep->GetTrack()->GetLocalTime());
  newHit->SetParentID   (aStep->GetTrack()->GetParentID());

  //Get copy no and position from readout geometry
  if(ROgeom)
    {
      newHit->SetPixelPos   (ROgeom->GetVolume()->GetTranslation());
      newHit->SetPixelNb    (ROgeom->GetVolume()->GetCopyNo());
    }

  //Get paticle defintions for comparison
  G4int particleType = 0;
  G4ParticleTable* particleTable   = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition* electron_def = particleTable->FindParticle("e-");
  G4ParticleDefinition* proton_def   = particleTable->FindParticle("proton");

  //Assign particle identifier 1 = proton, 2 = electron
  if(aStep->GetTrack()->GetDefinition() == proton_def) particleType = 1;
  else if(aStep->GetTrack()->GetDefinition() == electron_def) particleType = 2;
  else if(aStep->GetTrack()->GetDefinition()->GetParticleName() == "gamma") particleType = 3;
  else particleType = 0;
  newHit->SetParticleType(particleType);
  if(aStep->GetTrack()->GetParentID() != 0)
    newHit->SetProcessName(aStep->GetTrack()->GetCreatorProcess()->GetProcessName());

  //Access the eventaction via runmanager to get bounces
  G4RunManager* RunMan = G4RunManager::GetRunManager();
  NabEventAction* eventaction = (NabEventAction*) RunMan->GetUserEventAction();
  if(particleType==2) 
	{ 
		G4int bounceE = eventaction->GetBouncesE();
		newHit->SetBounceE(bounceE);
	}
  else if(particleType==1) 
	{
		G4int bounceP = eventaction->GetBouncesP();
		newHit->SetBounceP(bounceP);
	}
  //determine impact angle
  //if(aStep->IsFirstStepInVolume()) 
  newHit->SetAngle(aStep->GetTrack()->GetMomentum().getTheta());
  //else newHit->SetAngle(-10.0);

  //add hit to collection
  trackerCollection->insert( newHit );
  return true;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....


void NabSensitiveDetector::EndOfEvent(G4HCofThisEvent*)
{//associates the hit with the HitsCollectionOfthisEvent for later access
}
