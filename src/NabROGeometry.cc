///////////////////////////////////////////////////////////////
//Name: NabROGeometry.cc
//Description: Responsible for Readout geometry of Detector
///////////////////////////////////////////////////////////////

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
#include "NabROGeometry.hh"

#include "NabDummySD.hh"
#include "CBDetectorConstruction.hh"

#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SDManager.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Polyhedra.hh"
#include "G4ThreeVector.hh"
#include "G4Material.hh"

#include "G4RunManager.hh"

//debug
#include "G4UnitsTable.hh"
#include "G4UIcommand.hh"

//Units and Constants
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

//dummy constructor (required) do not use
NabROGeometry::NabROGeometry()
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

//proper constructor
NabROGeometry::NabROGeometry(G4String aString)
  : G4VReadOutGeometry(aString), dummyMat(0)
{
  //Get the detector from Run Manager
  G4RunManager* RunMan = G4RunManager::GetRunManager();
  CBDetectorConstruction* detector = (CBDetectorConstruction*)RunMan->GetUserDetectorConstruction();

  //Import defined quantities from detector
  //All values are set in DetectorConstruction

  //World
  WorldSizeX      = detector->GetWorldSizeX();
  WorldSizeY      = detector->GetWorldSizeY();
  WorldSizeZ      = detector->GetWorldSizeZ();

  //Upper Detecor:
  UDRadius        = detector->GetUDRadius();
  UDThickness     = detector->GetUDThickness();
  UDZfront        = detector->GetUDZfront();
  UDDLThickness   = detector->GetUDDLThickness();
 
  //Lower Detector:
  LDRadius        = detector->GetLDRadius();
  LDThickness     = detector->GetLDThickness();
  LDZfront        = detector->GetLDZfront();
  LDDLThickness   = detector->GetLDDLThickness();

  PixelSideLength = detector->GetPixelSideLength();
  MaxRadius       = detector->GetActiveDetectorRadius();

  //*****************************************
  //compute required derived quantities

  UDZpos   = UDZfront + 0.5*UDThickness;
  LDZpos   = LDZfront - 0.5*LDThickness;
  
  innerCircleRadius = PixelSideLength * sqrt(3)/2;
  outerCircleRadius = PixelSideLength;
  CtoCdistance = 2*innerCircleRadius; //Center-to-Center distance

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

//destructor
NabROGeometry::~NabROGeometry()
{
  delete dummyMat;
  delete dummySD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

//main method constructs the RO-geometry and returns the RO-World
G4VPhysicalVolume* NabROGeometry::Build()
{
  //dummy Material (required) for definition of volumes. 
  //Does NOT affect the tracking geometry
  dummyMat  = new G4Material(name="dummyMat", 1., 1.*g/mole, 1.*g/cm3);

  //dummy Sensitive Detector does absolutely nothing
  //required by the readout geometry to correctly function
  dummySD = new NabDummySD;

  //********************************************
  //World:
  //same as Detector Construction
  G4Box* ROWorld_solid = new G4Box("World",           
				  WorldSizeX,             
				  WorldSizeY,
				  WorldSizeZ); 
                         
  G4LogicalVolume* ROWorld_logic = new G4LogicalVolume(ROWorld_solid,
						       dummyMat,
						       "World");
                                   
  G4PVPlacement* ROWorld_physical = new G4PVPlacement(0,		
						      G4ThreeVector(),
						      ROWorld_logic,	
						      "World",	
						      0,		
						      false,	
						      0);

  G4int NbOfPixels = 0;

  //implementation of the pixels. 
  //It may be prudent to constuct this as a parametrization at some point
  for(G4int k=0; k<2; k++)
    {//loop over both sides
      for (G4int j=0; j<=12; j++)
	{
	  for (G4int i=0; i<=12; i++)	    
	    {
	      G4double xPos, yPos, zPos;

	      xPos=1.5*outerCircleRadius*(i-6);
	      yPos=CtoCdistance*(j-6)+innerCircleRadius*(i-6);
	    
	      //only place pixels falling within the thr max radius
	      if(sqrt(xPos*xPos+yPos*yPos)<MaxRadius)
		{
		  //Dimensions of the heaxagons
		  G4double innerRadius[] = {0.*mm, 0.*mm};
		  G4double outerRadius[] = {innerCircleRadius, innerCircleRadius};
		  G4double zPlanes[] = {-0.5*UDThickness, 0.5*UDThickness};

		  //Hexagon
		  G4VSolid* Pixel_solid = 
		    new G4Polyhedra("Pixel",      //name
				    0.*deg,       //min angle
				    360.*deg,     //full angle
				    6,            //vertices
				    2,            //number of z-planes
				    zPlanes,      //psotion of z planes
				    innerRadius,  //inner radius (0)
				    outerRadius); //radius of inner circle
		  
		  G4LogicalVolume* Pixel_logic =
		    new G4LogicalVolume(Pixel_solid, 
					dummyMat,
					"Pixel");      
		  
		  Pixel_logic->SetSensitiveDetector(dummySD);
		 
		  G4LogicalVolume* Detector_logic;
		  
		  //assign zpos based on which detector it is
		  if(k == 0)
		    {
		
		      zPos = UDZpos;
		    }
		  else if (k == 1) 
		    {
		      zPos = LDZpos;
		    }
		  else continue;

		  //places the pixel
		  G4VPhysicalVolume* Pixel_physical =
		    new G4PVPlacement(0,
				      G4ThreeVector(xPos, yPos, zPos),
				      Pixel_logic,
				      "Pixel" + G4UIcommand::ConvertToString(i+j),
				      ROWorld_logic,
				      false,
				      100*i+j);
		  //count pixels
		  NbOfPixels++;
    
		}
	    }
	}
    }

  //return RO-World
  return ROWorld_physical;
  
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
