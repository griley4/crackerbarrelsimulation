
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "NabStackingAction.hh"

#include "NabRunAction.hh"
#include "NabEventAction.hh"

#include "G4Track.hh"
#include "G4VProcess.hh"


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

NabStackingAction::NabStackingAction(NabRunAction* RA, NabEventAction* EA)
:runaction(RA), eventaction(EA)
{
  killSecondary  =1;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

NabStackingAction::~NabStackingAction()
{

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4ClassificationOfNewTrack
NabStackingAction::ClassifyNewTrack(const G4Track* aTrack)
{
  //keep primary particle
  if (aTrack->GetParentID() == 0) 
	{
		eventaction->ResetBouncesE();
		eventaction->ResetBouncesP();
		return fUrgent;
	}
  //count secondary particles
  if(aTrack->GetDefinition()->GetPDGEncoding() == 22) eventaction->CountGamma(); //count gamma
  if(aTrack->GetDefinition()->GetPDGEncoding() == 11) eventaction->CountSE(); //count secondary electron

  //stack or delete secondaries
  G4ClassificationOfNewTrack status = fUrgent;
  if (killSecondary) {
     status = fKill;
  }

  return status;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
