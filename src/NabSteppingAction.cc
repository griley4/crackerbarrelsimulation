
////////////////////////////////////////////////////////////////
//Name: NabSteppingAction.cc
//Description: Controls user interaction with a Step 
///////////////////////////////////////////////////////////////

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "NabSteppingAction.hh"

#include "CBDetectorConstruction.hh"
#include "NabRunAction.hh"
#include "NabEventAction.hh"
#include "NabField.hh"

#include "G4Step.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"

#include "elcd3.h"
#include "magfield3.h"
#include "fieldelcd3.h"

#include "G4RunManager.hh"

#include <time.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifdef GEANT4_9
NabSteppingAction* NabSteppingAction::fSteppingAction = 0;
#else
G4ThreadLocal NabSteppingAction* NabSteppingAction::fSteppingAction = 0; //added G4ThreadLocal
#endif

NabSteppingAction::NabSteppingAction(CBDetectorConstruction* DET, NabRunAction* RA,  NabEventAction* EA)
:detector(DET), runaction(RA), eventaction(EA)
{

  fSteppingAction = this;

 }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

NabSteppingAction::~NabSteppingAction()
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void NabSteppingAction::UserSteppingAction(const G4Step* aStep)
{

  currentStep = aStep;

  //get points before and after step
  G4StepPoint* prePoint  = aStep->GetPreStepPoint();
  G4StepPoint* postPoint = aStep->GetPostStepPoint();

  //get track
  G4Track* thisTrack = aStep->GetTrack();

  double zPos = postPoint->GetPosition().z()/m;
  double rPos = (postPoint->GetPosition().x()/m)*(postPoint->GetPosition().x()/m) + (postPoint->GetPosition().y()/m)*(postPoint->GetPosition().y()/m); //Note: this is actually the square of the radial coordinate
//grant
  //If we are outside of the electrode walls, the particle should be dead. Kill and report.  Currently, this is only mostly gammas
  if ( (zPos>1.) || (zPos>0.075 && rPos>1.0) || (zPos>-0.075 && zPos<0.075 && rPos>0.6) || (zPos<-0.075 && rPos>1.0) || (zPos<-1.) )
    {
      thisTrack->SetTrackStatus(fStopAndKill);
      G4cout << "Particle outside allowed region. ParentID " << thisTrack->GetParentID() << ", z= " <<zPos<< ", r^2 = " <<rPos<<G4endl;
    }

  //get volumes before and after step
  G4VPhysicalVolume* preVolume  = prePoint->GetTouchableHandle()->GetVolume();
  G4VPhysicalVolume* postVolume = postPoint->GetTouchableHandle()->GetVolume();
//grant
  //setup bools for each volume so it is only done once here. Check these bools in if (...) comparisons below
  //preVolume
  G4bool preVolume_LowerDL = false; G4bool preVolume_UpperDL = false; G4bool preVolume_LowerDetector = false; G4bool preVolume_UpperDetector = false; G4bool preVolume_LDRegion = false; G4bool preVolume_UDRegion = false; G4bool preVolume_DriftRegion = false; G4bool preVolume_NoRegion = false;
  if (preVolume->GetName() == "DriftRegion")  preVolume_DriftRegion = true;
  else if (preVolume->GetName() == "LDRegion")  preVolume_LDRegion = true;
  else if (preVolume->GetName() == "UDRegion")  preVolume_UDRegion = true;
  else if (preVolume->GetName() == "LowerDetector")  preVolume_LowerDetector = true;
  else if (preVolume->GetName() == "UpperDetector")  preVolume_UpperDetector = true;
  else if (preVolume->GetName() == "LowerDL")  preVolume_LowerDL = true;
  else if (preVolume->GetName() == "UpperDL")  preVolume_UpperDL = true;
  else preVolume_NoRegion = true;

  //postVolume:
  G4bool postVolume_LowerDL = false; G4bool postVolume_UpperDL = false; G4bool postVolume_LowerDetector = false; G4bool postVolume_UpperDetector = false; G4bool postVolume_LDRegion = false; G4bool postVolume_UDRegion = false; G4bool postVolume_DriftRegion = false; G4bool postVolume_NoRegion = false;
  if (postVolume->GetName() == "DriftRegion")  postVolume_DriftRegion = true;
  else if (postVolume->GetName() == "LDRegion")  postVolume_LDRegion = true;
  else if (postVolume->GetName() == "UDRegion")  postVolume_UDRegion = true;
  else if (postVolume->GetName() == "LowerDetector")  postVolume_LowerDetector = true;
  else if (postVolume->GetName() == "UpperDetector")  postVolume_UpperDetector = true;
  else if (postVolume->GetName() == "LowerDL")  postVolume_LowerDL = true;
  else if (postVolume->GetName() == "UpperDL")  postVolume_UpperDL = true;
  else postVolume_NoRegion = true;

  //get particle definition
  G4double thisParticlePDG = thisTrack->GetDefinition()->GetPDGEncoding();
//grant WTF
  //flag for tracking electrons (debugging purposes)
  G4bool killElectrons = false;
  
  //flag for tracking protons (debugging purposes)
  G4bool killProtons = false;

  if(killElectrons && thisParticlePDG == 11){
      thisTrack->SetTrackStatus(fStopAndKill);
  }

  if(killProtons && thisParticlePDG == 2212){
      thisTrack->SetTrackStatus(fStopAndKill);
  }


  //kill gammas after 2nd sep to avoid memory leakage
  if(thisParticlePDG == 22 && !(preVolume_LowerDetector || preVolume_UpperDetector)) {
    thisTrack->SetTrackStatus(fStopAndKill);
  }
  
  //last 10 tracking before deadlayer
  if (thisTrack->GetCurrentStepNumber()!=0 && thisTrack->GetParentID()==0 && thisTrack->GetTrackStatus()==0) {

    if(thisParticlePDG == 11) {
      eventaction->ENextStep(postPoint->GetPosition(),prePoint->GetMomentum(),postPoint->GetLocalTime(), postVolume_UpperDL || postVolume_LowerDL, preVolume_UpperDL || preVolume_LowerDL,prePoint->GetKineticEnergy());
    }
    if(thisParticlePDG == 2212){
      eventaction->PNextStep(postPoint->GetPosition(),prePoint->GetMomentum(),postPoint->GetLocalTime(), postVolume_UpperDL || postVolume_LowerDL, preVolume_UpperDL || preVolume_LowerDL,prePoint->GetKineticEnergy());
    }
  }
  
  //tracking info on midplanes
  if (thisParticlePDG == 2212 && thisTrack->GetCurrentStepNumber()!=0 && thisTrack->GetParentID()==0 && thisTrack->GetTrackStatus()==0 && ((postPoint->GetPosition().z()/mm>=100 && prePoint->GetPosition().z()/mm<100) || (postPoint->GetPosition().z()/mm<=-100 && prePoint->GetPosition().z()/mm>-100))) {
    eventaction->PMidplane(prePoint->GetPosition());
  }
  
  //tracking info on stopping planes
  if (thisParticlePDG == 2212 && thisTrack->GetCurrentStepNumber()!=0 && thisTrack->GetParentID()==0 && thisTrack->GetTrackStatus()==0 && ((postPoint->GetPosition().z()/mm>=250 && prePoint->GetPosition().z()/mm<250) || (postPoint->GetPosition().z()/mm<=-250 && prePoint->GetPosition().z()/mm>-250))) {
    eventaction->PSPplane(prePoint->GetPosition(),postPoint->GetPosition(),prePoint->GetMomentum(),prePoint->GetKineticEnergy(),postPoint->GetKineticEnergy(),prePoint->GetGlobalTime(),postPoint->GetGlobalTime());
  }


  //tracking info on 1mm planes
  if (thisTrack->GetCurrentStepNumber()!=0 && thisTrack->GetParentID()==0 && thisTrack->GetTrackStatus()==0 && ((postPoint->GetPosition().z()/mm>=446 && prePoint->GetPosition().z()/mm<446) || (postPoint->GetPosition().z()/mm<=-446 && prePoint->GetPosition().z()/mm>-446))){ //need to change for moving Det or detHV down!! Be careful!
    
    if(thisParticlePDG == 11) {
      eventaction->EDetMMplane(prePoint->GetPosition(),postPoint->GetPosition(),prePoint->GetMomentum(),prePoint->GetKineticEnergy(),postPoint->GetKineticEnergy(),prePoint->GetLocalTime(),postPoint->GetLocalTime());
    }
    if(thisParticlePDG == 2212){
      eventaction->PDetMMplane(prePoint->GetPosition(),postPoint->GetPosition(),prePoint->GetMomentum(),prePoint->GetKineticEnergy(),postPoint->GetKineticEnergy(),prePoint->GetGlobalTime(),postPoint->GetGlobalTime());
    }
  }


  //remove primary electrons that take more than 4 microseconds to be detected
  //this prevents infinite loops an optimizes run times

  if (thisParticlePDG == 11 && aStep->GetTrack()->GetLocalTime() > 4000*ns && thisTrack->GetParentID()==0){
    eventaction->CountDeadParticles();
    
    thisTrack->SetTrackStatus(fStopAndKill);
    
    G4cout << "Delete electrons > 4 us - PDG ID: " << thisParticlePDG << ", ParentID: " << thisTrack->GetParentID() << ", Event ID: " << eventaction->GetEventID() <<G4endl;
  }
  
  //remove all particles that take more than 40 microseconds to be detected
  //this prevents infinite loops an optimizes run times

  if (aStep->GetTrack()->GetLocalTime() > 60000*ns){
    eventaction->CountDeadParticles();
    eventaction->SetpTOFCut(1.0);

    thisTrack->SetTrackStatus(fStopAndKill);
    
    G4cout << "Delete all particles > 60 us - PDG ID: " << thisParticlePDG << ", ParentID: " << thisTrack->GetParentID() << ", Event ID: " << eventaction->GetEventID() <<G4endl;
  }

  
  /*
  if(thisParticlePDG == -12) //anti_nu_e
    { //kill all neutrinos (only relevant if running in neutron generation and decay mode)
      thisTrack->SetTrackStatus(fStopAndKill);
    }*/

  //counts particles as bouncing if they are:
  if ((prePoint->GetStepStatus() == fGeomBoundary) //at boundary,
      ||
      (postPoint->GetStepStatus() == fGeomBoundary)){ //second check as the fGeomBoundary Flag fails on the order of 10E-4, unresolved so far
    if(((preVolume_LowerDetector)
	&&
	(postVolume_LowerDL))//leaving a detector
       ||
       ((preVolume_UpperDetector)
	&&
	(postVolume_UpperDL))) {
      if(thisParticlePDG == 11) eventaction->CountBouncesE();  
      if(thisParticlePDG == 2212) eventaction->CountBouncesP();  
    }
    
    if((preVolume_UpperDL) && (postVolume_UpperDetector) && thisTrack->GetParentID()==0) {
      if(thisParticlePDG == 2212 && eventaction->GetPFirstHitDetNb()==0) {
	eventaction->SetPFirstHitDetNb(1);
	eventaction->SetpE(postPoint->GetKineticEnergy());
	eventaction->SetPFirstHitDetTime(postPoint->GetLocalTime());
      }
      if(thisParticlePDG == 11 && eventaction->GetEFirstHitDetNb()==0) {
	eventaction->SetEFirstHitDetNb(1);
	eventaction->SeteE(postPoint->GetKineticEnergy());
	eventaction->SetEFirstHitDetTime(postPoint->GetLocalTime());
      }
    }

    if((preVolume_LowerDL) && (postVolume_LowerDetector) && thisTrack->GetParentID()==0) {
      if(thisParticlePDG == 2212 && eventaction->GetPFirstHitDetNb()==0) {
	eventaction->SetPFirstHitDetNb(-1);
	eventaction->SetpE(postPoint->GetKineticEnergy());
	eventaction->SetPFirstHitDetTime(postPoint->GetLocalTime());
      }
      if(thisParticlePDG == 11 && eventaction->GetEFirstHitDetNb()==0) {
	eventaction->SetEFirstHitDetNb(-1);
	eventaction->SeteE(postPoint->GetKineticEnergy());
	eventaction->SetEFirstHitDetTime(postPoint->GetLocalTime());
      }
    }
    
      
    if((thisParticlePDG == 11) //electrons,
       &&
       (aStep->GetTrack()->GetParentID() == 0) //primary particles,
       && postVolume && postVolume->GetName() //Stevens: make sure postVolume exists and name exists. segfaults occurring sometimes after moving particles w/ NabKillStatus==1
      && ((postVolume_LowerDL)
	||
	(postVolume_UpperDL)
	)//entering a deadlayer
       &&
       ((preVolume_LDRegion)
	||
	(preVolume_UDRegion)
	)){//leaving Vacuum.
      //save impact angle for eventaction
      angle thisImpactAngle;
      thisImpactAngle.angle = acos(aStep->GetPostStepPoint()->GetMomentumDirection().z());
      thisImpactAngle.time = aStep->GetPostStepPoint()->GetGlobalTime();
      thisImpactAngle.trackID = aStep->GetTrack()->GetTrackID();
      eventaction->AddElectronAngle(thisImpactAngle);
    }
    if((thisParticlePDG == 2212) //proton
       &&
       (aStep->GetTrack()->GetParentID() == 0) //primary particles,
       &&
       ((postVolume_UpperDL)
	)//entering a deadlayer
       &&
       ((preVolume_UDRegion))){//leaving Vacuum.
      //save angle for eventaction
      angle thisImpactAngle;
      thisImpactAngle.angle = acos(aStep->GetPostStepPoint()->GetMomentumDirection().z());
      thisImpactAngle.time = aStep->GetPostStepPoint()->GetGlobalTime();
      thisImpactAngle.trackID = aStep->GetTrack()->GetTrackID();
      eventaction->AddProtonAngle(thisImpactAngle);
    }
    
    //grid interaction counter
    //not ready for use! requires developement and debugging!
    //uncomment at own risk
    /*
      if(	 
      (thisParticlePDG == 2212) //proton
      &&
      (aStep->GetTrack()->GetParentID() == 0) //primary particles,
      &&
      (
      (postVolume->GetName() == "LDXGrid")
      ||
      (postVolume->GetName() == "UDXGrid")
      ||
      (postVolume->GetName() == "LDYGrid")
      ||
      (postVolume->GetName() == "UDYGrid")
      )
      ) 
      {
      eventaction->CountProtonHitGrid();
      G4cout << "###############  Proton hits grid" << G4endl;
      }
      
      if(	 
      (thisParticlePDG == 11) //proton
      &&
      (aStep->GetTrack()->GetParentID() == 0) //primary particles,
      &&
      (
      (postVolume->GetName() == "LDXGrid")
      ||
      (postVolume->GetName() == "UDXGrid")
      ||
      (postVolume->GetName() == "LDYGrid")
      ||
      (postVolume->GetName() == "UDYGrid")
      )
      ) 
      {
      eventaction->CountElectronHitGrid();
      G4cout << "###############  Electron hits grid" << G4endl;
      }
    */


  }
/*
//print the guiding center coordinates of a particle
  if (thisParticlePDG == 2212 )
    {
	char inputcoil[100];	
	strcpy(inputcoil,"Roger2.dat"); //inputcoil_Roger2.dat
	double y1[7],ygc[7];
	//for the tracking
	y1[0] = postPoint->GetPosition().x()/m;
	y1[1] = postPoint->GetPosition().y()/m;
	y1[2] = postPoint->GetPosition().z()/m;
	y1[3] = postPoint->GetMomentumDirection().x();
	y1[4] = postPoint->GetMomentumDirection().y();
	y1[5] = postPoint->GetMomentumDirection().z();
	y1[6] = postPoint->GetKineticEnergy()/kg/m/m*s*s;
	move_particle_to_GC_coordinates( y1, ygc, inputcoil); 

	double r = sqrt(ygc[0]*ygc[0]+ygc[1]*ygc[1]);
    	int n= 20;
	double A, Bx, By, Br, Bz, Epotential, EfieldZcomp, EfieldRcomp, ElDerivatives[3];
	magfield2(ygc[2],r,inputcoil,n,&A,&Bz,&Br);
        elfield(ygc[2],r, &Epotential, &EfieldZcomp, &EfieldRcomp, &ElDerivatives[0], &ElDerivatives[1], &ElDerivatives[2]);
	Bx = Br*ygc[0]/r;
	By = Br*ygc[1]/r;
	std::ofstream PMagmomConservation;
	PMagmomConservation.open ("03292017_guidingcenter_x_3p24cm_y_0cm_z_n17p189cm.dat", std::ofstream::out | std::ofstream::app);
	PMagmomConservation << ygc[0] << "\t"
			    << ygc[1] << "\t"
			    << ygc[2] << "\t"
			    << Bz << "\t"
			    << Epotential << G4endl;				  						  
	PMagmomConservation.close(); 
    }
*/

  if(eventaction->GetGammaCount()){ //if there are gammas
    if(thisParticlePDG == 22){ //if this particle is a gamma
      if(aStep->GetTotalEnergyDeposit() > 1*eV){ //if energy deposit
	eventaction->IncrementGammaLoss(aStep->GetTotalEnergyDeposit()); //count energy deposited
      }
      
      if(postPoint->GetPosition().x() > 1*m
	 ||
	 postPoint->GetPosition().x() < -1*m
	 ||
	 postPoint->GetPosition().y() > 1*m
	 ||
	 postPoint->GetPosition().y() < -1*m
	 ||
	 postPoint->GetPosition().z() > 2.5*m
	 ||
	 postPoint->GetPosition().z() < -2.5*m){ //out of detector
	eventaction->IncrementGammaEscape(thisTrack->GetKineticEnergy());
	thisTrack->SetTrackStatus(fStopAndKill);
      }
    }
  }
  
  //count deposits for primary electrons
  if((thisParticlePDG == 11)
     &&
     (thisTrack->GetParentID() == 0)){
    eventaction->IncrementelectronTotalEnergyDeposit(aStep->GetTotalEnergyDeposit());
  }
  
  //count deposits for primary protons
  if((thisParticlePDG == 2212)
     &&
     (thisTrack->GetParentID() == 0)){
    eventaction->IncremenprotonTotalEnergyDepositt(aStep->GetTotalEnergyDeposit());
  }
  
  
  //count deadlayer deposits for primary electrons
  if((postPoint->GetPosition().z() >= 0.45*m || postPoint->GetPosition().z() <= -0.45*m)
     &&
     (thisParticlePDG == 11)
     &&
     (postVolume_LowerDL || postVolume_UpperDL)
     && 
     (thisTrack->GetParentID() == 1)){
    eventaction->IncrementDLLoss(aStep->GetTotalEnergyDeposit());
    //G4cout << "Edep: " <<  aStep->GetTotalEnergyDeposit()/eV << G4endl;
    
  }
  
  //count energy deposits by SEs
  if(eventaction->GetSECount() > 0){ //if there are SEs
    if(aStep->GetTotalEnergyDeposit() > 1*eV //energy deposit
       &&
       thisParticlePDG == 11 //particle is electron
       &&
       thisTrack->GetParentID() != 0){ //particle is secondary  
      eventaction->IncrementSELoss(aStep->GetTotalEnergyDeposit());
    }
    
  }
  
  G4bool calculatemagneticmoment = false;
  if (calculatemagneticmoment){
    //magnetic moment parallel(to check orbitla magnetic momentum conservation)
    if (thisTrack->GetParentID()==0 && thisTrack->GetTrackStatus()==0 && thisParticlePDG == 2212 && (postPoint->GetPosition().z()/mm<4990 || postPoint->GetPosition().z()/mm>-1190)) 
      {
	//if (postPoint->GetPosition().z()/mm<10 && postPoint->GetPosition().z()/mm>0)
	//if (thisTrack->GetCurrentStepNumber()==1 || (postPoint->GetPosition().z()/mm>=-100 && prePoint->GetPosition().z()/mm<-100) || (postPoint->GetPosition().z()/mm>=-50 && prePoint->GetPosition().z()/mm<-50) || (postPoint->GetPosition().z()/mm>=-10 && prePoint->GetPosition().z()/mm<-10) || (postPoint->GetPosition().z()/mm>=50 && prePoint->GetPosition().z()/mm<50) || (postPoint->GetPosition().z()/mm>=100 && prePoint->GetPosition().z()/mm<100) || (postPoint->GetPosition().z()/mm>=200 && prePoint->GetPosition().z()/mm<200) || (postPoint->GetPosition().z()/mm>=2500 && prePoint->GetPosition().z()/mm<2500) || (postPoint->GetPosition().z()/mm>=4700 && prePoint->GetPosition().z()/mm<4700) || (postPoint->GetPosition().z()/mm>=4800 && prePoint->GetPosition().z()/mm<4800) || (postPoint->GetPosition().z()/mm>=4999 && prePoint->GetPosition().z()/mm<4999))
	//{
	char inputcoil[100];	
	strcpy(inputcoil,"CBSim.dat"); //inputcoil_Roger2.dat
	double y1[7],ygc[7];
	//for the tracking
	y1[0] = postPoint->GetPosition().x()/m;
	y1[1] = postPoint->GetPosition().y()/m;
	y1[2] = postPoint->GetPosition().z()/m;
	y1[3] = postPoint->GetMomentumDirection().x();
	y1[4] = postPoint->GetMomentumDirection().y();
	y1[5] = postPoint->GetMomentumDirection().z();
	y1[6] = postPoint->GetKineticEnergy()/kg/m/m*s*s;
	move_particle_to_GC_coordinates( y1, ygc, inputcoil); 
	
	double r = sqrt(ygc[0]*ygc[0]+ygc[1]*ygc[1]);
	int n= 20;
	double A, Bx, By, Br, Bz;
	magfield2(ygc[2],r,inputcoil,n,&A,&Bz,&Br);
	Bx = Br*ygc[0]/r;
	By = Br*ygc[1]/r;
	/*
	  NabField* GlobalField = new NabField(inputcoil, true,true);
	  G4double Point[4] = {ygc[0]*m,ygc[1]*m,ygc[2]*m, 0};
	  G4double FieldVal[6];
	  GlobalField->GetFieldValue(Point, FieldVal);
	  Bx = FieldVal[0]/tesla;
	  By = FieldVal[1]/tesla;
	  Bz = FieldVal[2]/tesla;
	*/
	//double mass= 1.67262158e-27;
	//double P0 = sqrt(2*y1[6]*mass);
	double cosine=(Bx*y1[3]+By*y1[4]+Bz*y1[5])/sqrt(pow(Bx,2)+pow(By,2)+pow(Bz,2));
	double sine2=1-pow(cosine,2);
	//double P_alongB=P0*cosine;
	double PMagmoment=y1[6]/sqrt(pow(Bx,2)+pow(By,2)+pow(Bz,2))*sine2;
	
	eventaction->PMagmom(PMagmoment);
	//}
      }
    
  }
  
  
}


void move_GC_to_particle_coordinates(double *ygc, double *yp, char* inputcoil) {
  //This subroutine calculates particle position and momentum from
  //guiding center coordinates
  //
  //Component     Particle Coordinates     GC Coordinates
  //---------     --------------------     --------------
  //        0     Particle x position      GC x position
  //        1     Particle y position      GC y position
  //        2     Particle z position      GC z position
  //        3     Particle x momentum      Momentum parallel to magnetic field
  //        4     Particle y momentum      Conserved Magnetic Moment
  //        5     Particle z momentum      Gyrophase in [0,2pi)
  //
  	  
	double e0= 1.60217646e-19;
	double charge= e0;
	double mass= 1.67262158e-27;
	double c= 2.99792458e8;

    	int n= 20;
	double A, Br, Bz;
	const double RMIN=1.e-20;

	double r = sqrt(ygc[0]*ygc[0]+ygc[1]*ygc[1]);
/*
	NabField* GlobalField = new NabField(inputcoil, true,true);
	double Point[4] = {ygc[0]*m,ygc[1]*m,ygc[2]*m, 0};
	double FieldVal[6];
	GlobalField->GetFieldValue(Point, FieldVal);
	Br=sqrt(FieldVal[0]/tesla*FieldVal[0]/tesla+FieldVal[1]/tesla*FieldVal[1]/tesla);
	Bz=FieldVal[2]/tesla;
*/
    	magfield2(ygc[2],r,inputcoil,n,&A,&Bz,&Br);
    	double magb = sqrt(Br*Br+Bz*Bz);
    	double gamma = sqrt(1.+pow(ygc[3]/(mass*c),2)+2.*ygc[4]*magb/(mass*c*c));
    	double radius = sqrt(2.0*mass*ygc[4]/(charge*charge*magb));

	//Calculate final conditions of particle from final center conditions.
	double vperp = (charge/fabs(charge))*sqrt(2.0*magb*ygc[4]*mass)/(mass*gamma);
	// new determination of particle coordinates
	double e1[3], e2[3];
	if(r<RMIN){
		e1[0]= 1;
		e1[1]= 0;
		e1[2]= 0;
		e2[0]= 0;
		e2[1]= 1;
		e2[2]= 0;
	} 
	else {
		double a= pow( Bz,2)/ pow(magb,2);
		e1[0]= ygc[0]/r*a;
		e1[1]= ygc[1]/r*a;
		e1[2]= sqrt(1-a*a);
		if (Br > 0) {
			e1[2]= -e1[2];
		}
		e2[0]= -ygc[1]/r;
		e2[1]= ygc[0]/r;
		e2[2]= 0;
	}
	if (Bz < 0) {
		e1[0]= -e1[0];
		e1[1]= -e1[1];
		e1[2]= -e1[2];
	}
	yp[0]=ygc[0]+radius*(cos(ygc[5])*e1[0]+sin(ygc[5])*e2[0]);
	yp[1]=ygc[1]+radius*(cos(ygc[5])*e1[1]+sin(ygc[5])*e2[1]);
	yp[2]=ygc[2]+radius*(cos(ygc[5])*e1[2]+sin(ygc[5])*e2[2]);
	yp[3]= vperp*(-cos(ygc[5])*e2[0]+sin(ygc[5])*e1[0])*mass*gamma;
	yp[4]= vperp*(-cos(ygc[5])*e2[1]+sin(ygc[5])*e1[1])*mass*gamma;
	yp[5]= vperp*(-cos(ygc[5])*e2[2]+sin(ygc[5])*e1[2])*mass*gamma;
	if(r>=RMIN) {
		yp[3]+= ygc[3]*ygc[0]/r*Br/magb;
		yp[4]+= ygc[3]*ygc[1]/r*Br/magb;
		yp[5]+= ygc[3]*Bz/magb;
	} 
	else {
		yp[5]+= ygc[3];
	}

	/*
	//Prevent division by 0.
	if(r<RMIN){
		Bx=0.;
		By=0.;
	} else {
		Bx = Br*ygc[0]/r;
		By = Br*ygc[1]/r;
	}

	//Calculate final conditions of particle from final center conditions.
	double vperp = (charge/fabs(charge))*sqrt(2.0*magb*ygc[4]*mass)/(mass*gamma);
	if(fabs( Br)<RMIN){ // $$$$
		yp[0] = radius/(magb*r)*(ygc[0]*Bz*cos(ygc[5])-magb*ygc[1]*sin(ygc[5]))+ygc[0];
		yp[1] = radius/(magb*r)*(ygc[1]*Bz*cos(ygc[5])+magb*ygc[0]*sin(ygc[5]))+ygc[1];
		yp[2] = ygc[2];
		yp[3] = vperp/(magb*r)*(Bz*ygc[0]*sin(ygc[5])+magb*ygc[1]*cos(ygc[5]))
				+ygc[3]*Bx/(magb*mass*gamma);
		yp[4] = vperp/(magb*r)*(Bz*ygc[1]*sin(ygc[5])-magb*ygc[0]*cos(ygc[5]))
				+ygc[3]*By/(magb*mass*gamma);
		yp[5] = ygc[3]*Bz/(magb*mass*gamma)-vperp/magb*Br*sin(ygc[5]);
	} else {
		yp[0] = radius/(magb*Br)*(Bx*Bz*cos(ygc[5])-magb*By*sin(ygc[5]))+ygc[0];
		yp[1] = radius/(magb*Br)*(By*Bz*cos(ygc[5])+magb*Bx*sin(ygc[5]))+ygc[1];
		yp[2] = (-1.0)*radius*Br*cos(ygc[5])/magb+ygc[2];
		yp[3] = vperp/(magb*Br)*(Bz*Bx*sin(ygc[5])+magb*By*cos(ygc[5]))
				+ygc[3]*Bx/(magb*mass*gamma);
		yp[4] = vperp/(magb*Br)*(Bz*By*sin(ygc[5])-magb*Bx*cos(ygc[5]))
				+ygc[3]*By/(magb*mass*gamma);
		yp[5] = ygc[3]*Bz/(magb*mass*gamma)-vperp/magb*Br*sin(ygc[5]);
	}
	for(int i=3;i<6;i++)
		yp[i] = yp[i]*mass*gamma;
	*/

}

void move_particle_to_GC_coordinates(double *yp, double *ygc, char* inputcoil) {
  //This subroutine sets up the initial guiding center coordinates from
  //the given position and momentum.
  //
  //Component     Particle Coordinates     GC Coordinates
  //---------     --------------------     --------------
  //        0     Particle x position      GC x position
  //        1     Particle y position      GC y position
  //        2     Particle z position      GC z position
  //        3     Particle x momentum      Momentum parallel to magnetic field
  //        4     Particle y momentum      Conserved Magnetic Moment
  //        5     Particle z momentum      Gyrophase in [0,2pi)
  //
	  
	double e0= 1.60217646e-19;
	double charge= e0;
	double mass= 1.67262158e-27;
	double c= 2.99792458e8;

    	int n= 20;
	double A, Bx, By, Br, Bz;
	const double RMIN= 1.e-20;
    	const double accept_r= 1e-6;
	double disp[3];
	double yp_test[6];

	//Record initial position and momentum.
	double P0 = sqrt(yp[6]*yp[6]/c/c+2*yp[6]*mass);
	double px0 = yp[3]*P0;
	double py0 = yp[4]*P0;
	double pz0 = yp[5]*P0;
	//Calculate magnetic field at initial particle position.
	double r = sqrt(yp[0]*yp[0]+yp[1]*yp[1]);
/*
	NabField* GlobalField = new NabField(inputcoil, true,true);
	double Point[4] = {yp[0]*m,yp[1]*m,yp[2]*m, 0};
	double FieldVal[6];
	GlobalField->GetFieldValue(Point, FieldVal);
	Br=sqrt(FieldVal[0]/tesla*FieldVal[0]/tesla+FieldVal[1]/tesla*FieldVal[1]/tesla);
	Bz=FieldVal[2]/tesla;
*/
	magfield2(yp[2],r,inputcoil,n,&A,&Bz,&Br);
	double magb = sqrt(Br*Br+Bz*Bz);
	//Calculate momentum of the particle.
	double momentum = sqrt(px0*px0+py0*py0+pz0*pz0);

	//Calculate components of magnetic field.
	//Prevent division by 0.
	if(r<RMIN){
		Bx=0.0;
		By=0.0;
		ygc[3] = pz0;
	} else {
		Bx = Br*yp[0]/r;
		By = Br*yp[1]/r;
		//Calculate momentum along Bfield
		ygc[3] = (Bx*px0+By*py0+Bz*pz0)/magb;
	}

	//Calculate conserved magnetic moment.
	if(magb>RMIN)
		ygc[4] = (momentum*momentum-ygc[3]*ygc[3])/(2.0*mass*magb);
	else
		ygc[4] = 0.;

	//Calculate Lorentz factor.
	double gamma = sqrt(1.+pow(ygc[3]/(mass*c),2)+2.*ygc[4]*magb/(mass*c*c));

	//Move initial position from particle position to center of
	//circle around field line with Larmor radius.
	double vz = pz0/(gamma*mass);
	double vx = px0/(gamma*mass);
	double vy = py0/(gamma*mass);
	double radius = sqrt(2.*mass*ygc[4]/(charge*charge*magb));
	ygc[6] = radius;
	double alpha = (charge/fabs(charge))*radius
					/sqrt(pow(By*vx-Bx*vy,2)+pow(Bz*vx-Bx*vz,2)+pow(Bz*vy-By*vz,2));
	disp[0] = alpha*(Bz*vy-By*vz);
	disp[1] = alpha*(Bx*vz-Bz*vx);
	disp[2] = alpha*(By*vx-Bx*vy);
  
	for (int i=0;i<3;i++)
		ygc[i]= yp[i]+disp[i];

	//Recalculate magnetic field and components at center.
	r = sqrt(ygc[0]*ygc[0]+ygc[1]*ygc[1]);
/*
	Point[0] = ygc[0]*m;
	Point[1] = ygc[1]*m;
	Point[2] = ygc[2]*m;
	GlobalField->GetFieldValue(Point, FieldVal);
	Br=sqrt(FieldVal[0]/tesla*FieldVal[0]/tesla+FieldVal[1]/tesla*FieldVal[1]/tesla);
	Bz=FieldVal[2]/tesla;
*/	
	magfield2(ygc[2],r,inputcoil,n,&A,&Bz,&Br);
	magb = sqrt(Br*Br+Bz*Bz);

	double e1[3], e2[3];
	if(r<RMIN){
		e1[0]= 1;
		e1[1]= 0;
		e1[2]= 0;
		e2[0]= 0;
		e2[1]= 1;
		e2[2]= 0;
	} else {
		double a= pow( Bz,2)/ pow(magb,2);
		e1[0]= ygc[0]/r*a;
		e1[1]= ygc[1]/r*a;
		e1[2]= sqrt(1-a*a);
		if (Br > 0) {
			e1[2]= -e1[2];
		}
		e2[0]= -ygc[1]/r;
		e2[1]= ygc[0]/r;
		e2[2]= 0;
	}
	if (Bz < 0) {
		e1[0]= -e1[0];
		e1[1]= -e1[1];
		e1[2]= -e1[2];
	}
	ygc[5] = acos((-disp[0]*e1[0]-disp[1]*e1[1]-disp[2]*e1[2])/radius);
	if ((-disp[0]*e2[0]-disp[1]*e2[1]-disp[2]*e2[2]) < 0)
		ygc[5]= -ygc[5];
	
	//Calculate initial gyrophase.
	/*
	//Prevent division by 0.
	if(r<RMIN){
		Bx=0.;
		By=0.;
	} else {
		Bx = Br*ygc[0]/r;
		By = Br*ygc[1]/r;
	}
	if(fabs( Br)<RMIN){ // $$$$
		ygc[5] = acos((-Bz*(ygc[0]*disp[0]+ygc[1]*disp[1]))
				/(radius*magb*r));
		if (disp[0]*ygc[0]/r-disp[1]*ygc[1]/r < 0)
			ygc[5] *= (-1.0);
	} else {
		ygc[5] = acos((Br*Br*disp[2]-Bz*(Bx*disp[0]+By*disp[1]))
					/(radius*magb*Br));
		if (disp[0]*By/Br-disp[1]*Bx/Br < 0)
			ygc[5] *= (-1.0);
	}
	*/
	//printf("Particle->GC: Gyroradius %.3f cm, Gyrophase %.1f\n", radius*1e2, ygc[5]*180./M_PI);

	// iterate initial position to make transformation reversible
	double diff;
	int loop_counter= 0;
	do {
		move_GC_to_particle_coordinates( ygc, yp_test, inputcoil);

		for (int i= 0; i < 3; i++) 
			disp[i]= yp[i]-yp_test[i];
		diff= sqrt( pow( disp[0],2)+pow( disp[1], 2)+pow( disp[2], 2));
		for (int i= 0; i < 3; i++) 
			ygc[i]+= disp[i];
		loop_counter++;
		//if (diff > 1e-4) {
		//	printf( "Initial GC position wrong by %.0f um!\n", diff*1e6);
		//}
		if (loop_counter > 10) {
			printf( "Strange magnetic field. Transformation to GC coordinates doesn't converge.\n");
			printf( "Initial GC position wrong by %.0f um!\n", diff*1e6);
			break;			
			//exit( -1);
		}
/*
		r = sqrt(ygc[0]*ygc[0]+ygc[1]*ygc[1]);
		magfield2(ygc[2],r,inputcoil,n,&A,&Bz,&Br);
		magb = sqrt(Br*Br+Bz*Bz);

		//Calculate components of magnetic field.
		//Prevent division by 0.
		if(r<RMIN){
			Bx=0.0;
			By=0.0;
			ygc[3] = pz0;
		} else {
			Bx = Br*ygc[0]/r;
			By = Br*ygc[1]/r;
			//Calculate momentum along Bfield
			ygc[3] = (Bx*px0+By*py0+Bz*pz0)/magb;
		}

		//Calculate conserved magnetic moment.
		if(magb>RMIN)
			ygc[4] = (momentum*momentum-ygc[3]*ygc[3])/(2.0*mass*magb);
		else
			ygc[4] = 0.;

		//Calculate Lorentz factor.
		gamma = sqrt(1.+pow(ygc[3]/(mass*c),2)+2.*ygc[4]*magb/(mass*c*c));

		//Move initial position from particle position to center of
		//circle around field line with Larmor radius.
		vz = pz0/(gamma*mass);
		vx = px0/(gamma*mass);
		vy = py0/(gamma*mass);
		radius = sqrt(2.*mass*ygc[4]/(charge*charge*magb));
		ygc[6] = radius;
		alpha = (charge/fabs(charge))*radius
						/sqrt(pow(By*vx-Bx*vy,2)+pow(Bz*vx-Bx*vz,2)+pow(Bz*vy-By*vz,2));
		disp[0] = alpha*(Bz*vy-By*vz);
		disp[1] = alpha*(Bx*vz-Bz*vx);
		disp[2] = alpha*(By*vx-Bx*vy);

		if(r<RMIN){
			e1[0]= 1;
			e1[1]= 0;
			e1[2]= 0;
			e2[0]= 0;
			e2[1]= 1;
			e2[2]= 0;
		} else {
			e1[0]= ygc[0]/r*(pow( Bz,2)/ pow(magb,2));
			e1[1]= ygc[1]/r*(pow( Bz,2)/ pow(magb,2));
			e1[2]= sqrt(1-(pow( Bz,2)/ pow(magb,2))*(pow( Bz,2)/ pow(magb,2)));
			if (Br > 0) {
				e1[2]= -e1[2];
			}
			e2[0]= -ygc[1]/r;
			e2[1]= ygc[0]/r;
			e2[2]= 0;
		}
		if (Bz < 0) {
			e1[0]= -e1[0];
			e1[1]= -e1[1];
			e1[2]= -e1[2];
		}
		ygc[5] = acos((-disp[0]*e1[0]-disp[1]*e1[1]-disp[2]*e1[2])/radius);
		if ((-disp[0]*e2[0]-disp[1]*e2[1]-disp[2]*e2[2]) < 0)
			ygc[5]= -ygc[5];
*/
	} while (diff >= accept_r);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

