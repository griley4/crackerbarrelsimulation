///////////////////////////////////////////////////////////////
//Name: NabRunAction.cc
//Description: Allows user interaction with the run (before and after)
///////////////////////////////////////////////////////////////

#include "NabRunAction.hh"
#include "CBDetectorConstruction.hh"
#include "NabPrimaryGeneratorAction.hh"
#include "NabEventAction.hh"

#include "G4Run.hh"
#include "G4RunManager.hh"

#include "G4UIcommand.hh"
#include "Randomize.hh"

//#include "TFile.h"
//#include "TTree.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

NabRunAction::NabRunAction(CBDetectorConstruction* det, NabPrimaryGeneratorAction* kin) //G4String FN)
  :detector(det), primary(kin) //OutputFilePrefix(FN)
{ 
  runID = 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

NabRunAction::~NabRunAction()
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void NabRunAction::BeginOfRunAction(const G4Run* aRun)
{
  runID = aRun->GetRunID();

  //print run # and random number generator status
  G4cout << "### Run " << runID << " start." << G4endl;

  G4cout << aRun->GetNumberOfEventToBeProcessed() << " to be processed" << G4endl;

  CLHEP::HepRandom::showEngineStatus();

}


void NabRunAction::EndOfRunAction(const G4Run* aRun)
{
 
  //get eventaction from runamanager
  NabEventAction* eventaction = (NabEventAction*)G4RunManager::GetRunManager()->GetUserEventAction();
  //write Tree to file at end of run
  eventaction->WriteOutput();

  
  G4cout << "\n ======================== run summary ======================\n"
	 << G4endl;
  CLHEP::HepRandom::showEngineStatus();

  G4cout << "End of Run: " << aRun->GetRunID() << G4endl;
  G4cout << "Processed " << aRun->GetNumberOfEvent() << " Events" << G4endl;

}
