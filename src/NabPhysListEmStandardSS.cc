
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo...... 

#include "NabPhysListEmStandardSS.hh"

#include "G4ParticleDefinition.hh"
#include "G4ProcessManager.hh"

#include "G4ComptonScattering.hh"
#include "G4GammaConversion.hh"
#include "G4PhotoElectricEffect.hh"

#include "G4CoulombScattering.hh"
#include "G4IonCoulombScatteringModel.hh"

#include "G4eIonisation.hh"
#include "G4eBremsstrahlung.hh"
#include "G4eplusAnnihilation.hh"

#include "G4MuIonisation.hh"
#include "G4MuBremsstrahlung.hh"
#include "G4MuPairProduction.hh"

#include "G4hIonisation.hh"
#include "G4ionIonisation.hh"

#include "G4EmProcessOptions.hh"

//Units and Constants
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

NabPhysListEmStandardSS::NabPhysListEmStandardSS(const G4String& name)
   :  G4VPhysicsConstructor(name)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

NabPhysListEmStandardSS::~NabPhysListEmStandardSS()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void NabPhysListEmStandardSS::ConstructProcess()
{
  // Add standard EM Processes
#ifdef GEANT4_10
#ifdef GEANT4_10_3
  auto aParticleIterator = GetParticleIterator(); //LJB change for 10.3
#endif
  aParticleIterator->reset();
  while( (*aParticleIterator)() ){
    G4ParticleDefinition* particle = aParticleIterator->value();
#endif
#ifdef GEANT4_9
  theParticleIterator->reset();
  while( (*theParticleIterator)() ){
    G4ParticleDefinition* particle = theParticleIterator->value();
#endif
   G4ProcessManager* pmanager = particle->GetProcessManager();
    G4String particleName = particle->GetParticleName();
     
    if (particleName == "gamma") {
      // gamma
      pmanager->AddDiscreteProcess(new G4PhotoElectricEffect);
      pmanager->AddDiscreteProcess(new G4ComptonScattering);
      pmanager->AddDiscreteProcess(new G4GammaConversion);
      
    } else if (particleName == "e-") {
      //electron
      pmanager->AddDiscreteProcess(new G4CoulombScattering);            
      pmanager->AddProcess(new G4eIonisation,        -1, 1, 1);
      pmanager->AddProcess(new G4eBremsstrahlung,    -1, 2, 2);
	    
    } else if (particleName == "e+") {
      //positron
      pmanager->AddDiscreteProcess(new G4CoulombScattering);            
      pmanager->AddProcess(new G4eIonisation,        -1, 1, 1);
      pmanager->AddProcess(new G4eBremsstrahlung,    -1, 2, 2);
      pmanager->AddProcess(new G4eplusAnnihilation,   0,-1, 3);
            
    } else if (particleName == "mu+" || 
               particleName == "mu-"    ) {
      //muon
      pmanager->AddDiscreteProcess(new G4CoulombScattering);              
      pmanager->AddProcess(new G4MuIonisation,       -1, 1, 1);
      pmanager->AddProcess(new G4MuBremsstrahlung,   -1, 2, 2);
      pmanager->AddProcess(new G4MuPairProduction,   -1, 3, 3);
             
    } else if (particleName == "alpha" || particleName == "He3") {
      pmanager->AddProcess(new G4ionIonisation,      -1, 1, 1);
      G4CoulombScattering* cs = new G4CoulombScattering();
      cs->AddEmModel(0, new G4IonCoulombScatteringModel());
      cs->SetBuildTableFlag(false);
      pmanager->AddDiscreteProcess(cs);

    } else if (particleName == "GenericIon" ) { 
      pmanager->AddProcess(new G4ionIonisation,      -1, 1, 1);      
      G4CoulombScattering* cs = new G4CoulombScattering();
      cs->AddEmModel(0, new G4IonCoulombScatteringModel());
      cs->SetBuildTableFlag(false);
      pmanager->AddDiscreteProcess(cs);
     
    } else if ((!particle->IsShortLived()) &&
	       (particle->GetPDGCharge() != 0.0) && 
	       (particle->GetParticleName() != "chargedgeantino")) {
      //all others charged particles except geantino
      pmanager->AddDiscreteProcess(new G4CoulombScattering);            
      pmanager->AddProcess(new G4hIonisation,        -1, 1, 1);
    }
  }
  
  // Em options
  //
  // Main options and setting parameters are shown here.
  // Several of them have default values.
  //
  G4EmProcessOptions emOptions;
  
  //physics tables
  //
  emOptions.SetMinEnergy(100*eV);	//default    
  emOptions.SetMaxEnergy(100*TeV);	//default  
  emOptions.SetDEDXBinning(12*20);	//default=12*7  
  emOptions.SetLambdaBinning(12*20);	//default=12*7
  emOptions.SetSplineFlag(true);	//default
      
  //energy loss
  //
  emOptions.SetStepFunction(0.2, 100*um);	//default=(0.2, 1*mm)      
  emOptions.SetLinearLossLimit(1.e-2);		//default
   
  //ionization
  //
  emOptions.SetSubCutoff(false);	//default  

  // scattering
  emOptions.SetPolarAngleLimit(0.0);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

