//////////////////////////////////////////////////////////////
//Name: NabField.cc
//Description: Responsible for the Electromagnetic Field of the Detector
///////////////////////////////////////////////////////////////

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

#include "NabField.hh"

//Tong: new includes are needed
//#include "magfield2.hh"
//#include "elcd2_2.hh"

//Tong: use these includes instead
#include "magfield3.h"  //for magsource() and magfield2()
#include "elcd3.h" //for elcd2()
#include "fieldelcd3.h" //for elfield()

#include "CBDetectorConstruction.hh"
#include "NabFieldMap.hh"
#include "NabBFieldMap.hh"
#include "NabEMFieldMap.hh"

#include "G4RunManager.hh"

//Units and Constants
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

//Note: All of Ferenc's routines use SI units and not the native G4 units ensure all G4doubles must be divided by their respective SI units before being based to Ferenc's routines!!!


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

NabField::NabField(char* fieldfile, G4bool flag,G4bool GlobalFieldMapFlagFcn)
{
  //initialize filename and UseInterpolate flag
  filename = fieldfile;
  UseInterpolate = flag;
  GlobalFieldMapFlag = GlobalFieldMapFlagFcn;

  //initialize Ferenc's routines via FERENC() function
  if (!GlobalFieldMapFlag) FERENC();
  //FERENC();

  //get detector from runmananger
  G4RunManager *runMan = G4RunManager::GetRunManager();
  CBDetectorConstruction *detector = (CBDetectorConstruction*)runMan->GetUserDetectorConstruction();
  
  //get region boundaries from detector
  UD_Boundary                = detector->GetUDZfront();
  UDR_UEFR_Boundary          = detector->GetUDR_UEFR_Boundary();
  DriftR_UEFR_Boundary       = detector->GetDriftR_UEFR_Boundary();
  UpperDriftR_Pinch_Boundary = detector->GetUDriftR_Pinch_Boundary();
  LowerDriftR_Pinch_Boundary = detector->GetLDriftR_Pinch_Boundary();
  DriftR_LEFR_Boundary       = detector->GetDriftR_LEFR_Boundary();
  LDR_LEFR_Boundary          = detector->GetLDR_LEFR_Boundary();
  LD_Boundary                = detector->GetLDZfront();

  DriftRegionRadius          = detector->GetDriftRegionRadius();
  DetectorRegionRadius       = detector->GetUDRegionRadius();
  //PinchRadius                = detector->GetPinchRadius();

  //set common delta
  //individual deltas can be selected if it becomes desireable
  Delta = 1*mm;

  if(UseInterpolate) //if interpolation is to be used
    { //make fieldmap
      //if fieldmap allready exists -> load from file
      //else it will be recalculated
      //to force recalculation delete fieldmap file

      if (GlobalFieldMapFlag) {
	//GlobalFieldMap = new NabEMFieldMap("GlobalFieldMap",filename,-1199*mm,4999*mm,0,1*mm);//LD_Boundary,UD_Boundary
	//GlobalFieldMap = new NabEMFieldMap("GlobalFieldMap",filename,-1200*mm,4990*mm,0,1*mm);//GlobalFieldMap for shifting Det or HV down
	GlobalFieldMap = new NabEMFieldMap("GlobalFieldMap",filename,-450*mm,450*mm,0,1*mm);//Changed 2018/01/23 due to new GlobalFieldMap
	GlobalFieldMap->LoadFieldMap();
	//FilterFieldMap = new NabBFieldMap("FilterFieldMap",filename,-150*mm,350*mm,150*mm,0.1*mm);
	//FilterFieldMap->LoadFieldMap();
	}

      else {
      UpperDetectorRegionFieldMap = new NabEMFieldMap("UpperDetectorFieldMap",
						      filename,
						      UDR_UEFR_Boundary,
						      UD_Boundary-0.5*mm,
						      DetectorRegionRadius,
						      Delta);
      UpperDetectorRegionFieldMap->ApproximateTop();
      UpperDetectorRegionFieldMap->LoadFieldMap();


      UEFRegionFieldMap = new NabEMFieldMap("UEFRFieldMap",
				       filename,
				       DriftR_UEFR_Boundary,
				       UDR_UEFR_Boundary,
				       DriftRegionRadius,
				       Delta);
      UEFRegionFieldMap->LoadFieldMap();

      LEFRegionFieldMap = new NabEMFieldMap("LEFRFieldMap",
					    filename,
					    LDR_LEFR_Boundary,
					    DriftR_LEFR_Boundary,
					    DriftRegionRadius,
					    Delta);
      LEFRegionFieldMap->LoadFieldMap();
      
      LowerDetectorRegionFieldMap = new NabEMFieldMap("LowerDetectorFieldMap",
						      filename,
						      LD_Boundary+0.5*mm,
						      LDR_LEFR_Boundary,
						      DetectorRegionRadius,
						      Delta);
      LowerDetectorRegionFieldMap->ApproximateBottom();
      LowerDetectorRegionFieldMap->LoadFieldMap();

      UpperDriftRegionFieldMap = new NabBFieldMap("UDriftRFieldMap",
						  filename,
						  UpperDriftR_Pinch_Boundary,
						  DriftR_UEFR_Boundary,
						  DriftRegionRadius,
						  Delta);
      UpperDriftRegionFieldMap->LoadFieldMap();	

      LowerDriftRegionFieldMap = new NabBFieldMap("LDriftRFieldMap",
						  filename,
						  DriftR_LEFR_Boundary,
						  LowerDriftR_Pinch_Boundary,
     						  DriftRegionRadius,
						  Delta);
      LowerDriftRegionFieldMap->LoadFieldMap();			  
      }

    }
  
    
  //print a sweep of the field to a file if uncommented
  //use for debugging only
  /*
  std::ofstream fieldProfile("Z4_99CrossField.dat");
  G4double EfieldZcomp, EfieldRcomp, Epotential;
  for(G4int i=0; i<10000; i++)
    {
      //G4double Point[4] = {0.01*nm,0.01*nm , 4.99*m+((G4double)i/10000.0)*1*cm, 0};
      //G4double Xpos       = Point[0];
      //G4double Ypos       = Point[1];
      G4double Zpos = 4.99*m;
      G4double Rpos = ((G4double)i/10000.0)*0.085*m;

      elfield(Zpos/m,       // Z position in meters
	      Rpos/m,       // R position in meters
	      &Epotential,  // pointer to electric potential
	      &EfieldZcomp, // pointer to the Z component of the magnetic field
	      &EfieldRcomp);// pointer to the R component of the magnetic field
      
      

      //emField->GetFieldValue(Point, FieldVal);
      fieldProfile << Zpos/m << "\t" 
		   << Rpos/m << "\t"
		   << EfieldZcomp << "\t" 
		   << EfieldRcomp << "\t"
		   << Epotential << "\t"
		   << G4endl;
    }
  fieldProfile.close();
  
  */
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

NabField::~NabField() {}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

void NabField::FERENC() 
{
  //initialize elcd3 and magfield methods for later use

  //Parameters required by the elcd3() and magsource() methods
  G4int    scale   = 1;
  G4double power   = 2.0;

  G4RunManager *runMan = G4RunManager::GetRunManager();
  CBDetectorConstruction *detector = (CBDetectorConstruction*)runMan->GetUserDetectorConstruction();
  
  //Define Limits for source points of EM-fields
  G4double MaxZ0   = 1*(detector->GetUDZpos());  //max Z value of sourcepoint
  G4double MinZ0   = 1*(detector->GetLDZpos());  //min Z value of sourcepoint
  G4double DeltaZ0 = 0.03;   //distance between neighbouring sourcpoints

  //calls magsource() method (see magfield2.cc)
  magsource(MinZ0/m, MaxZ0/m, DeltaZ0, filename);

  //Tong:
  G4double z0mine[Ngroup+1], z0maxe[Ngroup+1], delz0e[Ngroup+1];
  //to change E, only 1 group
  G4int igroup = 1;

  //G4double npt = floor( 1.0*(detector->GetUDZpos()-detector->GetLDZpos())/DeltaZ0); 
  //delz0e[igroup] = 1.0*(detector->GetUDZpos()-detector->GetLDZpos())/npt;  
    delz0e[igroup] = DeltaZ0;  
    z0mine[igroup] = 1*(detector->GetLDZpos()) / m + 0.001;
    z0maxe[igroup] = 1*(detector->GetUDZpos()) / m - 0.001;
   //z0mine[igroup] = 1*(detector->GetLDZpos()) / m + delz0e[igroup]/2; 
   //z0maxe[igroup] = 1*(detector->GetUDZpos()) / m - delz0e[igroup]/2; 

  commonfieldelcd2.magfac=1.;  // scaling factor for magnetic field
  commonfieldelcd2.elfac=1.;  // scaling factor for electric field
  commonfieldelcd2.rclimit=0.985;
                    //  radius of convergence factor for
                    //  Legendre polynomial expansion
  commonfieldelcd2.maxdistance_electrode=0.000005;
                    // maximal allowed distance
                    // of electron from electrode surface
  commonfieldelcd2.electrode_encounter_index=0;
                    // in the beginning of each tracking should be
                    // set to zero

  //series for E
  elcd2(scale,power,z0mine,z0maxe,delz0e,filename);

  //elfield and magfield can now be called

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

void NabField::GetFieldValue(const G4double Point[4], G4double* Bfield) const
{ 
  //This function is given a point in space-time and returns the magnetic field at this point.

  //The space components of the point
  G4double Xpos       = Point[0];
  G4double Ypos       = Point[1];
  G4double Zpos       = Point[2];
  G4double Rpos       = std::sqrt(Xpos*Xpos+Ypos*Ypos);
  //G4double Phipos     = std::atan2(Ypos, Xpos);
  //atan computationally expensive avoid if possible
  G4double Xcomponent;
  G4double Ycomponent;
  G4double ElDerivatives[3];


  if(Rpos > 1E-14)
    {
      Xcomponent = Xpos/Rpos;
      Ycomponent = Ypos/Rpos;
    }
  else //prevent divide by zero error
    {
      Xcomponent = 0;
      Ycomponent = 0;
    }

  //Maximum number of coils
  G4int NbCoilMax = 20;

  //The Z and radial components and potential of the magnetic field
  G4double BfieldZcomp, BfieldRcomp, BVectorPotential;
  
  //The Z and radial components and potential of the electric field
  G4double EfieldZcomp, EfieldRcomp, Epotential;

  //G4double dummy;

  //EfieldRcomp = 0;
  //EfieldZcomp = 0;
  //magfield2(Zpos/m, Rpos/m, filename, NbCoilMax, &BVectorPotential, &BfieldZcomp, &BfieldRcomp, &MagDerivatives[0], &MagDerivatives[1], &MagDerivatives[2]);
  //elfield(Zpos/m, Rpos/m, &Epotential, &EfieldZcomp, &EfieldRcomp, &ElDerivatives[0], &ElDerivatives[1], &ElDerivatives[2]);
  //magfield2_elliptic(Zpos/m, Rpos/m, filename, NbCoilMax, &BVectorPotential, &BfieldZcomp, &BfieldRcomp);

  //**************************************************
  //Excluded Regions

  //while these regions do have real EM fields they are not well known and hence should be ignored until know better
  
  // no e-field above upper detector
  if(Zpos >= UD_Boundary)
    {
      EfieldRcomp = 0;
      EfieldZcomp = 0;
      BfieldRcomp = 0;
      BfieldZcomp = 0;
    }
  
  //no e-field below lower detector
  else if(Zpos <= LD_Boundary)
    {
      EfieldRcomp = 0;
      EfieldZcomp = 0;
      BfieldRcomp = 0;
      BfieldZcomp = 0;
      //Use field from boundary and assume it's close enough
      //if (UseInterpolate) {
	//if (GlobalFieldMap) GlobalFieldMap->Interpolate(LD_Boundary,Rpos,&BfieldZcomp,&BfieldRcomp,&dummy,&dummy);
	//else LowerDetectorRegionFieldMap->Interpolate(LD_Boundary,Rpos,&BfieldZcomp, &BfieldRcomp, &dummy, &dummy);
      //}
      //else  magfield2(LD_Boundary/m, Rpos/m, filename, NbCoilMax, &BVectorPotential, &BfieldZcomp, &BfieldRcomp, &MagDerivatives[0], &MagDerivatives[1], &MagDerivatives[2]);	
    }


  //**************************************************
  //Global field
  else if (GlobalFieldMapFlag && UseInterpolate) {

    GlobalFieldMap->Interpolate(Zpos,Rpos,&BfieldZcomp,&BfieldRcomp,&EfieldZcomp,&EfieldRcomp);

    //magfield2(Zpos/m, Rpos/m, filename, NbCoilMax, &BVectorPotential, &BfieldZcomp, &BfieldRcomp);
    //EfieldRcomp = 0;  //no e-field in drift region
    //EfieldZcomp = 0;
  } 
  
  else if (!GlobalFieldMapFlag && !UseInterpolate) {
    magfield2(Zpos/m, Rpos/m, filename, NbCoilMax, &BVectorPotential, &BfieldZcomp, &BfieldRcomp);
    elfield(Zpos/m, Rpos/m, &Epotential, &EfieldZcomp, &EfieldRcomp, &ElDerivatives[0], &ElDerivatives[1], &ElDerivatives[2]);	
    //EfieldRcomp = 0;  //no e-field in drift region
    //EfieldZcomp = 0;
  } 
  
  //**************************************************
  //drift region


  else if(Zpos <= DriftR_UEFR_Boundary && Zpos >= DriftR_LEFR_Boundary)
    {
      EfieldRcomp = 0;  //no e-field in drift region
      EfieldZcomp = 0;
      //elfield(Zpos/m, Rpos/m, &Epotential, &EfieldZcomp, &EfieldRcomp, &ElDerivatives[0], &ElDerivatives[1], &ElDerivatives[2]); 
      if(Rpos > DriftRegionRadius)
	{//no fields outside of spectrometer
	  BfieldRcomp = 0;
	  BfieldZcomp = 0;
	  G4cout << "************************************** Attempted to access field outside of spectrometer! Could lead to weird results! ******************************" << G4endl; //If these messages are being printed, INVESTIGATE!
	}
      else if (UseInterpolate){


	if(Zpos > UpperDriftR_Pinch_Boundary) //Upper Drift
	  {
	    UpperDriftRegionFieldMap->Interpolate(Zpos,Rpos,&BfieldZcomp, &BfieldRcomp);
	  }
	else if(Zpos < LowerDriftR_Pinch_Boundary) //Lower Drift
	  {
	    LowerDriftRegionFieldMap->Interpolate(Zpos,Rpos,&BfieldZcomp, &BfieldRcomp);
	  }
	else magfield2(Zpos/m, Rpos/m, filename, NbCoilMax, &BVectorPotential, &BfieldZcomp, &BfieldRcomp); //Filter && UseInterpolate
      }
      else
	{

	  // //Use analytic formula here
	  // double alpha=17.5*17.5;
	  // double B0=-0.0112234;//-.0000009899400644317287;
	  // double z0=0.687086;//8.241276258716710;
	  // BfieldZcomp=B0*(1.-alpha*(Zpos/m-z0)*(Zpos/m-z0))-0.25*Rpos/m*Rpos/m*B0*2.*alpha;
	  // BfieldRcomp=-0.5*Rpos/m*B0*2.*alpha*(Zpos/m-z0);
	  magfield2(Zpos/m, Rpos/m, filename, NbCoilMax, &BVectorPotential, &BfieldZcomp, &BfieldRcomp);

	}
    }


  //**************************************************
  //upper detector field
  else if(Zpos >=  UDR_UEFR_Boundary)
    {
      if(Rpos > DetectorRegionRadius)
	{//no fields outside of spectrometer
	  EfieldRcomp = 0; 
	  EfieldZcomp = 0;
	  BfieldRcomp = 0;
	  BfieldZcomp = 0;
	  G4cout << "************************************** Attempted to access field outside of spectrometer! Could lead to weird results! ******************************" << G4endl; //If these messages are being printed, INVESTIGATE!
	}
      else if(UseInterpolate)
	{//interpolate field map
	  UpperDetectorRegionFieldMap->Interpolate(Zpos,Rpos,&BfieldZcomp, &BfieldRcomp, &EfieldZcomp, &EfieldRcomp);
	}
      else
	{//otherwise fall back to ferences routines
	  elfield(Zpos/m, Rpos/m, &Epotential, &EfieldZcomp, &EfieldRcomp, &ElDerivatives[0], &ElDerivatives[1], &ElDerivatives[2]);
	  magfield2(Zpos/m, Rpos/m, filename, NbCoilMax, &BVectorPotential, &BfieldZcomp, &BfieldRcomp);	  
	}
    }

  //**************************************************
  //lower detector field
  else if(Zpos <=  LDR_LEFR_Boundary)
    {
      if(Rpos > DetectorRegionRadius)
	{//no e-fields outside of spectrometer
	  EfieldRcomp = 0; 
	  EfieldZcomp = 0;
	  BfieldRcomp = 0;
	  BfieldZcomp = 0;
	  G4cout << "************************************** Attempted to access field outside of spectrometer! Could lead to weird results! ******************************" << G4endl; //If these messages are being printed, INVESTIGATE!
	}
      else if (UseInterpolate) {
	LowerDetectorRegionFieldMap->Interpolate(Zpos,Rpos,&BfieldZcomp, &BfieldRcomp, &EfieldZcomp, &EfieldRcomp);
      }
      else
	{//otherwise fall back to ferences routines
	  elfield(Zpos/m, Rpos/m, &Epotential, &EfieldZcomp, &EfieldRcomp, &ElDerivatives[0], &ElDerivatives[1], &ElDerivatives[2]);
	  magfield2(Zpos/m, Rpos/m, filename, NbCoilMax, &BVectorPotential, &BfieldZcomp, &BfieldRcomp);	  
	}
    }

  //**************************************************
  //lower electric field region
  else if(Zpos <  DriftR_LEFR_Boundary && Zpos > LDR_LEFR_Boundary)
    {
      if(Rpos > DriftRegionRadius)
	{//no e-fields outside of spectrometer
	  EfieldRcomp = 0; 
	  EfieldZcomp = 0;
	  BfieldRcomp = 0;
	  BfieldZcomp = 0;
	  G4cout << "************************************** Attempted to access field outside of spectrometer! Could lead to weird results! ******************************" << G4endl; //If these messages are being printed, INVESTIGATE!
	}
      else if (UseInterpolate){
	LEFRegionFieldMap->Interpolate(Zpos,Rpos,&BfieldZcomp, &BfieldRcomp, &EfieldZcomp, &EfieldRcomp);
      }
       else
	{//otherwise fall back to ferences routines
	  elfield(Zpos/m, Rpos/m, &Epotential, &EfieldZcomp, &EfieldRcomp, &ElDerivatives[0], &ElDerivatives[1], &ElDerivatives[2]);
	  magfield2(Zpos/m, Rpos/m, filename, NbCoilMax, &BVectorPotential, &BfieldZcomp, &BfieldRcomp);	 
	}
    }

  //**************************************************
  //upper electric field region
  else if(Zpos >  DriftR_UEFR_Boundary && Zpos < UDR_UEFR_Boundary)
    {
      if(Rpos > DriftRegionRadius)
	{//no fields outside of spectrometer
	  EfieldRcomp = 0; 
	  EfieldZcomp = 0;
	  BfieldRcomp = 0;
	  BfieldZcomp = 0;
	  G4cout << "************************************** Attempted to access field outside of spectrometer! Could lead to weird results! ******************************" << G4endl; //If these messages are being printed, INVESTIGATE!
	}
      else if (UseInterpolate) {

	UEFRegionFieldMap->Interpolate(Zpos,Rpos,&BfieldZcomp, &BfieldRcomp, &EfieldZcomp, &EfieldRcomp);
      }
      else
	{//otherwise fall back to ferences routines
	  elfield(Zpos/m, Rpos/m, &Epotential, &EfieldZcomp, &EfieldRcomp, &ElDerivatives[0], &ElDerivatives[1], &ElDerivatives[2]);
	  magfield2(Zpos/m, Rpos/m, filename, NbCoilMax, &BVectorPotential, &BfieldZcomp, &BfieldRcomp);  
	}
    }
  
  
  else { //fall back to ferences routines

    if (UseInterpolate) G4cout << "Impossible! Forced to fall back to Ferenc routines while UseInterpolate was set to true. Investigate! -----Message from NabField.cc" << "Zpos=" << Zpos << "Rpos=" << Rpos << G4endl;

	  elfield(Zpos/m, Rpos/m, &Epotential, &EfieldZcomp, &EfieldRcomp, &ElDerivatives[0], &ElDerivatives[1], &ElDerivatives[2]);
     
        //calls the magfield2 method (see "magfield2.cc"):
	  magfield2(Zpos/m, Rpos/m, filename, NbCoilMax, &BVectorPotential, &BfieldZcomp, &BfieldRcomp);	
      
      
 
    }
     
  //assign X,Y,Z components of the magnetic field based on the results form magfield2
  //avoid using sin & cos (see atan above)
//  Bfield[0] = BfieldRcomp*Xcomponent*tesla;
//  Bfield[1] = BfieldRcomp*Ycomponent*tesla;
//  Bfield[2] = BfieldZcomp*tesla;
  Bfield[0] = 0*tesla;
  Bfield[1] = 0*tesla;
  Bfield[2] = 0*tesla;


  //assign X,Y,Z components of the electric based on the results from elfield
  //avoid using sin & cos (see atan above)  
  Bfield[3] = Xcomponent*EfieldRcomp*(volt/m);
  Bfield[4] = Ycomponent*EfieldRcomp*(volt/m);
  Bfield[5] = EfieldZcomp*(volt/m);


}
