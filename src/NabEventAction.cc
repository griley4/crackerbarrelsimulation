///////////////////////////////////////////////////////////////
//Name: NabEventAction.cc
//Description: Pre and post Event User Action, Analysis tasks
///////////////////////////////////////////////////////////////

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

#include "NabEventAction.hh"

#include "G4Event.hh"

#include "G4UImanager.hh" //Used for turning on tracking for certain events

#include "NabRunAction.hh"
#include "NabSDHit.hh"
#include "NabPrimaryGeneratorAction.hh"
#include "CBDetectorConstruction.hh"

#include "G4RunManager.hh"
#include "G4UIcommand.hh"
#include "G4Run.hh"
#include "G4SDManager.hh"
#include "G4UIcommand.hh"

//root headers
//#include "TFile.h"
#include "TSystem.h"
//#include "TTree.h"

//Units and Constants
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

#include <time.h>
#include <vector>
#include <iostream>


//all event based data analysis and output should go here

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

NabEventAction::NabEventAction(NabRunAction* RA)
  :runaction(RA),drawFlag("none"),printModulo(50)
{// the value passed to print modulo determines how often the event number is printed to the screen

  //set default output flags
  SetDefault();
  //SetStoppingPlanes();

  //make angles vectors which are filled by steppingAction
  ProtonImpactAngles   = new std::vector<angle>;
  ElectronImpactAngles = new std::vector<angle>;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

NabEventAction::~NabEventAction()
{
  //destructor

  //clean up to avoid memory leaks
  delete ProtonImpactAngles;
  delete ElectronImpactAngles;
  delete dynamicTree;
  delete OutputFile;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

void NabEventAction::BeginOfEventAction(const G4Event* evt)
{
  //runs before event is processed

  //event ID for TTree
  G4int evtNb = evt->GetEventID();
  eventID = evtNb;

  //start event clock
  //used for timing event processing time
  //startEventTime = clock();
  time(&startEventTime);
  //print event number every printModulo events
  if (evtNb%printModulo == 0) 
    G4cout << "\n---> Begin of Event: " << evtNb << ", seed: " << CLHEP::HepRandom::getTheSeed() << G4endl;

  //zero event owned counters
  bouncesE        = 0;
  bouncesP        = 0;
  PFirstHitDetNb  = 0;
  EFirstHitDetNb  = 0;
  PFirstHitDetTime= -1;
  EFirstHitDetTime= -1;
  TimeOfFlight_proton_mm=-1;
  deadParticles   = 0;
  protonHitGrid   = 0;
  electronHitGrid = 0;
  pTOFCut = -1;

  //secondary particle/Secondary particle energies counters
  gammaCounter = 0;
  SECounter    = 0;
  gammaEscape  = 0;
  gammaLoss    = 0;
  gammaDeposit = 0;
  SELoss       = 0;
  SEDeposit    = 0;
  
  //empty vectors
  ProtonImpactAngles->clear();
  ElectronImpactAngles->clear();

  //initialize values for decay position and momenta to error values.
  pE   = 0;
  eE   = 0;
  /*
  x0decay = -1000;
  y0decay = -1000;
  z0decay = -1000;
  pP0x = -1000;
  pP0y = -1000;
  pP0z = -1000;
  pE0  = -1000;
  eP0x = -1000;
  eP0y = -1000;
  eP0z = -1000;
  eE0  = -1000;
  vP0x = -1000;
  vP0y = -1000;
  vP0z = -1000;
  vE0  = -1000;
  */

  //more error values
  PixelStatus = -10;
  PixelInterference = 0;
  ProperDeltaR = -10;
  PixelDeltaR = -10;
  
  //zero energy counters
  EtotProtonUD   = 0;
  EtotProtonLD   = 0;
  EtotElectronUD = 0;
  EtotElectronLD = 0;
  EDepElectron   = 0;
  DLLoss         = 0;
  eTotalDeposit  = 0;
  pTotalDeposit  = 0;

  //clear trigger vectors
  Trigger_time.clear();
  Trigger_energy.clear();
  Trigger_x.clear();
  Trigger_y.clear();
  Trigger_particleType.clear();
  Trigger_parentID.clear();
  Trigger_detectorNb.clear();
  
  Trigger_zmax.clear();
  Trigger_zavg.clear();
  Trigger_impactAngle.clear();
  Trigger_bouncesEAtTrigger.clear();
  Trigger_bouncesPAtTrigger.clear();
  Trigger_tshifted.clear();
  
  Trigger_xPixel.clear();
  Trigger_yPixel.clear();

    Ppassing=0;
    //PMagmoment=0;
    //PMagmoment0=0;
    PMagmoment_total_mean=0;
    PMagmoment_total_RMS=0;
    PMagmoment_mean=0;
    PMagmoment_RMS=0;

    PmidPreZ=-1e4;

    pUSPX=-1e4;
    pUSPY=-1e4;
    pUSPZ=-1e4;
    pUSPPx=-1e4;
    pUSPPy=-1e4;
    pUSPPz=-1e4;
    pUSPE=-1;
    pUSPT=-1;

    EdetMMPreX=-1e4;
    EdetMMPreY=-1e4;
    EdetMMPreZ=-1e4;
    EdetMMPostX=-1e4;
    EdetMMPostY=-1e4;
    EdetMMPostZ=-1e4;
    EdetMMPx=-1e4;
    EdetMMPy=-1e4;
    EdetMMPz=-1e4;
    EdetMME=-1;
    EdetMMPostT=-1;

    PdetMMPreX=-1e4;
    PdetMMPreY=-1e4;
    PdetMMPreZ=-1e4;
    PdetMMPostX=-1e4;
    PdetMMPostY=-1e4;
    PdetMMPostZ=-1e4;
    PdetMMPx=-1e4;
    PdetMMPy=-1e4;
    PdetMMPz=-1e4;
    PdetMME=-1;
    PdetMMPostT=-1;

    EpreviousPosition=G4ThreeVector(-1000,-1000,-1000);
    EpreviousLocalTime=0;
    EimpactAngleViaPos=-1000;
    EimpactAngleViaMomentum=-1000;
    EenteringDetector=false;
    EenergyBeforeDetector=-1000000;

    EpassingDetMMplane=false;

    for (int i=0;i<3;i++) EimpactPosition[i]=0;

    PpreviousPosition=G4ThreeVector(-1000,-1000,-1000);
    PpreviousLocalTime=0;
    PimpactAngleViaPos=-1000;
    PimpactAngleViaMomentum=-1000;
    PenteringDetector=false;
    PenergyBeforeDetector=-1000000;

    PpassingMidplane=false;
    PpassingDetMMplane=false;
    PpassingUSPplane=false;

    for (int i=0;i<3;i++) PimpactPosition[i]=0;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

void NabEventAction::EndOfEventAction(const G4Event*)
{  
  //Get Run manager and current event
  G4RunManager* RunMan = G4RunManager::GetRunManager(); //get run manager
  //the runmanager can access other user classes from anywhere in the program
  const G4Event* currentEvent = RunMan->GetCurrentEvent(); //get event
  eventID = currentEvent->GetEventID(); //get eventID


  CBDetectorConstruction* detector = (CBDetectorConstruction*)RunMan->GetUserDetectorConstruction(); //get detector from Runmanager
  //make OutputFile and Tree on first (#=0) event
  if(eventID==0) {
 
    G4String OutputFileName = OutputFilePrefix + "_" + G4UIcommand::ConvertToString(runaction->GetRunID()) + ".root"; 
    while ( access(OutputFileName,F_OK)==0 && !overwriteOutputFile){ 
      G4cout << "File: " << OutputFileName << " already exists.  Would you like to overwrite? (y/n)" << G4endl;
	char userResponseChar[10];
	std::string userResponse;
	scanf("%9s",userResponseChar);
	userResponse=userResponseChar;
	if (userResponse=="n" || userResponse=="no") {G4cout << "Aborting Run" << G4endl; 
	  G4UImanager* UI = G4UImanager::GetUIpointer(); 
	  UI->ApplyCommand("/run/abort");
	  dynamicTree=NULL;
	  return;}
	else if (userResponse=="y" || userResponse=="yes") {G4cout << "File: " << OutputFileName << " will be overwritten." << G4endl;
	  break;}
	else {G4cout << "Please respond with either y or n" << G4endl;}
    }
    
    OutputFile = new TFile((char*)(OutputFileName.data()), "RECREATE");;
    
    //create ROOT TTree to store data
    dynamicTree = new TTree("dynamicTree", "dynamicTree");
    
    //add mandatory branches
    dynamicTree->Branch("eventID", &eventID, "eventID/I");
    //dynamicTree->Branch("eventTime", &eventTime, "eventTime/D");
    if(StoppingPlanes){
      dynamicTree->Branch("eE0", &eE0, "E0electron/D");
      dynamicTree->Branch("pE0", &pE0, "E0proton/D");
      dynamicTree->Branch("x0", &x0decay, "x0/D");
      dynamicTree->Branch("y0", &y0decay, "y0/D");
      dynamicTree->Branch("z0", &z0decay, "z0/D");
      dynamicTree->Branch("costev", &costev0, "costev/D");
      dynamicTree->Branch("pTOFCut", &pTOFCut, "pTOFCut/D");
      
      dynamicTree->Branch("TimeOfFlight_proton_mm", &TimeOfFlight_proton_mm, "TimeOfFlight_proton_mm/D");
      
      dynamicTree->Branch("PmidPreZ",&PmidPreZ,"PmidPreZ/D");
      dynamicTree->Branch("PdetMMPostX",&PdetMMPostX,"PdetMMPostX/D");
      dynamicTree->Branch("PdetMMPostY",&PdetMMPostY,"PdetMMPostY/D");
      
      dynamicTree->Branch("pP0x", &pP0x, "P0xproton/D");
      dynamicTree->Branch("pP0y", &pP0y, "P0yproton/D");
      dynamicTree->Branch("pP0z", &pP0z, "P0zproton/D");
            
      dynamicTree->Branch("pUSPX",&pUSPX,"pUSPX/D");
      dynamicTree->Branch("pUSPY",&pUSPY,"pUSPY/D");
      dynamicTree->Branch("pUSPZ",&pUSPZ,"pUSPZ/D");
      dynamicTree->Branch("pUSPPx",&pUSPPx,"pUSPPx/D");
      dynamicTree->Branch("pUSPPy",&pUSPPy,"pUSPPy/D");
      dynamicTree->Branch("pUSPPz",&pUSPPz,"pUSPPz/D");
      dynamicTree->Branch("pUSPE",&pUSPE,"pUSPE/D");
      dynamicTree->Branch("pUSPT",&pUSPT,"pUSPT/D");
    }
    
    //additional branches are added if flags are set
    if(TrackInitialPosition){
      dynamicTree->Branch("x0", &x0decay, "x0/D");
      dynamicTree->Branch("y0", &y0decay, "y0/D");
      dynamicTree->Branch("z0", &z0decay, "z0/D");
    }
    
    if(TrackProtonMomentum){
      dynamicTree->Branch("pP0x", &pP0x, "P0xproton/D");
      dynamicTree->Branch("pP0y", &pP0y, "P0yproton/D");
      dynamicTree->Branch("pP0z", &pP0z, "P0zproton/D");
      dynamicTree->Branch("pE0", &pE0, "E0proton/D");
      dynamicTree->Branch("pE", &pE, "Eproton/D");
    }
	   
    if(TrackElectronMomentum){
      dynamicTree->Branch("eP0x", &eP0x, "P0xelectron/D");
      dynamicTree->Branch("eP0y", &eP0y, "P0yelectron/D");
      dynamicTree->Branch("eP0z", &eP0z, "P0zelectron/D");
      dynamicTree->Branch("eE0", &eE0, "E0electron/D");
      dynamicTree->Branch("eE", &eE, "Eelectron/D");
    }
    
    if(TrackNeutrinoMomentum){
      dynamicTree->Branch("vP0x", &vP0x, "P0xneutrino/D");
      dynamicTree->Branch("vP0y", &vP0y, "P0yneutrino/D");
      dynamicTree->Branch("vP0z", &vP0z, "P0zneutrino/D");
      dynamicTree->Branch("vE0", &vE0, "E0neutrino/D");
    }

    if(FullOutput){
      if(CountBouncesFlag){
	dynamicTree->Branch("bouncesE", &bouncesE, "bouncesE/I");
	dynamicTree->Branch("bouncesP", &bouncesP, "bouncesP/I");
	dynamicTree->Branch("PFirstHitDetNb", &PFirstHitDetNb, "PFirstHitDetNb/I");
	dynamicTree->Branch("EFirstHitDetNb", &EFirstHitDetNb, "EFirstHitDetNb/I");
	dynamicTree->Branch("PFirstHitDetTime", &PFirstHitDetTime, "PFirstHitDetTime/D");
	dynamicTree->Branch("EFirstHitDetTime", &EFirstHitDetTime, "EFirstHitDetTime/D");
	dynamicTree->Branch("DeadParticles", &deadParticles, "deadParticles/I");
      }
      
      if(CountSecondariesFlag){
	dynamicTree->Branch("NbOfGamma", &gammaCounter, "NbOfGammas/I");
	dynamicTree->Branch("NbOfSE", &SECounter, "NbOfSE/I");
	dynamicTree->Branch("GammaEscape", &gammaEscape, "gammaEscape/D");
	dynamicTree->Branch("GammaLoss", &gammaLoss, "gammaLoss/D");
	dynamicTree->Branch("DLLoss", &DLLoss, "DLLoss/D");
	dynamicTree->Branch("SELoss", &SELoss, "SELoss/D");
	dynamicTree->Branch("SEDeposit", &SEDeposit, "SEDeposit/D");
	dynamicTree->Branch("eTotalDeposit", &eTotalDeposit, "eTotalDeposit/D");
	dynamicTree->Branch("pTotalDeposit", &pTotalDeposit, "pTotalDeposit/D");
      }
    }

    if(TrackTriggers){
      if(Triggers_basic){
	dynamicTree->Branch("Hit_time", &Trigger_time);
	dynamicTree->Branch("Hit_x", &Trigger_x);
	dynamicTree->Branch("Hit_y", &Trigger_y);
	dynamicTree->Branch("Hit_energy", &Trigger_energy);
	dynamicTree->Branch("Hit_particleType", &Trigger_particleType);
	dynamicTree->Branch("Hit_parentID", &Trigger_parentID);
	dynamicTree->Branch("Hit_detectorNb", &Trigger_detectorNb);
	
	if(CountSecondariesFlag) dynamicTree->Branch("GammaDeposit", &gammaDeposit, "gammaDeposit/D");
      }
      if(Triggers_zmaxFlag) dynamicTree->Branch("Trigger_zmax", &Trigger_zmax);
      if(Triggers_zavgFlag) dynamicTree->Branch("Trigger_zavg", &Trigger_zavg);
      if(Triggers_impactAngleFlag) dynamicTree->Branch("Trigger_impactAngle", &Trigger_impactAngle);
      if(Triggers_electronDrift) dynamicTree->Branch("Trigger_tshifted", &Trigger_tshifted);
      if(CountBouncesFlag) dynamicTree->Branch("Trigger_bouncesEAtTrigger", &Trigger_bouncesEAtTrigger);
      if(CountBouncesFlag) dynamicTree->Branch("Trigger_bouncesPAtTrigger", &Trigger_bouncesPAtTrigger);
      if(PixelFlag && CountBouncesFlag) dynamicTree->Branch("PixelStatus", &PixelStatus, "PixelStatus/I");
      if(PixelFlag && CountBouncesFlag && MorePixelFlag){	      
	dynamicTree->Branch("PixelDeltaR", &PixelDeltaR, "PixelDeltaR/D");
	dynamicTree->Branch("DeltaR", &ProperDeltaR, "DeltaR/D");	      
      }
      if(TrackKeyVars && CountBouncesFlag){
	dynamicTree->Branch("TimeOfFlight", &TimeOfFlight, "TimeOfFlight/D");
	dynamicTree->Branch("TimeOfFlight_proton_mm", &TimeOfFlight_proton_mm, "TimeOfFlight_proton_mm/D");
	dynamicTree->Branch("EDepElectron", &EDepElectron, "EDepElectron/D");
      } 
      
      if(TrackPixelInterference && detector->GetPixelFlag()){
	dynamicTree->Branch("PixelInterference", &PixelInterference, "PixelInterference/I");
      }
    }//if(TrackTriggers)
    if(FullOutput){
      dynamicTree->Branch("elapsedTime",&elapsedTime,"elapsedTime/D");
      dynamicTree->Branch("costev", &costev0, "costev/D");
      
      dynamicTree->Branch("EimpactAngleViaPos",&EimpactAngleViaPos,"EimpactAngleViaPos/D");
      dynamicTree->Branch("EimpactAngleViaMomentum",&EimpactAngleViaMomentum,"EimpactAngleViaMomentum/D");
      dynamicTree->Branch("EenergyBeforeDetector",&EenergyBeforeDetector,"EenergyBeforeDetector/D");
      dynamicTree->Branch("EimpactPosition[3]",&EimpactPosition[0],"EimpactPosition[3]/D");
      
      dynamicTree->Branch("PimpactAngleViaPos",&PimpactAngleViaPos,"PimpactAngleViaPos/D");
      dynamicTree->Branch("PimpactAngleViaMomentum",&PimpactAngleViaMomentum,"PimpactAngleViaMomentum/D");
      dynamicTree->Branch("PenergyBeforeDetector",&PenergyBeforeDetector,"PenergyBeforeDetector/D");
      dynamicTree->Branch("PimpactPosition[3]",&PimpactPosition[0],"PimpactPosition[3]/D");
      
      dynamicTree->Branch("PMagmoment_mean",&PMagmoment_mean,"PMagmoment_mean/D");
      dynamicTree->Branch("PMagmoment_RMS",&PMagmoment_RMS,"PMagmoment_RMS/D");
      
      dynamicTree->Branch("PmidPreZ",&PmidPreZ,"PmidPreZ/D");
      
      dynamicTree->Branch("EdetMMPreX",&EdetMMPreX,"EdetMMPreX/D");
      dynamicTree->Branch("EdetMMPreY",&EdetMMPreY,"EdetMMPreY/D");
      dynamicTree->Branch("EdetMMPreZ",&EdetMMPreZ,"EdetMMPreZ/D");
      dynamicTree->Branch("EdetMMPostX",&EdetMMPostX,"EdetMMPostX/D");
      dynamicTree->Branch("EdetMMPostY",&EdetMMPostY,"EdetMMPostY/D");
      dynamicTree->Branch("EdetMMPostZ",&EdetMMPostZ,"EdetMMPostZ/D");
      dynamicTree->Branch("EdetMMPx",&EdetMMPx,"EdetMMPx/D");
      dynamicTree->Branch("EdetMMPy",&EdetMMPy,"EdetMMPy/D");
      dynamicTree->Branch("EdetMMPz",&EdetMMPz,"EdetMMPz/D");
      dynamicTree->Branch("EdetMME",&EdetMME,"EdetMME/D");
      dynamicTree->Branch("EdetMMPostT",&EdetMMPostT,"EdetMMPostT/D");
      
      dynamicTree->Branch("PdetMMPreX",&PdetMMPreX,"PdetMMPreX/D");
      dynamicTree->Branch("PdetMMPreY",&PdetMMPreY,"PdetMMPreY/D");
      dynamicTree->Branch("PdetMMPreZ",&PdetMMPreZ,"PdetMMPreZ/D");
      dynamicTree->Branch("PdetMMPostX",&PdetMMPostX,"PdetMMPostX/D");
      dynamicTree->Branch("PdetMMPostY",&PdetMMPostY,"PdetMMPostY/D");
      dynamicTree->Branch("PdetMMPostZ",&PdetMMPostZ,"PdetMMPostZ/D");
      dynamicTree->Branch("PdetMMPx",&PdetMMPx,"PdetMMPx/D");
      dynamicTree->Branch("PdetMMPy",&PdetMMPy,"PdetMMPy/D");
      dynamicTree->Branch("PdetMMPz",&PdetMMPz,"PdetMMPz/D");
      dynamicTree->Branch("PdetMME",&PdetMME,"PdetMME/D");
      dynamicTree->Branch("PdetMMPostT",&PdetMMPostT,"PdetMMPostT/D");
      
      dynamicTree->Branch("pUSPX",&pUSPX,"pUSPX/D");
      dynamicTree->Branch("pUSPY",&pUSPY,"pUSPY/D");
      dynamicTree->Branch("pUSPZ",&pUSPZ,"pUSPZ/D");
      dynamicTree->Branch("pUSPPx",&pUSPPx,"pUSPPx/D");
      dynamicTree->Branch("pUSPPy",&pUSPPy,"pUSPPy/D");
      dynamicTree->Branch("pUSPPz",&pUSPPz,"pUSPPz/D");
      dynamicTree->Branch("pUSPE",&pUSPE,"pUSPE/D");
      dynamicTree->Branch("pUSPT",&pUSPT,"pUSPT/D");
    }
  }//if(first event)
  
  
  //File is now ready for writing
  
  
  eventID = (long) CLHEP::HepRandom::getTheSeed();
  //G4cout << "Current seed: " << CLHEP::HepRandom::getTheSeed() << G4endl;
  
  //*******************************************
  //get Hits collections

  //acces Sensitive detector manager to get hits collections
  G4SDManager* SDMan = G4SDManager::GetSDMpointer();
  G4int upperID = SDMan->GetCollectionID("upperCollection");
  G4int lowerID = SDMan->GetCollectionID("lowerCollection");
  G4HCofThisEvent* HCE = currentEvent->GetHCofThisEvent();
  NabSDHitsCollection* upperCollection = (NabSDHitsCollection*)(HCE->GetHC(upperID));
  NabSDHitsCollection* lowerCollection = (NabSDHitsCollection*)(HCE->GetHC(lowerID));


  //*******************************************
  //Analyze Hits collections

  //number of detectors and first detector
  G4int NbofDetectors = 2, firstDetector = 1;

  //declare collection pointer
  NabSDHitsCollection* trackerCollection;

  //detector identifier 1 => upper, -1 => lower
  Int_t detNb = 0;
	      
  //Collection of hits of this event
  //The name trigger is used to avoid confusion, as hit is allready used by geant4 to describe a step in a sensitive volume

  //std::vector<NabTriggerData> TriggerVector;

  //differentiating criteria for Triggers 
  // 1. Same Track (Particle)
  // 2. Same Pixel
  // 3. Within 50 ns of the preceeding hit
  // if any of these are not met a new trigger is created
  if(TrackTriggers)
    {

      NbOfTriggers = 0;

      for(G4int k=firstDetector;k<=NbofDetectors;k++)
	{//loop over both detectors

	  //check for existing of hits collection and set detector number and collection pointer
	  if(k == 1 && lowerCollection) 
	    {
	      trackerCollection = lowerCollection;
	      detNb = -1;
	      
	    }
	  else if(k == 2 && upperCollection) 
	    {
	      trackerCollection = upperCollection;
	      detNb = 1;
	    }
	  else continue;

	  //get size of collection
	  G4int NbHits = trackerCollection->entries();
	  
	  //reset counters
	  G4double TriggerEnergy = 0;
	  G4double TriggerWeightedTime = 0;
	  G4double TriggerWeightedShiftedTime = 0;
	  G4ThreeVector TriggerWeightedPosition;
      
	  for(G4int i=0;i<NbHits;i++)
	    {
	      //static variables used to differentiate triggers
	      static G4int oldTrackID;
	      static G4int oldPixelNb;
	      static G4double oldTime;
	      // static NabTriggerData* aTrigger = new NabTriggerData;

	      G4int parentID;
	      G4int particleType;
	      G4int currentBounceE;
	      G4int currentBounceP;

	      G4double xPixel=-1000;
	      G4double yPixel=-1000;
	      

	      //obtain differentiating information
	      G4int trackID = ((*trackerCollection)[i]->GetTrackID());
	      G4int pixelNb = ((*trackerCollection)[i]->GetPixelNb());
	      G4double time = ((*trackerCollection)[i]->GetTimeGlobal());
	      
	      static G4double zmax;
	      
	      
	      if(i == 0)
		{//first entry of HC
		 
		  zmax = -1000;//error value

		  //determine pixel coordinates, bounces, particleType
		  if(detector->GetPixelFlag() && PixelFlag)
		    {
		      xPixel = ((*trackerCollection)[i]->GetPixelPos().x()/mm);
		      yPixel = ((*trackerCollection)[i]->GetPixelPos().y()/mm);
		    }
		  if(Triggers_basic)
		    {
		      particleType = ((*trackerCollection)[i]->GetParticleType());
		      parentID = ((*trackerCollection)[i]->GetParentID());
		    }
		  
		  if(CountBouncesFlag && parentID==0)
		    {
		      currentBounceE = ((*trackerCollection)[i]->GetBounceE());
		      currentBounceP = ((*trackerCollection)[i]->GetBounceP());
		    }
		}
	      
	      if(!(i == 0) && !(i == NbHits))
		{//checks if differentiating criteria are met by intermediated hits
		  if(
		     (
		      (detector->GetPixelFlag()) //pixels are simulated
		      && // and
		      (
		       (oldTrackID != trackID) //different track
		       ||
		       (oldPixelNb != pixelNb) //different pixel
		       ||
		       (std::fabs(oldTime-time) > 5.*ns) //to much time inbetween hits -> left detector intermittently
		       )
		      )
		     ||
		     (
		      (!detector->GetPixelFlag()) //pixels are not simulated
		      &&
		      (
		       (oldTrackID != trackID) //different track
		       ||	  
		       (std::fabs(oldTime-time) > 5.*ns) //to much time inbetween hits
		       )
		      )
		     )//if
		     
		    {
		      if( //look for pixel interference (particle hitting multiple pixels)
			 detector->GetPixelFlag()
			 &&
			 oldTrackID == trackID
			 &&
			 std::fabs(oldTime-time) < 50.*ns
			 &&
			 oldPixelNb != pixelNb
			 )//if
			{
			  PixelInterference++; //count pixel interference
			  
			  //print out for debugging only
			  //G4cout << "pixel interference detected" << G4endl;
			}

		      //if yes assign energy, time and position values as energy weighted mean values
		      
		      if(TriggerEnergy > 0) //energy was deposited
			{
			  //add to vectors if output flags are raised
			  if(Triggers_basic)
			    {
			     
			      Trigger_x.push_back((TriggerWeightedPosition.x()/(mm))/(TriggerEnergy));
			      Trigger_y.push_back((TriggerWeightedPosition.y()/(mm))/(TriggerEnergy));
			      Trigger_time.push_back(TriggerWeightedTime/TriggerEnergy/ns);
			      Trigger_energy.push_back(TriggerEnergy/keV);
			      Trigger_particleType.push_back(particleType);
			      Trigger_parentID.push_back(parentID);
			      Trigger_detectorNb.push_back(detNb);

			    }
			  if(Triggers_zmaxFlag) Trigger_zmax.push_back(zmax/mm);
			  if(Triggers_zavgFlag) 
			    { 
			      if(k==1) //lower Detetctor
				Trigger_zavg.push_back(fabs((TriggerWeightedPosition.z())/(TriggerEnergy)-detector->GetLDZfront())/mm);
			      else if(k==2) //upper detector
				Trigger_zavg.push_back(((TriggerWeightedPosition.z())/(TriggerEnergy)-detector->GetUDZfront())/mm);
			    }

			  if(Triggers_electronDrift) Trigger_tshifted.push_back(TriggerWeightedShiftedTime/TriggerEnergy/ns);

		          if(CountBouncesFlag && parentID==0) Trigger_bouncesEAtTrigger.push_back(currentBounceE);
		          if(CountBouncesFlag && parentID==0) Trigger_bouncesPAtTrigger.push_back(currentBounceP);
			  
			  NbOfTriggers++;
			}
		      
		      //reset all the things
		      TriggerEnergy = 0;
		      TriggerWeightedTime = 0;
		      TriggerWeightedPosition.setX(0.);
		      TriggerWeightedPosition.setY(0.);
		      TriggerWeightedPosition.setZ(0.); 
		      zmax = -1000;

		      if(detector->GetPixelFlag() && PixelFlag)
			{//get position of impacted pixel
			  xPixel = ((*trackerCollection)[i]->GetPixelPos().x()/mm);
			  yPixel = ((*trackerCollection)[i]->GetPixelPos().y()/mm);
			}
		      
		      if(Triggers_basic)
			{
			  particleType = ((*trackerCollection)[i]->GetParticleType());
			  parentID = ((*trackerCollection)[i]->GetParentID());
			}

		      if(CountBouncesFlag && parentID==0)
			{
			  currentBounceE = ((*trackerCollection)[i]->GetBounceE());
			  currentBounceP = ((*trackerCollection)[i]->GetBounceP());
			}
		    }
		}
	      
	      //get energy and position of hit
	      G4double energy = ((*trackerCollection)[i]->GetEdep());
	      G4ThreeVector position = ((*trackerCollection)[i]->GetPos());
	      
	      
	      
	      G4double depth = -1000;
	      G4double thickness = -1000;
	      if(k == 1) //lower detector
		{
		  depth = fabs(position.z()-detector->GetLDZfront());
		  thickness = detector->GetLDThickness();
		}
	      else if(k == 2) //upper detector
		{
		  depth = fabs(position.z()-detector->GetUDZfront());
		  thickness = detector->GetUDThickness();
		}
	      
	      if(depth > zmax) zmax = depth;
	

	      G4double Delta_t = 25*ns*((thickness-depth)/thickness);

	      
	      //energy weighted time and position and total energy are added to counters
	      TriggerWeightedTime += (time*energy);
	      TriggerWeightedShiftedTime += ((time+Delta_t)*energy);
	      TriggerEnergy  += energy;
	      TriggerWeightedPosition += (position*energy);
	      
	      
	      oldPixelNb = pixelNb;
	      oldTrackID = trackID;
	      oldTime = time;
	      
	      if(i == (NbHits-1))
		{//last entry summarize and add to collection
		  if(TriggerEnergy > 0)
		    {
		      //TriggerVector.push_back(*aTrigger);
		      //zmax.push_back(aTrigger->zmax);
		      if(Triggers_basic)
			{
			  Trigger_x.push_back((TriggerWeightedPosition.x()/(mm))/(TriggerEnergy));
			  Trigger_y.push_back((TriggerWeightedPosition.y()/(mm))/(TriggerEnergy));
			  Trigger_time.push_back(TriggerWeightedTime/TriggerEnergy/ns);
			  Trigger_energy.push_back(TriggerEnergy/keV);
			  Trigger_particleType.push_back(particleType);
			  Trigger_parentID.push_back(parentID);
			  Trigger_detectorNb.push_back(detNb);

			  
			}
		      if(Triggers_zmaxFlag) Trigger_zmax.push_back(zmax/mm);
		      if(Triggers_zavgFlag)
			{ 
			  if(k==1) //lower Detetctor
			    Trigger_zavg.push_back(fabs((TriggerWeightedPosition.z())/(TriggerEnergy)-detector->GetLDZfront())/mm);
			  else if(k==2) //upper detector
			    Trigger_zavg.push_back(((TriggerWeightedPosition.z())/(TriggerEnergy)-detector->GetUDZfront())/mm);
			}
		      if(Triggers_electronDrift) Trigger_tshifted.push_back(TriggerWeightedShiftedTime/TriggerEnergy/ns);

		      if(CountBouncesFlag && parentID==0) Trigger_bouncesEAtTrigger.push_back(currentBounceE);
		      if(CountBouncesFlag && parentID==0) Trigger_bouncesPAtTrigger.push_back(currentBounceP);

		      if(detector->GetPixelFlag() && PixelFlag)
			{
			  Trigger_xPixel.push_back(xPixel/mm);
			  Trigger_yPixel.push_back(yPixel/mm);
			}
		      NbOfTriggers++;
		    }
		  
		}//last entry
	     
		//G4cout<<"particleType&parentID&currentBounce&bounces:" << particleType << parentID << currentBounce << bounces << G4endl;

	    }//loop over hits collection
	}//loop over both detectors
    }//if(TrackTriggers)

  
  //get pixel dimensions form detector construction
  G4double PixelSideLength = detector->GetPixelSideLength();
  G4double PixelSeparation = std::sqrt(3)*PixelSideLength;

  //determine pixel status
  //pixel status indicates pixel separation of proton trigger and first electron trigger
  PixelStatus = -10;
      
  //default index initialized to -1
  G4int protonIndex = -1;
  G4int electronIndex = -1;

  if(TrackTriggers && Triggers_impactAngleFlag)
    {
      for(G4int i = 0;i < NbOfTriggers; i++)
	{//loop over all triggers
	  Trigger_impactAngle.push_back(-10);//assign error value
	}
    }
	  

  //determine measure TOF from electron first hit and proton hit
  //also determine total deposits and match triggers to impact angles
  if(TrackTriggers)
    {
      //determine Index of proton and first electron Hit
      for(G4int i = 0;i < NbOfTriggers; i++)
	{//loop over all triggers
	  if(CountBouncesFlag)
	    { 
	      if(Trigger_particleType[i] == 1 && Trigger_parentID[i] == 0) protonIndex = i;
	      if(Trigger_particleType[i] == 2 && Trigger_parentID[i] == 0) electronIndex = i; //electron first hit
	      if(TrackKeyVars && Trigger_particleType[i] == 2 && Trigger_parentID[i] == 0)
		{
		  EDepElectron += Trigger_energy[i];
		}
	    }
	  
	  
	  if(TotalDeposits)
	    {

	      //also get total energy deposits
	      if(Trigger_detectorNb[i] == 1) //upper detector
		{
		  if(Trigger_particleType[i] == 1 //is proton
		     &&
		     Trigger_parentID[i] == 0) //is primary
		     EtotProtonUD += Trigger_energy[i];
		  else if(Trigger_particleType[i] == 2 //is electron
			  &&
			  Trigger_parentID[i] == 0) //is primary
		     EtotElectronUD += Trigger_energy[i];
		}
	      else //lower detector
		{
		  if(Trigger_particleType[i] == 1 //is proton
		     &&
		     Trigger_parentID[i] == 0) //is primary
		     EtotProtonLD += Trigger_energy[i];
		  else if(Trigger_particleType[i] == 2 //is electron
			  &&
			  Trigger_parentID[i] == 0) //is primary
		    EtotElectronLD += Trigger_energy[i];
		}
	    }//if(totaldeposits)

	  

	  if(Triggers_basic && Triggers_impactAngleFlag)
	    {

	      G4double MinDeltat = 100*s;
	      if(ProtonImpactAngles->size() > 0 && Trigger_particleType[i] == 1)
		//is a proton and Proton angles Set is non-empty
		{

		  for(G4int j=0;j<(G4int)ProtonImpactAngles->size();j++) 
		    {//loop over possible impact angles
		      G4double Deltat = fabs(Trigger_time[i]-(*ProtonImpactAngles)[j].time); //Get time difference between impact angle and trigger time
		      
		      if(Deltat < 50*ns && Deltat < MinDeltat) 
			{ //record impact angle if it is in the time neighbourhood of trigger and less than previous values
		      
			  Trigger_impactAngle[i] = (*ProtonImpactAngles)[j].angle/deg;
			  MinDeltat=Deltat; //set new MinDeltat
			}
		    }
		}
	      else if(ElectronImpactAngles->size() > 0 && Trigger_particleType[i] == 2) 
		//is a Electron and Electron angles Set is non-empty
		{
		  for(G4int j=0;j<(G4int)ElectronImpactAngles->size();j++) 
		    {//loop over possible impact angles
		      G4double Deltat = fabs(Trigger_time[j]-(*ElectronImpactAngles)[j].time); //Get time difference between impact angle and trigger time
		      
		      if(Deltat < 1*ns && Deltat < MinDeltat) 
			{ //record impact angle if it is in the time neighbourhood of trigger and less than previous values
			  Trigger_impactAngle[j] = (*ElectronImpactAngles)[j].angle/deg;
			  MinDeltat=Deltat; //set new MinDeltat
			}
		    }
		}
	      else //assign error value
		{
		  Trigger_impactAngle[i] = -20; //no impact angle exists
		} 	      
	    }// if(Triggers_basic && Triggers_impactAngleFlag)

	  if(gammaCounter) //if there are gammas check for gamma deposits
	    {
	      if(Trigger_particleType[i] == 3)
		gammaDeposit += Trigger_energy[i];
	    }

	  if(SECounter) //if there are SEs check for SE deposits
	    {
	      if(Trigger_particleType[i] == 2
		 &&
		 Trigger_parentID[i] != 0)
		SEDeposit += Trigger_energy[i];
	    }
	      
	  
	}//loop over all Triggers
    }//if(TrackTriggers)
  

  if(detector->GetPixelFlag() && TrackTriggers && PixelFlag)
    {
      //events with invalid deltaR (proton not detected etc) are assigned  -10
      
      if(protonIndex == -1 || electronIndex == -1) 
	{//assign pixel status -1 if proton or electron are not detected
	  PixelStatus = -1;
	}
      else
	{
	  //compute deltaR (pixel and proper)
	  G4double xProt = Trigger_xPixel[protonIndex];
	  G4double yProt = Trigger_yPixel[protonIndex];
	  G4double xElec = Trigger_xPixel[electronIndex];
	  G4double yElec = Trigger_yPixel[electronIndex];  
	  G4double DeltaX = fabs(xProt-xElec);
	  G4double DeltaY = fabs(yProt-yElec);
	  G4double DeltaR = std::sqrt(DeltaX*DeltaX+DeltaY*DeltaY);
	  
	  PixelDeltaR  = DeltaR/mm;
	  
	  
	  ProperDeltaR = sqrt(pow(fabs(Trigger_x[protonIndex]-
				   Trigger_x[electronIndex]),2) + 
			      pow(fabs(Trigger_y[protonIndex]-
				   Trigger_y[electronIndex]),2)
			      )/mm;
	  
	  


	  //determine pixel status
	  // -1 = n/A
	  // 0  = same Pixel
	  // 1  = first ring (1 radius)
	  // 2  = second ring (2 radii)
	  // 3  = third ring (3 radii)
	  if(DeltaR < 0.5*PixelSeparation)
	    PixelStatus = 0;
	  else if(DeltaR <= PixelSeparation*1.1 && DeltaR >= PixelSeparation*std::sqrt(3)/2)
	    PixelStatus = 1;
	  else if(DeltaR <= 2.1*PixelSeparation && DeltaR >= PixelSeparation*std::sqrt(3))
	    PixelStatus = 2;
	  else if(DeltaR <= 3.1*PixelSeparation && DeltaR >= PixelSeparation*1.5*std::sqrt(3))
	    PixelStatus = 3;
	}
    }

  if(TrackKeyVars && CountBouncesFlag)
    {
	if(PFirstHitDetTime == -1 || EFirstHitDetTime == -1)
	  {
		TimeOfFlight = -1;
	  }
	else
	  {
		TimeOfFlight = (PFirstHitDetTime - EFirstHitDetTime)/microsecond;
	  }

      //if(protonIndex == -1 || electronIndex == -1)
	//{
	  //TimeOfFlight = -1;
	//}
      //else
	//{
	  //TimeOfFlight = (Trigger_time[protonIndex]-Trigger_time[electronIndex])/microsecond;
	//}
    }


  //determine event processing time
  //endEventTime = clock();
  time(&endEventTime);
  elapsedTime = difftime(startEventTime,endEventTime);
  //if (endEventTime<=0) {G4cout << "endEventTime<=0!!" << endEventTime << G4endl;
    //endEventTime=-(clock_t)pow(10,5);
    //endEventTime = clock(); G4cout << "Called clock() again: " << endEventTime << G4endl;}
  //elapsedTime = (G4double)(endEventTime-startEventTime)/CLOCKS_PER_SEC;

  if(PmidPreZ>0 && TimeOfFlight_proton_mm>0)
  {
   PMagmoment_mean=PMagmoment_total_mean/Ppassing;
   PMagmoment_RMS=sqrt(PMagmoment_total_RMS/Ppassing-PMagmoment_mean*PMagmoment_mean);
  }
  else
  {
   PMagmoment_mean=-1000;
   PMagmoment_RMS=-1000;
  }

  //fill tree
  dynamicTree->Fill();
  
  //write tree to file every writeModulo events
  if(!eventID%(writeModulo)) dynamicTree->Write("", TObject::kOverwrite);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....


void NabEventAction::SetStoppingPlanes()
{
  //Tree branch flags
  //considere adding messenger class to switch these via UI
  StoppingPlanes = true;
  FullOutput = false;
  RootOut = false;
  TrackInitialPosition = false;
  TrackProtonMomentum = false;
  TrackElectronMomentum = false;
  TrackNeutrinoMomentum = false;
  TrackTriggers = false;
  Triggers_basic = false;
  Triggers_impactAngleFlag = false;
  Triggers_zmaxFlag = false;
  Triggers_zavgFlag = false;
  Triggers_electronDrift = false;
  CountBouncesFlag = false;
  //Propagate = false;
  Waveform = false;
  DeadParticlesFlag = false;
  TotalDeposits = false;
  PixelFlag = false;
  TrackPixelInterference = false;
  MorePixelFlag = false;
  TrackKeyVars = false;
  CountSecondariesFlag = false;

  writeModulo = 100;
 
}


void NabEventAction::SetDefault()
{
  //Tree branch flags
  //considere adding messenger class to switch these via UI
  StoppingPlanes = false;
  FullOutput = true;
  RootOut = true;
  TrackInitialPosition = true;
  TrackProtonMomentum = true;
  TrackElectronMomentum = true;
  TrackNeutrinoMomentum = false;
  TrackTriggers = true;
  Triggers_basic = true;
  Triggers_impactAngleFlag = false;
  Triggers_zmaxFlag = false;
  Triggers_zavgFlag = false;
  Triggers_electronDrift = false;
  CountBouncesFlag = true;
  //Propagate = true;
  Waveform = false;
  DeadParticlesFlag = false;
  TotalDeposits = true;
  PixelFlag = true;
  TrackPixelInterference = false;
  MorePixelFlag = false;
  TrackKeyVars = true;
  CountSecondariesFlag = true;

  writeModulo = 100;
 
}

void NabEventAction::WriteOutput()
{
  //enables writing Tree to file from endOFrunaction
  if (dynamicTree) dynamicTree->Write("", TObject::kOverwrite);
}

void NabEventAction::ENextStep(const G4ThreeVector& position, G4ThreeVector momentum, G4double localTime, G4bool postInDetector, G4bool preInDetector, G4double preStepEnergy)
{
  if (EenteringDetector) return;
  G4ThreeVector diffPos=position-EpreviousPosition;
  
  if (postInDetector && !preInDetector) {

    EenteringDetector=true;
    //impactAngleViaPos=90-((position)-previousPosition).theta()*degree;
    EimpactAngleViaPos=momentum.theta();
    EimpactAngleViaMomentum=acos(momentum.z()/sqrt(momentum.x()*momentum.x()+momentum.y()*momentum.y()+momentum.z()*momentum.z()));
    EenergyBeforeDetector=preStepEnergy;
    EimpactPosition[0]=position.x();
    EimpactPosition[1]=position.y();
    EimpactPosition[2]=position.z();

  }
  

  EpreviousPosition=(position);
  EpreviousLocalTime=localTime;

}

void NabEventAction::PNextStep(const G4ThreeVector& position, G4ThreeVector momentum, G4double localTime, G4bool postInDetector, G4bool preInDetector, G4double preStepEnergy)
{
  if (PenteringDetector) return;
  G4ThreeVector diffPos=position-PpreviousPosition;
  
  if (postInDetector && !preInDetector) {

    PenteringDetector=true;
    //impactAngleViaPos=90-((position)-previousPosition).theta()*degree;
    PimpactAngleViaPos=momentum.theta();
    PimpactAngleViaMomentum=acos(momentum.z()/sqrt(momentum.x()*momentum.x()+momentum.y()*momentum.y()+momentum.z()*momentum.z()));
    PenergyBeforeDetector=preStepEnergy;
    PimpactPosition[0]=position.x();
    PimpactPosition[1]=position.y();
    PimpactPosition[2]=position.z();

  }
  

  PpreviousPosition=(position);
  PpreviousLocalTime=localTime;

}

void NabEventAction::PMagmom(G4double PMagmoment)
{
  if (PpassingDetMMplane) return;
    Ppassing++;
    PMagmoment_total_mean+=PMagmoment;
    PMagmoment_total_RMS+=PMagmoment*PMagmoment;
    //G4cout<<"Ppassing"<<Ppassing<<G4endl;
}

void NabEventAction::PMidplane(const G4ThreeVector& prePosition)//, G4double postmagmoment
{
  if (PpassingMidplane) return;

    PmidPreZ=prePosition.z();
    PpassingMidplane=true;

}


void NabEventAction::EDetMMplane(const G4ThreeVector& prePosition, const G4ThreeVector& postPosition, G4ThreeVector momentum, G4double prekenergy, G4double postkenergy, G4double prelocalTime, G4double postlocalTime)
{
  if (EpassingDetMMplane) return;
 
    EdetMMPreX=prePosition.x();
    EdetMMPreY=prePosition.y();
    EdetMMPreZ=prePosition.z();
    EdetMMPostX=postPosition.x();
    EdetMMPostY=postPosition.y();
    EdetMMPostZ=postPosition.z();
    EdetMMPx=momentum.x();
    EdetMMPy=momentum.y();
    EdetMMPz=momentum.z();
    EdetMMPostT=postlocalTime;

    if (EdetMMPreZ>0)   {
	 EdetMME=((EdetMMPostZ-4999)*prekenergy+(4999-EdetMMPreZ)*postkenergy)/(EdetMMPostZ-EdetMMPreZ);
	}
    else if (EdetMMPreZ<0) {
	 EdetMME=((-EdetMMPostZ-1199)*prekenergy+(1199+EdetMMPreZ)*postkenergy)/(-EdetMMPostZ+EdetMMPreZ);
	}
    EpassingDetMMplane=true;

}

void NabEventAction::PDetMMplane(const G4ThreeVector& prePosition, const G4ThreeVector& postPosition, G4ThreeVector momentum, G4double prekenergy, G4double postkenergy, G4double prelocalTime, G4double postlocalTime)
{
  if (PpassingDetMMplane) return;

    PdetMMPreX=prePosition.x()/mm;
    PdetMMPreY=prePosition.y()/mm;
    PdetMMPreZ=prePosition.z()/mm;
    PdetMMPostX=postPosition.x()/mm;
    PdetMMPostY=postPosition.y()/mm;
    PdetMMPostZ=postPosition.z()/mm;
    PdetMMPx=momentum.x();
    PdetMMPy=momentum.y();
    PdetMMPz=momentum.z();
    TimeOfFlight_proton_mm=postlocalTime/ns;
    PdetMMPostT=postlocalTime/ns;
    PdetMME=postkenergy/keV;
    PpassingDetMMplane=true;

}


void NabEventAction::PSPplane(const G4ThreeVector& prePosition, const G4ThreeVector& postPosition, G4ThreeVector momentum, G4double prekenergy, G4double postkenergy, G4double prelocalTime, G4double postlocalTime)
{
  if (PpassingUSPplane) return;

    pUSPX=postPosition.x()/mm;
    pUSPY=postPosition.y()/mm;
    pUSPZ=postPosition.z()/mm;
    pUSPPx=momentum.x();
    pUSPPy=momentum.y();
    pUSPPz=momentum.z();
    pUSPT=postlocalTime/ns;
    pUSPE = postkenergy/keV;
    PpassingUSPplane=true;

}

