///////////////////////////////////////////////////////////////
//Name: NabBFieldMap.cc
//Description: Field Map for magnetic fields
///////////////////////////////////////////////////////////////

#include "NabBFieldMap.hh"

#include "globals.hh"

//Tong: new includes are needed
//#include "magfield2.hh"
//#include "elcd2_2.hh"

//Tong: use these includes instead
#include "magfield3.h"  //for magsource() and magfield2()
#include "elcd3.h" //for elcd2()
#include "fieldelcd3.h" //for elfield()

#include <new>
#include <iostream>
#include <fstream>

//Units and Constants
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

//This is a copy of NabEMFieldMap without the electric field.
//See NabEMFieldMap for additional information


NabBFieldMap::NabBFieldMap()
{
  FieldMapFileName = "DefaulFieldMap.dat";

  GridR = 50*cm;
  GridDelta = 1*mm;

  //do not use
  //default constructor only, will not work properly


  //GridZ

}

NabBFieldMap::NabBFieldMap(G4String name, G4String FerencFile, G4double z1, G4double z2, G4double rmax, G4double Delta)
{			
  //Constructor requires the following information
  // - name
  // - File name for coil definitions (required by ferenc's routines)
  // - Dimensions:
  //             - lower boundary (Z1)
  //             - upper boundary (Z2)
  //             - radius (rmax)
  // - map point spacing (Delta)

  FieldMapFileName = name;
  FerencFilename = FerencFile;
  GridDelta = Delta;
  GridZ1 = z1;
  GridZ2 = z2;
  GridR  = rmax;
  NbCoilMax = 20;

}

NabBFieldMap::~NabBFieldMap()
{
  //destructor
  delete FieldMapZ;
  delete FieldMapR;

}


void NabBFieldMap::SetFieldMapName(G4String newName)
{
  //added for completeness
  FieldMapFileName = newName;
  //FieldMapFile();
}

void NabBFieldMap::SetMapDimensions(G4double R, G4double Delta, G4double Z1, G4double Z2)
{
  //added for completeness not currently in use
  GridR = R;
  GridDelta = Delta;
  GridZ1 = Z1;
  GridZ2 = Z2;
}


void NabBFieldMap::LoadFieldMap()
{
  
  FieldMapFile.open(FieldMapFileName.data(),std::ios_base::out | std::ios_base::in);
  if(FieldMapFile.is_open())
    {
      G4cout << "Using existing field map file: " << FieldMapFileName << ". To force generation of fielmap rename or delete existing field mape file" << G4endl;

      FieldMapFile >> ZMax;
      FieldMapFile >> RMax;
      
      //use one dimensional array for greatest simplicity
      FieldMapZ = new G4double[ZMax*RMax]; 
      FieldMapR = new G4double[ZMax*RMax];

      for(int i=0; i<ZMax; i++)
	{
	  for(int j=0; j<RMax; j++)
	    {
	      FieldMapFile >> FieldMapZ[i*RMax+j];
	      FieldMapFile >> FieldMapR[i*RMax+j];
	    }
	}
      
    }
  else
    {
      FieldMapFile.close();
      G4cout << "field map file: " << FieldMapFileName << " not found. Creating new field map" << G4endl;
      MakeFieldMap();
    }
}

void NabBFieldMap::MakeFieldMap()
{
  G4double Zpos;
  G4double Rpos;
  G4double potentialDummy;
  G4double Bz;
  G4double Br;

  G4cout << "blah" << G4endl;

  RMax = (int)(GridR/GridDelta)+1;
  ZMax = (int)((GridZ2-GridZ1)/GridDelta)+1;

  G4cout << ZMax << "\t" << RMax << G4endl;
  
  FieldMapZ = new G4double[ZMax*RMax];
  FieldMapR = new G4double[ZMax*RMax];
  
  FieldMapFile.open(FieldMapFileName.data(),std::ios_base::out);
  
  FieldMapFile << ZMax << G4endl;
  FieldMapFile << RMax << G4endl;

  for(int i=0; i<ZMax; i++)
    {
      Zpos = GridZ1 + i*GridDelta;
      //G4cout << Zpos/m << G4endl;
      for(int j=0; j<RMax; j++)
	{
	  Rpos = j*GridDelta;

	  //elfield(Zpos/m, Rpos/m, &potentialDummy, &Ez, &Er);

	  magfield2(Zpos/m, Rpos/m, (char*)FerencFilename.data(), NbCoilMax, &potentialDummy, &Bz, &Br);
	  
	  FieldMapFile << Bz << "\t" << Br << G4endl;
	  
	  FieldMapZ[i*RMax+j] = Bz;
	  FieldMapR[i*RMax+j] = Br;
	  

	}
    }



  FieldMapFile.close();

}

void NabBFieldMap::Interpolate(G4double Zpos, G4double Rpos, G4double* Bz, G4double* Br)
{
  static G4int lastzIndex = -1;
  static G4int lastrIndex = -1;
  
  static G4double lastZ00Point = 0;
  static G4double lastZ10Point = 0;
  static G4double lastZ01Point = 0;
  static G4double lastZ11Point = 0;

  static G4double lastR00Point = 0;
  static G4double lastR10Point = 0;
  static G4double lastR01Point = 0;
  static G4double lastR11Point = 0;
  
  G4double zRel = Zpos-GridZ1;
  G4int zIndex = (int)(zRel/GridDelta);
  G4int rIndex = (int)(Rpos/GridDelta);

  if (zIndex == lastzIndex && rIndex == lastrIndex)
    {
      
      G4double x = (zRel/GridDelta-zIndex);
      G4double y = (Rpos/GridDelta-rIndex);
      
      *Bz = lastZ00Point*(1-x)*(1-y)
	+ lastZ10Point*x*(1-y)
	+ lastZ01Point*(1-x)*y
	+ lastZ11Point*x*y;

      *Br = lastR00Point*(1-x)*(1-y)
	+ lastR10Point*x*(1-y)
	+ lastR01Point*(1-x)*y
	+ lastR11Point*x*y;
    }

  else
    {
  
      G4double x = (zRel/GridDelta-zIndex);
      G4double y = (Rpos/GridDelta-rIndex);

      lastzIndex = zIndex;
      lastrIndex = rIndex;

      lastZ00Point = FieldMapZ[(zIndex)*RMax+(rIndex)];
      lastZ10Point = FieldMapZ[(zIndex+1)*RMax+(rIndex)];
      lastZ01Point = FieldMapZ[(zIndex)*RMax+(rIndex+1)];
      lastZ11Point = FieldMapZ[(zIndex+1)*RMax+(rIndex+1)];
      
      lastR00Point = FieldMapR[(zIndex)*RMax+(rIndex)];
      lastR10Point = FieldMapR[(zIndex+1)*RMax+(rIndex)];
      lastR01Point = FieldMapR[(zIndex)*RMax+(rIndex+1)];
      lastR11Point = FieldMapR[(zIndex+1)*RMax+(rIndex+1)];
      
      *Bz = lastZ00Point*(1-x)*(1-y)
	+ lastZ10Point*x*(1-y)
	+ lastZ01Point*(1-x)*y
	+ lastZ11Point*x*y;
      
      *Br = lastR00Point*(1-x)*(1-y)
	+ lastR10Point*x*(1-y)
	+ lastR01Point*(1-x)*y
	+ lastR11Point*x*y;
  
      
    }

}
