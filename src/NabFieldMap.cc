///////////////////////////////////////////////////////////////
//Name: NabFieldMap.cc
//Description: Field Map for the electric field close to the detector
///////////////////////////////////////////////////////////////

#include "NabFieldMap.hh"

#include "globals.hh"

//Tong: new includes are needed
//#include "magfield2.hh"
//#include "elcd2_2.hh"

//Tong: use these includes instead
#include "magfield3.h"  //for magsource() and magfield2()
#include "elcd3.h" //for elcd2()
#include "fieldelcd3.h" //for elfield()

#include <new>
#include <iostream>
#include <fstream>

//Units and Constants
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

//field map for purely electric fields
//replaced by NabEMFieldMap for E-M fields
//see NabEMFieldMap for details

NabFieldMap::NabFieldMap()
{
  FieldMapFileName = "DefaulFieldMap.dat";

  GridR = 50*cm;
  GridDelta = 1*mm;
  InverseGridDelta = 1./GridDelta;
  

  //GridZ

}

NabFieldMap::NabFieldMap(G4String name, G4double z1, G4double z2, G4double rmax, G4double Delta)
{						
  FieldMapFileName = name;
  GridDelta = Delta;
  InverseGridDelta = 1./GridDelta;
  GridZ1 = z1;
  GridZ2 = z2;
  GridR  = rmax;

}

NabFieldMap::~NabFieldMap()
{
  delete FieldMapZ;
  delete FieldMapR;

}


void NabFieldMap::SetFieldMapName(G4String newName)
{
  FieldMapFileName = newName;
  //FieldMapFile();
}

void NabFieldMap::SetMapDimensions(G4double R, G4double Delta, G4double Z1, G4double Z2)
{
  GridR = R;
  GridDelta = Delta;
  InverseGridDelta = 1./GridDelta;
  GridZ1 = Z1;
  GridZ2 = Z2;
}


void NabFieldMap::LoadFieldMap()
{
  

    FieldMapFile.open(FieldMapFileName.data(),std::ios_base::out | std::ios_base::in);
    if(FieldMapFile.is_open())
      {
	G4cout << "Using existing field map file: " << FieldMapFileName << ". To force generation of fielmap rename or delete existing field mape file" << G4endl;

	FieldMapFile >> ZMax;
	FieldMapFile >> RMax;
	
	FieldMapZ = new G4double[ZMax*RMax];
	FieldMapR = new G4double[ZMax*RMax];
	
	for(int i=0; i<ZMax; i++)
	  {
	    for(int j=0; j<RMax; j++)
	      {
		FieldMapFile >> FieldMapZ[i*RMax+j];
		FieldMapFile >> FieldMapR[i*RMax+j];
	      }
	  }
      }
    else
      {
	FieldMapFile.close();
	G4cout << "field map file: " << FieldMapFileName << " not found. Creating new field map" << G4endl;
	MakeFieldMap();
      }
}

void NabFieldMap::MakeFieldMap()
{
  G4double Zpos;
  G4double Rpos;
  G4double potentialDummy;
  G4double Ez;
  G4double Er;

  G4cout << "blah" << G4endl;

  RMax = (int)(GridR*InverseGridDelta)+1;
  ZMax = (int)((GridZ2-GridZ1)*InverseGridDelta)+1;

  G4cout << ZMax << "\t" << RMax << G4endl;
  
  FieldMapZ = new G4double[ZMax*RMax];
  FieldMapR = new G4double[ZMax*RMax];
  
  FieldMapFile.open(FieldMapFileName.data(),std::ios_base::out);
  
  FieldMapFile << ZMax << G4endl;
  FieldMapFile << RMax << G4endl;

  for(int i=0; i<ZMax; i++)
    {
      Zpos = GridZ1 + i*GridDelta;
      G4cout << Zpos/m << G4endl;
      for(int j=0; j<RMax; j++)
	{
	  Rpos = j*GridDelta;

	  if(i==0 || i == ZMax-1) 
	    {
	      FieldMapZ[i*RMax+j] = 0;
	      FieldMapR[i*RMax+j] = 0;
	      continue;
	  
	    }

    //Tong:
    G4double taa[3];

	  elfield(Zpos/m, Rpos/m, &potentialDummy, &Ez, &Er, &taa[0], &taa[1], &taa[2]);
	  
	  FieldMapFile << Ez << "\t" << Er << G4endl;
	  
	  FieldMapZ[i*RMax+j] = Ez;
	  FieldMapR[i*RMax+j] = Er;
	  

	}
    }



  FieldMapFile.close();

}

void NabFieldMap::Interpolate(G4double Zpos, G4double Rpos, G4double* Ez, G4double* Er)
{


      G4double zRel = Zpos-GridZ1;
      G4int zIndex = (int)(zRel*InverseGridDelta);
      G4int rIndex = (int)(Rpos*InverseGridDelta);
      
      G4double x = (zRel*InverseGridDelta-zIndex);
      G4double y = (Rpos*InverseGridDelta-rIndex);
      
      *Ez = FieldMapZ[zIndex*RMax+rIndex]*(1.-x)*(1.-y)
	+ FieldMapZ[(zIndex+1)*RMax+rIndex]*x*(1.-y)
	+ FieldMapZ[(zIndex)*RMax+(rIndex+1)]*(1.-x)*y
	+ FieldMapZ[(zIndex+1)*RMax+(rIndex+1)]*x*y;

      *Er = FieldMapR[zIndex*RMax+rIndex]*(1.-x)*(1.-y)
	+ FieldMapR[(zIndex+1)*RMax+rIndex]*x*(1.-y)
	+ FieldMapR[(zIndex)*RMax+(rIndex+1)]*(1.-x)*y
	+ FieldMapR[(zIndex+1)*RMax+(rIndex+1)]*x*y;



}
