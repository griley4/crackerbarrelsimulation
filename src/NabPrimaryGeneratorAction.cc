///////////////////////////////////////////////////////////////
//Name: NabPrimaryGeneratorAction.cc
//Description: Starts Events by generating primary particles
///////////////////////////////////////////////////////////////

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//#include "TTree.h"
//#include "TSystem.h"
//#include "TFile.h"
//#include "TBranch.h"

#include "NabPrimaryGeneratorAction.hh"

#include "CBDetectorConstruction.hh"
#include "NabPrimaryGeneratorMessenger.hh"
#include "NabEventAction.hh"
#include "NabSteppingAction.hh"
#include "NabField.hh"

#include "G4Event.hh"
#include "G4ParticleTable.hh"
#include "G4IonTable.hh"
#include "G4ParticleDefinition.hh"
#include "Randomize.hh"
#include "G4RunManager.hh"

#include "elcd3.h"

#include <fstream>
#include <iomanip>
#include "G4ios.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

NabPrimaryGeneratorAction::NabPrimaryGeneratorAction(
						     CBDetectorConstruction* DC, G4int seed)
  :detector(DC), rand_seed(seed)
{
  LDZfront = DC->GetLDZfront();
  //number of particles generated per call to GeneratePrimaryVertex()
  G4int n_particle = 1;
  
  //create particle gun
  particleGPS  = new G4GeneralParticleSource();
  particleGun = new G4ParticleGun(n_particle);

  //Create Messenger
  gunMessenger = new NabPrimaryGeneratorMessenger(this);
  
  //set definition of proton and electron
  G4ParticleTable* particleTable   = G4ParticleTable::GetParticleTable();
  electron_def      = particleTable->FindParticle("e-");
  proton_def        = particleTable->FindParticle("proton");
  decay_neutron_def = particleTable->FindParticle("nabNeutron");

  //calculate neutron proton mass difference using global constants
  G4double NPMassDifference = (neutron_mass_c2
				-proton_mass_c2);
  
  //maximal reaction energy = maximal electron energy
  electronEnergy_max = NPMassDifference-electron_mass_c2;  

  //all masses are geant4 internal hardwired constants

  //a seperate eventID counter is necessarry as Primary generator action preceds event => no calls to GetEventID can be made
  eventID = 0;

  //set to default behavior if no other input is received from the user
  SetDefaultBehavior();

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

NabPrimaryGeneratorAction::~NabPrimaryGeneratorAction()
{
  //clean up:
  //see detector construction for additional information on memory leaks etc
  delete particleGun;
  delete particleGPS;
  delete gunMessenger;
}


void NabPrimaryGeneratorAction::SetDefaultBehavior()
{ //default behavior of particle gun

  RunMode             = Nab_MonoEnergetic;//Nab_ContinousSpectrum;//Nab_WithCoefficients;//Nab_MonoEnergetic;//Nab_ProtonResponse;//Nab_DiscreteSpectrum;//Nab_TwoStepProtons;
  //RunMode             = Nab_WithCoefficients;//Nab_ContinousSpectrum;//Nab_WithCoefficients;//Nab_MonoEnergetic;//Nab_ProtonResponse;//Nab_DiscreteSpectrum;//Nab_TwoStepProtons;

  DecayVolume         = Nab_VolBeamNoEdge; //decay volume w/o edge effects

  PointSource         = false; //For Nab_WithCoefficients RunMode

  //RunProton           = false;
  RunProton           = true;
  RunElectron         = true;
  RunForwardProton    = 0;
  //run both particles

  neutronVelocity     = false;
  neutronVelocityAmp  = 100; //amplification factor of neutron velocity

  //default energies (in case user fails to supply them)
  //GRANT 6km/s velocity for H atoms * masses of particles for kenitic energy
  //monoEnergyProton = 10.*eV;
  monoEnergyElectron = 300.1*eV;
  monoEnergyProton = 0.190*eV;
  //monoEnergyElectron = 0.19*eV;
  RingRadius = -1.*cm;

  //all parameters in the standard model used in Nab_WithCoefficients RunMode
  //GRANT no more mr. "a"
  littleA = 0.0;
  littleB = 0.0;
  bigA    = -0.118;
  bigB    = 0.985;
  Pn      = 0.0;
//
//  if(RunMode == Nab_TwoStepProtons){
//
//    //Set Address of ROOT file once!
//    f=new TFile(Form("/net/data4/pocanic4/jaf2qc/Nab/NabSimulation/NabSimulation/64bit/20180126_1DE1DB_WithCoeff_StoppingPlanes_60us_1M_%d_0.root",rand_seed), "READONLY");
//    t=(TTree*)f->Get("dynamicTree");
//    t->SetBranchAddress("eE0", &initialeE0);
//    t->SetBranchAddress("pUSPX", &bPX); 
//    t->SetBranchAddress("pUSPY", &bPY);
//    t->SetBranchAddress("pUSPZ", &bPZ); 
//    t->SetBranchAddress("pUSPPx", &bPPx); 
//    t->SetBranchAddress("pUSPPy", &bPPy); 
//    t->SetBranchAddress("pUSPPz", &bPPz); 
//    t->SetBranchAddress("pUSPE", &bPE); 
//    t->SetBranchAddress("pUSPT", &bPT); 
//    //EntryCounter=(rand_seed-1)*1000000;
//    EntryCounter=0;
//    G4cout<<"seed in TwoStep = "<<rand_seed<<G4endl;
//    G4cout<<"file for TwoStep =  "<<f<<G4endl;
//  }

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void NabPrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  //this function is called at the begining of event
  //This is the main method of the primary generator action several options can be set and changed via UI messenger and RunMode Variable

  if(!eventAction) eventAction = (NabEventAction*) G4RunManager::GetRunManager()->GetUserEventAction();
  //if eventaction pointer is empty get eventaction from RunManager


  //number generator is seed as a funtion of:
  //1. User command line imput
  //2. Event ID
  //this ensures reproducability, independent of runs
  //ResetRandomNumberGenerator();
  IncrementeventID();
  
  //manual mode is intended for debugging testing purposes only!
  // use only in conjuction with sensible input parameters via UI or macro
  //garantied to fail/crash/be useless without proper user input
  if(RunMode == Nab_ManualMode){
      particleGun->GeneratePrimaryVertex(anEvent);
  }

  //discrete spectrum
  //sample beta spectrum but only use 5 discrete electon energies (150, 300, 450, 600, 750 keV)
  //see spring 2014 paper for more information on beta spectrum 
  //easier to analyse than continous spectrum

  else if(RunMode == Nab_DiscreteSpectrum){
    
    do{
      DiscreteEnergies(); //randomly choose one of the electron energies
      ComputeMomenta(); //generate random momenta
      ComputePosition(DecayVolume); //uniform random position in decay volume
    } while (pPz<(-1+1.7*RunForwardProton));//if RunForwardProton==1 then only events starting with pPz>0.7 will be generated

	  
	  
    //set particle gun to initial position
    particleGun->SetParticlePosition(G4ThreeVector(x0, y0, z0));
    
    //Runs a proton with energy and momenta computed above
    if(RunProton){
      particleGun->SetParticleDefinition(proton_def);
      particleGun->SetParticleMomentumDirection(G4ThreeVector(pPx,pPy,pPz));
      particleGun->SetParticleEnergy(protonEnergy);  
      //generate proton
      particleGun->GeneratePrimaryVertex(anEvent);
      eventAction->SetpP0x(pPx);
      eventAction->SetpP0y(pPy);
      eventAction->SetpP0z(pPz);
      eventAction->SetpE0(protonEnergy);	
    }
    
    //Runs a electron with energy and momenta computed 
    if(RunElectron){
      particleGun->SetParticleDefinition(electron_def);
      particleGun->SetParticleEnergy(electronEnergy);  
      particleGun->SetParticleMomentumDirection(G4ThreeVector(ePx,ePy,ePz));
      //generate electron
      particleGun->GeneratePrimaryVertex(anEvent);
      eventAction->SeteP0x(ePx);
      eventAction->SeteP0y(ePy);
      eventAction->SeteP0z(ePz);
      eventAction->SeteE0(electronEnergy);
      //  G4cout<<"primary  "<<pPx<<G4endl;
      // G4cout<<"primary  "<<protonEnergy<<G4endl; 
      
    }
    eventAction->Setx0decay(x0);
    eventAction->Sety0decay(y0);
    eventAction->Setz0decay(z0);	
    
  }
  

  //continous spectrum
  //all energy values of the beta spectrum are allowed sampled from distribution via rejection sampling algorithm
  else if(RunMode == Nab_ContinousSpectrum){
    do{
      ContinousEnergies(); //sample beta spectrum for electron energy
      ComputeMomenta();
      
      /*
      pPx = 0;
      pPy = 0;
      pPz = 1;

      ePx = 0;
      ePy = 0;
      ePz = -1;
      */

      ComputePosition(DecayVolume);
    } while (pPz<(-1+1.7*RunForwardProton));//if RunForwardProton==1 then only events starting with pPz>0.7 will be generated
    
    /*
      x0 = 0*cm; //2*cm;
      y0 = 0*cm; //2*cm;
      z0 = -13.189*cm; //-17.189*cm; -9.189*cm;
    */
    particleGun->SetParticlePosition(G4ThreeVector(x0, y0, z0));
    //Runs a proton with energy and momenta computed above
    if(RunProton){
      particleGun->SetParticleDefinition(proton_def);
      particleGun->SetParticleMomentumDirection(G4ThreeVector(pPx,pPy,pPz));
      particleGun->SetParticleEnergy(protonEnergy);  
      //generate proton
      particleGun->GeneratePrimaryVertex(anEvent);
      eventAction->SetpP0x(pPx);
      eventAction->SetpP0y(pPy);
      eventAction->SetpP0z(pPz);
      eventAction->SetpE0(protonEnergy);	
      
    }
    
    //Runs a electron with energy and momenta computed 
    if(RunElectron){
      particleGun->SetParticleDefinition(electron_def);
      particleGun->SetParticleEnergy(electronEnergy);  
      particleGun->SetParticleMomentumDirection(G4ThreeVector(ePx,ePy,ePz));
      //generate electron
      particleGun->GeneratePrimaryVertex(anEvent);
      
      eventAction->SeteP0x(ePx);
      eventAction->SeteP0y(ePy);
      eventAction->SeteP0z(ePz);
      eventAction->SeteE0(electronEnergy);
    }
    eventAction->Setx0decay(x0);
    eventAction->Sety0decay(y0);
    eventAction->Setz0decay(z0);	
    
  }
  
  //full simulation with coefficients set by user via UI messenger
  //employes rejection sampling
  //it is recommend to use this only to generate pseudo data
  //use weights calculated from initial momenta otherwise
  

  else if(RunMode == Nab_WithCoefficients){
    G4double xPropose, yPropose;
    G4double beta_e;
    G4double cospvtheta, cospetheta;
    do{
      do{
	
	xPropose = G4UniformRand()*electronEnergy_max;
	yPropose = G4UniformRand()*(0.13*(1+fabs(littleA)));
	
	electronEnergy = xPropose;
	
	G4double pe_= sqrt(pow(electronEnergy+electron_mass_c2,2)-pow(electron_mass_c2,2));
	beta_e= pe_/(electron_mass_c2+electronEnergy);
	G4double pv_= (electronEnergy_max-electronEnergy);
	cospetheta= G4UniformRand()*2.0-1.0;
	G4double sinpetheta= sqrt( 1-pow( cospetheta, 2));
	cospvtheta= G4UniformRand()*2.0-1.0;
	G4double sinpvtheta= sqrt( 1-pow( cospvtheta, 2));
	G4double pephi= G4UniformRand()*twopi;
	G4double pvphi= G4UniformRand()*twopi;
	
	costev= cospetheta*cospvtheta+sinpetheta*sinpvtheta*cos(pephi-pvphi);
	
	G4double pp2=pe_*pe_+pv_*pv_+2*pe_*pv_*costev;
	G4double pp_= sqrt(pp2);
	protonEnergy = pp2/(2.*proton_mass_c2); // MeV
	  
	ePx = sinpetheta*cos(pephi);
	ePy = sinpetheta*sin(pephi);
	ePz = cospetheta;
	
	G4double vpxt= sinpvtheta*cos(pvphi);
	G4double vpyt= sinpvtheta*sin(pvphi);
	G4double vpzt= cospvtheta;
	
	pPx= -(pe_*ePx+pv_*vpxt)/pp_;
	pPy= -(pe_*ePy+pv_*vpyt)/pp_;
	pPz= -(pe_*ePz+pv_*vpzt)/pp_;
	
	//Neutron velocity
	//*******************************************
	if(neutronVelocity) {

	nV = 1000; // neutron velocity m/s
	nV = -nV * neutronVelocityAmp; // negative factor to set beam direction along positive y-axis and amplification factor to amplify effect of velocity
	
	G4double nBeta = nV/(c_light*pow(10,6));

	G4double nVx = 0;
	G4double nVy = nV;
	G4double nVz = 0;

	G4double nBetax = nVx/(c_light*pow(10,6));
	G4double nBetay = nVy/(c_light*pow(10,6));
	G4double nBetaz = nVz/(c_light*pow(10,6));

	G4double lorentz_gamma = 1. / sqrt(1. - nV*nV/(c_squared*pow(10,12)));

	//electron
	G4double ePx_mag = ePx*pe_;
	G4double ePy_mag = ePy*pe_;
	G4double ePz_mag = ePz*pe_;

	G4double ePx_mag_lab = -lorentz_gamma*nBetax*(electronEnergy+0.5110) + ePx_mag + (nBetax/pow(nBeta,2))*(lorentz_gamma-1)*(nBetax*ePx_mag+nBetay*ePy_mag+nBetaz*ePz_mag);
	G4double ePy_mag_lab = -lorentz_gamma*nBetay*(electronEnergy+0.5110) + ePy_mag + (nBetay/pow(nBeta,2))*(lorentz_gamma-1)*(nBetax*ePx_mag+nBetay*ePy_mag+nBetaz*ePz_mag);
	G4double ePz_mag_lab = -lorentz_gamma*nBetaz*(electronEnergy+0.5110) + ePz_mag + (nBetaz/pow(nBeta,2))*(lorentz_gamma-1)*(nBetax*ePx_mag+nBetay*ePy_mag+nBetaz*ePz_mag);
	electronEnergy = lorentz_gamma*((electronEnergy+0.5110) - nBetax*ePx_mag - nBetay*ePy_mag - nBetaz*ePz_mag) - 0.5110;

	pe_ = sqrt(ePx_mag_lab*ePx_mag_lab + ePy_mag_lab*ePy_mag_lab + ePz_mag_lab*ePz_mag_lab);
	ePx = ePx_mag_lab/pe_;
	ePy = ePy_mag_lab/pe_;
	ePz = ePz_mag_lab/pe_;
	
	//proton
	G4double pPx_mag = pPx*pp_;
	G4double pPy_mag = pPy*pp_;
	G4double pPz_mag = pPz*pp_;

	G4double pPx_mag_lab = -lorentz_gamma*nBetax*(protonEnergy+938.3) + pPx_mag + (nBetax/pow(nBeta,2))*(lorentz_gamma-1)*(nBetax*pPx_mag+nBetay*pPy_mag+nBetaz*pPz_mag);
	G4double pPy_mag_lab = -lorentz_gamma*nBetay*(protonEnergy+938.3) + pPy_mag + (nBetay/pow(nBeta,2))*(lorentz_gamma-1)*(nBetax*pPx_mag+nBetay*pPy_mag+nBetaz*pPz_mag);
	G4double pPz_mag_lab = -lorentz_gamma*nBetaz*(protonEnergy+938.3) + pPz_mag + (nBetaz/pow(nBeta,2))*(lorentz_gamma-1)*(nBetax*pPx_mag+nBetay*pPy_mag+nBetaz*pPz_mag);
	protonEnergy = lorentz_gamma*((protonEnergy+938.3) - nBetax*pPx_mag - nBetay*pPy_mag - nBetaz*pPz_mag) - 938.3;

	pp_ = sqrt(pPx_mag_lab*pPx_mag_lab + pPy_mag_lab*pPy_mag_lab + pPz_mag_lab*pPz_mag_lab);
	pPx = pPx_mag_lab/pp_;
	pPy = pPy_mag_lab/pp_;
	pPz = pPz_mag_lab/pp_;

	//neutrino
	G4double vpxt_mag = vpxt*pv_;
	G4double vpyt_mag = vpyt*pv_;
	G4double vpzt_mag = vpzt*pv_;

	G4double vpxt_mag_lab = -lorentz_gamma*nBetax*(pv_) + vpxt_mag + (nBetax/pow(nBeta,2))*(lorentz_gamma-1)*(nBetax*vpxt_mag+nBetay*vpyt_mag+nBetaz*vpzt_mag);
	G4double vpyt_mag_lab = -lorentz_gamma*nBetay*(pv_) + vpyt_mag + (nBetay/pow(nBeta,2))*(lorentz_gamma-1)*(nBetax*vpxt_mag+nBetay*vpyt_mag+nBetaz*vpzt_mag);
	G4double vpzt_mag_lab = -lorentz_gamma*nBetaz*(pv_) + vpzt_mag + (nBetaz/pow(nBeta,2))*(lorentz_gamma-1)*(nBetax*vpxt_mag+nBetay*vpyt_mag+nBetaz*vpzt_mag);
	pv_ = lorentz_gamma*(pv_ - nBetax*vpxt_mag - nBetay*vpyt_mag - nBetaz*vpzt_mag);

	vpxt = vpxt_mag_lab/pv_;
	vpyt = vpyt_mag_lab/pv_;
	vpzt = vpzt_mag_lab/pv_;
	
	}// if(neutronVelocity)
	
      }while(yPropose > e2spek(xPropose)*(1
					  +littleA*beta_e*costev
					  +littleB*electron_mass_c2/(electronEnergy+electron_mass_c2)
					  +bigA*beta_e*cospetheta*Pn
					  +bigB*cospvtheta*Pn)
	     );
      if (PointSource) {x0=0.; y0=0.; z0=0.0*cm;}
      else ComputePosition(DecayVolume);	
    } while (pPz<(-1+1.7*RunForwardProton));//if RunForwardProton==1 then only events starting with pPz>0.7 will be generated
    
    particleGun->SetParticlePosition(G4ThreeVector(x0, y0, z0));
    
    
    if(RunProton){
      particleGun->SetParticleDefinition(proton_def);
      particleGun->SetParticleMomentumDirection(G4ThreeVector(pPx,pPy,pPz));
      particleGun->SetParticleEnergy(protonEnergy);  
      //generate proton
      particleGun->GeneratePrimaryVertex(anEvent);
      eventAction->SetpP0x(pPx);
      eventAction->SetpP0y(pPy);
      eventAction->SetpP0z(pPz);
      eventAction->SetpE0(protonEnergy);	
    }
    
    //Runs a electron with energy and momenta computed 
    if(RunElectron){
      particleGun->SetParticleDefinition(electron_def);
      particleGun->SetParticleEnergy(electronEnergy);  
      particleGun->SetParticleMomentumDirection(G4ThreeVector(ePx,ePy,ePz));
      //generate electron
      particleGun->GeneratePrimaryVertex(anEvent);
      
      eventAction->SeteP0x(ePx);
      eventAction->SeteP0y(ePy);
      eventAction->SeteP0z(ePz);
      eventAction->SeteE0(electronEnergy);
      
    }
    eventAction->Setx0decay(x0);
    eventAction->Sety0decay(y0);
    eventAction->Setz0decay(z0);	
    eventAction->Setcostev(costev);
  }
  
  //Generate protons at the upper plane where the potential starts to affect the kinematics
  //and propagate through to the UDet. Take a ROOT file and start from the SP where the 
  //potential is 100 mV 
  else if(RunMode ==Nab_TwoStepProtons){
    
    
    t->GetEntry(EntryCounter);
    
    if(bPZ<-400){RunProton=false; RunElectron=false;}
    else{RunProton=true; RunElectron=true;}

    if(RunProton){
      //set kinematics to last kinematics from ROOT file
      particleGun->SetParticleDefinition(proton_def);
      particleGun->SetParticlePosition(G4ThreeVector(bPX,bPY,bPZ));
      particleGun->SetParticleMomentumDirection(G4ThreeVector(bPPx,bPPy,bPPz));
      particleGun->SetParticleEnergy(bPE*keV);
      particleGun->SetParticleTime(bPT);
      //generate proton
      particleGun->GeneratePrimaryVertex(anEvent);
      eventAction->SetpP0x(bPPx);
      eventAction->SetpP0y(bPPy);
      eventAction->SetpP0z(bPPz);
      eventAction->SetpE0(bPE);
    }

    //Runs a electron with energy and momenta computed 
    if(RunElectron){
      particleGun->SetParticleDefinition(electron_def);
      particleGun->SetParticleEnergy(initialeE0*keV);  
      particleGun->SetParticleMomentumDirection(G4ThreeVector(bPPx,bPPy,bPPz)); //for now, set to the proton until we get the electron kinematics in there
      //generate electron
      particleGun->GeneratePrimaryVertex(anEvent);
      
      eventAction->SeteP0x(bPPx);
      eventAction->SeteP0y(bPPy);
      eventAction->SeteP0z(bPPz);
      eventAction->SeteE0(initialeE0);
      
    }

    //Set decay point to last coordinates from ROOT file
    eventAction->Setx0decay(bPX);
    eventAction->Sety0decay(bPY);
    eventAction->Setz0decay(bPZ);
    eventAction->SetEventTime(bPT);
    
    //printf("event %d, x=(%lf,%lf,%lf), p=(%lf,%lf,%lf), E=%lf, t=%lf\n",eventID,bPX,bPY,bPZ,bPPx,bPPy,bPPz,bPE,bPT);
    //printf("z = %lf\n",bPZ);
    EntryCounter++;

  }


  //generate electrons of a single energy
  //protons are optional
  else if(RunMode == Nab_MonoEnergetic){
    int tries=0;
    do {	
      electronEnergy = monoEnergyElectron;
      protonEnergy = monoEnergyProton;
      //ComputeMomenta();
      ePy=ePx=pPy=pPz = 0.;  //atomic hydrogen source has all momentum in one direction (for now)
      pPx =   1.;  //see def above
      ePz =   -1.; //see def above
      ComputePosition(DecayVolume);
      tries++;
      //printf("event %i tries %i\n",eventID,tries);
    //grant} while (tries<(10.));//if RunForwardProton==1 then only events starting with pPz>0.7 will be generated
    } while (pPz<(-1+1.7*RunForwardProton));//if RunForwardProton==1 then only events starting with pPz>0.7 will be generated
    //ComputePosition(DecayVolume);
    
    particleGun->SetParticlePosition(G4ThreeVector(x0, y0, z0));
    
    //Runs a proton with energy and momenta computed above  
    if(RunProton){
      particleGun->SetParticleDefinition(proton_def);
      particleGun->SetParticleMomentumDirection(G4ThreeVector(pPx,pPy,pPz));
      particleGun->SetParticleEnergy(protonEnergy);  
      //generate proton
      particleGun->GeneratePrimaryVertex(anEvent);
      eventAction->SetpP0x(pPx);
      eventAction->SetpP0y(pPy);
      eventAction->SetpP0z(pPz);
      eventAction->SetpE0(protonEnergy);	
    }
    //Runs a electron with energy and momenta computed 
    if(RunElectron){
      particleGun->SetParticleDefinition(electron_def);
      particleGun->SetParticleEnergy(electronEnergy);  
      particleGun->SetParticleMomentumDirection(G4ThreeVector(ePx,ePy,ePz));
      //generate electron
      particleGun->GeneratePrimaryVertex(anEvent);
      eventAction->SeteP0x(ePx);
      eventAction->SeteP0y(ePy);
      eventAction->SeteP0z(ePz);
      eventAction->SeteE0(electronEnergy);
      
    }
    eventAction->Setx0decay(x0);
    eventAction->Sety0decay(y0);
    eventAction->Setz0decay(z0);	
    
  }
  
  
  // run protons of a single energy only (no electrons)
  // momentum is uniform
  else if(RunMode == Nab_ProtonResponse){

    double y1[7],ygc[7];
    char inputcoil[100];	
    strcpy(inputcoil,"Roger2.dat"); //inputcoil_Roger2.dat
    
    do{
      do{
	ContinousEnergies();//DiscreteEnergies(); //ContinousEnergies(); //sample beta spectrum for electron energy
	ComputeMomenta();
	ComputePosition(DecayVolume);
      } while (pPz<(-1+1.7*RunForwardProton));
      
      y1[0] = x0/m;
      y1[1] = y0/m;
      y1[2] = z0/m;
      y1[3] = pPx;
      y1[4] = pPy;
      y1[5] = pPz;
      y1[6] = protonEnergy/kg/m/m*s*s;
      move_particle_to_GC_coordinates( y1, ygc, inputcoil); 
    } while ((ygc[0]*ygc[0]+ygc[1]*ygc[1])<1e-12);
    
    //x0 = 0*cm; //2*cm;
    //y0 = 0*cm; //2*cm;
    //z0 = -13.189*cm; //-17.189*cm; -9.189*cm;
    //pPx = 0.43589;
    //pPy = 0.0;
    //pPz = 0.9;
    //protonEnergy = 300.*eV;
    particleGun->SetParticlePosition(G4ThreeVector(x0, y0, z0));
    /*
      x0=(eventID%1000)*0.002*cm;
      y0=0.0*cm;
      z0=((floor(eventID/1000))*0.08-17.189)*cm;
      electronEnergy=(rand_seed*7.0+80)*keV;
      do{
      ComputeMomenta();
      } while (pPz<(-1+1.7*RunForwardProton));
      particleGun->SetParticlePosition(G4ThreeVector(x0, y0, z0));
    */
    //Runs a proton with energy and momenta computed above
    if(RunProton){
      particleGun->SetParticleDefinition(proton_def);
      particleGun->SetParticleMomentumDirection(G4ThreeVector(pPx,pPy,pPz));
      particleGun->SetParticleEnergy(protonEnergy);  
      //generate proton
      particleGun->GeneratePrimaryVertex(anEvent);
      eventAction->SetpP0x(pPx);
      eventAction->SetpP0y(pPy);
      eventAction->SetpP0z(pPz);
      eventAction->SetpE0(protonEnergy);	
      
    }
    
    //Runs a electron with energy and momenta computed 
    if(false){
      particleGun->SetParticleDefinition(electron_def);
      particleGun->SetParticleEnergy(electronEnergy);  
      particleGun->SetParticleMomentumDirection(G4ThreeVector(ePx,ePy,ePz));
      //generate electron
      particleGun->GeneratePrimaryVertex(anEvent);
      
      eventAction->SeteP0x(ePx);
      eventAction->SeteP0y(ePy);
      eventAction->SeteP0z(ePz);
      eventAction->SeteE0(electronEnergy);
    }
    
    eventAction->Setx0decay(x0);
    eventAction->Sety0decay(y0);
    eventAction->Setz0decay(z0);	
  }
  
  //force a neutron to decay at the initial vertex
  //not recommended as geant4 beta spectrum is not accurate at low energies
  else if (RunMode == Nab_NeutronDecay){
    ComputePosition(DecayVolume);
    
    particleGun->SetParticlePosition(G4ThreeVector(x0, y0, z0));
    
    particleGun->SetParticleDefinition(decay_neutron_def);
    particleGun->SetParticleEnergy(0.015*eV);
    particleGun->GeneratePrimaryVertex(anEvent);
  }
  
  else if (RunMode == Ce139){
    G4IonTable* ionTable = (G4IonTable*)(G4ParticleTable::GetParticleTable()->GetIonTable());
    G4ParticleDefinition* Ce139_def = ionTable->GetIon(58,139,0);
    //ce139_def         = particleTable->FindParticle("ion");
    ComputePosition(DecayVolume);
    
    particleGun->SetParticlePosition(G4ThreeVector(0,0,-13.189*cm));
    particleGun->SetParticleEnergy(0*eV);
    particleGun->SetParticleDefinition(Ce139_def);
    particleGun->GeneratePrimaryVertex(anEvent);
  }
  
  else if (RunMode == SourceTest){
    //ComputePosition(DecayVolume);
    //G4double front = DC->GetLDZfront();
    //G4cout << "PrimaryGeneratorAction LDZfront: " << LDZfront << G4endl;
    particleGPS->SetParticlePosition(G4ThreeVector(0,0,LDZfront+117.4));
    particleGPS->GeneratePrimaryVertex(anEvent);
  }
  
  else if (RunMode == ManyDiscreteParticles){
    
    G4double x=0,y=0,z=0;
    G4double r=0,theta=0;
    G4double energy=0;
    
    r=(int)floor(eventID/100)%10;
    theta=2*pi/100*(eventID%100);
    x=r*cos(theta)*cm;
    y=r*sin(theta)*cm;
    z=(ceil(eventID/1000)-5)/2*cm-13.189*cm;
    
    ComputeMomenta();
    
    energy=rand_seed*0.01;
    
    particleGun->SetParticlePosition(G4ThreeVector(x,y,z));
    particleGun->SetParticleMomentumDirection(G4ThreeVector(ePx,ePy,ePz));
    particleGun->SetParticleEnergy(energy);
    particleGun->SetParticleDefinition(electron_def);
    particleGun->GeneratePrimaryVertex(anEvent);
    
    eventAction->SeteP0x(ePx);
    eventAction->SeteP0y(ePy);
    eventAction->SeteP0z(ePz);
    eventAction->SeteE0(energy);
    
    eventAction->Setx0decay(x);
    eventAction->Sety0decay(y);
    eventAction->Setz0decay(z);	
    
  }
  
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

void NabPrimaryGeneratorAction::DiscreteEnergies()
{
  //sets energy of electron to one of the following 5 values: 150, 300, 450, 600 or 750 keV with probability according to the beta spectrum
  G4double e2speknorm= e2spek(0.15)+e2spek(0.30)+e2spek(0.45)+e2spek(0.60)+e2spek(0.75);
  //G4double xhelp= eventID*sqrt(3.0)/e2speknorm;
  //xhelp= xhelp-floor( xhelp);
  G4double xhelp = G4UniformRand();
  if (xhelp*e2speknorm < e2spek( 0.15)) 
    {
    electronEnergy= 0.15;
    } 
  else if (xhelp*e2speknorm < e2spek( 0.15)+e2spek( 0.3)) 
    {
      electronEnergy= 0.30;
    } 
  else if (xhelp*e2speknorm < e2spek( 0.15)+e2spek( 0.3)+e2spek(0.45)) 
    {
      electronEnergy= 0.45;
    } 
  else if (xhelp*e2speknorm < e2spek( 0.15)+e2spek( 0.3)+e2spek(0.45)+e2spek(0.60)) 
    {
      electronEnergy= 0.60;
    }
  else 
    {
    electronEnergy= 0.75;
    }

} 

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

void NabPrimaryGeneratorAction::ContinousEnergies()
{
  //sets electron energy to a allowed beta decay energy with probability given by the beat spectrum

  G4double xPropose, yPropose;
  do{
    xPropose = G4UniformRand()*electronEnergy_max;
    yPropose = G4UniformRand()*0.13;
  }while(yPropose > e2spek(xPropose));
  
  electronEnergy = xPropose;
}
  

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

void NabPrimaryGeneratorAction::ComputeMomenta()
{
  //compute the initial momenta by radomly generating two directions and applying conservation of momentum to obtain the 3rd direction does not take into account correlation coefficients

  G4double pe_= sqrt(pow(electronEnergy+electron_mass_c2,2)-pow(electron_mass_c2,2));
  //G4double beta_e= pe_/(electron_mass_c2+electronEnergy);
  G4double pv_= (electronEnergy_max-electronEnergy);
  G4double cospetheta= G4UniformRand()*2.0-1.0;
//  std::ofstream myFile;
//   myFile.open("randTest.out",std::ios::app);
//   myFile << cospetheta << G4endl;
//   myFile.close();
  G4double sinpetheta= sqrt( 1-pow( cospetheta, 2));
  G4double cospvtheta= G4UniformRand()*2.0-1.0;
  G4double sinpvtheta= sqrt( 1-pow( cospvtheta, 2));
  G4double pephi= G4UniformRand()*twopi;
  G4double pvphi= G4UniformRand()*twopi;

  costev= cospetheta*cospvtheta+sinpetheta*sinpvtheta*cos(pephi-pvphi);
  G4double pp2=pe_*pe_+pv_*pv_+2*pe_*pv_*costev;
  G4double pp_= sqrt(pp2);
  protonEnergy = pp2/(2.*proton_mass_c2); // MeV

  ePx = sinpetheta*cos(pephi);
  ePy = sinpetheta*sin(pephi);
  ePz = cospetheta;
    
  G4double vpxt= sinpvtheta*cos(pvphi);
  G4double vpyt= sinpvtheta*sin(pvphi);
  G4double vpzt= cospvtheta;
 
  pPx= -(pe_*ePx+pv_*vpxt)/pp_;
  pPy= -(pe_*ePy+pv_*vpyt)/pp_;
  pPz= -(pe_*ePz+pv_*vpzt)/pp_;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

void NabPrimaryGeneratorAction::CylindricalVolume(G4double beamRadius, G4double beamPosition, G4double beamHeight)
{ //assigns the starting point from a
  //uniform Random cylindrical distribution
  z0 = beamPosition + (G4UniformRand()-0.5)*beamHeight;
  do {//this loops rejects all x,y pairs outside the radius
    x0 = (2.0*(G4double)G4UniformRand()-1.0)*beamRadius; //grant offset of 25 mm to compensate for hydrogen in motion
    y0 = (2.0*(G4double)G4UniformRand()-1.0)*beamRadius;
  } while (sqrt(pow(x0,2)+pow(y0,2)) > beamRadius);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void NabPrimaryGeneratorAction::BeamNoEdgeVolume(G4double z, G4double deltaz, G4double diagonal2)
{
  z0 = z + (G4UniformRand()-0.5)*deltaz;
  x0 = sqrt(diagonal2/2)*(G4UniformRand()-0.5);
  y0 = sqrt(diagonal2/2)*(G4UniformRand()-0.5);
} 


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void NabPrimaryGeneratorAction::ResetRandomNumberGenerator()
{// reinitialize random number generator, if needed
  
  if (rand_seed > 0) {
    CLHEP::HepRandom::setTheSeed(long(eventID)*1000+rand_seed); //Do not use initial seeds > 1000 or there will be repeated events
  }
  eventID++;
}

void NabPrimaryGeneratorAction::IncrementeventID()
{// just increment eventID, DO NOT give a different random seed
  eventID++;
}

G4double NabPrimaryGeneratorAction::e2spek(G4double Ee)
{
  //beta decay spectrum including coulomb correction
  //no radiative or proton mass correction terms

  //returns probability of a electron energy given a certain electron energy

  if ((Ee >= 0) && (Ee < electronEnergy_max)) {
    G4double alpha_SI= 1/137.036;
    G4double pe_= sqrt(pow(Ee+electron_mass_c2,2)-pow(electron_mass_c2,2));
    G4double beta_= pe_/(electron_mass_c2+Ee);
    //G4double CC= 1+alpha_SI*twopi/2/beta_+pow(alpha_SI,2)*(11/4-0.5772-log(2*beta_*(electron_mass_c2+Ee)*0.01/4/electron_mass_c2+sqr(twopi/2/beta_)/3));
    G4double CC = twopi*alpha_SI/beta_/(1-exp(-twopi*alpha_SI/beta_));
    return pow(electronEnergy_max-Ee,2)*pe_*(Ee+electron_mass_c2)*CC;
  } 
  else {
    return 0;
  }
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void NabPrimaryGeneratorAction::ComputePosition(NabDecayVolume flag)
{
  //compute initial position as function of decay volume
  //currently only one option
  //can be expanded to a volume including edge effects etc.
  
  if(RingRadius>=0){ 
    G4double phi = twopi*G4UniformRand();
    x0 = 0.0*cm;
    y0 = 0.0*cm;
    z0 = 0.0*cm;
  }
  else{
    
    if(flag == Nab_VolBeamNoEdge){
      //dimensions and position of the decay volume
      G4double beamRadius   = 0.5*cm;
      G4double beamPosition = 0.0*cm;
      G4double beamHeight   = 0.*cm;
      
      CylindricalVolume(beamRadius, beamPosition, beamHeight);
      
    }
    
  }
  if(!eventAction) eventAction = (NabEventAction*) G4RunManager::GetRunManager()->GetUserEventAction();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
