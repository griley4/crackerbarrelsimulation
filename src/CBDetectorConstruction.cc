///////////////////////////////////////////////////////////////
//Name: CBDetectorConstruction.cc
//Description: Responsible for geometry of Detector
///////////////////////////////////////////////////////////////

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

#include "CBDetectorConstruction.hh"
#include "NabDetectorMessenger.hh"

//Units and Constants
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

//Objects
#include "G4SubtractionSolid.hh"
#include "G4UnionSolid.hh"
#include "G4Tubs.hh"
#include "G4Box.hh"
#include "G4Cons.hh"
#include "G4Polyhedra.hh"
#include "G4Polycone.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"

#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"


//Materials
#include "G4UnitsTable.hh"
#include "G4NistManager.hh"
#include "G4RunManager.hh"

//Fields
#include "NabField.hh"
#include "G4TransportationManager.hh"
#include "G4FieldManager.hh"
#include "G4EqMagElectricField.hh"
#include "G4Mag_UsualEqRhs.hh"
#include "G4MagIntegratorStepper.hh"
#include "G4CashKarpRKF45.hh"
#include "G4ClassicalRK4.hh"
#include "G4SimpleHeum.hh"
#include "G4HelixMixedStepper.hh"
#include "G4SimpleRunge.hh"
#include "G4MagIntegratorDriver.hh"
#include "G4PropagatorInField.hh"
#include "G4ChordFinder.hh"

//Sensitive Detectors
#include "NabSensitiveDetector.hh"
#include "NabROGeometry.hh"
#include "G4SDManager.hh"

#include "G4UserLimits.hh"

//Visualization
#include "G4VisAttributes.hh"
#include "G4Color.hh"

//electordes and magnets from File
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "G4UIcommand.hh"

const G4int MaxNbOfCoils      = 1;
const G4int MaxNbOfElectrodes = 200;



//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

CBDetectorConstruction::CBDetectorConstruction(G4String fieldfile)
  :WorldMaterial(0), World_solid(0), World_logic(0), World_physical(0), UD_logic(0), LD_logic(0),
   FieldFileSuffix(fieldfile)
{
  //For the two detectors the defining quantities are:
  // - Z-coordinate of their frontside (front defined as facing origin)
  // - thickness
  // - radius
  // - thickness of the deadlayer 
  //all other quantities:
  // - center Z-coordinate
  // - back Z-coordinate
  // - DeadLayer center Z-coordinate
  //are derived form these

  //For the Ion foil the important quantities are:
  // - Lenghth
  // - Thickness
  // - separation from the top Detector
  //derived from these are
  // - center Z-coordinate
  // - front and back Z-coordinate
  //NOTE: Ion Foil is not currently fully implemented and tested: It will likely not work if turned on!!

  //Field Region Boundaries and Radii are currently hardwired
  //It is highly recommende to add these to the list of quantities read in from a file as they might vary in between electrode setups
  //the configurations ROGER2 and BIGMOUTH3 should work all other may cause undesired behavior!!


  //Defined Dimensions:

  //World:
  WorldSizeX           = 2.*m;
  WorldSizeY           = 2.*m;
  WorldSizeZ           = 2.*m;


  //Upper Detecor:
  UDRadius             = 6.3*cm;
  UDThickness          = 2.*mm;
  //UDZfront             = 0.25*m;
  UDDLThickness        = 100.*nm;
 
  //Lower Detector:
  LDRadius             = 6.3*cm;
  LDThickness          = 2.*mm;
  //LDZfront             = -0.25*m;
  LDDLThickness        = 100.*nm;
  
  //Ion Foil
  IFThickness          = 100.*nm;
  FoilSpacing          = 10.*cm;
  IFLength             = 8.*cm;

  //Readout Pix2s
  PixelSideLength      = 5.2*mm;
  ActiveDetectorRadius = 5.45*cm;

  //Grid
  GridWireWidth        = 0.01*mm;//wire has square 0.01*0.01 mm^2 cross section
  GridWireSpacing      = 4*mm; //center-to-center

  //FieldRegions
  DriftR_UEFR_Boundary    = 0.5*m;
  DriftR_LEFR_Boundary    = -0.5*m;
  DriftRegionRadius       = 1.*m;
  //DriftRegionRadius       = 0.15*m;
  UDR_UEFR_Boundary       = 0.5*m;
  LDR_LEFR_Boundary       = -0.5*m;
  //UEFRegionRadius         = 0.15*m;
  //LEFRegionRadius         = 0.15*m;
  UEFRegionRadius         = 1.*m;
  LEFRegionRadius         = 1.*m;
  UDRegionRadius          = 0.085*m;
  LDRegionRadius          = 0.085*m;
  UDriftR_Pinch_Boundary  = 0.075*m;
  LDriftR_Pinch_Boundary  = -0.075*m;
  

  //Coil, Elcetrode and Grid build flags
  CoilsFromFile        = false;  //turned off for CB
  ElectrodesFromFile   = true;
  PGONelectrodes       = false;
  PixelFlag            = true;
  FieldsOn             = true;
  DriftRegionFlag      = false;
  GridFlag             = false;
  IFFlag               = false;   //NOTE: Ion Foil is not currently fully implemented and tested: It will likely not work if turned on!!
  UseFieldMapFlag      = true;    //switch to false if want to use ferenc for everywhere.
  GlobalFieldMapFlagDetector = true; //1D EM Field
  Ce139Flag            = false;
  globalField          = true;
  oldStepper           = false;
   
  //////////////////////////////////////////////////////
  //Define Materials:

  //this method is old and should be replaced with calls to the NIST manager as shown below.
  DefineMaterials();


  ElectrodeMaterial = G4NistManager::Instance()->FindOrBuildMaterial("G4_Cu");
  CoilMaterial = G4NistManager::Instance()->FindOrBuildMaterial("G4_Fe");
  GridMaterial = G4NistManager::Instance()->FindOrBuildMaterial("G4_Al");
  IFMaterial = G4NistManager::Instance()->FindOrBuildMaterial("G4_Al");
  SetWorldMaterial("Galactic");
  SetElectrodeMaterial_Ti("TiGradeV");
  SetLDMaterial("Silicon");
  SetUDMaterial("Silicon");
  //SetCarbonFoilMaterial("Carbon");
CarbonFoilMaterial = G4NistManager::Instance()->FindOrBuildMaterial("G4_C");

  //create UI messenger for Detector Construction
  detectorMessenger = new NabDetectorMessenger(this);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

CBDetectorConstruction::~CBDetectorConstruction()
{ 
  //delete FoilStepLimit;
  //clean up: delete UI messenger
  delete detectorMessenger;

  //to find out about potential memory leaks resulting from insufficient clean up use valgrind to monitor and potentially add delete statements
  
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....



G4VPhysicalVolume* CBDetectorConstruction::Construct()
{

  // Cleanup old geometry
  //
  G4GeometryManager::GetInstance()->OpenGeometry();
  G4PhysicalVolumeStore::GetInstance()->Clean();
  G4LogicalVolumeStore::GetInstance()->Clean();
  G4SolidStore::GetInstance()->Clean();


  //This is the main method of the detector Construction.
  //Three objects are associated with very geometriacal obejct: solid, logic and physical
  
  //Solid: Defines dimensions of an object:
  //requires:
  //     - name
  //     - dimensions
  //standard shapes: cubes (G4Box), cylindres (G4Tubs)etc.

  //Logic: Defines Materials and is used to assign field manager or Sensitive Detector to an object
  //requires:
  //       - name
  //       - material
  //       - pointer to solid
  //Additional attributes can be attached to the logic volume:
  //Vis: Visual Attributes such as color
  //FieldManager: E-M field associated with the volume/region
  //sensitive detector: Flags the volume for processing hits on each step particles take in the volume using user defined sensitive detector class

  //Physical or Placement: Defines Position of the obejct relative to a mother volume
  //requires:
  //       - name
  //       - position
  //       - orientation (rotation matrix etc.)
  //       - pointer to logic volume
  //       - pointer to logic volume of mother volume
  //       - copy number (to distinguish identical placements of the same solid)
   

  /////////////////////////////////////////////////////
  //Derived/Computed Dimensions:
  
  //pos = position: refers to the Z-coordinate of the center
  // geant4 places volumes according to their center coordinate
  // most definitions are made by boundary positions
  // the following calculate center coords from boundary positions

  UDZpos   = UDZfront + UDDLThickness + 0.5*UDThickness;
  UDDLZpos = UDZfront + 0.5*UDDLThickness;
  UDZback  = UDZfront + UDThickness;

  
  LDZpos   = LDZfront - LDDLThickness - 0.5*LDThickness;
  LDDLZpos = LDZfront - 0.5*LDDLThickness;
  LDZback  = LDZfront - LDThickness;

  IFZpos   = UDZfront - FoilSpacing - 0.5*IFThickness;
  IFZfront = IFZpos - 0.5*IFThickness; 
  IFZback  = IFZpos + 0.5*IFThickness;

  DriftRegionExtent    = (DriftR_UEFR_Boundary - DriftR_LEFR_Boundary)/2.0;
  DriftRegionZpos      = DriftR_LEFR_Boundary+ DriftRegionExtent;
//grant
  UEFRegionExtent  = (UDR_UEFR_Boundary-DriftR_UEFR_Boundary)/2.0;
  UEFRegionZpos    = DriftR_UEFR_Boundary+UEFRegionExtent;

  LEFRegionExtent  = (DriftR_LEFR_Boundary-LDR_LEFR_Boundary)/2.0;
  LEFRegionZpos    = DriftR_LEFR_Boundary-LEFRegionExtent;

  UDRegionExtent  = (UDZfront-UDR_UEFR_Boundary)/2.0;
  UDRegionZpos    = UDR_UEFR_Boundary+UDRegionExtent;

  LDRegionExtent  = (LDR_LEFR_Boundary-LDZfront)/2.0;
  LDRegionZpos    = LDR_LEFR_Boundary-LDRegionExtent;
  
  //Fields
  //********************************************
  //
  if(FieldsOn) SetupFields(); //see below and NabField.cc for details

  //Sensitive detectors & ReadOut Geometry/Pixelation
  //********************************************
  //Get sensitive detector Manager
  SDman = G4SDManager::GetSDMpointer();  

  //build readout geometry if pixel flag is set. Readout geometry is non physical and will only be used by the sensitive detector class.
  if(PixelFlag)
    {
      ROGeometry = new NabROGeometry("ROGeom");
      ROGeometry->BuildROGeometry();
    }
 

  // World
  //********************************************
  World_solid    = new G4Box("World",                   //Name
			     WorldSizeX,                //Dimensions
			     WorldSizeY,
			     WorldSizeZ); 
                         
  World_logic    = new G4LogicalVolume(World_solid,     //solid	   
				       WorldMaterial,   //Material
				       "World");	//Name
                                   
  World_physical = new G4PVPlacement(0,			//no rotation
				     G4ThreeVector(),	//at (0,0,0)
				     World_logic,	//its logical volume
				     "World",		//its name
				     0,			//its mother  volume
				     false,		//no boolean operation
				     0);		//copy number

  // stopping plane
  //********************************************
  USP_solid    = new G4Box("USP_s",                   //Name
			     WorldSizeX,                //Dimensions
			     WorldSizeY,
			     0.5*mm); 
                         
  USP_logic    = new G4LogicalVolume(USP_solid,     //solid	   
				       WorldMaterial,   //Material
				       "USP_l");	//Name
                                   
  USP_physical = new G4PVPlacement(0,			//no rotation
				     G4ThreeVector(0,0,0.25*m),	//at (0,0,0)
				     USP_logic,	//its logical volume
				     "USP_FU",		//its name
				     World_logic,			//its mother  volume
				     false,		//no boolean operation
				     0);		//copy number

  //********************************************
  LSP_solid    = new G4Box("LSP",                   //Name
			     WorldSizeX,                //Dimensions
			     WorldSizeY,
			     0.5*mm); 
                         
  LSP_logic    = new G4LogicalVolume(LSP_solid,     //solid	   
				       WorldMaterial,   //Material
				       "LSP");	//Name
                                   
  LSP_physical = new G4PVPlacement(0,			//no rotation
				     G4ThreeVector(0,0,-0.5*m),	//at (0,0,0)
				     LSP_logic,	//its logical volume
				     "LSP",		//its name
				     World_logic,			//its mother  volume
				     false,		//no boolean operation
				     0);		//copy number

  // 1mm plane
  //********************************************
  UCM_solid    = new G4Box("UCM",                   //Name
			     WorldSizeX,                //Dimensions
			     WorldSizeY,
			     0.5*mm); 
                         
  UCM_logic    = new G4LogicalVolume(UCM_solid,     //solid	   
				       WorldMaterial,   //Material
				       "UCM");	//Name
                                   
  UCM_physical = new G4PVPlacement(0,			//no rotation
				     G4ThreeVector(0,0,0.446*m),	//at (0,0,0)
				     UCM_logic,	//its logical volume
				     "UCM",		//its name
				     World_logic,			//its mother  volume
				     false,		//no boolean operation
				     0);		//copy number

  //********************************************
  LCM_solid    = new G4Box("LCM",                   //Name
			     WorldSizeX,                //Dimensions
			     WorldSizeY,
			     0.5*mm); 
                         
  LCM_logic    = new G4LogicalVolume(LCM_solid,     //solid	   
				       WorldMaterial,   //Material
				       "LCM");	//Name
                                   
  LCM_physical = new G4PVPlacement(0,			//no rotation
				     G4ThreeVector(0,0,-0.446*m),	//at (0,0,0)
				     LCM_logic,	//its logical volume
				     "LCM",		//its name
				     World_logic,			//its mother  volume
				     false,		//no boolean operation
				     0);		//copy number


  // Drift Region
  //********************************************

  //Drifte Region is at center of detector where electric fields are neglible: ie far away from detectors.
  if(DriftRegionFlag)
    {

      DriftRegion_solid    = new G4Tubs("DriftRegion",                  
					0,
					DriftRegionRadius,      
					DriftRegionExtent,  
					0.*deg,             
					360.*deg); 

      
      DriftRegion_logic    = new G4LogicalVolume(DriftRegion_solid,     	   
						 WorldMaterial,  
						 "DriftRegion");
      
      //set drift region special field manager
      //DriftRegion_logic->SetFieldManager(DriftRegionFieldManager, true);

      DriftRegion_physical = new G4PVPlacement(0,		
					       G4ThreeVector(0, 0, DriftRegionZpos),
					       DriftRegion_logic,
					       "DriftRegion",	
					       World_logic,
					       false,
					       0);
    }

  if (!globalField) {
  UEFRegion_solid    = new G4Tubs("UEFRegion",                  
				    0,
				    UEFRegionRadius,      
				    UEFRegionExtent,  
				    0.*deg,             
				    360.*deg); 
  
  
  UEFRegion_logic    = new G4LogicalVolume(UEFRegion_solid,     	   
					   WorldMaterial,  
					   "UEFRegion");
  
  //set Upper Electric Field region special field manager
  UEFRegion_logic->SetFieldManager(UEFRegionFieldManager, true);
  
  UEFRegion_physical = new G4PVPlacement(0,		
					 G4ThreeVector(0, 0, UEFRegionZpos),
					 UEFRegion_logic,
					 "UEFRegion",	
					 World_logic,
					 false,
					 0);

  //upper detector Region
 
  UDRegion_solid    = new G4Tubs("UDRegion",                  
				    0,
				    UDRegionRadius,      
				    UDRegionExtent,  
				    0.*deg,             
				    360.*deg); 
  
  
  UDRegion_logic    = new G4LogicalVolume(UDRegion_solid,     	   
					     WorldMaterial,  
					     "UDRegion");
  
  //set upper Detector Field region special field manager
  UDRegion_logic->SetFieldManager(UDRegionFieldManager, true);

  
  UDRegion_physical = new G4PVPlacement(0,		
					   G4ThreeVector(0, 0, UDRegionZpos),
					   UDRegion_logic,
					   "UDRegion",	
					   World_logic,
					   false,
					   0);
  



  LEFRegion_solid    = new G4Tubs("LEFRegion",                  
				    0,
				    LEFRegionRadius,      
				    LEFRegionExtent,  
				    0.*deg,             
				    360.*deg); 
  
  
  LEFRegion_logic    = new G4LogicalVolume(LEFRegion_solid,     	   
					     WorldMaterial,  
					     "LEFRegion");
  
  //set Upper Electric Field region special field manager
  LEFRegion_logic->SetFieldManager(LEFRegionFieldManager, true);
  
  LEFRegion_physical = new G4PVPlacement(0,		
					   G4ThreeVector(0, 0, LEFRegionZpos),
					   LEFRegion_logic,
					   "LEFRegion",	
					   World_logic,
					   false,
					   0);


  //lower detector Region
 
  LDRegion_solid    = new G4Tubs("LDRegion",                  
				    0,
				    LDRegionRadius,      
				    LDRegionExtent,  
				    0.*deg,             
				    360.*deg); 
  
  
  LDRegion_logic    = new G4LogicalVolume(LDRegion_solid,     	   
					     WorldMaterial,  
					     "LDRegion");
  
  //set Lower Detector Field region special field manager
  LDRegion_logic->SetFieldManager(LDRegionFieldManager, true);

  
  LDRegion_physical = new G4PVPlacement(0,		
					   G4ThreeVector(0, 0, LDRegionZpos),
					   LDRegion_logic,
					   "LDRegion",	
					   World_logic,
					   false,
					   0);
  

  } // end regions  



  //Upper Detector
  //********************************************
  UD_solid          = new G4Tubs("UpperDetector",                  
				 0,
				 UDRadius,      
				 0.5*UDThickness,  
				 0.*deg,             
				 360.*deg); 
                          
  UD_logic          = new G4LogicalVolume(UD_solid, 
					  UDMaterial,   
					  "UpperDetector");  

  UD_SD             = new NabSensitiveDetector("upper", "upperCollection");

  if(PixelFlag) UD_SD->SetROgeometry(ROGeometry);
  SDman->AddNewDetector(UD_SD);
  UD_logic->SetSensitiveDetector(UD_SD);

  UD_vis = new G4VisAttributes;
  UD_vis->SetColor(G4Color::Red());
  UD_logic->SetVisAttributes(UD_vis);

  			                  
  UD_physical       = new G4PVPlacement(0,
					G4ThreeVector(0.,0.,UDZpos), 
					UD_logic,    
					"UpperDetector",   
					World_logic,      
					false,   
					0);     

  //Upper Dead Layer
  UDDL_solid        = new G4Tubs("UpperDL",
			 0,
			 UDRadius,
			 0.5*UDDLThickness,
			 0.*deg,
			 360.*deg);
				     
  UDDL_logic        = new G4LogicalVolume(UDDL_solid,
					  UDMaterial,
					  "UpperDL");

  UDDL_physical     = new G4PVPlacement(0,
					G4ThreeVector(0.,0.,UDDLZpos), 
					UDDL_logic,       
					"UpperDL",        
					World_logic,        
					false,        
					0); 
  
  UDDL_vis = new G4VisAttributes;
  UDDL_vis->SetColor(G4Color::Red());
  UDDL_logic->SetVisAttributes(UDDL_vis);


                              
  // LowerDetector
  //********************************************
  LD_solid       = new G4Tubs("LowerDetector",  
			      0,
			      LDRadius,
			      0.5*LDThickness, 
			      0.*deg, 
			      360.*deg); 
                          
  LD_logic       = new G4LogicalVolume(LD_solid,
				       LDMaterial, 
				       "LowerDetector"); 

  LD_SD             = new NabSensitiveDetector("lower", "lowerCollection");
  if(PixelFlag) LD_SD->SetROgeometry(ROGeometry);
  SDman->AddNewDetector(LD_SD);
  LD_logic->SetSensitiveDetector(LD_SD);
  
  LD_vis = new G4VisAttributes;
  LD_vis->SetColor(G4Color::Red());
  LD_logic->SetVisAttributes(LD_vis);   
  
  LD_physical    = new G4PVPlacement(0,
				     G4ThreeVector(0.,0.,LDZpos), 
				     LD_logic,   
				     "LowerDetector",  
				     World_logic,  
				     false,   
				     0);
  
  //lower Detector Dead Layer
  LDDL_solid      = new G4Tubs("LowerDL",
			       0,
			       LDRadius,
			       0.5*LDDLThickness,
			       0.*deg,
			       360.*deg);
				     
  LDDL_logic      = new G4LogicalVolume(LDDL_solid,
					LDMaterial,
					"LowerDL");
  

  LDDL_physical   = new G4PVPlacement(0,  
				      G4ThreeVector(0.,0.,LDDLZpos),  
				      LDDL_logic,      
				      "LowerDL",      
				      World_logic,        
				      false,              
				      0);     
  
  LDDL_vis = new G4VisAttributes;
  LDDL_vis->SetColor(G4Color::Red());
  LDDL_logic->SetVisAttributes(LDDL_vis);


  //IonFoil
  //********************************************
  //Mother volume needs to be updated (so, position needs to be updated too)
  if(IFFlag)
    {
      IF_solid        = new G4Box("IonConversionFoil",
				  IFLength,
				  IFLength,
				  IFThickness);
                          
      IF_logic        = new G4LogicalVolume(IF_solid, 
					    IFMaterial, 
					    "IonConversionFoil");

      IF_vis = new G4VisAttributes;
      IF_vis->SetColor(G4Color::Red());
      IF_logic->SetVisAttributes(IF_vis); 

      			                  
      IF_physical     = new G4PVPlacement(0,	
					  G4ThreeVector(0.,0.,IFZpos),   
					  IF_logic,    
					  "IonConversionFoil",        
					  UEFRegion_logic,       
					  false,          
					  0);         
    } // if(IFFlag)
  
  //Coils (File Input)
  //********************************************
  
  if(CoilsFromFile)
    {
      //open coil File (has prefix "inputcoil_")
      std::ifstream coilFile;
      coilFileName = "inputcoil_" + FieldFileSuffix;
      coilFile.open(coilFileName.data());
      
      //determine number of coils
      G4int NbOfCoils;
      coilFile >> NbOfCoils;
            if (MaxNbOfCoils<NbOfCoils) {G4cout << "Requesting more coils than MaxNbOfCoils as specified in CBDetectorConstruction.  Critical error.  Causes undefined memory behavior." << G4endl; exit(1);}
      //Define Array to hold hold the files information
      G4double CoilZpos[MaxNbOfCoils], CoilRadius[MaxNbOfCoils], CoilThickness[MaxNbOfCoils], CoilLength[MaxNbOfCoils];
      
      for (G4int i=0; i<NbOfCoils; i++) 
	{//loop over all entries in the file
	  G4double dummyCurrent;
	  coilFile >> CoilZpos[i] >> CoilRadius[i] >> CoilThickness[i] >> CoilLength[i] >> dummyCurrent;

	  //create a solid, logic and placement for each coil
	  Coil_solid    = new G4Tubs("Magnet" + i,
				     CoilRadius[i]*m,
				     (CoilRadius[i]+CoilThickness[i])*m,
				     0.5*CoilLength[i]*m,
				     0.*deg,
				     360.*deg);    
	  
	  Coil_logic    = new G4LogicalVolume(Coil_solid,
					      CoilMaterial,
					      "Magnet");    

	  Coil_vis = new G4VisAttributes;
	  Coil_vis->SetColor(G4Color::Green());
	  Coil_logic->SetVisAttributes(G4VisAttributes::GetInvisible()); 
	  
	  Coil_physical = new G4PVPlacement(0,
					    G4ThreeVector(0.,0.,CoilZpos[i]*m),
					    Coil_logic,
					    "Magnet" + G4UIcommand::ConvertToString(i+1),
					    World_logic,
					    false,
					    i+1);
	}
    }// if(CoilsFromFile)
  

  //Electrodes (File Input)
  //********************************************
  if(ElectrodesFromFile)
    {
      //open electrode File (has prefix "inputelectrodes_")
      electrodeFileName = "inputelectrodes_" + FieldFileSuffix;
      std::ifstream electrodeFile;
      electrodeFile.open(electrodeFileName.data());
      
      G4int NbOfElectrodes;
      electrodeFile >> NbOfElectrodes; //read number of electrodes
      if (MaxNbOfElectrodes<NbOfElectrodes) {G4cout << "Requesting more electrodes than MaxNbOfElectrodes as specified in CBDetectorConstruction.  Critical error.  Causes undefined memory behavior." << G4endl; exit(1);}
      //define arrays to store information from file
      G4double ElecZposA[MaxNbOfElectrodes], ElecRadiusA[MaxNbOfElectrodes], ElecZposB[MaxNbOfElectrodes], ElecRadiusB[MaxNbOfElectrodes];
      
      for (G4int i=0; i<NbOfElectrodes; i++) 
	{//loop over all entries in the file
	  G4int dummyElecGp, dummyID;
	  G4double dummyVoltage;
	  G4double ElectrodeHalfLength = 0;
	  G4double RAmin = 0, RAmax = 0, RBmin = 0, RBmax = 0;
	  G4double ElectrodeThickness = 2.*mm;
	
	  //read a line from the file and save the entries in the arrays and dummy variables for unneeded entries
	  electrodeFile >> dummyElecGp >> ElecZposA[i] >> ElecRadiusA[i] >> ElecZposB[i] >> ElecRadiusB[i] >> dummyVoltage >> dummyID;
	  
	  //this if statement is needed for vertical (disk-like) electrodes
	  //these would otherwise have a zero half-length
	  if(fabs(ElecZposA[i]-ElecZposB[i])/m < 1e-10/m)
	    {
	      if(ElecZposA[i] == -1.2 || ElecZposA[i] == 5) continue;
	      
	      if(ElecRadiusA[i] < ElecRadiusB[i])
		{//determine larger radius
		  //set up the cone as a cylinder
		  //halflenght is the thickness of the electrode
		  RAmin = ElecRadiusA[i]*m;
		  RBmin = ElecRadiusA[i]*m;
		  RAmax = ElecRadiusB[i]*m;
		  RBmax = ElecRadiusB[i]*m;
		  ElectrodeHalfLength = 0.5*ElectrodeThickness;
		}
	      else 
		{
		  RAmin = ElecRadiusB[i]*m;
		  RBmin = ElecRadiusB[i]*m;
		  RAmax = ElecRadiusA[i]*m;
		  RBmax = ElecRadiusA[i]*m;
		  ElectrodeHalfLength = 0.5*ElectrodeThickness;
		}
     }
	  else
	    {//this case covers regular (non disk-like) electrodes
	      RAmin = ElecRadiusA[i]*m;
	      RBmin = ElecRadiusB[i]*m;
	      RAmax = ElecRadiusA[i]*m + ElectrodeThickness;
	      RBmax = ElecRadiusB[i]*m + ElectrodeThickness;
	      ElectrodeHalfLength = 0.5*(fabs(ElecZposA[i]-ElecZposB[i]))*m;//-1*mm; //Stevens: The 1*mm corrects for 2mm thick disks to avoid overlaps
	    }
	  //create a solid, logic and placement for each coil
	  //cones are used to accomodate both for cylinders and cones
	  Electrode_solid    = new G4Cons("Electrode",
					  RAmin,
					  RAmax,
					  RBmin,
					  RBmax,
					  ElectrodeHalfLength,
					  0.*deg,
					  360.*deg);    
	  
	  Electrode_logic    = new G4LogicalVolume(Electrode_solid,
						   ElectrodeMaterial,
						   "Electrode");
	  
	  //Set Electrode Color: 
	  //blue = electrodes at a non-zero potential
	  //yellow = electrodes at zero potential
	  	Electrode_vis = new G4VisAttributes;
	  	if(fabs(dummyVoltage) > 1e-10) Electrode_vis->SetColor(G4Color::Yellow());
	  	else Electrode_vis->SetColor(G4Color::Yellow());
	  //Electrode_vis->SetForceSolid(true);
	  	Electrode_logic->SetVisAttributes(Electrode_vis); 

	  
	  Electrode_physical = new G4PVPlacement(0,
						 G4ThreeVector(0.,0.,0.5*(ElecZposA[i]-ElecZposB[i])*m+ElecZposB[i]*m),
						 Electrode_logic,
						 "Electrode",
						 World_logic,
						 false,
						 i+1);
	}
    }//  if(ElectrodesFromFile)


  // PGONelectrodes
  //********************************************
  if(PGONelectrodes) {

  // ElectrodePGON_vis
  ElectrodePGON_vis = new G4VisAttributes;
  ElectrodePGON_vis->SetColor(G4Color::Red());
  ElectrodePGON_vis->SetForceSolid(true);

  // ElectrodeTop
  G4double zPlane_electrodeTop[]= {-28*mm,22.4*mm,111.4*mm};
  G4double rInner_electrodeTop[] = {26.798*mm,26.798*mm,82.928*mm};
  G4double rOuter_electrodeTop[] = {27.592*mm,27.592*mm,83.722*mm};

  G4RotationMatrix* RotMat_electrode = new G4RotationMatrix();
  RotMat_electrode->rotateZ(22.5*deg);

  ElectrodeTop_solid	= new G4Polyhedra(
				"ElectrodeTopPGON",
				0*deg,
				360*deg,
				8,
				3,
				zPlane_electrodeTop,
				rInner_electrodeTop,
				rOuter_electrodeTop);
  ElectrodeTop_logic	= new G4LogicalVolume(
				ElectrodeTop_solid,
				ElectrodeMaterial_Ti,
				"ElectrodeTopPGON");
  ElectrodeTop_physical	= new G4PVPlacement(
				RotMat_electrode,
				G4ThreeVector(0,0,0),
				ElectrodeTop_logic,
				"ElectrodeTopPGON",
				World_logic,
				false,
				0);
  ElectrodeTop_logic->SetVisAttributes(ElectrodePGON_vis);

  // ElectrodeBot
  G4double zPlane_electrodeBot[]= {-84*mm,-24*mm};
  G4double rInner_electrodeBot[] = {49.974*mm,30.54*mm};
  G4double rOuter_electrodeBot[] = {50.768*mm,31.334*mm};

  ElectrodeBot_solid	= new G4Polyhedra(
				"ElectrodeBotPGON",
				0*deg,
				360*deg,
				8,
				2,
				zPlane_electrodeBot,
				rInner_electrodeBot,
				rOuter_electrodeBot);
  ElectrodeBot_logic	= new G4LogicalVolume(
				ElectrodeBot_solid,
				ElectrodeMaterial_Ti,
				"ElectrodeBotPGON");
  ElectrodeBot_physical	= new G4PVPlacement(
				RotMat_electrode,
				G4ThreeVector(0,0,0),
				ElectrodeBot_logic,
				"ElectrodeBotPGON",
				World_logic,
				false,
				0);
  ElectrodeBot_logic->SetVisAttributes(ElectrodePGON_vis);

  // ElectrodeLA_Center
  ElectrodeLA_Center_Outer_solid = new G4Box(
					"ElectrodeLA_Center_Outer",
					52.269*mm,
					142*mm,
					50.794*mm);
  ElectrodeLA_Center_Inner_solid = new G4Box(
					"ElectrodeLA_Center_Inner",
					51.475*mm,
					143*mm,
					50*mm);

  // ElectrodeLA_Center_Hole
  G4double zPlane_electrodeLA_Center_Hole[]= {40*mm,60*mm};
  G4double rInner_electrodeLA_Center_Hole[] = {0*mm,0*mm};
  G4double rOuter_electrodeLA_Center_Hole[] = {51*mm,51*mm};

  ElectrodeLA_Center_Hole_solid	= new G4Polyhedra("ElectrodeLA_Center_Hole",
					0*deg,
					360*deg,
					8,
					2,
					zPlane_electrodeLA_Center_Hole,
					rInner_electrodeLA_Center_Hole,
					rOuter_electrodeLA_Center_Hole);
  
  // ElectrodeLA_Access
  ElectrodeLA_Access_Outer_solid = new G4Box("ElectrodeLA_Access_Outer",
					132.5*mm,
					52.694*mm,
					15.794*mm);
  ElectrodeLA_Access_Inner_solid = new G4Box("ElectrodeLA_Access_Inner",
					133.5*mm,
					51.9*mm,
					15*mm);

  // ElectrodeLA_Bot
  ElectrodeLA_Bot_Outer_solid	= new G4Trd("ElectrodeLA_Bot_Outer",
					62.413*mm,
					52.269*mm,
					55.794*mm,
					55.794*mm,
					50*mm);
  ElectrodeLA_Bot_Inner_solid	= new G4Trd("ElectrodeLA_Bot_Inner",
					61.619*mm,
					51.475*mm,
					55*mm,
					55*mm,
					50.01*mm);

  // ElectrodeLA
  ElectrodeLA_0_solid 		= new G4UnionSolid(
					"ElectrodeLA_0",
					ElectrodeLA_Center_Outer_solid,
					ElectrodeLA_Access_Outer_solid);
  ElectrodeLA_1_solid 		= new G4UnionSolid(
					"ElectrodeLA_1",
					ElectrodeLA_0_solid,
					ElectrodeLA_Bot_Outer_solid,
					0,
					G4ThreeVector(0,0,-99.99*mm));
  ElectrodeLA_2_solid 		= new G4SubtractionSolid(
					"ElectrodeLA_2", 
					ElectrodeLA_1_solid,
					ElectrodeLA_Center_Inner_solid);
  ElectrodeLA_3_solid 		= new G4SubtractionSolid(
					"ElectrodeLA_3", 
					ElectrodeLA_2_solid,
					ElectrodeLA_Access_Inner_solid);
  ElectrodeLA_4_solid 		= new G4SubtractionSolid(
					"ElectrodeLA_4", 
					ElectrodeLA_3_solid,
					ElectrodeLA_Center_Hole_solid,
					RotMat_electrode,
					G4ThreeVector(0,0,0));
  ElectrodeLA_5_solid 		= new G4SubtractionSolid(
					"ElectrodeLA_5", 
					ElectrodeLA_4_solid,
					ElectrodeLA_Bot_Inner_solid,
					0,
					G4ThreeVector(0,0,-99.99*mm));
  ElectrodeLA_logic 		= new G4LogicalVolume(
					ElectrodeLA_5_solid,
					ElectrodeMaterial_Ti,
					"ElectrodeLA_5");
  ElectrodeLA_physical 		= new G4PVPlacement(
					0,
					G4ThreeVector(0,0,-134*mm),
					ElectrodeLA_logic,
					"ElectrodeLA_5",
					World_logic,
					false,
					0);
  ElectrodeLA_logic->SetVisAttributes(ElectrodePGON_vis);

  // HVelectrodeTop
  G4double zPlane_HVelectrodeTop[]= {-100*mm, -2*mm, -2*mm, 25.9772840203611*mm, 39.0705921603611*mm, 47.0117949903611*mm, 52.3785176703611*mm, 56.2757310003611*mm, 59.2561458103611*mm, 63.5728120803611*mm, 65.2110786503611*mm, 66.6202368903611*mm, 68.9531197703611*mm, 69.9441652103611*mm, 71.6841413603611*mm, 73.1919586303611*mm, 73.8821520703611*mm, 74.5384357853611*mm, 75.7670598993611*mm, 76.9039435143611*mm, 77.9634153963611*mm, 78.4662539263611*mm, 78.9514840203611*mm, 79.4189547213611*mm, 79.8682706823611*mm, 80.2988468233611*mm, 80.7099604863611*mm, 81.1008012133611*mm, 81.4705178383611*mm, 81.8182622853611*mm, 82.1432292763611*mm, 82.4446911193611*mm, 82.7220267503611*mm, 82.9747443843611*mm, 83.2024973403611*mm, 83.4050928383611*mm, 83.5824939203611*mm, 83.7348148413611*mm, 83.8623105973611*mm, 83.9653613893611*mm, 84.0444529733611*mm, 84.1001539063611*mm, 84.1330906573611*mm, 84.1439215203611*mm};
  G4double rInner_HVelectrodeTop[] = {0, 0, 85.72527262*mm, 85.72572014*mm, 85.72654641*mm, 85.72795218*mm, 85.73019698*mm, 85.7336*mm, 85.73854111*mm, 85.75486607*mm, 85.76732093*mm, 85.78345668*mm, 85.82960508*mm, 85.86118698*mm, 85.94571512*mm, 86.06509639*mm, 86.14038221*mm, 86.22745567*mm, 86.44113953*mm, 86.714226*mm, 87.05391649*mm, 87.25058201*mm, 87.46585624*mm, 87.70009141*mm, 87.95346422*mm, 88.22596323*mm, 88.51738083*mm, 88.82731046*mm, 89.15514966*mm, 89.50010906*mm, 89.86122734*mm, 90.23739167*mm, 90.62736296*mm, 91.02980499*mm, 91.4433162*mm, 91.86646291*mm, 92.29781273*mm, 92.73596672*mm, 93.17958948*mm, 93.62743605*mm, 94.07837497*mm, 94.53140721*mm, 94.98568061*mm, 95.4405*mm};
  G4double rOuter_HVelectrodeTop[] = {122.5261979*mm, 122.5261979*mm, 122.5261979*mm, 121.3836561*mm, 119.9977384*mm, 119.2401638*mm, 117.6457787*mm, 116.8266589*mm, 116.0031089*mm, 115.1814826*mm, 114.3671079*mm, 113.5643328*mm, 112.7765959*mm, 112.0065123*mm, 111.2559662*mm, 110.5262059*mm, 109.8179351*mm, 109.131399*mm, 108.4664627*mm, 107.8226814*mm, 107.1993619*mm, 106.595616*mm, 106.0104057*mm, 105.4425825*mm, 104.8909191*mm, 104.3541373*mm, 103.8309303*mm, 103.3199816*mm, 102.8199818*mm, 102.3296414*mm, 101.8477036*mm, 101.3729545*mm, 100.9042333*mm, 100.4404417*mm, 99.52362102*mm, 99.06879036*mm, 98.61530504*mm, 98.16251867*mm, 97.7099042*mm, 97.25706364*mm, 96.80373746*mm, 96.34981325*mm, 95.89533319*mm, 95.4405*mm};

  HVelectrodeTop_solid	= new G4Polycone(
				"HVelectrodeTop",
				0*deg,
				360*deg,
				44,
				zPlane_HVelectrodeTop,
				rInner_HVelectrodeTop,
				rOuter_HVelectrodeTop);
  HVelectrodeTop_logic	= new G4LogicalVolume(
				HVelectrodeTop_solid,
				ElectrodeMaterial_Ti,
				"HVelectrodeTop");
  HVelectrodeTop_physical	= new G4PVPlacement(
				0,
				G4ThreeVector(0,0,LDZpos),
				HVelectrodeTop_logic,
				"HVelectrodeTop",
				World_logic,
				false,
				0);
  HVelectrodeTop_logic->SetVisAttributes(ElectrodePGON_vis);

  // HVelectrodeBot
  G4double zPlane_HVelectrodeBot[]= {100*mm, 2*mm, 2*mm, -25.9772840203611*mm, -39.0705921603611*mm, -47.0117949903611*mm, -52.3785176703611*mm, -56.2757310003611*mm, -59.2561458103611*mm, -63.5728120803611*mm, -65.2110786503611*mm, -66.6202368903611*mm, -68.9531197703611*mm, -69.9441652103611*mm, -71.6841413603611*mm, -73.1919586303611*mm, -73.8821520703611*mm, -74.5384357853611*mm, -75.7670598993611*mm, -76.9039435143611*mm, -77.9634153963611*mm, -78.4662539263611*mm, -78.9514840203611*mm, -79.4189547213611*mm, -79.8682706823611*mm, -80.2988468233611*mm, -80.7099604863611*mm, -81.1008012133611*mm, -81.4705178383611*mm, -81.8182622853611*mm, -82.1432292763611*mm, -82.4446911193611*mm, -82.7220267503611*mm, -82.9747443843611*mm, -83.2024973403611*mm, -83.4050928383611*mm, -83.5824939203611*mm, -83.7348148413611*mm, -83.8623105973611*mm, -83.9653613893611*mm, -84.0444529733611*mm, -84.1001539063611*mm, -84.1330906573611*mm, -84.1439215203611*mm};
  G4double rInner_HVelectrodeBot[] = {0, 0, 85.72527262*mm, 85.72572014*mm, 85.72654641*mm, 85.72795218*mm, 85.73019698*mm, 85.7336*mm, 85.73854111*mm, 85.75486607*mm, 85.76732093*mm, 85.78345668*mm, 85.82960508*mm, 85.86118698*mm, 85.94571512*mm, 86.06509639*mm, 86.14038221*mm, 86.22745567*mm, 86.44113953*mm, 86.714226*mm, 87.05391649*mm, 87.25058201*mm, 87.46585624*mm, 87.70009141*mm, 87.95346422*mm, 88.22596323*mm, 88.51738083*mm, 88.82731046*mm, 89.15514966*mm, 89.50010906*mm, 89.86122734*mm, 90.23739167*mm, 90.62736296*mm, 91.02980499*mm, 91.4433162*mm, 91.86646291*mm, 92.29781273*mm, 92.73596672*mm, 93.17958948*mm, 93.62743605*mm, 94.07837497*mm, 94.53140721*mm, 94.98568061*mm, 95.4405*mm};
  G4double rOuter_HVelectrodeBot[] = {122.5261979*mm, 122.5261979*mm, 122.5261979*mm, 121.3836561*mm, 119.9977384*mm, 119.2401638*mm, 117.6457787*mm, 116.8266589*mm, 116.0031089*mm, 115.1814826*mm, 114.3671079*mm, 113.5643328*mm, 112.7765959*mm, 112.0065123*mm, 111.2559662*mm, 110.5262059*mm, 109.8179351*mm, 109.131399*mm, 108.4664627*mm, 107.8226814*mm, 107.1993619*mm, 106.595616*mm, 106.0104057*mm, 105.4425825*mm, 104.8909191*mm, 104.3541373*mm, 103.8309303*mm, 103.3199816*mm, 102.8199818*mm, 102.3296414*mm, 101.8477036*mm, 101.3729545*mm, 100.9042333*mm, 100.4404417*mm, 99.52362102*mm, 99.06879036*mm, 98.61530504*mm, 98.16251867*mm, 97.7099042*mm, 97.25706364*mm, 96.80373746*mm, 96.34981325*mm, 95.89533319*mm, 95.4405*mm};

  HVelectrodeBot_solid	= new G4Polycone(
				"HVelectrodeBot",
				0*deg,
				360*deg,
				42,
				zPlane_HVelectrodeBot,
				rInner_HVelectrodeBot,
				rOuter_HVelectrodeBot);
  HVelectrodeBot_logic	= new G4LogicalVolume(
				HVelectrodeBot_solid,
				ElectrodeMaterial_Ti,
				"HVelectrodeBot");
  HVelectrodeBot_physical	= new G4PVPlacement(
				0,
				G4ThreeVector(0,0,UDZpos),
				HVelectrodeBot_logic,
				"HVelectrodeBot",
				World_logic,
				false,
				0);
  HVelectrodeBot_logic->SetVisAttributes(ElectrodePGON_vis);

  }// if(PGONelectrodes)


  if(GridFlag)
    { //builds Al wire grids on the detector faces if GridFlag is true
      //seperate loops for each detector consistent with seperate variables such as radius for both detectors
      G4int NWiresUD = (G4int)(2*UDRadius/GridWireSpacing); //number of wires
      G4int NWiresLD = (G4int)(2*LDRadius/GridWireSpacing);

      for(G4int i=0; i<NWiresUD; i++)
	{ //loop to place the wirdes on the upper detector
	  G4int j = i - NWiresUD/2;
	  G4double UDWireLength = sqrt(UDRadius*UDRadius - GridWireSpacing*GridWireSpacing*j*j); //length of wires 
	  
	  GridZPosUD = UDZfront - UDDLThickness - GridWireWidth/2;

	  //solid, logic and placement for X Grid
	  UDXGrid_solid = new G4Box("UDXGrid",
				    UDWireLength,
				    GridWireWidth/2, 
				    GridWireWidth/2);
	  
	  UDXGrid_logic = new G4LogicalVolume(UDXGrid_solid,
					      GridMaterial,
					      "UDXGrid");
	  
	  UDXGrid_physical = new G4PVPlacement(0,
					       G4ThreeVector(0.,GridWireSpacing*j, 
							     GridZPosUD),
					       UDXGrid_logic,
					       "UDXGrid",
					       World_logic,
					       false,
					       i);


	  //solid, logic and palcement for y grid
	  UDYGrid_solid = new G4Box("UDYGrid",
				    GridWireWidth/2,
				    UDWireLength,				     
				    GridWireWidth/2);
	  
	  UDYGrid_logic = new G4LogicalVolume(UDYGrid_solid,
					      GridMaterial,
					      "UDYGrid");
	  
	  UDYGrid_physical = new G4PVPlacement(0,
					       G4ThreeVector(GridWireSpacing*j,0,
							     GridZPosUD),
					       UDYGrid_logic,
					       "UDYGrid",
					       World_logic,
					       false,
					       i);					       					  
	}
      
      for(G4int i=0; i<NWiresLD; i++)
	{//identical loop for lower detector
	  G4int j = i -NWiresLD/2;
	  G4double LDWireLength = sqrt(LDRadius*LDRadius - GridWireSpacing*GridWireSpacing*j*j);
	  GridZPosLD = LDZfront + LDDLThickness + GridWireWidth/2;

	  LDXGrid_solid = new G4Box("LDXGrid",
				    LDWireLength,
				    GridWireWidth/2, 
				    GridWireWidth/2);
	  
	  LDXGrid_logic = new G4LogicalVolume(LDXGrid_solid,
					      GridMaterial,
					      "LDXGrid");
	  
	  LDXGrid_physical = new G4PVPlacement(0,
					       G4ThreeVector(0.,GridWireSpacing*j,
							     GridZPosLD),
					       LDXGrid_logic,
					       "LDXGrid",
					       World_logic,
					       false,
					       i);


	  LDYGrid_solid = new G4Box("LDYGrid",
				    GridWireWidth/2,
				    LDWireLength,				     
				    GridWireWidth/2);
	  
	  LDYGrid_logic = new G4LogicalVolume(LDYGrid_solid,
					      GridMaterial,
					      "LDYGrid");
	  
	  LDYGrid_physical = new G4PVPlacement(0,
					       G4ThreeVector(GridWireSpacing*j,0,
							     GridZPosLD),
					       LDYGrid_logic,
					       "LDYGrid",
					       World_logic,
					       false,
					       i);					       					  
	}

    }

  if (Ce139Flag) {


    CarbonFoil_solid    = new G4Tubs("CarbonFoil",
				     0.*cm,
				     2.*cm,
				     0.0095*mm,
				     0.*deg,
				     360.*deg);    
    
    CarbonFoil_logic    = new G4LogicalVolume(CarbonFoil_solid,
					      CarbonFoilMaterial,
					      "CarbonFoil");    


    G4VisAttributes* CarbonFoil_vis = new G4VisAttributes;
    CarbonFoil_vis->SetColor(G4Color::Red());
    CarbonFoil_logic->SetVisAttributes(CarbonFoil_vis);
    //G4cout << "DetectorConstruction LDZfront: " << LDZfront << G4endl;
    CarbonFoil_physical = new G4PVPlacement(0,
					    G4ThreeVector(0.,0.,LDZfront+117.4),
					    CarbonFoil_logic,
					    "CarbonFoil",
					    World_logic,
					    false,
					    0);
	
    FoilStepLimit = new G4UserLimits(1*um);
    CarbonFoil_logic->SetUserLimits(FoilStepLimit);

  }


  // PrintDetectorDimensions();

  //always return the physical World
  //
  return World_physical;

}

void CBDetectorConstruction::SetLDMaterial(G4String materialChoice)
{
  // search the material by its name
  G4Material* pttoMaterial =
    G4NistManager::Instance()->FindOrBuildMaterial(materialChoice);

  if (pttoMaterial && LDMaterial != pttoMaterial) {
    LDMaterial = pttoMaterial;                  
    if(LD_logic) LD_logic->SetMaterial(LDMaterial);
    G4RunManager::GetRunManager()->PhysicsHasBeenModified();
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void CBDetectorConstruction::SetUDMaterial(G4String materialChoice)
{
  // search the material by its name
  G4Material* pttoMaterial =
    G4NistManager::Instance()->FindOrBuildMaterial(materialChoice);

  if (pttoMaterial && UDMaterial != pttoMaterial) {
    UDMaterial = pttoMaterial;                  
    if(UD_logic) UD_logic->SetMaterial(UDMaterial);
    G4RunManager::GetRunManager()->PhysicsHasBeenModified();
  }
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void CBDetectorConstruction::SetWorldMaterial(G4String materialChoice)
{
  // search the material by its name
  G4Material* pttoMaterial =
    G4NistManager::Instance()->FindOrBuildMaterial(materialChoice);

  if (pttoMaterial && WorldMaterial != pttoMaterial) {
    WorldMaterial = pttoMaterial;
    if(World_logic) 
      {
	World_logic->SetMaterial(WorldMaterial);
      }
    G4RunManager::GetRunManager()->PhysicsHasBeenModified();
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void CBDetectorConstruction::SetElectrodeMaterial_Ti(G4String materialChoice)
{
  // search the material by its name
  G4Material* pttoMaterial =
    G4NistManager::Instance()->FindOrBuildMaterial(materialChoice);

  if (pttoMaterial && ElectrodeMaterial_Ti != pttoMaterial) {
    ElectrodeMaterial_Ti = pttoMaterial;
    if(World_logic) 
      {
	ElectrodeTop_logic->SetMaterial(ElectrodeMaterial_Ti);
        ElectrodeBot_logic->SetMaterial(ElectrodeMaterial_Ti);
	ElectrodeLA_logic->SetMaterial(ElectrodeMaterial_Ti);
      }
    G4RunManager::GetRunManager()->PhysicsHasBeenModified();
  }
}
 
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void CBDetectorConstruction::SetIFMaterial(G4String materialChoice)
{
  // search the material by its name
  G4Material* pttoMaterial =
    G4NistManager::Instance()->FindOrBuildMaterial(materialChoice);

  if (pttoMaterial && IFMaterial != pttoMaterial) {
    IFMaterial = pttoMaterial;                  
    if(IF_logic) IF_logic->SetMaterial(IFMaterial);
    G4RunManager::GetRunManager()->PhysicsHasBeenModified();
  }
}

   
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void CBDetectorConstruction::SetCarbonFoilMaterial(G4String materialChoice)
{
  // search the material by its name
  G4Material* pttoMaterial =
    G4NistManager::Instance()->FindOrBuildMaterial(materialChoice);

  if (pttoMaterial && CarbonFoilMaterial != pttoMaterial) {
    CarbonFoilMaterial = pttoMaterial;                  
    //if(CarbonFoil_logic) CarbonFoil_logic->SetMaterial(CarbonFoilMaterial);
    G4RunManager::GetRunManager()->PhysicsHasBeenModified();
  }
}

   
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void CBDetectorConstruction::PrintDetectorDimensions()
{//Prints out dimensions of the detector
  G4cout.precision(10);
  /*

  G4cout << "Nab Detector:" << G4endl;
  G4cout << " Zpos      5m     4.9m		        0              -1m \n"
	 << "           ||	|					|| \n"
	 << "           ||	|			@		|| \n"
	 << "           ||	|					|| \n"
	 << "\n"	
	 << "           UD	IF		     Origin             LD \n" 
	 << "               (optional)           (decay center)       \n"
         << G4endl;
  */

  G4cout << "Upper Detector: \n"
         << "Between: " << G4BestUnit(UDZfront,"Length") << " and " 
	 << G4BestUnit(UDZback,"Length") << G4endl;

  G4cout << "Lower Detector: \n"
         << "Between: " << G4BestUnit(LDZfront,"Length") << " and " 
	 << G4BestUnit(LDZback,"Length") << G4endl;

  if(IFFlag)
    {
      G4cout << "Ion Foil: \n"
	     << "Between: " << G4BestUnit(IFZfront,"Length") << " and " 
	     << G4BestUnit(IFZback,"Length") << G4endl;
    }



}


void CBDetectorConstruction::SetupFields()
{ //this function sets up the EM-field and attaches it to the field manager
  
  //create Field (this will call FERENC() and create intermediate files etc.)
  NabField*                emField       = new NabField((char*)FieldFileSuffix.data(), UseFieldMapFlag,GlobalFieldMapFlagDetector);
  if(UseFieldMapFlag) emField->SetUseInterpolate(true);
  
  //Get the field manager from transportation manager
  G4TransportationManager* transportMan  = G4TransportationManager::GetTransportationManager();
  G4FieldManager*          emFieldMan    = transportMan->GetFieldManager();

  //The following parameters govern the accuracy of the integration of the equation of motion if set to large this wil cause violation of conservation of energy.
  G4double                 epsMin;
  G4double                 epsMax;
  G4double                 deltaonestep;
  G4double                 deltaintersection;
  if (oldStepper) {
    epsMin        = 1.e-11;
    epsMax        = 1.e-9;
    deltaonestep         = 0.00001*nm;
    deltaintersection    = 0.00001*nm;
  }
  else {
    epsMin        = 1.e-6; //1.e-6 default values as of 01/22/2016
    epsMax        = 1.e-5; //1.e-5 default values as of 01/22/2016
    //epsMin        = 1.e-8; //Changing values for tests
    //epsMax        = 1.e-7; //Changing values for tests
    deltaonestep        = 10.*nm; 
    deltaintersection   = 10.*nm; 
  }

  emFieldMan->SetMinimumEpsilonStep(epsMin);
  emFieldMan->SetMaximumEpsilonStep(epsMax);
  emFieldMan->SetDeltaOneStep(deltaonestep);
  emFieldMan->SetDeltaIntersection(deltaintersection);

  //define an equation of motion for the field
  G4EqMagElectricField*    emEquation    = new G4EqMagElectricField(emField);
  
  //number of variables (position[3], momentum[3], time, energy)
  G4int                    nvar          = 8;
  //minimal integration step smaller values yield more precision at expense of runtime
  G4double hmin;
  if (oldStepper) hmin          = 0.000001*nm;
  else            hmin          = 1.*nm;
  
  //define an integration stepper for the equation
  //currently using Cash-Karp 4/5th-order Runge-Kutta-Felhberg method 
  G4MagIntegratorStepper*  emStepper;
  if (oldStepper) emStepper     = new G4CashKarpRKF45(emEquation, nvar);
  else            emStepper     = new G4SimpleHeum(emEquation, nvar); //3rd order
  //else            emStepper     = new G4CashKarpRKF45(emEquation, nvar); //4/5th order
  //else            emStepper     = new G4ClassicalRK4(emEquation, nvar); //4th order classic
  //else            emStepper     = new G4SimpleRunge(emEquation, nvar); //2nd order

  //G4MagIntegratorStepper*  emStepper     = new G4HelixMixedStepper(emEquation, 8);

  //define an integration driver for the stepper
  G4MagInt_Driver*         emDriver      = new G4MagInt_Driver(hmin, emStepper, nvar, 1);

  //define chord finder for the integration driver
  //chord finder approximates the real trajectory using straight line segments
  emChordFinder = new G4ChordFinder(emDriver);
  
  //attach the field and its associates to the field manager
  //the field manager will call the GetFieldValue() function for every point under question
  if (globalField) emFieldMan->SetChordFinder(emChordFinder);
  

  //set the largest step the stepping action will take in a field typical steps will be far smaller in volumes
  //it should be on the order of the detector
  //too high values lead to lost particles (why?)
  //too low values lead to forcing unaturally many steps in vacuum -> waste of time
  
  //UPDATE: particles are lost due to deletion by PropagatorInField!
  //Propagator in Field marks particles as looping and deletes them
  //deletion is prevent by setting ahigh value for MaxLoopCount
  //unclear what value is required ore recommended
  //current value 1e6 works
  //further investigation necessary
  //no restriction on LargestAcceptable step anymore
  //use value on order of detector -> 6.2m seperation distance between detectors
  transportMan->GetPropagatorInField()->SetLargestAcceptableStep(1*mm);
  transportMan->GetPropagatorInField()->SetMaxLoopCount((int)1e5);

  if(globalField)emFieldMan->SetDetectorField(emField); //no more global field!!!

  else{
  UEFRegionFieldManager = new G4FieldManager(emField);
  
  UEFRegionFieldManager->SetMinimumEpsilonStep(epsMin);
  UEFRegionFieldManager->SetMaximumEpsilonStep(epsMax);
  UEFRegionFieldManager->SetDeltaOneStep(deltaonestep);
  
  UEFRegionFieldManager->SetChordFinder(emChordFinder);



  LEFRegionFieldManager = new G4FieldManager(emField);
  
  LEFRegionFieldManager->SetMinimumEpsilonStep(epsMin);
  LEFRegionFieldManager->SetMaximumEpsilonStep(epsMax);
  LEFRegionFieldManager->SetDeltaOneStep(deltaonestep);
  
  LEFRegionFieldManager->SetChordFinder(emChordFinder);

  

  UDRegionFieldManager = new G4FieldManager(emField);
  
  UDRegionFieldManager->SetMinimumEpsilonStep(epsMin);
  UDRegionFieldManager->SetMaximumEpsilonStep(epsMax);
  UDRegionFieldManager->SetDeltaOneStep(deltaonestep);
  
  UDRegionFieldManager->SetChordFinder(emChordFinder);



  LDRegionFieldManager = new G4FieldManager(emField);
  
  LDRegionFieldManager->SetMinimumEpsilonStep(epsMin);
  LDRegionFieldManager->SetMaximumEpsilonStep(epsMax);
  LDRegionFieldManager->SetDeltaOneStep(deltaonestep);
  
  LDRegionFieldManager->SetChordFinder(emChordFinder);
  }

  //for debugging/producing a Field profile purposes only
  //creates field profile during run/initialize in FieldProfile.dat
  //leave commented out otherwise
/*   
    std::ofstream fieldProfile("FilterFieldMap_elliptic_20160706");
	G4int imax=5001;
	G4int jmax=1501;
	fieldProfile << imax << G4endl;
	fieldProfile << jmax << G4endl;
    for(G4int i=0; i<imax; i++){
      for(G4int j=0; j<jmax; j++)
	{
	  G4double Point[4] = {((G4double)j/100.0)*cm, 0*cm , -0.15*m+((G4double)i/(imax-1))*0.5*m, 0};
	  G4double FieldVal[6];
	  emField->SetUseInterpolate(false);
	  emField->SetUseGlobalFieldMap(false);
	  emField->GetFieldValue(Point, FieldVal);

	    //Print Ferenc (reference)
	  fieldProfile << FieldVal[2]/tesla << "\t"
		       << sqrt(pow(FieldVal[1],2)+pow(FieldVal[0],2))/tesla << "\t"
		       << G4endl;
	}
    }
  
  
    fieldProfile.close(); 
*/
  bool debug=false;
  if (debug){
    
    std::ofstream fieldProfile("FieldProfile_ferenc_2D_1D_EM.dat");
    for(G4int i=0; i<1000; i++){
      for(G4int j=0; j<150; j++)
	{
	  G4double Point[4] = {((G4double)j/10.0)*cm, 0*cm , -1.2*m+((G4double)i/1000.0)*6.2*m, 0};
	  G4double FieldVal[6];
	  G4double FieldValInterpolated[6];
	  G4double FieldValGlobal[6];
	  emField->SetUseInterpolate(false);
	  emField->SetUseGlobalFieldMap(false);
	  emField->GetFieldValue(Point, FieldVal);
	  emField->SetUseInterpolate(true);
	  emField->GetFieldValue(Point, FieldValInterpolated);
	  emField->SetUseGlobalFieldMap(true);
	  emField->GetFieldValue(Point, FieldValGlobal);
	
	  //Print current point
	  fieldProfile << Point[2] << "\t" 
		       << Point[0] << "\t"

	    //Print Ferenc (reference)
		       << FieldVal[2]/tesla << "\t"
		       << sqrt(pow(FieldVal[1],2)+pow(FieldVal[0],2))/tesla << "\t"
		       << FieldVal[5]/(volt/m) << "\t" 
		       << sqrt(pow(FieldVal[4],2)+pow(FieldVal[3],2))/(volt/m) << "\t"

	    //Print interp
		       << FieldValInterpolated[2]/tesla << "\t"
		       << sqrt(pow(FieldValInterpolated[1],2)+pow(FieldValInterpolated[0],2))/tesla << "\t"
		       << FieldValInterpolated[5]/(volt/m) << "\t" 
		       << sqrt(pow(FieldValInterpolated[4],2)+pow(FieldValInterpolated[3],2))/(volt/m) << "\t"

	    //Print global
		       << FieldValGlobal[2]/tesla << "\t"
		       << sqrt(pow(FieldValGlobal[1],2)+pow(FieldValGlobal[0],2))/tesla << "\t"
		       << FieldValGlobal[5]/(volt/m) << "\t" 
		       << sqrt(pow(FieldValGlobal[4],2)+pow(FieldValGlobal[3],2))/(volt/m) << "\t"

	    //Print Diff between interp and Ferenc
		       << FieldVal[2]/tesla-(FieldValInterpolated[2]/tesla) << "\t"
		       << (sqrt(pow(FieldVal[1],2)+pow(FieldVal[0],2))-sqrt(pow(FieldValInterpolated[1],2)+pow(FieldValInterpolated[0],2)))/tesla << "\t"
		       << FieldVal[5]/(volt/m)-(FieldValInterpolated[5]/(volt/m)) << "\t"
		       << (sqrt(pow(FieldVal[4],2)+pow(FieldVal[3],2))-sqrt(pow(FieldValInterpolated[4],2)+pow(FieldValInterpolated[3],2)))/(volt/m) << "\t"

	    //Print diff between global and Ferenc
		       << FieldVal[2]/tesla-(FieldValGlobal[2]/tesla) << "\t"
		       << (sqrt(pow(FieldVal[1],2)+pow(FieldVal[0],2))-sqrt(pow(FieldValGlobal[1],2)+pow(FieldValGlobal[0],2)))/tesla << "\t"
		       << FieldVal[5]/(volt/m)-(FieldValGlobal[5]/(volt/m)) << "\t"
		       << (sqrt(pow(FieldVal[4],2)+pow(FieldVal[3],2))-sqrt(pow(FieldValGlobal[4],2)+pow(FieldValGlobal[3],2)))/(volt/m) << "\t"

		       << G4endl;
	}
    }
  
  
    fieldProfile.close();
  }  


  //******************************************
  //Drift Region Field

  if(DriftRegionFlag&&!globalField)
    {

      //Drift Region Field is designed to optimize run time in region with little or no electric field by using appropriate magnetic field steppers etc
      //use alternate constructor her and only here to bypass running of FERENC and associated initialization routines
      
      //NabBField*  BField = new NabBField((char*)FieldFileSuffix.data());
      
      //G4Mag_UsualEqRhs* BEquation = new G4Mag_UsualEqRhs(BField);
      
      //G4MagIntegratorStepper*  BStepper = new G4HelixMixedStepper(BEquation, 8);
      
      //G4MagInt_Driver* BDriver = new G4MagInt_Driver(hmin, BStepper, nvar, 1);
      
      //G4ChordFinder* BChordFinder = new G4ChordFinder(BDriver);
      
      
      DriftRegionFieldManager = new G4FieldManager(emField);

      DriftRegionFieldManager->SetMinimumEpsilonStep(epsMin);
      DriftRegionFieldManager->SetMaximumEpsilonStep(epsMax);
      DriftRegionFieldManager->SetDeltaOneStep(deltaonestep);

      DriftRegionFieldManager->SetChordFinder(emChordFinder);
      

    }
  
  
  
  

}

void CBDetectorConstruction::DefineMaterials()
{ //defines materials used in the detector
  //consider replacing with explicit calls to NIST database

  G4String name, symbol;             //a=mass of a mole;
  G4double a, z, density;      //z=mean number of protons;  

  G4int ncomponents;
  G4double fractionmass;
  G4double temperature, pressure;

  // Titanium
  G4Element* TiElectrode = new G4Element(name="Titanium", symbol="Ti", z=22, 47.87*g/mole);

  // Aluminum
  G4Element* AlElectrode = new G4Element(name="Aluminum", symbol="Al", z=13, 26.98*g/mole);

  // Vanadium
  G4Element* VElectrode = new G4Element(name="Vanadium", symbol="V", z=23, 50.94*g/mole);

  // Titanium Grade V
  G4Material* TiGradeV = new G4Material(name="TiGradeV", 4.374*g/cm3, ncomponents=3);
  TiGradeV->AddElement(TiElectrode, fractionmass=90*perCent);
  TiGradeV->AddElement(AlElectrode, fractionmass=6*perCent);
  TiGradeV->AddElement(VElectrode, fractionmass=4*perCent);
  
  //Silicon
  new G4Material("Silicon"  , z=14, a=28.09*g/mole, density= 2.330*g/cm3);

  //Vacuum
  density     = universe_mean_density;    //from PhysicalConstants.h
  pressure    = 3.e-19*pascal;
  temperature = 2.73*kelvin;
  new G4Material("Galactic", z=1, a=1.01*g/mole,density,
		 kStateGas,temperature,pressure);
  
  new G4Material("Aluminium", z=13, a=26.98*g/mole, density= 2.700*g/cm3);
 

  //fMaterialsManager->AddMaterial("Carbon","C",2.2670*g/cm3,""); 
  new G4Material("Carbon",z=6,a=12*g/mole,density=2.2670*g/cm3);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....




void CBDetectorConstruction::SetResidualGas(G4String newValue)
{
  G4String GasType;
  G4double pressure, temperature, density, z, a;
  G4int ncomponents; 
  std::istringstream iss (newValue);
  iss>>GasType>>pressure>>temperature;
 
   
  G4double R_gas= 8.3144598*joule/(mole*kelvin);
  G4double const Torr  = atmosphere/760.;	

	 
  pressure=pressure*Torr;	 
  temperature=temperature*kelvin;

  if(GasType=="H2")  { 
		      G4Element* elH = new G4Element("Hydrogen", "H", z= 1., a= 1.01*g/mole);
                      density = pressure*a/(R_gas*temperature);
                      G4Material* ResidualGas = new G4Material("ResidualGas",density, ncomponents= 1,kStateGas, temperature, pressure);
                      ResidualGas->AddElement(elH, 2);
                      }

   else if(GasType=="He")  { 
		      G4Element* elHe = new G4Element("Galactic", "He", z= 2., a=4.002602*g/mole);
                      density = pressure*a/(R_gas*temperature);
                      G4Material* ResidualGas = new G4Material("ResidualGas", density, ncomponents= 1, kStateGas, temperature, pressure);
                      ResidualGas->AddElement(elHe, 1);
                      }

  else if(GasType=="Galactic") {
                     density     = universe_mean_density;              
                     pressure    = 3.e-19*pascal;
                     temperature = 2.73*kelvin;
                     G4Material* ResidualGas = new G4Material("ResidualGas", z=1., a=1.01*g/mole, density, kStateGas,temperature,pressure);
                     }
  else {
		     G4cout<<"GasType undefined, will use Galactic"<<G4endl;
                     density     = universe_mean_density;              
                     pressure    = 3.e-19*pascal;
                     temperature = 2.73*kelvin;
                     G4Material* ResidualGas = new G4Material("ResidualGas", z=1., a=1.01*g/mole, density, kStateGas,temperature,pressure);
                     }
                              
                     SetWorldMaterial("ResidualGas");      


  G4cout<<"GasType : "<<GasType<<G4endl<<"atomic mass(g/mole) : "<<a/(g/mole)<<G4endl<<"pressure(Torr) : "<<pressure/Torr<<G4endl<<"temperature(K) : "<<temperature/kelvin<<G4endl<<"density(g/m3) : "<<density/(g/m3)<<G4endl;


}
