///////////////////////////////////////////////////////////////
//Name: NabEMFieldMap.cc
//Description: Field Map for magnetic fields
///////////////////////////////////////////////////////////////

#include "NabEMFieldMap.hh"

#include "globals.hh"

//Tong: new includes are needed
//#include "magfield2.hh"
//#include "elcd2_2.hh"

//Tong: use these includes instead
#include "magfield3.h"  //for magsource() and magfield2()
#include "elcd3.h" //for elcd2()
#include "fieldelcd3.h" //for elfield()

#include <new>
#include <iostream>
#include <fstream>

//Units and Constants
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

NabEMFieldMap::NabEMFieldMap()
{
  FieldMapFileName = "DefaulFieldMap.dat";

  GridR = 50*cm;
  GridDelta = 1*mm;

  //do not use

  //GridZ

}

NabEMFieldMap::NabEMFieldMap(G4String name, G4String FerencFile, G4double z1, G4double z2, G4double rmax, G4double Delta)
{

  //Constructor requires the following information
  // - name
  // - File name for coil definitions (required by ferenc's routines)
  // - Dimensions:
  //             - lower boundary (Z1)
  //             - upper boundary (Z2)
  //             - radius (rmax)
  // - map point spacing (Delta)

  FieldMapFile.precision(12);
  FieldMapFile << std::scientific;
  FieldMapFile >> std::scientific;
  FieldMapFileName = name;
  FerencFilename = FerencFile;
  GridDelta = Delta;
  InverseGridDelta = 1./GridDelta;
  GridZ1 = z1;
  GridZ2 = z2;
  GridR  = rmax;
  NbCoilMax = 20;
  ApproximateBottomFlag = false;
  ApproximateTopFlag    = false;

}

NabEMFieldMap::~NabEMFieldMap()
{
  //destructor
  delete BFieldMapZ;
  delete BFieldMapR;
  delete EFieldMapZ;
  delete EFieldMapR;  
  //delte arrays

}


void NabEMFieldMap::SetFieldMapName(G4String newName)
{
  //added for completeness
  //can be used in conjunction with default constructor
  //not recommended
  FieldMapFileName = newName;
  //FieldMapFile();
}

void NabEMFieldMap::SetMapDimensions(G4double R, G4double Delta, G4double Z1, G4double Z2)
{
  //see above
  GridR = R;
  GridDelta = Delta;
  GridZ1 = Z1;
  GridZ2 = Z2;
}

void NabEMFieldMap::ApproximateTop()
{
  //used to set the approximate top flag (use for region close to upper detector)
  //this prevents the routines from trying to get field values from the detector surface, as these are not well defined
  ApproximateTopFlag = true;
}

void NabEMFieldMap::ApproximateBottom()
{
  //see above
  ApproximateBottomFlag = true;
}




void NabEMFieldMap::LoadFieldMap()
{
  //generates or load the field map

   if (GridR==0) {
    FieldMapFile.open(FieldMapFileName.data(),std::ios_base::in);
    if (!(FieldMapFile.is_open())) {printf("Unable to open %s\n",FieldMapFileName.data()); exit(1);}
  
    //int iMax = 6.199*m*InverseGridDelta; 
    int iMax = 6.201*m*InverseGridDelta;//Changed 2018/01/23 to accommodate new GlobalFieldMap 
    BFieldMapZ = new G4double[18*iMax];
    float dummy;

    G4cout << "Loading global field map" << G4endl;

    for (int i=0;i<iMax;i++) {
      FieldMapFile >> dummy; 
      //G4cout << "z = " << dummy <<G4endl;
      for (int j=0;j<18;j++) {
	FieldMapFile >> BFieldMapZ[i*18+j];
	//G4cout <<"B"<< BFieldMapZ[i*14+j] << " "<<G4endl;
      }
       //G4cout << G4endl;
    }
    
  }
  else {
  FieldMapFile.open(FieldMapFileName.data(),std::ios_base::out | std::ios_base::in);
  if(FieldMapFile.is_open()) //if a fieldmap of the name FieldMapName exists it will be loaded from the file
    {
      G4cout << "Using existing field map file: " << FieldMapFileName << ". To force generation of fieldmap rename or delete existing field mape file" << G4endl;
      //read in number of points from file
      FieldMapFile >> ZMax;
      FieldMapFile >> RMax;
      
      //generate arrays
      //1 D arrays are used for simplicity
      BFieldMapZ = new G4double[ZMax*RMax];
      BFieldMapR = new G4double[ZMax*RMax];
      EFieldMapZ = new G4double[ZMax*RMax];
      EFieldMapR = new G4double[ZMax*RMax];

      //populate arrays with values from file
      for(int i=0; i<ZMax; i++)
	{
	  for(int j=0; j<RMax; j++)
	    {
	      FieldMapFile >> BFieldMapZ[i*RMax+j];
	      FieldMapFile >> BFieldMapR[i*RMax+j];
	      FieldMapFile >> EFieldMapZ[i*RMax+j];
	      FieldMapFile >> EFieldMapR[i*RMax+j];
	    }
	}
      
    }
  else
    {
      //if the file of name FieldMapName does not exist run MakeFieldMap() to create it
      FieldMapFile.close();
      G4cout << "field map file: " << FieldMapFileName << " not found. Creating new field map" << G4endl;
      MakeFieldMap();
    }
  }
}

// void NabEMFieldMap::LoadFieldMap()
// {
//   //generates or load the field map

  
//   FieldMapFile.open(FieldMapFileName.data(),std::ios_base::out | std::ios_base::in);
//   if(!FieldMapFile.is_open()) //if a fieldmap of the name FieldMapName exists it will be loaded from the file
//     {
//       //if the file of name FieldMapName does not exist run MakeFieldMap() to create it
//       FieldMapFile.close();
//       G4cout << "field map file: " << FieldMapFileName << " not found. Creating new field map" << G4endl;
//       MakeFieldMap();
//     }
//     {
//       G4cout << "Using existing field map file: " << FieldMapFileName << ". To force generation of fieldmap rename or delete existing field mape file" << G4endl;
//       //read in number of points from file
//       FieldMapFile >> ZMax;
//       FieldMapFile >> RMax;
      
//       //generate arrays
//       //1 D arrays are used for simplicity
//       BFieldMapZ = new G4double[ZMax*RMax];
//       BFieldMapR = new G4double[ZMax*RMax];
//       EFieldMapZ = new G4double[ZMax*RMax];
//       EFieldMapR = new G4double[ZMax*RMax];

//       //populate arrays with values from file
//       for(int i=0; i<ZMax; i++)
// 	{
// 	  for(int j=0; j<RMax; j++)
// 	    {
// 	      FieldMapFile >> BFieldMapZ[i*RMax+j];
// 	      FieldMapFile >> BFieldMapR[i*RMax+j];
// 	      FieldMapFile >> EFieldMapZ[i*RMax+j];
// 	      FieldMapFile >> EFieldMapR[i*RMax+j];
// 	    }
// 	}
      
//     }

// }

void NabEMFieldMap::MakeFieldMap()
{
  //this function generated a field map

  G4double Zpos;
  G4double Rpos;
  G4double potentialDummy; //required by ferences routines
  G4double Bz; //magnetic field z component
  G4double Br; //magneric field r component
  G4double Ez; //electric field z "
  G4double Er; //electric field r "

  RMax = (int)(GridR/GridDelta)+1; //number of R nodes
  ZMax = (int)floor(((GridZ2-GridZ1)/GridDelta)+1); //number of z nodes
  
  //generate arrays
  //1 D arrays are used for simplicity
  BFieldMapZ = new G4double[ZMax*RMax];
  BFieldMapR = new G4double[ZMax*RMax];
  EFieldMapZ = new G4double[ZMax*RMax];
  EFieldMapR = new G4double[ZMax*RMax];

  //open a file to save the new fieldmap
  //fieldmap will not have to be repopulated on next run but read in using loadfieldmap() above
  FieldMapFile.open(FieldMapFileName.data(),std::ios_base::out);
  
  FieldMapFile << ZMax << G4endl; //write number of z and r nodes to file
  FieldMapFile << RMax << G4endl;

  
  for(int i=0; i<ZMax; i++) //loop over Z
    {
      Zpos = GridZ1 + i*GridDelta; 
      for(int j=0; j<RMax; j++) // loop over R
	{
	  Rpos = j*GridDelta; 


	  // ****Magnetic Field

    magfield2(Zpos/m, Rpos/m, (char*)FerencFilename.data(), NbCoilMax, &potentialDummy, &Bz, &Br); //call ferenc's magfield
	  //Note: All of Ferenc's routines use SI units not native Geant4 units
	  //ensure all quantities are properly converted!!!
	  

	  //save magnetic field values to file
	  FieldMapFile << Bz << "\t" << Br << "\t";
   

	  //fill array
	  BFieldMapZ[i*RMax+j] = Bz;
	  BFieldMapR[i*RMax+j] = Br;

	  //******* Electric Field

	  //if electrode borders region of interpolation at bottom
	  if(ApproximateBottomFlag)
	    { //move slightly of axis, move slightly away from electrode
	      if(i == 0)                Zpos += 0.01*mm;
	      if(i == 0 && j == 0)      Rpos += 0.001*mm;
	    }
	  
	  //if electrode borders region of interpolation at bottom
	  if(ApproximateTopFlag)
	    { //move slightly of axis, move slightly away from electrode
	      if(i == ZMax-1)           Zpos -= 0.01*mm;
	      if(i == ZMax-1 && j == 0) Rpos += 0.001*mm;
	    }
	  //the above if statements avoid problems associated with calling ferences routines on axis at the site of the detector or very close to the detector!
	  
	  if(j == RMax-1)           Rpos -= 0.01*mm;
	  //move slightly away from outer electrode (same reasoning as above)

    //Tong:
    G4double aa[3];

    commonfieldelcd2.trajstartfield=0;
    commonfieldelcd2.electrode_encounter_index=0;

	  //call ferenc's electric field routine
    elfield(Zpos/m, Rpos/m, &potentialDummy, &Ez, &Er, &(aa[0]), &(aa[1]), &(aa[2]));

	  //debbuging tool
	  //if(i == ZMax-1 || i == 0) G4cout << Zpos << "\t"<< Rpos << "\t" << Ez << "\t" << Er << G4endl;
	  
	  //save e field values to file
    FieldMapFile << Ez << "\t" << Er << G4endl;
    //FieldMapFile << "\t" << Zpos/m << "\t" << Rpos/m << G4endl;
	  
	  //store e field in array
	  EFieldMapZ[i*RMax+j] = Ez;
	  EFieldMapR[i*RMax+j] = Er;
	  
	  //if (abs(Ez)>1000000) {printf("Ez is big!! Ez: %e, EFieldMapZ[arrayvalue]: %e, Z: %e, R: %e\n",Ez,EFieldMapZ[i*RMax+j],Zpos/m,Rpos/m);}


	} //loop over R
    }//loop of Z


  //close file after all writing is complete
  FieldMapFile.close();

}

void NabEMFieldMap::Interpolate(G4double Zpos, G4double Rpos, G4double* Bz, G4double* Br, G4double* Ez, G4double* Er)
{
  //main function for accessing the fieldmaps
  //interpolates the field through bilinear interpolation on the 4 closest source points.

  //determine quadrant
  G4double zRel = Zpos-GridZ1;
  G4int zIndex = (int)(zRel*InverseGridDelta);
  G4int rIndex = (int)(Rpos*InverseGridDelta);
  
  //If we're using a global, 1D field, we need to extrapolate from source points on axis
  if (GridR==0) {
   
    //Interpolate BFieldMapZ[0-3]
    Rpos=Rpos/m;
    G4double x = (zRel*InverseGridDelta-zIndex);
    G4double oneminusx = 1.-x;
    G4double Bderivs[9];
    G4double Phiderivs[9];
    G4double Rpos2 = Rpos*Rpos;
    G4double Rpos3 = Rpos2*Rpos;
    G4double Rpos4 = Rpos3*Rpos;
    G4double Rpos5 = Rpos4*Rpos;
    G4double Rpos6 = Rpos5*Rpos;
    G4double Rpos7 = Rpos6*Rpos;

    for (int i=0;i<9;i++) {
      Bderivs[i] = BFieldMapZ[zIndex*18+i]*oneminusx
	+ BFieldMapZ[(zIndex+1)*18+i]*x;
      Phiderivs[i] = BFieldMapZ[zIndex*18+9+i]*oneminusx
	+ BFieldMapZ[(zIndex+1)*18+9+i]*x;
    }
      
    //G4cout << "Rpos " << Rpos << G4endl; 
    
    //Use interpolated Bderivs[n] to calculate Bz, Br
    *Bz = Bderivs[0] - 0.25*Rpos2*Bderivs[2] + (1./64.)*Bderivs[4]*Rpos4 - (1./2304.)*Bderivs[6]*Rpos6; //up to 6th order derivative
    //+ (1./147456.)*Bderivs[8]*Rpos*Rpos*Rpos*Rpos*Rpos*Rpos*Rpos*Rpos; //add to go up to 8th order derivative
    
    *Br = - 0.5*Rpos*Bderivs[1] + 0.0625*Bderivs[3]*Rpos3 - (1./384.)*Bderivs[5]*Rpos5; //up to 6th order derivative
    // + (1./18432.)*Bderivs[7]*Rpos*Rpos*Rpos*Rpos*Rpos*Rpos*Rpos; //add to go up to 7th order derivative
    
    //Use interpolated Phiderivs[n] to calculate Ez, Er
    
    *Ez = - Phiderivs[1] + 0.25*Rpos2*Phiderivs[3] - 0.015625*Rpos4*Phiderivs[5] + (1./2304.)*Rpos6*Phiderivs[7];
    *Er = 0.5*Rpos*Phiderivs[2] - 0.0625*Rpos3*Phiderivs[4] + (1./384.)*Rpos5*Phiderivs[6] - (1./18432.)*Rpos7*Phiderivs[8]; 
    //*Ez = - Phiderivs[1] + 0.25*Rpos*Rpos*Phiderivs[3] - 0.015625*Rpos4*Phiderivs[5];
    //*Er = 0.5*Rpos*Phiderivs[2] - 0.0625*Rpos3*Phiderivs[4];
    
  }

  else {

  //static variables which are used to store the last values of the 4 source points
  static G4int lastzIndex = -1;
  static G4int lastrIndex = -1;
  
  static G4double lastZ00BPoint = 0;
  static G4double lastZ10BPoint = 0;
  static G4double lastZ01BPoint = 0;
  static G4double lastZ11BPoint = 0;

  static G4double lastR00BPoint = 0;
  static G4double lastR10BPoint = 0;
  static G4double lastR01BPoint = 0;
  static G4double lastR11BPoint = 0;

  static G4double lastZ00EPoint = 0;
  static G4double lastZ10EPoint = 0;
  static G4double lastZ01EPoint = 0;
  static G4double lastZ11EPoint = 0;

  static G4double lastR00EPoint = 0;
  static G4double lastR10EPoint = 0;
  static G4double lastR01EPoint = 0;
  static G4double lastR11EPoint = 0;
 

  if (zIndex == lastzIndex && rIndex == lastrIndex) 
    {//if the quadrant is the same
      //use previous vertices saved in static variables
      G4double x = (zRel/GridDelta-zIndex);
      G4double y = (Rpos/GridDelta-rIndex);
      
      //bilinear interpolation
      //see spring 2014 paper for details
      *Bz = lastZ00BPoint*(1-x)*(1-y)
	+ lastZ10BPoint*x*(1-y)
	+ lastZ01BPoint*(1-x)*y
	+ lastZ11BPoint*x*y;

      *Br = lastR00BPoint*(1-x)*(1-y)
	+ lastR10BPoint*x*(1-y)
	+ lastR01BPoint*(1-x)*y
	+ lastR11BPoint*x*y;

      *Ez = lastZ00EPoint*(1-x)*(1-y)
	+ lastZ10EPoint*x*(1-y)
	+ lastZ01EPoint*(1-x)*y
	+ lastZ11EPoint*x*y;

      *Er = lastR00EPoint*(1-x)*(1-y)
	+ lastR10EPoint*x*(1-y)
	+ lastR01EPoint*(1-x)*y
	+ lastR11EPoint*x*y;
    }//if()

  else
    {
      //get vertices of quadrant from arrays
      G4double x = (zRel/GridDelta-zIndex);
      G4double y = (Rpos/GridDelta-rIndex);

      lastzIndex = zIndex;
      lastrIndex = rIndex;
      
      //reset static variables for ease of access in next step
      lastZ00BPoint = BFieldMapZ[(zIndex)*RMax+(rIndex)];
      lastZ10BPoint = BFieldMapZ[(zIndex+1)*RMax+(rIndex)];
      lastZ01BPoint = BFieldMapZ[(zIndex)*RMax+(rIndex+1)];
      lastZ11BPoint = BFieldMapZ[(zIndex+1)*RMax+(rIndex+1)];
      
      lastR00BPoint = BFieldMapR[(zIndex)*RMax+(rIndex)];
      lastR10BPoint = BFieldMapR[(zIndex+1)*RMax+(rIndex)];
      lastR01BPoint = BFieldMapR[(zIndex)*RMax+(rIndex+1)];
      lastR11BPoint = BFieldMapR[(zIndex+1)*RMax+(rIndex+1)];

      lastZ00EPoint = EFieldMapZ[(zIndex)*RMax+(rIndex)];
      lastZ10EPoint = EFieldMapZ[(zIndex+1)*RMax+(rIndex)];
      lastZ01EPoint = EFieldMapZ[(zIndex)*RMax+(rIndex+1)];
      lastZ11EPoint = EFieldMapZ[(zIndex+1)*RMax+(rIndex+1)];
      
      lastR00EPoint = EFieldMapR[(zIndex)*RMax+(rIndex)];
      lastR10EPoint = EFieldMapR[(zIndex+1)*RMax+(rIndex)];
      lastR01EPoint = EFieldMapR[(zIndex)*RMax+(rIndex+1)];
      lastR11EPoint = EFieldMapR[(zIndex+1)*RMax+(rIndex+1)];
      

      //interpolate
      *Bz = lastZ00BPoint*(1-x)*(1-y)
	+ lastZ10BPoint*x*(1-y)
	+ lastZ01BPoint*(1-x)*y
	+ lastZ11BPoint*x*y;
      
      *Br = lastR00BPoint*(1-x)*(1-y)
	+ lastR10BPoint*x*(1-y)
	+ lastR01BPoint*(1-x)*y
	+ lastR11BPoint*x*y;
  
      *Ez = lastZ00EPoint*(1-x)*(1-y)
	+ lastZ10EPoint*x*(1-y)
	+ lastZ01EPoint*(1-x)*y
	+ lastZ11EPoint*x*y;
      
      *Er = lastR00EPoint*(1-x)*(1-y)
	+ lastR10EPoint*x*(1-y)
	+ lastR01EPoint*(1-x)*y
	+ lastR11EPoint*x*y;
  

      
    }//else
  }
}
