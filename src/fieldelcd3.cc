#include "stdafx.h"
#include "fieldelcd3.h"
#include "unistd.h"

//////////////////////////// global variable ///////////
typefieldelcd2 commonfieldelcd2;
int elliptic_errorcount= 0;

////////////////////////////////////////////////////////

//void magfieldtraj(double *P,double *B)
//{
// Computes in a point P[1],P[2],P[3] 
//  the magnetic field B[1],B[2],B[3]
//  (Descartes coordinates).
// The magnetic field components are multiplied by commonfieldelcd2.magfac
//  double z,r,Bz,Br,A,sine,cosine,Bm[4],magfac;
//  int k,j;
//
  
//  magfac=commonfieldelcd2.magfac;
//  magfield(P,B);
//  B[1]*=magfac; B[2]*=magfac; B[3]*=magfac; 
//  B[1]+=commonfieldelcd2.Bearth[1]; B[2]+=commonfieldelcd2.Bearth[2]; B[3]+=commonfieldelcd2.Bearth[3]; 
//
//  return;
//}

////////////////////////////////////////////////////////

//void elfieldtraj(double *P,double *E,double *Phi)
//{
// Computes in a point P[1],P[2],P[3] the electric field
//  E[1],E[2],E[3] and the electric potential Phi.
// The potential and the electric field components are multiplied by commonfieldelcd2.elfac
//  double z,r,Ez,Er,sine,cosine,elfac;
//
//  elfac=commonfieldelcd2.elfac;
//  z=P[3];
//  r=sqrt(P[1]*P[1]+P[2]*P[2]);
//
//  elfield(z,r,Phi,&Ez,&Er);
//  if(*Phi>1.e30) return;
//  E[3]=Ez; 
//  if(r<1.e-12)
//    E[1]=E[2]=0.;
//  else
//  {
//    cosine=P[1]/r; sine=P[2]/r;
//    E[1]=cosine*Er;
//    E[2]=sine*Er; 
//  }
//  *Phi*=elfac;
//  E[1]*=elfac; E[2]*=elfac; E[3]*=elfac; 
//
//  return;
//}

///////////////////////////////////////////////////////////////////

void elfield(double z,double r,double *Phi,double *Ez,double *Er,
	     double *Ezr,double *Err,double *Ezz)
// This subroutine computes the electric potential and the axial and radial
//   electric field components Ez and Er, in a point with cylindrical
//   coordinates z and r.
// Method: Legendre polynomial expansion or elliptic integrals,
//         if the former is not convergent
//  Nsp[igroup]: number of z0 source points in electrode group igroup;
//  nmax: maximal index of the source constants (maximum of n).
//  nmax is given by global #define command.
//  Important (if elfield is used separated from elsource):
//    the same nmax number used for elsource
//    should also be used for elfield !!!
// In the beginning of a trajectory calculation, or at a larger change of
// the z and r coordinates:  the global variable 
// commonfieldelcd2.trajstartfield should be put to zero !!!
{
  FILE *fp;
  static double z0input[Ngroup+1][Nspmax+1],roceninput[Ngroup+1][Nspmax+1];
  static double Phininput[Ngroup+1][Nspmax+1][nmax+1];
  static double c1[nmax+1],c2[nmax+1],c3[nmax+1],c4[nmax+1];
  static int Nsp[Ngroup+1],klast[Ngroup+1];
  static int iff=0;
  int kloop,kx,n,nmaxtest,k,kmin,kmax,kbest,j;
  double ro,u,delz,s,rcmin,z0,rocen;
  double P[nmax+1],Pp[nmax+1],Ppp[nmax+1],rc,rcn;
  int iPhi,iEz,iEr;
  double Phiplus,Ezplus,Erplus,Errplus,Ezrplus,Ezzplus;
  double Phiplus1,Ezplus1,Erplus1;
  double Phi1,Ez1,Er1;
  double Phin[nmax+1],Cz,Cr,Phigroup,Ezgroup,Ergroup,rclimit;
  double Errgroup=0.0,Ezrgroup=0.0,Ezzgroup=0.0;
  int igroup,i,Ngrouptest;
//
  rclimit=commonfieldelcd2.rclimit;
  if(iff==0)
  {
// Input from file elsource.dat:
    do {
      fp=fopen(elsourcefile,"r");}
    while (fp==NULL && !sleep(5)); //Stevens: make sure file was opened

    fscanf(fp,"%i %i",&nmaxtest,&Ngrouptest);

    if(Ngrouptest != Ngroup)
    {
      G4cout<<"Message from subroutine elfield: "
		<<"different Ngroup values used in elcd2 and in elfield !!! "
		<<"Computation is  stopped !!! "<<G4endl;
      exit(0);
    }
    if(nmaxtest != nmax)
    {
      G4cout<<"Message from subroutine elfield: "
		<<"different nmax values used in elsource "
		<<"and elfield !! "
		<<"Computation is  stopped !!! "<<G4endl;
      exit(0);
    }
    for(igroup=1;igroup<=Ngroup;igroup++)
      fscanf(fp,"%i",&Nsp[igroup]);

    for(igroup=1;igroup<=Ngroup;igroup++)
      if(Nsp[igroup]>Nspmax)
      {
        G4cout<<"Message from subroutine elfield: "
		<<"Nsp[igroup]>Nspmax  !!! "
		<<"Computation is  stopped !!! "<<G4endl;
        exit(0);
      }
    for(igroup=1;igroup<=Ngroup;igroup++)
      for(kloop=1;kloop<=Nsp[igroup];kloop++)
      {
        fscanf(fp,"%i %i",&i,&kx);
        fscanf(fp,"%le %le",&z0input[igroup][kloop],&roceninput[igroup][kloop]);

        for(n=0;n<=nmax;n++)
	  
          //use fscanf
          //fscanf_s(fp,"%le",&Phininput[igroup][kloop][n]);
          fscanf(fp,"%le",&Phininput[igroup][kloop][n]);

      }
    fclose(fp);
// Initialization of c1,c2,c3,c4 vectors:
    for(n=2;n<=nmax;n++)
    {
      c1[n]=(2.*n-1.)/(1.*n);
      c2[n]=(n-1.)/(1.*n);
      c3[n]=(2.*n-1.)/(1.*(n-1.));
      c4[n]=(1.*n)/(1.*(n-1.));
    }
    iff=1;
  }
// Searching the best source point in the beginning of a trajectory calc.:
  if(commonfieldelcd2.trajstartfield==0)
  {
    commonfieldelcd2.trajstartmag=0;
// The best source point is searched here for each electrode group 
//    (with minimal convergence ratio rc):
    for(igroup=1;igroup<=Ngroup;igroup++)
    {
      rcmin=1.e20;
      for(k=1;k<=Nsp[igroup];k++)
      {
        delz=z-z0input[igroup][k]; 
	ro=sqrt(r*r+delz*delz);
	rc=ro/roceninput[igroup][k];
        if(rc<rcmin)
        { rcmin=rc; klast[igroup]=k; }
      }
    }
    commonfieldelcd2.trajstartfield=1;
// End of source point searching in the beginning of a trajectory calc.
  }

// Initializations:
  *Phi=*Ez=*Er=0.;
  *Err=0; *Ezr=0; *Ezz=0;
// Electrode group loop:
  for(igroup=1;igroup<=Ngroup;igroup++)
  {
// Source point searching for an arbitrary call of elfield
// The best source point is searched here for each electrode group
//   (starting from the last source point)
    for(j=1;j<=2;j++)
    {
      if(j==1)
      {  kmin=klast[igroup]-2; kmax=klast[igroup]+2; } 
//      else if(j==2)
//      {  kmin=klast[igroup]-10; kmax=klast[igroup]+10; } 
      else
      {  kmin=1;  kmax=Nsp[igroup]; }	
      if(kmin<1) kmin=1;
      if(kmax>Nsp[igroup]) kmax=Nsp[igroup];
      rcmin=1.e20;
      kbest=kmin;
      for(k=kmin;k<=kmax;k++)
      { 
        z0=z0input[igroup][k]; rocen=roceninput[igroup][k]; 
        delz=z-z0; ro=sqrt(r*r+delz*delz); rc=ro/rocen;
        if(rc<rcmin)
        { rcmin=rc; kbest=k; }
      }
      z0=z0input[igroup][kbest]; rocen=roceninput[igroup][kbest]; 
      delz=z-z0; ro=sqrt(r*r+delz*delz); rc=ro/rocen;
//      if(j==1 && rc<0.9) break;
//      if(j==2 && rc<0.99) break;
      if(rc<rclimit) break;
    }
    klast[igroup]=k=kbest;
//  End of source point searching
//////////////////////////////////////
// If the field point is very close to the source point:
    if(r<1.e-12 && fabs(z-z0)<1.e-12)
    {
      Phigroup=Phininput[igroup][k][0];
      Ezgroup=-Phininput[igroup][k][1]/rocen;
      Ergroup=0.;
      Ezrgroup=0.;
      Errgroup=Phin[2]/(rocen*rocen);
      Ezzgroup=-Phin[2]*2./(rocen*rocen);
      printf("Probably had a memory leak right here!!\n");
      goto end;
    }
// ro,u,s:
    delz=z-z0;
    ro=sqrt(r*r+delz*delz);
    u=delz/ro;
    s=r/ro;
// First 2 terms of Legendre polynomial P and its derivative Pp (P-primed)
    P[0]=1.; P[1]=u;
    Pp[0]=0.; Pp[1]=1.;
    Ppp[0]=0.; Ppp[1]=0.;
// Convergence ratio:
    rc=ro/rocen;
// If rc>rclimit (the Legendre polynomial series is not convergent)
//  ---> computation of this electrode group by elliptic integrals
    commonfieldelcd2.rc=rc;
    if(rc>0.999)
    {
		if (elliptic_errorcount < 5) {
			
                     //G4cout<<"Elliptic Integrals!!! z="<<z<<", r="<<r<<", z0="<<z0
                     //      << ", rocen="<<rocen<<G4endl; //// time consuming
			elliptic_errorcount++;
		}
      elfield_elliptic_group(igroup,z,r,&Phigroup,&Ezgroup,&Ergroup);
      Ezrgroup=0.;
      Errgroup=0.;
      Ezzgroup=0.;

      if(commonfieldelcd2.electrode_encounter_index==1)
      { *Phi=1.e31;  return;  }
      goto end;
    }
// Source coefficients:
    for(n=0;n<=nmax-1;n++)
      Phin[n]=Phininput[igroup][k][n];
// First 2 terms of the series:
    rcn=rc;
    Cz=-1./rocen; Cr=s/rocen;
    Phigroup=Phin[0]+Phin[1]*rc*u;
    Ezgroup=(Phin[1]+Phin[2]*2.*rc*u)*Cz;
    Ergroup=Phin[2]*rc*Cr;
    Ezrgroup=0.;
    // Errgroup=Phin[2]*rc/(ro*rocen);
    Errgroup=Phin[2]/pow(rocen,2); // $$$$ no real change
    // Ezzgroup=Phin[2]*rc/(ro*rocen)*(-2.);
    Ezzgroup=Phin[2]*2./rocen*Cz; // $$$$ no real change
//
    iPhi=0; iEz=0; iEr=0;
    Phiplus1=1.e30; Ezplus1=1.e30; Erplus1=1.e30;
// We start here the series expansion:
    for(n=2;n<=nmax-1;n++)
    {
      rcn*=rc;
      P[n]=c1[n]*u*P[n-1]-c2[n]*P[n-2];
      Pp[n]=c3[n]*u*Pp[n-1]-c4[n]*Pp[n-2];
      Ppp[n]=(n+1)*Pp[n-1]+u*Ppp[n-1];
      Phiplus=Phin[n]*rcn*P[n];
      Ezplus=Phin[n+1]*(n+1)*rcn*P[n]*Cz;
      Erplus=Phin[n+1]*rcn*Pp[n]*Cr;
      // Ezrplus=Phin[n+1]*rcn*s/(ro*rocen)*(u*Pp[n]-n*P[n]);
      Ezrplus=Phin[n+1]*(n+1)*rcn*Cz*s/ro*(n*P[n]-Pp[n]*u); // $$$$
      Errplus=Phin[n+1]*rcn/(ro*rocen)*(n*s*s*Pp[n]+u*u*Pp[n]-s*s*u*Ppp[n]);
      // Ezzplus=Phin[n+1]*rcn*(-1.)/(ro*rocen)*(n*u*P[n]+s*s*Pp[n]);
      Ezzplus=Phin[n+1]*(n+1)*rcn*Cz/ro*(n*u*P[n]+Pp[n]*s*s); // $$$$ 
      Phigroup+=Phiplus; Ezgroup+=Ezplus; Ergroup+=Erplus;
      Ezrgroup+=Ezrplus; Errgroup+=Errplus; Ezzgroup+=Ezzplus;
      Phi1=1.e-15*fabs(Phigroup);
      Ez1=1.e-15*fabs(Ezgroup);
      Er1=1.e-15*fabs(Ergroup);
      if(fabs(Phiplus)<Phi1 && fabs(Phiplus1)<Phi1) iPhi=1;
      if(fabs(Ezplus)<Ez1 && fabs(Ezplus1)<Ez1) iEz=1;
      if(fabs(Erplus)<Er1 && fabs(Erplus1)<Er1) iEr=1;
      if(r<1.e-12) iEr=1;
      if(iPhi*iEz*iEr == 1) break;
      Phiplus1=Phiplus; Ezplus1=Ezplus; Erplus1=Erplus;
    }
end: ;
    *Phi+=Phigroup;
    *Ez+=Ezgroup;
    *Er+=Ergroup;
    *Ezr+=Ezrgroup;
    *Err+=Errgroup;
    *Ezz+=Ezzgroup;
    if (z==-1.1995&&r==0) G4cout << "Ez: " << Ez << G4endl;
  }
//
  return;
}

////////////////////////////////////////////////////////

void elementread2()
{
// This function reads the charge density, geometry and potential parameters
// of the independent subelements from the data file element.dat.
// Then it computes the parameters of all subelements
//   (by mirror symmetry transform.)
  int N,Ngroupx;
  FILE *fp;
  int i,j;
// 1. step: input from file 'element.dat'
    do {
      fp=fopen("element.dat","r");}
    while (fp==NULL && !sleep(5)); //Stevens: make sure file was opened
    //fp=fopen("element.dat","r");

  //Tong: use fscanf instead
  //fscanf_s(fp,"%i %i %i %le",&commonfieldelcd2.Nsubel,&Ngroupx,&commonfieldelcd2.indexmirror,&commonfieldelcd2.zmirror);
  fscanf(fp,"%i %i %i %le",&commonfieldelcd2.Nsubel,&Ngroupx,&commonfieldelcd2.indexmirror,&commonfieldelcd2.zmirror);


  if(Ngroupx!=Ngroup)
  {
      G4cout<<"Message from subroutine elementread2: "
	    <<"Ngroup value from file 'element.dat': " << Ngroupx << " is different from Ngroup value of #define: " << Ngroup << G4endl
		<<"Computation is  stopped !!! " << G4endl;
      exit(1);
  }
  if(commonfieldelcd2.Nsubel>Nmax)
  {
    G4cout<<"Message from function elementread2: "
		<<"commonfieldelcd2.Nsubel>Nmax !!! Computation is  stopped !!! " << G4endl;
    exit(0);
  }
  N=commonfieldelcd2.Nsubel;
  for(i=1;i<=N;i++)
  {

    //Tong: use fscanf instead
    //fscanf_s(fp,"%i",&commonfieldelcd2.indexgroup[i]);
    fscanf(fp,"%i",&commonfieldelcd2.indexgroup[i]);

    for(j=0;j<=5;j++)

      //Tong: use fscanf
      //fscanf_s(fp,"%le",&commonfieldelcd2.element[i][0][j]);
      fscanf(fp,"%le",&commonfieldelcd2.element[i][0][j]);
  }
  fclose(fp);
// 2. step: mirror symmetry transformations
  if(commonfieldelcd2.indexmirror==1)
    mirrorsymm2();
  return;
}

//////////////////////////////////////////////////////

void mirrorsymm2()
{
// This function computes electrode parameters by
// mirror symmetry transformation
// (computation of commonfieldelcd2.element[i][1][j] from commonfieldelcd2.element[i][0][j])
  int i,imax;
// 
  if (true)
  {
	  G4cout << "Message from Stefan in subroutine mirrorsym2: "
		  << "Should not be here" << G4endl;
	  exit(-1);
  }
  imax=commonfieldelcd2.Nsubel;
  for(i=1;i<=imax;i++)
  {
    commonfieldelcd2.element[i][1][0]=commonfieldelcd2.element[i][0][0];
    commonfieldelcd2.element[i][1][2]=commonfieldelcd2.element[i][0][2];
    commonfieldelcd2.element[i][1][4]=commonfieldelcd2.element[i][0][4];
    commonfieldelcd2.element[i][1][5]=commonfieldelcd2.element[i][0][5];
    commonfieldelcd2.element[i][1][1]=2.*commonfieldelcd2.zmirror-commonfieldelcd2.element[i][0][1];
    commonfieldelcd2.element[i][1][3]=2.*commonfieldelcd2.zmirror-commonfieldelcd2.element[i][0][3];
  }
  return;
}


///////////////////////////////////////////////

void elfield_elliptic(double z,double r,double *Phi,double *Ez,double *Er)
{
// This subroutine computes the electric potential and field
//   in a (z,r) point, due to all electrodes,
//   using the K and E elliptic integrals.
  int igroup;
  double Phigroup,Ezgroup,Ergroup;
  *Phi=*Ez=*Er=0.;
  for(igroup=1;igroup<=Ngroup;igroup++)
  {
    elfield_elliptic_group(igroup,z,r,&Phigroup,&Ezgroup,&Ergroup);
    if(commonfieldelcd2.electrode_encounter_index==1) return;
    *Phi+=Phigroup;
    *Ez+=Ezgroup;
    *Er+=Ergroup;
  }
  return;
}

///////////////////////////////////////////////

void elfield_elliptic_group(int igroup,double z,double r,double *Phi,double *Ez,double *Er)
{
// This subroutine computes the electric potential and field
//   in a (z,r) point, due to the electrode group with index igroup,
//   using the K and E elliptic integrals.
  int i,sm,m,M,itype;
  const double eps0=8.854188e-12;
  const double w9[10]={ 0.2803440531305107e0,0.1648702325837748e1,
                        -0.2027449845679092e0,0.2797927414021179e1,
                        -0.9761199294532843e0,0.2556499393738999e1,
                         0.1451083002645404e0,0.1311227127425048e1,
                          0.9324249063051143e0,0.1006631393298060e1};
  double w[5001];
  static int iff=0;
  double za,zb,ra,rb,Z,R,L,xm,x,sigma,del,sigmaper;
  double K,E,H,eta,dr,dz,sumr,S,S3,d;
  double sum1,sum2,sum3,uz,ur,beta,qz,qr,ratio,Da,Db,D,distance;
// itype=1: elliptic integrals with Chebysev approximation
// itype=2: elliptic integrals with arithmetic-geometric mean method
  itype=2;
  if(iff==0)
  {
// Input from file element.dat:
    elementread2();
    iff=1;
  }
// Initialization of Phi,Ez,Er:
  *Phi=*Ez=*Er=0.;
//
// Electrode subelement loop:
  for(i=1;i<=commonfieldelcd2.Nsubel;i++)
    for(sm=0;sm<=commonfieldelcd2.indexmirror;sm++)
      if(commonfieldelcd2.indexgroup[i]==igroup)
      {
        za=commonfieldelcd2.element[i][sm][1]; ra=commonfieldelcd2.element[i][sm][2];
        zb=commonfieldelcd2.element[i][sm][3]; rb=commonfieldelcd2.element[i][sm][4];
        sigma=commonfieldelcd2.element[i][sm][0];
        sum1=sum2=sum3=0.;
        sigmaper=sigma/(M_PI*eps0);
// Integration number M:
// L: distance between points (za,ra) and (zb,rb)
        L=sqrt((za-zb)*(za-zb)+(ra-rb)*(ra-rb)); xm=L/2.;
// Unit vector u --> from Pa to Pb
        uz=(zb-za)/L; ur=(rb-ra)/L;
// Point P: field point (z,r)
// Point Q: PQ is perp. to Pa-Pb line
// beta:  distance from Pa to Q
        beta=uz*(z-za)+ur*(r-ra);
// Q:
        qz=za+beta*uz;  qr=ra+beta*ur;
// d: PQ distance
        d=sqrt((z-qz)*(z-qz)+(r-qr)*(r-qr)); 
// D, ratio::   
	if(beta>=0. && beta <= L)
	{  ratio=d/L;  distance=d; }
	else
	{
// Da, Db:  Pa-P  and Pb-P distances
          Da=sqrt((za-z)*(za-z)+(ra-r)*(ra-r));
          Db=sqrt((zb-z)*(zb-z)+(rb-r)*(rb-r));
	  if(Da<Db)
	    D=Da;
	  else
	    D=Db;
	  ratio=D/L;
	  distance=D;
	}
// Electrode encounter test:
        if(distance<commonfieldelcd2.maxdistance_electrode)
	  {  commonfieldelcd2.electrode_encounter_index=1;  //printf("Electrode encountered za: %e, zb: %e, ra: %e, rb: %e\n",za,zb,ra,rb); 
return;  }
// M:
	if(ratio>0.001)
	  M=20+(int)(10./ratio);
	else
	  M=15000;
	if(M>=5000) 
        {
           G4cout<<"Message from function elfield_elliptic_group: "
		<<"The field point is too close to an electrode subelement !!! "
		<<"Smaller subelement size or larger commonfieldelcd2.maxdistance_electrode value should be used !!! "
		<<"Computation is  stopped !!! "
		<<"z="<<z<<", r="<<r<<", za="<<za<<", ra="<<ra<<", zb="<<zb<<", rb="<<rb<<G4endl;

           //Tong: no longer exit directly from the whole program, but instead exit from the function, while setting everything to zero.
           //exit(0);

	   Phi = 0;
	   Ez = 0;
	   Er = 0;
	   return;
        }
        del=L/M;
// Integration weight factors:
        for(m=0;m<=9;m++)
          w[m]=w9[m];
        for(m=10;m<=M-10;m++)
          w[m]=1.;
        for(m=M-9;m<=M;m++)
          w[m]=w9[M-m];
// Integration loop:
        for(m=0;m<=M;m++)
        {
          x=-xm+del*m;
          Z=(za+zb)/2.+x*(zb-za)/L;
          R=(ra+rb)/2.+x*(rb-ra)/L;
          dz=z-Z;
          dr=r-R;
          sumr=R+r;
          eta=(dr*dr+dz*dz)/(sumr*sumr+dz*dz);
	  //G4cout << "eta " << eta << G4endl;
          S=sqrt(sumr*sumr+dz*dz);
	  if(itype==1)
	  {
            K=Kelliptic2(eta);
            E=Eelliptic(eta);
            H=Helliptic(eta);
	  }
	  else
	    elliptic_integrals_agm(eta,&K,&E,&H);
          S3=S*S*S;
          sum1+=w[m]*del*R*K/S;
          sum2+=w[m]*del*(z-Z)/S3*E/eta*R;
          sum3+=w[m]*del*R/S3*(2.*R*H+(r-R)/eta*E);
	  //if (z==4.797 && Z==5.5) printf("Should be big: m: %i EzTot: %e, sum1: %e sum2: %e sum3: %e sigma: %e\n",m,*(Ez),sum1,sum2,sum3,commonfieldelcd2.element[i][sm][0]);
        }
        *Phi+=sigmaper*sum1;
        *Ez+=sigmaper*sum2;
        *Er+=sigmaper*sum3;

	//if (abs((int)floor(*Ez))>1000000) {printf("Ez is big!! i: %i EzTot: %e, sigmaper: %e, z: %e, r: %e Z: %e, R: %e, sum2: %e\n",i,*(Ez),sigmaper,z,r,Z,R,sum2);}

	//if (z==4.797 && r==8.9e-02) {printf("Ez bceomes big!! i: %i EzTot: %e, sigmaper: %e, z: %e, r: %e, Z: %e, R: %e, sum2: %e sigma: %e\n",i,*(Ez),sigmaper,z,r,Z,R,sum2,commonfieldelcd2.element[i][sm][0]);}

      }
  return;
}

////////////////////////////////////////////////////////

double Kelliptic2(double eta)
{
// Chebyshev approximation for complete elliptic integral K
//   (W.J.Cody, Math. of Comp. 19 (1965) 105).
// The variable eta should be between 0 and 1.
// The usual variable used is k;  eta=1-k*k.
  const double ln4=1.386294361119890;
  const double a[10]={9.657359028085625e-2,3.088514627130518e-2,
                       1.493801353268716e-2,8.789801874555064e-3,
                       6.179627446053317e-3,6.847909282624505e-3,
                       9.848929322176893e-3,8.003003980649985e-3,
                       2.296634898396958e-3,1.393087857006646e-4};
  const double b[10]={1.249999999999080e-1,7.031249973903835e-2,
                       4.882804190686239e-2,3.737773975862360e-2,
                       3.012484901289893e-2,2.393191332311079e-2,
                       1.553094163197720e-2,5.973904299155429e-3,
                       9.215546349632498e-4,2.970028096655561e-5};
  double etan,suma,sumb;
  int n;
  if(eta < 0. || eta > 1.)
  {
    G4cout<<"Message from function Kelliptic2: "
		<<"the variable eta is smaller than 0 or larger than 1 !!! "
		<<"Computation is  stopped !!! "<<G4endl;
    exit(0);
  }
  suma=sumb=0.;
  etan=eta;
  for(n=0;n<=9;n++)
  {
    suma+=a[n]*etan;
    sumb+=b[n]*etan;
    etan*=eta;
  }
  return ln4+suma-(1./2.+sumb)*log(fabs(eta)+1.e-19);
}

////////////////////////////////////////////////////////

double Eelliptic(double eta)
{
// Chebyshev approximation for complete elliptic integral E
//   (W.J.Cody, Math. of Comp. 19 (1965) 105)
// The variable eta should be between 0 and 1.
// The usual variable used is k;  eta=1-k*k.
  const double c[10]={4.431471805608895e-1,5.680519456755915e-2,
                      2.183181167613048e-2,1.156959574529540e-2,
                      7.595093422559432e-3,7.820404060959554e-3,
                      1.077063503986645e-2,8.638442173604074e-3,
                      2.468503330460722e-3,1.494662175718132e-4};
  const double d[10]={2.499999999999017e-1,9.374999972120314e-2,
                      5.859366125553149e-2,4.271789054738309e-2,
                      3.347894366576162e-2,2.614501470031387e-2,
                      1.680402334636338e-2,6.432146586438301e-3,
                      9.898332846225384e-4,3.185919565550157e-5};
  double etan,sumc,sumd;
  int n;
  if(eta < 0. || eta > 1.)
  {
    G4cout<<"Message from function Eelliptic: "
		<<"the variable eta is smaller than 0 or larger than 1 !!! "
		<<"Computation is  stopped !!! "<<G4endl;
    exit(0);
  }
  sumc=sumd=0.;
  etan=eta;
  for(n=0;n<=9;n++)
  {
    sumc+=c[n]*etan;
    sumd+=d[n]*etan;
    etan*=eta;
    if(etan<1.e-20) break;
  }
  return 1.+sumc-sumd*log(fabs(eta)+1.e-19);
}

///////////////////////////////////////////////////////////

double Helliptic(double eta)
{
// Computes (Eelliptic-Kelliptic2)/(k*k); eta=1-k*k
  double k2,EK,k2n,a,b,cn;
  int n;
  if(eta < 0. || eta > 1.)
  {
    G4cout<<"Message from function EKelliptic2: "
		<<"the variable eta is smaller than 0 or larger than 1 !!! "
		<<"Computation is  stopped !!! "<<G4endl;
    exit(0);
  }
  k2=1.-eta;
  if(k2>0.8)
    EK=(Eelliptic(eta)-Kelliptic2(eta))/k2;
  else
  {
    a=M_PI/2.;
    k2n=1.;
    EK=0.;
    for(n=1;n<=900;n++)
    {
      cn=(2.*n-1)/(2.*n);
      a*=cn*cn;
      b=-a/(2.*n-1.);
      EK+=(b-a)*k2n;
      k2n*=k2;
      if(k2n<1.e-16) break;
    }
  }
  return -EK;
}
//////////////////////////////////////////////

void elliptic_integrals_agm(double eta,double *K,double *E,double *H)
{
// Computes  the first and second complete elliptic integrals
//  K and E, and H=(K-E)/k^2 by arithmetic-geometric mean method.
//     eta=1-k*k
  double an,bn,cn,sum,st,twon,k2,d;
  int n;
  if(eta < 1.e-15 || eta > 1.)
  {
    G4cout<<"Message from function elliptic_integrals_agm: "
	  <<"eta<=0 or eta>1 !!! eta: "
		<<"Computation is  stopped !!! "<<G4endl;
    printf("eta: %lf\n",eta); 
    exit(0);
  }
  k2=1.-eta;
  an=1.; bn=sqrt(fabs(eta)+1.e-16); 
  sum=k2;
  twon=1.;
  n=0;
  do
  {
    cn=0.5*(an-bn);
    st=0.5*(an+bn);
    bn=sqrt(an*bn);
    an=st;
    twon*=2.;
    sum+=twon*cn*cn;
    n+=1;
  }
  while(fabs(cn)>1.e-15);
  *K=M_PI/(2.*an);
  d=*K*0.5*sum;
  if(k2>1.e-16)
    *H=d/k2;
  else
    *H=M_PI/4.;
  *E=*K-d;
  return;
}

/*

   *************************************************************************
    C program package fieldelcd2.c for axisymmetric electric and magnetic 
    field calculation using Legendre polynomial expansion and elliptic integrals.
   *************************************************************************

 Input #define variables: Ngroup,Nmax,nmax,Nspmax,nmagmax,Nspmagmax
   
   Ngroup:  number of electrode groups
   Nmax:    this integer should be larger than the number of independent
            electrode subelements
   nmax:    number of terms in the Legendre polynomial expansion
            for electric field calculation 
   Nspmax:  this integer should be larger than the maximal number of
            source points in each electrode group
   nmagmax:    number of terms in the Legendre polynomial expansion
               for magnetic field calculation 
   Nspmagmax:  this integer should be larger than the maximal number of
               source points for magnetic field calculation
   
   
   
The magnetic field components B[1],B[2],B[3], the electric potential Phi and
the electric field components E[1],E[2],E[3] can be computed in a point P
(with Descartes components P[1],P[2],P[3]) by calling the functions
magfieldtraj and elfieldtraj:
   magfieldtraj(P,B),  elfieldtraj(P,E,&Phi).
The user can arbitrarily scale these output values by the global variables
commonfieldelcd2.magfac and commonfieldelcd2.elfac.
F.e.: using commonfieldelcd2.magfac=0.5 the magnetic field values will be
everywhere by a factor 2 smaller.

The magnetic field calculation is performed by Legendre polynomial expansion.
If this is not convergent (the rc=ro/rocen ratio is larger than 0.99),
the computation stops with an error message.

The electric field calculation starts also with Legendre polynomial expansion.
The program searches a source point, for which the rc=ro/rocen ratio
(ro=field point -- source point distance, rocen: central convergence radius)
is smaller than the number commonfieldelcd2.rclimit, which is defined by the
user.
 Usually commonfieldelcd2.rclimit=0.98 or 0.99 value is recommended.
 commonfieldelcd2.rclimit cannot be negative, and it should be smaller than 1.
 Smaller 1- commonfieldelcd2.rclimit  requires larger nmax value in elcd2;
    for example: commonfieldelcd2.rclimit=0.99  ---> nmax=1000
If no source point with the above condition (rc<commonfieldelcd2.rclimit)
can be found, the program computes the potential and electric field by
elliptic integrals.

The user has to put the global variable commonfieldelcd2.trajstartfield
 equal to 0 at the beginning of each trajectory calculation, or at a larger
 change of the spatial coordinates. This is necessary for optimal source point
 searching.
   

 In the beginning of a trajectory calculation the user has to put
 the number commonfieldelcd2.electrode_encounter_index to zero.
 If the particle is close to an electrode (closer than 
 commonfieldelcd2.maxdistance_electrode), the program puts this number
 to the value 1. This is an indication that the particle is absorbed
 by the electrode, the given trajectory is at the end, a new
 trajectory calculation should be started.
 The distance limit commonfieldelcd2.maxdistance_electrode should also
 be given by the user.

*/

