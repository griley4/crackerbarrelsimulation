///////////////////////////////////////////////////////////////
//Name: NabPrimaryGeneratorMessenger.cc
//Description: UI Messenger for NabPrimaryGeneratorAction
///////////////////////////////////////////////////////////////

//The Messengers Creates commands to let UI access the PrimaryGeneratorAction's functions and through those its data members

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "NabPrimaryGeneratorMessenger.hh"

#include "NabPrimaryGeneratorAction.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADouble.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

NabPrimaryGeneratorMessenger::NabPrimaryGeneratorMessenger(
                                             NabPrimaryGeneratorAction* Gun)
:Action(Gun)
{
  //directories
  nabSimDir = new G4UIdirectory("/NabSim/");
  nabSimDir->SetGuidance("UI commands specific to the Nab Simulation");

  gunDir = new G4UIdirectory("/NabSim/gun/");
  gunDir->SetGuidance("gun control");
 
  //reset defaults
  DefaultCmd = new G4UIcmdWithoutParameter("/NabSim/gun/setDefault",this);
  DefaultCmd->SetGuidance("set/reset the kinematic defined in NabPrimaryGenerator");
  DefaultCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  ManualModeCmd = new G4UIcmdWithoutParameter("/NabSim/gun/setManualMode",this);
  ManualModeCmd->SetGuidance("set the program in manual mode i.e no randomization of starting position and momenta");
  ManualModeCmd->AvailableForStates(G4State_Idle);

  WithCoefficientsCmd = new G4UIcmdWithoutParameter("/NabSim/gun/RunWithCoefficients",this);
  WithCoefficientsCmd->SetGuidance("run simulation with nonzero coefficients");
  WithCoefficientsCmd->AvailableForStates(G4State_Idle);
  
  RunProtonCmd = new G4UIcmdWithABool("/NabSim/gun/RunProton", this);
  RunProtonCmd->SetGuidance("turns proton simulation On=true or Off=false");
  RunProtonCmd->SetParameterName("RunProton", false);
  RunProtonCmd->AvailableForStates(G4State_Idle);

  RunForwardProtonCmd = new G4UIcmdWithABool("/NabSim/gun/RunForwardProton", this);
  RunForwardProtonCmd->SetGuidance("turns only forward proton simulation(only available for NabDiscreteSpectrum and Nab_ContinousSpectrum mode) On=true or Off=false");
  RunForwardProtonCmd->SetParameterName("RunForwardProton", false);
  RunForwardProtonCmd->AvailableForStates(G4State_Idle);

  RunElectronCmd = new G4UIcmdWithABool("/NabSim/gun/RunElectron", this);
  RunElectronCmd->SetGuidance("turns electron simulatiopn On=true or Off=false");
  RunElectronCmd->SetParameterName("RunElectron", false);
  RunElectronCmd->AvailableForStates(G4State_Idle);

  DiscreteSpectrumCmd = new G4UIcmdWithoutParameter("/NabSim/gun/setDiscreteSpectrum",this);
  DiscreteSpectrumCmd->SetGuidance("runs discrete points from the full electron energy spectrum");
  DiscreteSpectrumCmd->AvailableForStates(G4State_Idle);

  NeutronDecayCmd = new G4UIcmdWithoutParameter("/NabSim/gun/setNeutronDecay",this);
  NeutronDecayCmd->SetGuidance("forces a neutron decay in the decay volume");
  NeutronDecayCmd->AvailableForStates(G4State_Idle);

  MonoEnergeticCmd = new G4UIcmdWithoutParameter("/NabSim/gun/setMonoEnergetic",this);
  MonoEnergeticCmd->SetGuidance("runs electrons of a single energy to be specified using setElectronEnergy");
  MonoEnergeticCmd->AvailableForStates(G4State_Idle);

  ProtonResponseCmd = new G4UIcmdWithoutParameter("/NabSim/gun/setProtonResponse",this);
  ProtonResponseCmd->SetGuidance("runs protons (and protons only) of a single energy to be specified using setProtonEnergy");
  ProtonResponseCmd->AvailableForStates(G4State_Idle);

  DecayBeamNoEdgeCmd = new G4UIcmdWithoutParameter("/NabSim/gun/setDecayBeamNoEdge",this);
  DecayBeamNoEdgeCmd->SetGuidance("use Beam decay volume w/o edge");
  DecayBeamNoEdgeCmd->AvailableForStates(G4State_Idle);

  ProtonEnergyCmd = new G4UIcmdWithADoubleAndUnit("/NabSim/gun/setProtonEnergy", this);
  ProtonEnergyCmd->SetGuidance("Set Energy of the Proton");
  ProtonEnergyCmd->SetGuidance("This will also set the program to monoenergetic mode");
  ProtonEnergyCmd->SetParameterName("ProtonEnergy", false);
  ProtonEnergyCmd->SetRange("ProtonEnergy>0.");
  ProtonEnergyCmd->SetUnitCategory("Energy");
  ProtonEnergyCmd->AvailableForStates(G4State_Idle);

  ElectronEnergyCmd = new G4UIcmdWithADoubleAndUnit("/NabSim/gun/setElectronEnergy", this);
  ElectronEnergyCmd->SetGuidance("Set Energy of the Electron");
  ElectronEnergyCmd->SetGuidance("This will also set the program to monoenergetic mode");
  ElectronEnergyCmd->SetParameterName("ElectronEnergy", false);
  ElectronEnergyCmd->SetRange("ElectronEnergy>0.");
  ElectronEnergyCmd->SetUnitCategory("Energy");
  ElectronEnergyCmd->AvailableForStates(G4State_Idle);

  RingRadiusCmd = new G4UIcmdWithADoubleAndUnit("/NabSim/gun/setRingRadius", this);
  RingRadiusCmd->SetGuidance("Set the radius of the ring");
  RingRadiusCmd->SetGuidance("This will also set the program to start the events on a ring with given radius");
  RingRadiusCmd->SetParameterName("RingRadius", false);
  RingRadiusCmd->SetRange("RingRadius>=-1");
  RingRadiusCmd->SetUnitCategory("Length");
  RingRadiusCmd->AvailableForStates(G4State_Idle);
  
  

  ForceEventCmd = new G4UIcmdWithAnInteger("/NabSim/gun/forceEvent", this);
  ForceEventCmd->SetGuidance("Force start on specific event number");
  ForceEventCmd->AvailableForStates(G4State_Idle);

  ChangeSeedCmd = new G4UIcmdWithAnInteger("/NabSim/gun/ChangeSeed", this);
  ChangeSeedCmd->SetGuidance("Change the initial random seed");
  ChangeSeedCmd->AvailableForStates(G4State_Idle);
 
  LittleACmd = new G4UIcmdWithADouble("/NabSim/gun/setLittleA", this);
  LittleACmd->SetGuidance("Set LittleA");
  LittleACmd->SetParameterName("LittleA", false);
  LittleACmd->SetRange("LittleA>-1 && LittleA<1");
  LittleACmd->AvailableForStates(G4State_Idle, G4State_PreInit);
  
  LittleBCmd = new G4UIcmdWithADouble("/NabSim/gun/setLittleB", this);
  LittleBCmd->SetGuidance("Set LittleB");
  LittleBCmd->SetParameterName("LittleB", false);
  LittleBCmd->AvailableForStates(G4State_Idle, G4State_PreInit);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

NabPrimaryGeneratorMessenger::~NabPrimaryGeneratorMessenger()
{
  
  delete   DefaultCmd;
  delete   ManualModeCmd;
  delete   RunProtonCmd;
  delete   RunElectronCmd;
  delete   DiscreteSpectrumCmd;
  delete   NeutronDecayCmd;
  delete   MonoEnergeticCmd;
  delete   ProtonResponseCmd;
  delete   DecayBeamNoEdgeCmd;
  delete   ProtonEnergyCmd;
  delete   ElectronEnergyCmd;
  
  delete   RingRadiusCmd;
  
  delete   ForceEventCmd;
  delete   LittleACmd;

  delete   RunForwardProtonCmd;
  delete   LittleBCmd;
  delete   WithCoefficientsCmd;

  delete gunDir;   
  delete nabSimDir;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void NabPrimaryGeneratorMessenger::SetNewValue(G4UIcommand* command,
                                               G4String newValue)
{ 
  if (command == DefaultCmd)
    { Action->SetDefaultBehavior();}  

  if (command == ManualModeCmd)
    { Action->SetRunMode(Nab_ManualMode);}
  
  if (command == WithCoefficientsCmd)
    { Action->SetRunMode(Nab_WithCoefficients);}

  if (command == RunProtonCmd)
    { Action->SetRunProton(RunProtonCmd->GetNewBoolValue(newValue));}

  if (command == RunForwardProtonCmd)
    { Action->SetRunForwardProton(RunForwardProtonCmd->GetNewBoolValue(newValue));}
 
  if (command == RunElectronCmd)
    { Action->SetRunElectron(RunElectronCmd->GetNewBoolValue(newValue));}

  if (command == DiscreteSpectrumCmd)
    { Action->SetRunMode(Nab_DiscreteSpectrum);}

  if (command == NeutronDecayCmd)
    { Action->SetRunMode(Nab_NeutronDecay);}

  if (command == MonoEnergeticCmd)
    { Action->SetRunMode(Nab_MonoEnergetic);}
  
  if (command == ProtonResponseCmd)
    { Action->SetRunMode(Nab_ProtonResponse);}

  if (command == DecayBeamNoEdgeCmd)
    { Action->SetDecayVolume(Nab_VolBeamNoEdge);}

  if (command == ProtonEnergyCmd)
    { Action->SetMonoEnergyProton(ProtonEnergyCmd->GetNewDoubleValue(newValue));}
  
  if (command == ElectronEnergyCmd)
    { Action->SetMonoEnergyElectron(ElectronEnergyCmd->GetNewDoubleValue(newValue));}
	
  if (command == RingRadiusCmd)
    { Action->setRingRadius(RingRadiusCmd->GetNewDoubleValue(newValue));}
	
  if (command == ForceEventCmd)
    { Action->SetEventID(ForceEventCmd->GetNewIntValue(newValue));}

  if (command == LittleACmd)
    { Action->SetLittleA(LittleACmd->GetNewDoubleValue(newValue)); }
   
  if (command == LittleBCmd)
    { Action->SetLittleB(LittleBCmd->GetNewDoubleValue(newValue)); }

  if (command == ChangeSeedCmd)
    {
    Action->SetSeed(ChangeSeedCmd->GetNewIntValue(newValue));   }
    }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

