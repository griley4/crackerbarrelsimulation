//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// $Id: G4SecondaryElectronEmission.cc,v 1.0 2011-08-11 12:07:26 jfw3eu Exp $
// GEANT4 tag $Name: geant4-09-04-patch-01 $
//
////////////////////////////////////////////////////////////////////////
// Secondary Electron Emission Class Implementation
////////////////////////////////////////////////////////////////////////
//
// File:        G4SecondaryElectronEmission.cc
// Description: PostStep Discrete Process - Generation of Secondary Electrons
// Version:     1.0
// Created:     2011-08-11
// Author:      James Wu
// Updated:     
//
// mail:        james.jimmy.wu@gmail.com
//
// Class description:
// Use this process if you want to simulate the production of secondary
// electrons at low energies due to charged particles entering materials
// in vacuum.
////////////////////////////////////////////////////////////////////////


#include "G4ios.hh"
#include "G4Electron.hh"
#include "G4Proton.hh"
#include "G4SecondaryElectronEmission.hh"
#include "G4GeometryTolerance.hh"
#include "G4EmCalculator.hh"
#include "G4Poisson.hh"
#include "G4RandomDirection.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include <math.h> 
// -_- ...........ooooooooooo000000000000OOOOOOOOOOOOOOOOOOOOOOOOOOOO00000

G4SecondaryElectronEmission::G4SecondaryElectronEmission(
                               const G4String& processName,
                               G4ProcessType type)
                               : G4VDiscreteProcess(processName, type)
{
   /*if (verboseLevel > 0) {
    G4cout << GetProcessName() << " is created " << G4endl;
    }*/
  SetProcessSubType(fUserDefined);
}

// -_- ...........ooooooooooo000000000000OOOOOOOOOOOOOOOOOOOOOOOOOOOO000000

G4SecondaryElectronEmission::~G4SecondaryElectronEmission()
{

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool G4SecondaryElectronEmission::IsApplicable (const G4ParticleDefinition& p)
{
  return (&p == G4Electron::Electron() || G4Proton::Proton());
}

// -_- ...........ooooooooooo000000000000OOOOOOOOOOOOOOOOOOOOOOOOOOOO000000

G4double G4SecondaryElectronEmission::GetMeanFreePath(const G4Track& ,
                                              G4double ,
                                              G4ForceCondition* condition)
{
  *condition = Forced;
  return DBL_MAX;
}

// -_- ...........ooooooooooo000000000000OOOOOOOOOOOOOOOOOOOOOOOOOOOO000000

G4VParticleChange* G4SecondaryElectronEmission::PostStepDoIt(
                                                const G4Track& aTrack,
                                                const G4Step& aStep)
{
  aParticleChange.Initialize(aTrack);

  //Print some information
  G4StepPoint* pPreStepPoint  = aStep.GetPreStepPoint();
  G4StepPoint* pPostStepPoint = aStep.GetPostStepPoint();

/*  theStatus = GeneratingElectrons;
  if ( verboseLevel > 0 ) {
    SEEVerbose();
    G4VPhysicalVolume* thePrePV = pPreStepPoint->GetPhysicalVolume();
    G4VPhysicalVolume* thePostPV = pPostStepPoint->GetPhysicalVolume();
    if (thePrePV)  G4cout << " thePrePV:  " << thePrePV->GetName();
    if (thePostPV) G4cout << " thePostPV: " << thePostPV->GetName() << G4endl;
    }
*/

  //Stop executing this process if the particle is not at a boundary
  if (pPostStepPoint->GetStepStatus() != fGeomBoundary){
    //theStatus = NotAtBoundary;
    //if ( verboseLevel > 0) SEEVerbose();
    return G4VDiscreteProcess::PostStepDoIt(aTrack, aStep);
  }
  if (aTrack.GetStepLength()<=kCarTolerance/2){
    //theStatus = StepTooSmall;
    //if ( verboseLevel > 0) SEEVerbose();
    return G4VDiscreteProcess::PostStepDoIt(aTrack, aStep);
  }
  
	

  //Get materials pre-boundary and post-boundary
  Material1 = pPreStepPoint  -> GetMaterial();
  Material2 = pPostStepPoint -> GetMaterial();
  
  if(Material2->GetName() == "Galactic")
    { 	
      return G4VDiscreteProcess::PostStepDoIt(aTrack, aStep);
    }

  if (Material1 == Material2)
    {
      //theStatus = SameMaterial;
      //if (verboseLevel > 0) SEEVerbose();
      return G4VDiscreteProcess::PostStepDoIt(aTrack, aStep);
    }
  

  //G4double MDensity1 = Material1->GetDensity();
  //G4double MDensity2 = Material2->GetDensity();

 

  //Get the particle and store some information to be used later
  const G4DynamicParticle* aParticle = aTrack.GetDynamicParticle();
  OldMomentum = aParticle->GetMomentumDirection();
  G4ThreeVector x0 = pPostStepPoint->GetPosition();
  G4ThreeVector x1 = pPreStepPoint->GetPosition();
  G4ThreeVector p0 = aStep.GetDeltaPosition().unit();
  G4double t0 = pPreStepPoint->GetGlobalTime();
  G4double dedx_el=-1;
  G4double PrimaryEnergy = aStep.GetTrack()->GetKineticEnergy(); //kinetic energy of the particle


  //Calculate the normal vector of the surface
  /*G4ThreeVector theSurfaceNormal = G4ThreeVector(0.,0.,0.);
  G4ThreeVector theGlobalPoint = pPostStepPoint->GetPosition();
  
  G4Navigator* theNavigator = G4TransportationManager::
  GetTransportationManager()->GetNavigatorForTracking();
  
  G4ThreeVector theLocalPoint = theNavigator->
  GetGlobalToLocalTransform().TransformPoint(theGlobalPoint);
  
  if(theNavigator->EnteredDaughterVolume()) {
    theSurfaceNormal = aStep.GetTrack()->GetNextVolume()->GetLogicalVolume()->
    GetSolid()->SurfaceNormal(pPostStepPoint->GetPosition());
    //G4cout << "entering" << G4endl;
    }
  else {
    theSurfaceNormal = aStep.GetTrack()->GetVolume()->GetLogicalVolume()->
    GetSolid()->SurfaceNormal(pPreStepPoint->GetPosition());
    //G4cout << "exiting" << G4endl;
    }*/


// ******** Calculate the number of secondary electrons to be generated ********

  //First, calculate the mean secondary yield
  G4EmCalculator emCalculator;

  //dedx_el is calculated from the kinetic energy of the particle, the type of      particle, and the material
  G4Material* useThisMaterial;

  useThisMaterial = Material2;	

  /*if(MDensity1 > MDensity2) {
  useThisMaterial = Material1;
  }
  else {
  useThisMaterial = Material2;
  }*/
	
	
  G4String currentP = aParticle->GetParticleDefinition()->
  GetParticleName(); //prevent calculation from running if particle is a gamma                         or an opticalphoton
  if (currentP!="gamma")// && currentP!="opticalphoton") 
    {
    dedx_el = emCalculator.GetDEDX(
    aStep.GetTrack()->GetKineticEnergy(), //kinetic energy of the particle
    aStep.GetTrack()->GetDynamicParticle()->GetDefinition(), //type of particle
    useThisMaterial)/(eV/nm); //the material

    //G4cout<< "dedx: " << dedx_el << useThisMaterial << G4endl;
    }
  if (dedx_el==-1) {
    G4cerr << "Error: unable to calculate stopping power dedx_el" << G4endl;
    return G4VDiscreteProcess::PostStepDoIt(aTrack, aStep);
    }
	
  //L_s is an empirical coefficient
  //G4double L_s=0.01*0.8/(0.23*0.5e23*1.6*1.817*1e-16);

  //SEY is the mean yield of secondary electrons
  G4double SEY=0;

  if (currentP="e-")
    {
  //MaxSEY is a normalization coefficient that is determined experimentally
  G4double MaxSEY = 0;
  //MaxPrimaryEnergy
  G4double MaxPrimaryEnergy = 1.0;

    if(useThisMaterial->GetName() == "G4_Al") {
    MaxPrimaryEnergy = 0.4;	}
    else if(useThisMaterial->GetName() == "Galactic"){
    MaxPrimaryEnergy = 1;	    }
    else if(useThisMaterial->GetName() == "Silicon"){
    MaxPrimaryEnergy = 0.45;	    }
    else if(useThisMaterial->GetName() == "G4_Fe"){
    MaxPrimaryEnergy = 0.35;	    }
    else if(useThisMaterial->GetName() == "G4_Cu"){
    MaxPrimaryEnergy = 0.4;	    }
    else {
    MaxPrimaryEnergy = 1.0;
    if(verboseLevel > 0) {	G4cout <<
    " Material not found. MaxPrimaryEnergy = 1.0 was used." << G4endl;  }}

    MaxPrimaryEnergy = MaxPrimaryEnergy * keV;

    if(useThisMaterial->GetName() == "G4_Al") {
    MaxSEY = 2.0;	}
    else if(useThisMaterial->GetName() == "Galactic"){
    MaxSEY = 0;	    }
    else if(useThisMaterial->GetName() == "Silicon"){
    MaxSEY = 0.89;	    }
    else if(useThisMaterial->GetName() == "G4_Fe"){
    MaxSEY = 1.15;	    }
    else if(useThisMaterial->GetName() == "G4_Cu"){
    MaxSEY = 1.53;	    }
    else {
    MaxSEY = 0;
    if(verboseLevel > 0) {	G4cout <<
    " Material not found. MaxSEY = 0 was used." << G4endl;  }}

	
    SEY= 1.28 * pow ((PrimaryEnergy/MaxPrimaryEnergy) , (-0.67)) * (1 - exp ( (-1.614) * pow ((PrimaryEnergy/MaxPrimaryEnergy) , (1.67)))) * MaxSEY;
}

  if (currentP="proton") 
    {

  //materialConstant is a normalization coefficient that is determined experimentally
    G4double materialConstant = 0;

    if(useThisMaterial->GetName() == "G4_Al") {
    materialConstant = 0.00268;	}
    else if(useThisMaterial->GetName() == "Galactic"){
    materialConstant = 0;	    }
    else if(useThisMaterial->GetName() == "Silicon"){
    materialConstant = 0.00168;	    }
    else if(useThisMaterial->GetName() == "G4_Fe"){
    materialConstant = 0.00633;	    }
    else if(useThisMaterial->GetName() == "G4_Cu"){
    materialConstant = 0.0086;	    }
    else {
    materialConstant = 0;
    if(verboseLevel > 0) {	G4cout <<
    " Material not found. materialConstant = 0 was used." << G4endl;  }}
	
    SEY= dedx_el * materialConstant;
    }

  //numY is the number of secondaries that will be generated in this step. It is    calculated from a Poisson process with a mean of SEY.
  G4int numY = G4Poisson(SEY);


  //Print some information
  //G4cout << " SEY: " << SEY << " SE number: "<< numY << G4endl;

// ************* Make the electrons and add them to the run *******************
  for(int i = 0; i < numY; i++) {

    //Give the electron a momentum. Ensure that the momentum is chosen uniformly      out of the hemisphere outside of the material
    G4ThreeVector sMomentum = G4RandomDirection();
    do{
      /*if(verboseLevel > 1)
      G4cout << "fail. old momentum is: " << sMomentum;*/
      sMomentum = G4RandomDirection();
      /*if(verboseLevel > 1)
      G4cout << " and new momentum is: " << sMomentum << G4endl;*/
      }while(sMomentum[2]*x0[2] > 0);

    //Give the electrons an energy based on LiF's secondary energy distribution
    G4double sEnergy = G4UniformRand()*5;
    sEnergy = fabs(G4RandGauss::
    shoot(sEnergy,sEnergy/100)); //smear the sharp edges with a Gaussian
    sEnergy = sEnergy * eV;

    //Print some information
    //G4cout << " New electron Momentum: " << sMomentum << G4endl;

    //Make the particle with the determined momentum, give it the determined          energy, add it to a track, add the track to the run
    G4DynamicParticle* aSecondaryElectron = new 
    G4DynamicParticle(G4Electron::Electron(),sMomentum);

    aSecondaryElectron->SetKineticEnergy(sEnergy);
    G4Track* aSecondaryTrack = new G4Track(aSecondaryElectron, t0, x0);
    aSecondaryTrack->SetTouchableHandle(aStep.GetPreStepPoint()->
    GetTouchableHandle());

    aSecondaryTrack->SetParentID(aTrack.GetTrackID());
    aParticleChange.AddSecondary(aSecondaryTrack);
    }

  return G4VDiscreteProcess::PostStepDoIt(aTrack, aStep);
}

// -_- ...........ooooooooooo000000000000OOOOOOOOOOOOOOOOOOOOOOOOOOOO000000

void G4SecondaryElectronEmission::SEEVerbose() const
{	
        if ( theStatus == Undefined )
                G4cout << " Undefined " << G4endl;
        if ( theStatus == GeneratingElectrons )
                G4cout << " **** Secondary electron process **** " << G4endl;
        if ( theStatus == NotAtBoundary )
                G4cout << " NotAtBoundary " << G4endl;
        if ( theStatus == SameMaterial )
                G4cout << " SameMaterial " << G4endl;
        if ( theStatus == StepTooSmall )
                G4cout << " StepTooSmall " << G4endl;
}

// -_- ...........ooooooooooo000000000000OOOOOOOOOOOOOOOOOOOOOOOOOOOO000000

