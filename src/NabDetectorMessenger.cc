///////////////////////////////////////////////////////////////
//Name: NabDetectorMessenger.cc
//Description: UI Messenger for CBDetectorConstruction
///////////////////////////////////////////////////////////////


#include "NabDetectorMessenger.hh"

#include "CBDetectorConstruction.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithABool.hh"



//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

NabDetectorMessenger::NabDetectorMessenger(CBDetectorConstruction * Det)
:Detector(Det)
{ 
  //make NabSim directory
  NabSimDir = new G4UIdirectory("/NabSim/");
  NabSimDir->SetGuidance("UI commands specific to this program.");
  
  //make directory for detector UI commands
  detDir = new G4UIdirectory("/NabSim/det/");
  detDir->SetGuidance("detector construction commands");

  //create individual UI commands
  
  
  GridCmd = new G4UIcmdWithABool("/NabSim/det/GridOn", this);
  //set UI command name and path (this is what appears in the program)
  GridCmd->SetGuidance("turns Grid on=true and off=false");
  //set Guidance parameters these are shown if "ls" is called in program
  GridCmd->SetParameterName("GridOn", false);
  //set parameter name which is passed to the function farther down
  GridCmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  //available states

  CoilCmd = new G4UIcmdWithABool("/NabSim/det/CoilOn", this);
  CoilCmd->SetGuidance("turns Coils on=true and off=false");
  CoilCmd->SetParameterName("CoilOn", false);
  CoilCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

  ElectrodeCmd = new G4UIcmdWithABool("/NabSim/det/ElectrodeOn", this);
  ElectrodeCmd->SetGuidance("turns Electrodes on=true and off=false");
  ElectrodeCmd->SetParameterName("ElectrodeOn", false);
  ElectrodeCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

  FieldCmd = new G4UIcmdWithABool("/NabSim/det/FieldOn", this);
  FieldCmd->SetGuidance("turns Fields on=true and off=false");
  FieldCmd->SetParameterName("FieldOn", false);
  FieldCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

  DriftRegionCmd = new G4UIcmdWithABool("/NabSim/det/DriftRegionOn", this);
  DriftRegionCmd->SetGuidance("turns DriftRegion special field on=true and off=false");
  DriftRegionCmd->SetParameterName("DriftRegionOn", false);
  DriftRegionCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

  UseFieldMapCmd = new G4UIcmdWithABool("/NabSim/det/UseFieldMapOn", this);
  UseFieldMapCmd->SetGuidance("turns FieldMap on=true and off=false");
  UseFieldMapCmd->SetParameterName("UseFieldMapOn", false);
  UseFieldMapCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

  ResidualGasCmd = new G4UIcmdWithAString("/NabSim/det/SetGas", this);
  ResidualGasCmd->SetGuidance("sets the type of the Residual Gas, H2=Hydrogen Gas, He=Helium, Galactic is defualt");
  ResidualGasCmd->SetGuidance("the units for pressure and temperature are Torr and Kelvin, input example: He 1e-7 77");
  ResidualGasCmd->SetParameterName("GasType", true);
  ResidualGasCmd->SetDefaultValue("Galactic");

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

NabDetectorMessenger::~NabDetectorMessenger()
{
  //destructor
  
  //clean up to avoid memroy leaks
  delete GridCmd;
  delete CoilCmd;
  delete ElectrodeCmd;
  delete FieldCmd;
  delete DriftRegionCmd;
  delete UseFieldMapCmd;
  delete detDir;  
  delete NabSimDir;
  delete ResidualGasCmd;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void NabDetectorMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{
  //this function processes all commands in the /det/ directory
 
  //if the entered command matches a known command the string following it will be interpreted as an argument
  
  if  ( command == GridCmd )
    {Detector->SetGridFlag(GridCmd->GetNewBoolValue(newValue));}
  
  if  ( command == CoilCmd )
    {Detector->SetCoilFlag(CoilCmd->GetNewBoolValue(newValue));}
  
  if  ( command == ElectrodeCmd )
    {Detector->SetElectrodeFlag(ElectrodeCmd->GetNewBoolValue(newValue));}

  if  ( command == FieldCmd )
    {Detector->SetFieldFlag(FieldCmd->GetNewBoolValue(newValue));}

  if  ( command == DriftRegionCmd )
    {Detector->SetDriftRegionFlag(DriftRegionCmd->GetNewBoolValue(newValue));}

  if  ( command == UseFieldMapCmd )
    {Detector->SetUseFieldMapFlag(UseFieldMapCmd->GetNewBoolValue(newValue));}


  if  ( command == ResidualGasCmd )
    {Detector->SetResidualGas(newValue);}


}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
