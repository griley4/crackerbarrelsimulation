source /Applications/root_v6.20.04/bin/thisroot.sh
source "/Volumes/Workspace/Tools/Geant4.10.04.p03/share/Geant4-10.4.3/geant4make/geant4make.sh"

echo "Setting enviroment variables"
export NABSIM="`pwd`"

export G4WORKDIR="`pwd`"
echo "G4WORKDIR= `pwd`"
export G4LOCALINSTALL="/Volumes/Workspace/Tools/Geant4.10.04.p03"
export G4INSTALL="/Volumes/Workspace/Tools/Geant4.10.04.p03/share/Geant4-10.4.3/geant4make"

export G4OPTIMISE=1
export GEANT4_10=1
export GEANT4_10_3=1
unset GEANT4_9
export G4AUTOOVERWRITEOUTPUT=1

# sets the G4EXE directory which containes the binary executables
export G4EXE=$G4WORKDIR/bin/$G4SYSTEM

#Don't use the QT or XM UIs, just interpret from command line
unset G4UI_USE_QT
unset G4UI_USE_XM

#Enable QT Visualization
export G4VIS_USE_QT=1

cd $NABSIM
