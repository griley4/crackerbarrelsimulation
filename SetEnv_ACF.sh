#module unload PE-intel
#module load  PE-gnu
module load gcc
module load geant4
module load root

source /sw/acf/root/6.10.02/centos7.3_intel17.2.174/root-6.10.02.install/bin/thisroot.sh
source /sw/acf/geant4/4.10.03.p01/centos7.3_gnu6.3.0/geant4.10.03.p01.install/share/Geant4-10.3.1/geant4make/geant4make.sh
echo "Setting enviroment variables"
export NABSIM="`pwd`"

export G4WORKDIR="`pwd`"
echo "G4WORKDIR= `pwd`"
export G4LOCALINSTALL="/sw/acf/geant4/4.10.03.p01/centos7.3_gnu6.3.0/geant4.10.03.p01.install"
export G4INSTALL="/sw/acf/geant4/4.10.03.p01/centos7.3_gnu6.3.0/geant4.10.03.p01.install/share/Geant4-10.3.1/geant4make/"
#export G4LOCALINSTALL="/usr/local/geant4.10.01.p03-install/"
#export G4INSTALL="/usr/local/geant4.10.01.p03-install/share/Geant4-10.1.3/geant4make/"

export G4OPTIMISE=1
export GEANT4_10=1
export GEANT4_10_3=1
unset GEANT4_9
export G4AUTOOVERWRITEOUTPUT=1

# sets the G4EXE directory which containes the binary executables
export G4EXE=$G4WORKDIR/bin/$G4SYSTEM

#Don't use the QT or XM UIs, just interpret from command line
unset G4UI_USE_QT
unset G4UI_USE_XM

#Enable QT Visualization
export G4VIS_USE_QT=1

cd $NABSIM
