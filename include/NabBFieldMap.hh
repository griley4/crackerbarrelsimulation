///////////////////////////////////////////////////////////////
//Name: NabBFieldMap.hh
//Description: Field Map for magnetic fields
///////////////////////////////////////////////////////////////

#ifndef NabBFieldMap_h
#define NabBFieldMap_h 1

#include "globals.hh"
#include <fstream>

class NabBFieldMap
{
public:

  NabBFieldMap();//do not use
  NabBFieldMap(G4String name, G4String FerencFile, G4double z1, G4double z2, G4double rmax, G4double Delta);
  ~NabBFieldMap();
  
  void SetFieldMapName(G4String newName);
  void LoadFieldMap();
  void Interpolate(G4double Zpos, G4double Rpos, G4double* Bz, G4double* Br);
  void SetMapDimensions(G4double R, G4double Delta, G4double Z1, G4double Z2);
  
private:
  
  void MakeFieldMap();

private:

  G4double GridDelta;
  G4double GridR;
  G4double GridZ1;
  G4double GridZ2;
  G4double* FieldMapR;
  G4double* FieldMapZ;
  std::fstream FieldMapFile;
  G4String FieldMapFileName;
  G4String FerencFilename;
  G4int NbCoilMax;

  G4int RMax, ZMax;
};

#endif
