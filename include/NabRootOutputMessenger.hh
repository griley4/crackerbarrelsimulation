///////////////////////////////////////////////////////////////
//Name: NabRootOutputMessenger.cc
//Description: UI Messenger for NabRootOutput
///////////////////////////////////////////////////////////////

//The Messengers Creates commands to let UI access the RootOutput class to select what data is saved

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

#ifndef NabRootOutputMessenger_h
#define NabRootOutputMessenger_h 1

#include "G4UImessenger.hh"
#include "globals.hh"

class NabRootOutput;
class G4UIdirectory;
class G4UIcmdWithoutParameter;
class G4UIcmdWithABool;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

class NabRootOutputMessenger: public G4UImessenger
{
public:
  NabRootOutputMessenger(NabRootOutput*);
  ~NabRootOutputMessenger();
  
  void SetNewValue(G4UIcommand*, G4String);
  
private:
  NabRootOutput* output;
  
  G4UIdirectory*             outputDir;
  G4UIdirectory*             nabSimDir;

  G4UIcmdWithoutParameter*   DefaultCmd;
  G4UIcmdWithoutParameter*   NoOutputCmd;
  

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

#endif
