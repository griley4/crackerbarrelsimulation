#include "stdafx.h"
#include "G4UIsession.hh"
#include <iostream>
#include <fstream>

using namespace std; 

#define FMIN(a,b) ((a)<=(b)?(a):(b))
#define FMAX(a,b) ((a)>(b)?(a):(b))
#define FMIN3(a,b,c) (FMIN(a,b)<=(c)?(FMIN(a,b)):(c))
#define FMAX3(a,b,c) (FMAX(a,b)>(c)?(FMAX(a,b)):(c))
#define pow2(x) ((x)*(x))

void magsource(double z0min,double z0max,double delz0,char *inputcoil);
void coilread(char *inputcoil);
void input_sourcepoints_mag(double z0min,double z0max,double delz0,double *z0vec);
void magfield2(double z,double r,char *inputcoil,int n,double *A,
	       double *Bz,double *Br);
void magfield2_elliptic(double z,double r,char *inputcoil,int n,
                        double *A,double *Bz,double *Br);
void magfield_elliptic_1coil(int n,double *coilpar,double z,double r,
                               double *A,double *Bz,double *Br);
double RF_Carlson(double x,double y,double z);
double RD_Carlson(double x,double y,double z);
double RJ_Carlson(double x,double y,double z,double p);
double RC_Carlson(double x,double y);


extern int Ncoil,Nspmag;
extern double coil[Ncoilmax+1][5];
