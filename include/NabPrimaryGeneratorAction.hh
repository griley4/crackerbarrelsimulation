///////////////////////////////////////////////////////////////
//Name: NabPrimaryGeneratorAction.hh
//Description: Starts Events by generating primary particles
///////////////////////////////////////////////////////////////

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef PrimaryGeneratorAction_h
#define PrimaryGeneratorAction_h 1

#include "TTree.h"
#include "TSystem.h"
#include "TFile.h"

#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4ParticleGun.hh"
#include "G4GeneralParticleSource.hh" //ES
#include "globals.hh"

class G4Event;
class CBDetectorConstruction;
class NabPrimaryGeneratorMessenger;
class NabEventAction;

enum NabRunMode      {Nab_DiscreteSpectrum, Nab_MonoEnergetic, 
		      Nab_ManualMode, Nab_NeutronDecay, Nab_ProtonResponse,
                      Nab_ContinousSpectrum, Nab_WithCoefficients, Ce139,
		      SourceTest, ManyDiscreteParticles, Nab_TwoStepProtons};
enum NabDecayVolume  {Nab_VolBeamNoEdge, Nab_VolBeamWithEdge, Nab_VolEdgeEffects};


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class NabPrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
public:
  NabPrimaryGeneratorAction(CBDetectorConstruction*, G4int);    
  ~NabPrimaryGeneratorAction();
  
public:
  
  void GeneratePrimaries(G4Event*);
  //G4GeneralParticleSource* GetParticleGun() {return particleGun;};
  G4ParticleGun* GetParticleGun() {return particleGun;};

  void SetDefaultBehavior();

  //doubles and trees for reading in TTree for Nab_TwoStepProtons RunMode:
  double initialeE0, bPX, bPY, bPZ, bPPx, bPPy, bPPz, bPE, bPT;

  //TFile *f = new TFile("/pool/a/pibeta/nab/wf5eq/nabsimulation_copy_1/NabSimulation64bit/test_10k_0.root", "READONLY"); //galileo4, /pool/a/
  //TFile *f = new TFile("/home/jaf2qc/test_10k_1stStep.root", "READONLY"); //Rivanna
  //TFile *f = new TFile("/net/research/pocanic/jaf2qc/test_10k_0.root", "READONLY"); //galileo64, home dir
  //TFile *f = new TFile("/net/data4/pocanic4/jaf2qc/Nab/NabSimulation/NabSimulation/64bit/merge20180126_1DE1DB_WCoeff_StoppingPlanes_60us_30kV_100M_seed1-100.root", "READONLY"); //galileo64, /net/data4
  TFile *f;

  //TTree *t = (TTree*)f->Get("dynamicTree");
  TTree *t;
  int EntryCounter = 0;
  

  //getters:
  
  //initial momenta
  G4double GetpPx() {return pPx;};
  G4double GetpPy() {return pPy;};
  G4double GetpPz() {return pPz;};
  G4double GetePx() {return ePx;};
  G4double GetePy() {return ePy;};
  G4double GetePz() {return ePz;};

  //initial position
  G4double GetX0() {return x0;};
  G4double GetY0() {return y0;};
  G4double GetZ0() {return z0;};

  //intitial energies
  G4double GetprotonEnergy()   {return protonEnergy;};
  G4double GetelectronEnergy() {return electronEnergy;};

  //modes
  //G4bool   GetProtonMode()         {return ProtonMode;};
  //G4bool   GetElectronMode()       {return ElectronMode;};
  //G4bool   GetEnergyModeDiscrete() {return EnergyMode_Discrete;};
  //G4bool   GetEnergyModeMono()     {return EnergyMode_Mono;};

  G4double GetMonoEnergyProton()   {return monoEnergyProton;};
  G4double GetMonoEnergyElectron() {return monoEnergyElectron;};

  //Setters

  //modes
  void SetDecayVolume(NabDecayVolume flag)   {DecayVolume = flag;};
  //void SetParticleMode(NabParticleMode flag) {ParticleMode = flag;};
  void SetRunMode(NabRunMode flag)           {RunMode = flag;};
  void SetMonoEnergyProton(G4double val)     {monoEnergyProton = val;};
  void SetMonoEnergyElectron(G4double val)   {monoEnergyElectron = val;};
  void SetMonoMomentumProton(G4double val)     {monoMomentumProton = val;};
  void SetMonoMomentumElectron(G4double val)   {monoMomentumElectron = val;};
  
  void setRingRadius(G4double val)           {RingRadius = val;};
  
  void SetEventID(G4int newID)               {eventID = newID;};
  void SetRunProton(G4bool flag)             {RunProton = flag;};
  void SetRunElectron(G4bool flag)           {RunElectron = flag;};

  void SetLittleA(G4double newVal)           {littleA = newVal;};
  void SetLittleB(G4double newVal)           {littleB = newVal;};

  void SetRunForwardProton(G4bool flag)      {RunForwardProton = flag;};

  void SetSeed(G4int val)                    {rand_seed = val;}; 

private:
  void ResetRandomNumberGenerator();
  void IncrementeventID();

  void DiscreteEnergies();
  void ContinousEnergies();
  void ComputeMomenta();
  
  //void ComputeMomenta (NabEnergyMode flag);
  void ComputePosition(NabDecayVolume flag);
  //void CreateParticles(NabParticleMode flag);

  void CylindricalVolume(G4double beamRadius, G4double beamPosition, G4double beamHeight);
  void BeamNoEdgeVolume(G4double z, G4double deltaz, G4double diagonal);
  
  G4double e2spek( G4double Ee);
  
private:
  //Data members
  G4ParticleGun*                particleGun;
  G4GeneralParticleSource*      particleGPS;
  NabPrimaryGeneratorMessenger* gunMessenger;
  CBDetectorConstruction*      detector;
  NabEventAction*               eventAction;

  G4ParticleDefinition* electron_def;
  G4ParticleDefinition* proton_def;
  G4ParticleDefinition* decay_neutron_def;

  G4double electronMass, protonMass, neutronMass;

  G4double littleA, littleB, bigA, bigB, Pn;

  NabRunMode      RunMode;
  NabDecayVolume  DecayVolume;

  G4bool   RunProton;
  G4bool   RunElectron;
  
  G4bool   PointSource;

  G4bool   RunForwardProton;

  G4bool   neutronVelocity;
  G4double neutronVelocityAmp;

  G4double nV;
  G4double pPx, pPy, pPz;
  G4double ePx, ePy, ePz;
  G4double costev;
  
  G4double x0, y0, z0;
  G4double RingRadius;

  G4double monoEnergyProton, monoEnergyElectron;
  G4double monoMomentumProton, monoMomentumElectron;
  G4double protonEnergy, electronEnergy;
  G4double electronEnergy_max;

  //G4double pp2max, pp2min;
  //G4double pp2;

  G4int eventID, rand_seed;

  G4double LDZfront;

  
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif


