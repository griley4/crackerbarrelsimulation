///////////////////////////////////////////////////////////////
//Name: NabDeadNeutron.hh
//Description: A neutron with 0 lifetime to simulate a decay event
///////////////////////////////////////////////////////////////
//Identical to regular neutron in every single way except for the lifetime

#ifndef NabDeadNeutron_h
#define NabDeadNeutron_h 1

#include "globals.hh"
#include "G4ios.hh"
#include "G4ParticleDefinition.hh"
#include "G4Ions.hh"

// ######################################################################
// ###                   NAB DEAD NEUTRON                             ###
// ######################################################################

class NabDeadNeutron : public G4Ions
{
 private:
   static NabDeadNeutron* theInstance;
   NabDeadNeutron(){}
   ~NabDeadNeutron(){}

 public:
   static NabDeadNeutron* Definition();
   static NabDeadNeutron* NeutronDefinition();
   static NabDeadNeutron* Neutron();
};

#endif
