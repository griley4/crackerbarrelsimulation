
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef StackingAction_h
#define StackingAction_h 1

#include "G4UserStackingAction.hh"
#include "globals.hh"

class NabRunAction;
class NabEventAction;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class NabStackingAction : public G4UserStackingAction
{
  public:
    NabStackingAction(NabRunAction*,NabEventAction*);
   ~NabStackingAction();
     
    G4ClassificationOfNewTrack ClassifyNewTrack(const G4Track*);
    
  private:
    NabRunAction*          runaction;
    NabEventAction*        eventaction;    
    
    G4int                  killSecondary;

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

