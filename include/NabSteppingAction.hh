 ///////////////////////////////////////////////////////////////
//Name: NabSteppingAction.hh
//Description: Controls user interaction with a Step 
///////////////////////////////////////////////////////////////

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef SteppingAction_h
#define SteppingAction_h 1

#include "G4UserSteppingAction.hh"
#include "globals.hh"

#include "G4Step.hh"

class CBDetectorConstruction;
class NabRunAction;
class NabEventAction;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class NabSteppingAction : public G4UserSteppingAction
{
  public:
   NabSteppingAction(CBDetectorConstruction*,NabRunAction*,NabEventAction*);
  ~NabSteppingAction();

  static NabSteppingAction* GetSteppingAction() {return fSteppingAction;};

   void UserSteppingAction(const G4Step*);

  NabEventAction* GetEventAction() {return eventaction;};
  const G4Step* GetCurrentStep() {return currentStep;};

  private:

  static G4ThreadLocal NabSteppingAction*  fSteppingAction;

    const G4Step*            currentStep;
    CBDetectorConstruction* detector;
    NabRunAction*            runaction;    
    NabEventAction*          eventaction;

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void move_GC_to_particle_coordinates(double *ygc, double *yp, char* inputcoil);
void move_particle_to_GC_coordinates(double *yp, double *ygc, char* inputcoil);


#endif
