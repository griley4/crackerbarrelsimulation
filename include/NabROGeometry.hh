///////////////////////////////////////////////////////////////
//Name: NabROGeometry.cc
//Description: Responsible for Readout geometry of Detector
///////////////////////////////////////////////////////////////

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

#ifndef NabROGeometry_h
#define NabROGeometry_h 1

#include "G4VReadOutGeometry.hh"

class G4Material;
class NabDummySD;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

class NabROGeometry : public G4VReadOutGeometry
{
public:
  //constructor and destructor
  NabROGeometry();
  NabROGeometry(G4String);
  ~NabROGeometry();

private:
  G4VPhysicalVolume* Build();
  G4Material*             dummyMat;
  NabDummySD*             dummySD;

  G4double                WorldSizeX;
  G4double                WorldSizeY;
  G4double                WorldSizeZ;

  G4double                UDRadius; 
  G4double                UDThickness;
  G4double                UDDLThickness;

  G4double                UDZpos, UDDLZpos;
  G4double                UDZfront, UDZback;

  G4double                LDRadius; 
  G4double                LDThickness;
  G4double                LDDLThickness;

  G4double                LDZpos, LDDLZpos;
  G4double                LDZfront, LDZback;

  G4double                PixelSideLength, CtoCdistance, MaxRadius;
  G4double                innerCircleRadius, outerCircleRadius;

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

#endif
