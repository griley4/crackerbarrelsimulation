///////////////////////////////////////////////////////////////
//Name: CBDetectorConstruction.hh
//Description: Responsible for geometry of Detector
///////////////////////////////////////////////////////////////

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"
#include "G4LogicalVolume.hh"
#include "G4RunManager.hh"
#include "G4ChordFinder.hh"

class NabDetectorMessenger;
class NabSensitiveDetector;
class G4physicalVolume;
class G4Material;
class G4SDManager;	
class G4SubtractionSolid;
class G4UnionSolid;				    
class G4Tubs;
class G4Box;
class G4Cons;
class G4Polyhedra;
class G4Polycone;
class G4VisAttributes;
class G4VReadOutGeometry;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

class CBDetectorConstruction : public G4VUserDetectorConstruction
{  
  /////////////////////////////////////////////
public:
  //Constructor and Destructor
  CBDetectorConstruction(G4String);
  ~CBDetectorConstruction();

  /////////////////////////////////////////////
public:

  //********************************************
  //ChordFinder: (Needs to be here for acces from SteppingAction)
  G4ChordFinder*          emChordFinder;


  //Getters:
  
  G4Material* GetLDMaterial()              {return LDMaterial;};
  G4double    GetLDRadius()                {return LDRadius;};
  G4double    GetLDThickness()             {return LDThickness;};
  G4double    GetLDDLThickness()           {return LDDLThickness;};

  G4double    GetLDZpos()                  {return LDZpos;};
  G4double    GetLDZfront()                {return LDZfront;};
  G4double    GetLDZback()                 {return LDZback;};

  G4Material* GetUDMaterial()              {return UDMaterial;};
  G4double    GetUDRadius()                {return UDRadius;};
  G4double    GetUDThickness()             {return UDThickness;};
  G4double    GetUDDLThickness()           {return UDDLThickness;};

  G4double    GetUDZpos()                  {return UDZpos;};
  G4double    GetUDZfront()                {return UDZfront;};
  G4double    GetUDZback()                 {return UDZback;};

  G4Material* GetIFMaterial()              {return IFMaterial;};
  G4double    GetIFRadius()                {return IFLength;};
  G4double    GetIFThickness()             {return IFThickness;};

  G4double    GetIFZpos()                  {return IFZpos;};
  G4double    GetZfrontIF()                {return IFZfront;};
  G4double    GetZbackIF()                 {return IFZback;};
  
  G4Material* GetWorldMaterial()           {return WorldMaterial;};
  G4double    GetWorldSizeZ()              {return WorldSizeZ;};
  G4double    GetWorldSizeX()              {return WorldSizeX;};
  G4double    GetWorldSizeY()              {return WorldSizeY;};

  G4double    GetPixelSideLength()         {return PixelSideLength;};
  G4double    GetActiveDetectorRadius()    {return ActiveDetectorRadius;};
  G4bool      GetPixelFlag()               {return PixelFlag;};

  G4double    GetDriftR_UEFR_Boundary()    {return DriftR_UEFR_Boundary;};
  G4double    GetDriftR_LEFR_Boundary()    {return DriftR_LEFR_Boundary;};
  G4double    GetUDriftR_Pinch_Boundary()  {return UDriftR_Pinch_Boundary;};
  G4double    GetLDriftR_Pinch_Boundary()  {return LDriftR_Pinch_Boundary;};
  G4double    GetDriftRegionRadius()       {return DriftRegionRadius;};

  G4double    GetUDR_UEFR_Boundary()       {return UDR_UEFR_Boundary;};
  G4double    GetLDR_LEFR_Boundary()       {return LDR_LEFR_Boundary;};
  G4double    GetUDRegionRadius()          {return UDRegionRadius;};
  
  const G4VPhysicalVolume* GetWorld()      {return World_physical;};
  const G4VPhysicalVolume* GetLD()         {return LD_physical;};
  const G4VPhysicalVolume* GetUD()         {return UD_physical;};
  const G4VPhysicalVolume* GetLDDL()       {return LDDL_physical;};
  const G4VPhysicalVolume* GetUDDL()       {return UDDL_physical;};
  const G4VPhysicalVolume* GetIF()         {return IF_physical;};
  
  //Setters:
  
  void SetWorldMaterial(G4String);
  void SetElectrodeMaterial_Ti(G4String);
  void SetUDMaterial(G4String);
  void SetLDMaterial(G4String);
  void SetIFMaterial(G4String);
  void SetCarbonFoilMaterial(G4String);
  void SetIFFlag(G4bool Flag)              {IFFlag = Flag;};
  void SetGridFlag(G4bool Flag)            {GridFlag = Flag;
   G4RunManager::GetRunManager()->GeometryHasBeenModified();};
  void SetCoilFlag(G4bool Flag)            {CoilsFromFile = Flag;
    G4RunManager::GetRunManager()->GeometryHasBeenModified();};
  void SetElectrodeFlag(G4bool Flag)       {ElectrodesFromFile = Flag;
    G4RunManager::GetRunManager()->GeometryHasBeenModified();};
  void SetFieldFlag(G4bool Flag)           {FieldsOn = Flag;
    G4RunManager::GetRunManager()->GeometryHasBeenModified();};
  void SetDriftRegionFlag(G4bool Flag)     {DriftRegionFlag = Flag;
    G4RunManager::GetRunManager()->GeometryHasBeenModified();};
  void SetUseFieldMapFlag(G4bool Flag)     {UseFieldMapFlag = Flag;
    G4RunManager::GetRunManager()->GeometryHasBeenModified();};

  void SetLDZfront(G4double newVal)        {LDZfront = newVal;};
  void SetUDZfront(G4double newVal)        {UDZfront = newVal;};  

  void SetResidualGas(G4String);

  /////////////////////////////////////////////
public:
  G4VPhysicalVolume* Construct();   //Construct: The main method inheritted from G4VDetectorConstruction
  void               UpdateGeometry();
  void               PrintDetectorDimensions();

  /////////////////////////////////////////////
private:
  //Variables

  //********************************************
  //Managers:
  G4SDManager*            SDman;

  //********************************************
  //UI Messenger
  NabDetectorMessenger*   detectorMessenger;

  //********************************************
  //FieldFlag
  G4bool                  FieldsOn;
  
  //********************************************
  //World:

  
  
  G4Material*             WorldMaterial;
  G4double                WorldSizeX;
  G4double                WorldSizeY;
  G4double                WorldSizeZ;

  G4Box*                  World_solid;
  G4LogicalVolume*        World_logic;
  G4VPhysicalVolume*      World_physical;

  G4bool                  ResidualGasFlag;
  
  //********************************************
  //SP plane:

  G4Box*                  USP_solid;
  G4LogicalVolume*        USP_logic;
  G4VPhysicalVolume*      USP_physical;

  G4Box*                  LSP_solid;
  G4LogicalVolume*        LSP_logic;
  G4VPhysicalVolume*      LSP_physical;
  
  //********************************************
  //CM plane:

  G4Box*                  UCM_solid;
  G4LogicalVolume*        UCM_logic;
  G4VPhysicalVolume*      UCM_physical;

  G4Box*                  LCM_solid;
  G4LogicalVolume*        LCM_logic;
  G4VPhysicalVolume*      LCM_physical;

  //********************************************
  //Drift Region:

  G4bool                  DriftRegionFlag;

  G4double                DriftR_UEFR_Boundary;
  G4double                DriftR_LEFR_Boundary;

  G4double                UDriftR_Pinch_Boundary;
  G4double                LDriftR_Pinch_Boundary;

  G4double                DriftRegionExtent;
  G4double                DriftRegionZpos;
  G4double                DriftRegionRadius;

  G4FieldManager*         DriftRegionFieldManager;
  
  G4Tubs*                 DriftRegion_solid;
  G4LogicalVolume*        DriftRegion_logic;
  G4VPhysicalVolume*      DriftRegion_physical;

  //********************************************
  //Upper Electric Field Region:
  
  G4double                UEFRegionExtent;
  G4double                UEFRegionZpos;
  G4double                UEFRegionRadius;

  G4FieldManager*         UEFRegionFieldManager;
  
  G4Tubs*                 UEFRegion_solid;
  G4LogicalVolume*        UEFRegion_logic;
  G4VPhysicalVolume*      UEFRegion_physical;

  //********************************************
  //Upper Detector Field Region:
  
  G4double                UDRegionExtent;
  G4double                UDRegionZpos;
  G4double                UDRegionRadius;

  G4double                UDR_UEFR_Boundary; 
  //boundary of upper detecor region and upper electric field region

  G4FieldManager*         UDRegionFieldManager;
  
  G4Tubs*                 UDRegion_solid;
  G4LogicalVolume*        UDRegion_logic;
  G4VPhysicalVolume*      UDRegion_physical;

  //********************************************
  //Lower Electric Field Region:
  
  G4double                LEFRegionExtent;
  G4double                LEFRegionZpos;
  G4double                LEFRegionRadius;

  G4FieldManager*         LEFRegionFieldManager;
  
  G4Tubs*                 LEFRegion_solid;
  G4LogicalVolume*        LEFRegion_logic;
  G4VPhysicalVolume*      LEFRegion_physical;


  G4bool                  UseFieldMapFlag;
  G4bool                  GlobalFieldMapFlagDetector;
  
  //********************************************
  //Lower Detector Field Region:
  
  G4double                LDRegionExtent;
  G4double                LDRegionZpos;
  G4double                LDRegionRadius;

  G4double                LDR_LEFR_Boundary; 
  //boundary of lower detecor region and lower electric field region

  G4FieldManager*         LDRegionFieldManager;
  
  G4Tubs*                 LDRegion_solid;
  G4LogicalVolume*        LDRegion_logic;
  G4VPhysicalVolume*      LDRegion_physical;

  //********************************************
  //Upper Detector:
  G4Material*             UDMaterial;
  G4double                UDRadius; 
  G4double                UDThickness;
  G4double                UDDLThickness;

  G4double                UDZpos, UDDLZpos;
  G4double                UDZfront, UDZback;

  G4Tubs*                 UD_solid;
  G4LogicalVolume*        UD_logic;
  G4VPhysicalVolume*      UD_physical;
  NabSensitiveDetector*   UD_SD;
  G4VisAttributes*        UD_vis;
  
  G4Tubs*                 UDDL_solid;
  G4LogicalVolume*        UDDL_logic;
  G4VPhysicalVolume*      UDDL_physical;
  G4VisAttributes*        UDDL_vis;

  //********************************************
  //Lower Detector:
  G4Material*             LDMaterial;
  G4double                LDRadius; 
  G4double                LDThickness;
  G4double                LDDLThickness;

  G4double                LDZpos, LDDLZpos;
  G4double                LDZfront, LDZback;

  G4Tubs*                 LD_solid;
  G4LogicalVolume*        LD_logic;
  G4VPhysicalVolume*      LD_physical;
  NabSensitiveDetector*   LD_SD;
  G4VisAttributes*        LD_vis;  

  G4Tubs*                 LDDL_solid;
  G4LogicalVolume*        LDDL_logic;
  G4VPhysicalVolume*      LDDL_physical;
  G4VisAttributes*        LDDL_vis;

  //********************************************
  //Ion Conversion Foil
  G4bool                  IFFlag;

  G4Material*             IFMaterial;
  G4double                IFLength; 
  G4double                IFThickness;
  
  G4double                IFZpos;
  G4double                FoilSpacing;
  G4double                IFZfront, IFZback;
  
  G4Box*                  IF_solid;
  G4LogicalVolume*        IF_logic;
  G4VPhysicalVolume*      IF_physical;
  G4VisAttributes*        IF_vis;

  //********************************************
  //Magnets (built form file)
  G4String                coilFileName;
  G4bool                  CoilsFromFile;


  G4Material*             CoilMaterial;

  G4Tubs*                 Coil_solid;
  G4LogicalVolume*        Coil_logic;
  G4VPhysicalVolume*      Coil_physical;
  G4VisAttributes*        Coil_vis;

  //********************************************
  //Electrodes (built form file)
  G4String                electrodeFileName;
  G4bool                  ElectrodesFromFile;

  G4Material*             ElectrodeMaterial;

  G4Cons*                 Electrode_solid;
  G4LogicalVolume*        Electrode_logic;
  G4VPhysicalVolume*      Electrode_physical;
  G4VisAttributes*        Electrode_vis;

  //********************************************
  //Electrodes (PGON)
  G4bool                  PGONelectrodes;

  G4Material*             ElectrodeMaterial_Ti;
  G4VisAttributes*        ElectrodePGON_vis;

  G4Polyhedra*            ElectrodeTop_solid;
  G4LogicalVolume*        ElectrodeTop_logic;
  G4VPhysicalVolume*      ElectrodeTop_physical;

  G4Polyhedra*            ElectrodeBot_solid;
  G4LogicalVolume*        ElectrodeBot_logic;
  G4VPhysicalVolume*      ElectrodeBot_physical;

  G4Box*            	  ElectrodeLA_Center_Outer_solid;
  G4Box*            	  ElectrodeLA_Center_Inner_solid;

  G4Polyhedra*            ElectrodeLA_Center_Hole_solid;

  G4Box*            	  ElectrodeLA_Access_Outer_solid;
  G4Box*            	  ElectrodeLA_Access_Inner_solid;

  G4Trd*            	  ElectrodeLA_Bot_Outer_solid;
  G4Trd*            	  ElectrodeLA_Bot_Inner_solid;

  G4UnionSolid*		  ElectrodeLA_0_solid;
  G4UnionSolid*	  	  ElectrodeLA_1_solid;
  G4SubtractionSolid*	  ElectrodeLA_2_solid;
  G4SubtractionSolid*	  ElectrodeLA_3_solid;
  G4SubtractionSolid*	  ElectrodeLA_4_solid;
  G4SubtractionSolid*	  ElectrodeLA_5_solid;
  G4LogicalVolume*        ElectrodeLA_logic;
  G4VPhysicalVolume*      ElectrodeLA_physical;

  G4Polycone*		  HVelectrodeTop_solid;
  G4LogicalVolume*        HVelectrodeTop_logic;
  G4VPhysicalVolume*      HVelectrodeTop_physical;

  G4Polycone*		  HVelectrodeBot_solid;
  G4LogicalVolume*        HVelectrodeBot_logic;
  G4VPhysicalVolume*      HVelectrodeBot_physical;

  //********************************************
  //Grid
  G4bool                  GridFlag;

  G4double                GridWireWidth;
  G4double                GridWireSpacing;

  G4Material*             GridMaterial;

  G4double                GridZPosUD;
  G4double                GridZPosLD;
  
  G4Box*                  LDXGrid_solid;
  G4LogicalVolume*        LDXGrid_logic;
  G4VPhysicalVolume*      LDXGrid_physical;
  
  G4Box*                  LDYGrid_solid;
  G4LogicalVolume*        LDYGrid_logic;
  G4VPhysicalVolume*      LDYGrid_physical;
  
  G4Box*                  UDXGrid_solid;
  G4LogicalVolume*        UDXGrid_logic;
  G4VPhysicalVolume*      UDXGrid_physical;
  
  G4Box*                  UDYGrid_solid;
  G4LogicalVolume*        UDYGrid_logic;
  G4VPhysicalVolume*      UDYGrid_physical;


  //*******************************************
  //Readout Geometry
  G4VReadOutGeometry*     ROGeometry;
  G4bool                  PixelFlag;
  G4double                PixelSideLength;
  G4double                ActiveDetectorRadius;
  

  //*******************************************
  //Carbon Foil for Radioactive Source Tests
  G4bool                  Ce139Flag;

  G4Material*             CarbonFoilMaterial;
  G4Tubs*                 CarbonFoil_solid;
  G4LogicalVolume*        CarbonFoil_logic;
  G4VPhysicalVolume*      CarbonFoil_physical;
  G4UserLimits*           FoilStepLimit;
  
  G4bool                  globalField;
  G4bool                  oldStepper;

  //********************************************
  //File Names:
  G4String                FieldFileSuffix;

  
  /////////////////////////////////////////////
private:
  //private internal functions
  void DefineMaterials();
  void SetupFields();
  void BuildCoilsFromFile();
  void BuildElectrodesFromFile();

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....


#endif
