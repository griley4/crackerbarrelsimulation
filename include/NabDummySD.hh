



#ifndef NabDummySD_h
#define NabDummySD_h 1

#include "G4VSensitiveDetector.hh"
class G4Step;

class NabDummySD : public G4VSensitiveDetector
{
public:
  NabDummySD();
  ~NabDummySD() {}
  
  void Initialize(G4HCofThisEvent*) {}
  G4bool ProcessHits(G4Step*,G4TouchableHistory*) {return false;}
  void EndOfEvent(G4HCofThisEvent*) {}
  void clear() {}
  void DrawAll() {}
  void PrintAll() {}
};
NabDummySD::NabDummySD()
  : G4VSensitiveDetector("dummySD")
{}
#endif
