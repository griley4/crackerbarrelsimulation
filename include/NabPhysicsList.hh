//////////////////////////////////////////////////////////////
//Name: NabPhysicsList.hh
//Description: Determines which physical processes will be applied in the Simulation
///////////////////////////////////////////////////////////////

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef PhysicsList_h
#define PhysicsList_h 1

#include "G4VModularPhysicsList.hh"
#include "globals.hh"

class G4VPhysicsConstructor;
class NabPhysicsListMessenger;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class NabPhysicsList: public G4VModularPhysicsList
{
public:
  NabPhysicsList();
  virtual ~NabPhysicsList();

  void ConstructParticle();
        
  void AddPhysicsList(const G4String& name);
    
  void ConstructProcess();    
  void AddDecay();
  void AddStepMax();       
    
  void SetCuts();
  void SetCutForGamma(G4double);
  void SetCutForElectron(G4double);
  void SetCutForPositron(G4double);
    
private:

  NabPhysicsListMessenger* pMessenger; 

  G4String emName;
  G4VPhysicsConstructor*  emPhysicsList;
  G4VPhysicsConstructor*  fRaddecayList;
  G4VPhysicsConstructor*  fParticleList;
    
  G4double cutForGamma;
  G4double cutForElectron;
  G4double cutForPositron;    

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

