///////////////////////////////////////////////////////////////
//Name: NabRunAction.hh
//Description: Allows user interaction with the run (before and after)
///////////////////////////////////////////////////////////////

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

#ifndef RunAction_h
#define RunAction_h 1

#include "G4UserRunAction.hh"
#include "globals.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

class G4Run;
class CBDetectorConstruction;
class NabPrimaryGeneratorAction;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

class NabRunAction : public G4UserRunAction
{

public:

  NabRunAction(CBDetectorConstruction*, NabPrimaryGeneratorAction*);
  virtual ~NabRunAction();
  
  void BeginOfRunAction(const G4Run*);
  void   EndOfRunAction(const G4Run*);
 
  G4int GetRunID() {return runID;};

private:
  
  CBDetectorConstruction*   detector;
  NabPrimaryGeneratorAction* primary;
  //G4String                   OutputFilePrefix;
  G4int runID;

};

#endif
