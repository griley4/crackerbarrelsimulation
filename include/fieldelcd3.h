#include "stdafx.h"
#include "G4UIsession.hh"
#include <iostream>
#include <fstream>

////////////////////////////////////////////////////////////////////////
//
//     Axisymmetric field calculation with Legendre polynomial expansion
//     and with elliptic integrals.
//
////////////////////////////////////////////////////////////////////////

//void magfieldtraj(double *x,double *B);
//void elfieldtraj(double *x,double *E,double *Phi);
void elfield(double z,double r,double *Phi,double *Ez,double *Er,
		 double *Ezr,double *Err,double *Ezz);
	     //double *Ezr,double *Err,double *Err);
void elementread2();
void mirrorsymm2();
void elfield_elliptic(double z,double r,double *Phi,double *Ez,double *Er);
void elfield_elliptic_group(int igroup,double z,double r,double *Phi,double *Ez,double *Er);
double Kelliptic2(double eta);
double Eelliptic(double eta);
double Helliptic(double eta);
void elliptic_integrals_agm(double eta,double *K,double *E,double *H);


struct typefieldelcd2 
{
// Variables needed to be defined by the user:
  int trajstartfield;
  double magfac,elfac,rclimit;
  int electrode_encounter_index;
  double maxdistance_electrode;

// Variables coming from  file 'element.dat':
  int indexmirror,Nsubel,indexgroup[Nmax+1];
  double zmirror,element[Nmax+1][2][6];
  
// Integer for magfield2x:
  int trajstartmag; 
// for elfield
  double rc;
// earth magnetic field: 
  double Bearth[4];
};

extern typefieldelcd2 commonfieldelcd2;
extern char elsourcefile[200];

