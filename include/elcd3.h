#include "stdafx.h"
#include "G4UIsession.hh"
#include <iostream>
#include <fstream>

using namespace std; 

//
//                          elcd2.c
// 

void elcd2(int scale,double power,double *z0min,double *z0max,double
           *delz0,char *inputelectrodesuffix);
void input_electrodes(int scale,double power,char *inputelectrode);

void inputAB(int igroup,double zA,double rA,double zB,double rB,                         double U,int num,int istart,double power);
void mirrorsymm();

void chargedensities();
void elementwrite();
void elementread();



double Phielement(int i,int sm,double *P);
double funRKS(double p);
double Coulomb(int i,int j);
double Phitot(double *P);
double Phigroup(int igroup,double *P);
void Elfield(double eps,double *P,double *E,double *Phi);
void Elfieldgroup(int igroup,double eps,double *P,double *E,double *Phi);


void input_sourcepoints(double *z0min,double *z0max,double *delz0);
void elsource( char *inputelectrode);

double Kelliptic(double eta);
double quad(double (*f)(double),double a,double b,int n);

//Tong: gauss is a unit in Geant4, so I changed its name...
//void gauss(int n,double *x);
void Gauss(int n, double* x);

// Input #define variables: Ngroup,Nmax,nmax,INTNUM,Nspmax
// Input global variables:
extern int indexmirror;
extern double zmirror;

// Output global variables:
extern int Nsubel,Nsp[Ngroup+1],indexgroup[Nmax+1];
extern double element[Nmax+1][2][6];
extern double common[100];
extern double Cgauss[Nmax+1][Nmax+2];
extern double z0a[Ngroup+1][Nspmax+1];
