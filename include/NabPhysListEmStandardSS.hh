
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef PhysListEmStandardSS_h
#define PhysListEmStandardSS_h 1

#include "G4VPhysicsConstructor.hh"
#include "globals.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class NabPhysListEmStandardSS : public G4VPhysicsConstructor
{
public: 
  NabPhysListEmStandardSS(const G4String& name = "standardSS");
  virtual ~NabPhysListEmStandardSS();

public: 
  // This method is dummy for physics
  void ConstructParticle() {};
 
  // This method will be invoked in the Construct() method.
  // each physics process will be instantiated and
  // registered to the process manager of each particle type 
  void ConstructProcess();
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif








