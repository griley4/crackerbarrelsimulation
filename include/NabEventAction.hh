///////////////////////////////////////////////////////////////
//Name: NabEventAction.hh
//Description: Pre and post Event User Action, Analysis tasks
///////////////////////////////////////////////////////////////

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

#ifndef EventAction_h
#define EventAction_h 1

#include "G4UserEventAction.hh"
#include "globals.hh"
#include <vector>
//#include "Rtypes.h"
#include <time.h>

#include "TFile.h"
#include "TTree.h"

//Units and Constants
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include "G4ThreeVector.hh"

class NabRunAction;
class NabRootOutput;
class TTree;
class TFile;

typedef struct{
  G4double angle;
  G4double time;
  G4int trackID;
} angle;


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

class NabEventAction : public G4UserEventAction
{
public:
  //Constructor & Destructor
  NabEventAction(NabRunAction*);
  ~NabEventAction();
  
public:
  //main functions
  void   BeginOfEventAction(const G4Event*);
  void   EndOfEventAction(const G4Event*);
  
  //getters and setters
  void    CountBouncesE()       {bouncesE++;};
  void    ResetBouncesE()       {bouncesE = 0;};
  G4int   GetBouncesE()         {return bouncesE;};
  void    CountBouncesP()       {bouncesP++;};
  void    ResetBouncesP()       {bouncesP = 0;};
  G4int   GetBouncesP()         {return bouncesP;};
  void    SetPFirstHitDetNb(G4int newVal)    {PFirstHitDetNb  = newVal;};
  G4int   GetPFirstHitDetNb()         {return PFirstHitDetNb;};
  void    SetTOFProton(G4double newVal)    {TimeOfFlight_proton_mm  = newVal;};
  G4double   GetTOFProton()         {return TimeOfFlight_proton_mm;};
  void    SetEFirstHitDetNb(G4int newVal)    {EFirstHitDetNb  = newVal;};
  G4int   GetEFirstHitDetNb()         {return EFirstHitDetNb;};
  void    SetPFirstHitDetTime(G4double newVal)    {PFirstHitDetTime  = newVal;};
  G4double   GetPFirstHitDetTime()         {return PFirstHitDetTime;};
  void    SetEFirstHitDetTime(G4double newVal)    {EFirstHitDetTime  = newVal;};
  G4double   GetEFirstHitDetTime()         {return EFirstHitDetTime;};
  void    CountDeadParticles() {deadParticles++;};
  void    AddProtonAngle(angle anAngle) {ProtonImpactAngles->push_back(anAngle);};
  void    AddElectronAngle(angle anAngle) {ElectronImpactAngles->push_back(anAngle);};
  void    ENextStep(const G4ThreeVector& position, G4ThreeVector momentum, G4double localTime, G4bool postInDetector, G4bool preInDetector, G4double preStepEnergy);
  void    PNextStep(const G4ThreeVector& position, G4ThreeVector momentum, G4double localTime, G4bool postInDetector, G4bool preInDetector, G4double preStepEnergy);
  void    PMagmom(G4double PMagmoment);
  void    PMidplane(const G4ThreeVector& prePosition);
  void    EDetMMplane(const G4ThreeVector& prePosition, const G4ThreeVector& postPosition, G4ThreeVector momentum,G4double prekenergy,G4double postkenergy, G4double prelocalTime, G4double postlocalTime);
  void    PDetMMplane(const G4ThreeVector& prePosition, const G4ThreeVector& postPosition, G4ThreeVector momentum,G4double prekenergy,G4double postkenergy, G4double prelocalTime, G4double postlocalTime);
  void    PSPplane(const G4ThreeVector& prePosition, const G4ThreeVector& postPosition, G4ThreeVector momentum,G4double prekenergy,G4double postkenergy, G4double prelocalTime, G4double postlocalTime);

  void    Setx0decay(G4double newVal) {x0decay = newVal/mm;};
  void    Sety0decay(G4double newVal) {y0decay = newVal/mm;};
  void    Setz0decay(G4double newVal) {z0decay = newVal/mm;};

  void    Setcostev(G4double newVal) {costev0 = newVal;};

  void    SetpTOFCut(G4double newVal) {pTOFCut = newVal;};

  void    SetpP0x(G4double newVal) {pP0x = newVal;};
  void    SetpP0y(G4double newVal) {pP0y = newVal;};
  void    SetpP0z(G4double newVal) {pP0z = newVal;};
  void    SetpE0 (G4double newVal) {pE0  = newVal/eV;};
  
  void    SeteP0x(G4double newVal) {eP0x = newVal;};
  void    SeteP0y(G4double newVal) {eP0y = newVal;};
  void    SeteP0z(G4double newVal) {eP0z = newVal;};
  void    SeteE0 (G4double newVal) {eE0  = newVal/keV;};

  void    SetpE  (G4double newVal) {pE   = newVal/eV;};
  void    SeteE  (G4double newVal) {eE   = newVal/keV;};

  void    SetvP0x(G4double newVal) {vP0x = newVal;};
  void    SetvP0y(G4double newVal) {vP0y = newVal;};
  void    SetvP0z(G4double newVal) {vP0z = newVal;};
  void    SetvE0 (G4double newVal) {vE0  = newVal/keV;};

  void    SetEventTime(G4double newVal) {eventTime = newVal;};

  void    CountProtonHitGrid()   {protonHitGrid++;};
  void    CountElectronHitGrid() {electronHitGrid++;};
  void    CountGamma()           {gammaCounter++;};
  void    CountSE()              {SECounter++;};

  void    IncrementGammaLoss(G4double newVal)    {gammaLoss+=newVal/keV;};
  void    IncrementGammaEscape(G4double newVal)  {gammaEscape+=newVal/keV;};
  void    IncrementSELoss(G4double newVal)       {SELoss+=newVal/keV;};
  void    IncrementDLLoss(G4double newVal)       {DLLoss+=newVal/keV;};
  void    IncrementelectronTotalEnergyDeposit(G4double newVal)       {eTotalDeposit+=newVal/keV;};
  void    IncremenprotonTotalEnergyDepositt(G4double newVal)       {pTotalDeposit+=newVal/keV;};

  G4int   GetGammaCount() {return (G4int)gammaCounter;};
  G4int   GetSECount()    {return (G4int)SECounter;};

  G4int   GetEventID() {return eventID;};

  void    SetDefault();
  void    SetStoppingPlanes();

  void    SetOutputFilePrefix(G4String val) {OutputFilePrefix = val;};
  void    AutoOverwriteOutputFile(G4bool val) {overwriteOutputFile = val;};

  void    WriteOutput();

private: 

  NabRunAction*    runaction; //pointer to runaction

  TTree* dynamicTree;
  TFile* OutputFile;

  //data members
  G4String OutputFilePrefix;
  G4bool overwriteOutputFile;
  G4String drawFlag;
  G4int    printModulo, writeModulo;
  G4int    bouncesE, bouncesP, protonHitGrid, electronHitGrid, PFirstHitDetNb, EFirstHitDetNb;
  G4double PFirstHitDetTime, EFirstHitDetTime;
  G4int    deadParticles;
  time_t  startEventTime, endEventTime;
  std::vector<angle>* ProtonImpactAngles;
  std::vector<angle>* ElectronImpactAngles;

  G4ThreeVector EpreviousPosition;
  G4double EpreviousLocalTime;
  G4double EimpactAngleViaPos,EimpactAngleViaMomentum;
  G4bool EenteringDetector;
  G4double EenergyBeforeDetector,EimpactPosition[3];

  G4ThreeVector PpreviousPosition;
  G4double PpreviousLocalTime;
  G4double PimpactAngleViaPos,PimpactAngleViaMomentum;
  G4bool PenteringDetector;
  G4double PenergyBeforeDetector,PimpactPosition[3];

  //float PMagmoment,PMagmoment0;
  G4double PMagmoment_total_mean,PMagmoment_total_RMS,PMagmoment_mean,PMagmoment_RMS;
  G4int Ppassing;

  G4double EdetMMPreX,EdetMMPreY,EdetMMPreZ,EdetMMPostX,EdetMMPostY,EdetMMPostZ,EdetMMPx,EdetMMPy,EdetMMPz,EdetMME,EdetMMPostT;
  G4bool EpassingDetMMplane;

  G4double PmidPreZ;
  G4bool PpassingMidplane;
  G4double PdetMMPreX,PdetMMPreY,PdetMMPreZ,PdetMMPostX,PdetMMPostY,PdetMMPostZ,PdetMMPx,PdetMMPy,PdetMMPz,PdetMME,PdetMMPostT;
  G4bool PpassingDetMMplane;
  G4double pUSPX,pUSPY,pUSPZ,pUSPPx,pUSPPy,pUSPPz,pUSPE,pUSPT;
  G4bool PpassingUSPplane;

  long eventID;
  G4double eventTime;
  G4double elapsedTime;

  Double_t gammaEscape, gammaLoss, gammaDeposit, DLLoss, SELoss, SEDeposit, eTotalDeposit, pTotalDeposit;
  Int_t gammaCounter, SECounter;

  //G4double E0electron, E0proton;
  //position of the decay
  Double_t x0decay, y0decay, z0decay;

  //initial momenta of electron and proton
  Double_t pP0x, pP0y, pP0z, pE0, pE;
  Double_t eP0x, eP0y, eP0z, eE0, eE;
  Double_t vP0x, vP0y, vP0z, vE0;
  Double_t costev0;
  Double_t pTOFCut;

  Double_t EtotProtonUD,  EtotProtonLD, EtotElectronUD, EtotElectronLD;

  Double_t TimeOfFlight, TimeOfFlight_proton_mm, EDepElectron;

  Double_t PixelDeltaR, ProperDeltaR;

  G4int PixelStatus;
  G4int PixelInterference;

  Int_t NbOfTriggers;

  std::vector<double> Trigger_time;
  std::vector<double> Trigger_energy;
  std::vector<double> Trigger_x;
  std::vector<double> Trigger_y;
  std::vector<int> Trigger_particleType;
  std::vector<int> Trigger_parentID;
  std::vector<int> Trigger_detectorNb;

  std::vector<double> Trigger_zmax;
  std::vector<double> Trigger_zavg;
  std::vector<double> Trigger_impactAngle;
  std::vector<int> Trigger_bouncesEAtTrigger;
  std::vector<int> Trigger_bouncesPAtTrigger;
  std::vector<double> Trigger_tshifted;

  std::vector<double> Trigger_xPixel;
  std::vector<double> Trigger_yPixel;


  G4bool FullOutput;
  G4bool StoppingPlanes;
  G4bool RootOut;
  G4bool Waveform;
  G4bool Propagate;
  G4bool TrackInitialPosition;
  G4bool TrackProtonMomentum;
  G4bool TrackElectronMomentum;
  G4bool TrackNeutrinoMomentum;
  G4bool TotalDeposits;
  G4bool TrackTriggers;
  G4bool Triggers_impactAngleFlag;
  G4bool Triggers_basic;
  G4bool Triggers_zmaxFlag;
  G4bool Triggers_zavgFlag;
  G4bool Triggers_electronDrift;
  G4bool PixelFlag;
  G4bool TrackPixelInterference;
  G4bool EventVariables;
  G4bool CountBouncesFlag;
  G4bool DeadParticlesFlag;
  G4bool TrackKeyVars;
  G4bool MorePixelFlag;
  G4bool CountSecondariesFlag;
  G4bool CountGammaEnergy;

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

#endif


