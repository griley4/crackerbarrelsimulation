
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef TrackingAction_h
#define TrackingAction_h 1

#include "G4UserTrackingAction.hh"
#include "globals.hh"

class CBDetectorConstruction;
class NabRunAction;
class NabEventAction;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class NabTrackingAction : public G4UserTrackingAction {

  public:  
    NabTrackingAction(CBDetectorConstruction*,NabRunAction*,NabEventAction*);
   ~NabTrackingAction() {};
   
    void  PreUserTrackingAction(const G4Track*);   
    void PostUserTrackingAction(const G4Track*);
    
  private:
    CBDetectorConstruction* detector;
    NabRunAction*            runaction;    
    NabEventAction*          eventaction;
    
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
