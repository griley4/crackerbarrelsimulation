///////////////////////////////////////////////////////////////
//Name: NabField.hh
//Description: Responsible for the Electromagnetic Field of the Detector
///////////////////////////////////////////////////////////////

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

#ifndef NabField_h
#define NabField_h 1

#include "globals.hh"
#include "G4Field.hh"
#include "G4ElectroMagneticField.hh"

class NabEMFieldMap;
class NabBFieldMap;

class NabField : public G4ElectroMagneticField
{
public:

  //Constructor and Destructor
  NabField(char* fieldfile, G4bool flag, G4bool GlobalFieldMapFlagFcn);
  
  ~NabField();
  
  //Field has electric component => true
  //Pure Magnetic Field => false
  G4bool DoesFieldChangeEnergy() const { return true;};
  
  //Main method of NabField.
  //Called by the field manager to determine the field at any point in time
  void GetFieldValue(const G4double Point[4], G4double* Bfield) const;

  //this function invokes ferenc's code
  void FERENC();

  void SetUseInterpolate(G4bool Flag) {UseInterpolate = Flag;};
  void SetUseGlobalFieldMap(G4bool flag) {GlobalFieldMapFlag = flag;};

private:
  char* filename;
  NabEMFieldMap* GlobalFieldMap;
  NabBFieldMap* FilterFieldMap;
  NabBFieldMap* UpperDriftRegionFieldMap;
  NabBFieldMap* LowerDriftRegionFieldMap;
  NabEMFieldMap* UpperDetectorRegionFieldMap;
  NabEMFieldMap* LowerDetectorRegionFieldMap;
  NabEMFieldMap* UEFRegionFieldMap;
  NabEMFieldMap* LEFRegionFieldMap;
  G4bool UseInterpolate;
  G4bool GlobalFieldMapFlag;

  G4double DriftR_UEFR_Boundary;
  G4double DriftR_LEFR_Boundary;
  G4double UpperDriftR_Pinch_Boundary;
  G4double LowerDriftR_Pinch_Boundary;
  G4double UDR_UEFR_Boundary;
  G4double LDR_LEFR_Boundary;
  G4double UD_Boundary;
  G4double LD_Boundary;
  
  G4double DriftRegionRadius;
  G4double DetectorRegionRadius;
  G4double PinchRadius;

  G4double Delta;
  

};

  
#endif
