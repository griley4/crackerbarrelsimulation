// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

//Tong: no need to include "targetver.h" in Linux
//#include "targetver.h"

//Stefan: should not use stdio.h, but G4cout and such
// #include <stdio.h>

//Tong: no need to include "tchar.h"
//#include <tchar.h>

#include <stdlib.h>
#define _USE_MATH_DEFINES 1
#include <math.h>
#include <string.h>
#include <time.h>

//Quantities required for functions in headers "magfield3.c"
//and "fieldelcd3.c" which are needed to calculate the fields
//at arbitrary points in space given by file "inputcoil"
//and coil and electrode files "magsource.dat" and "elsource.dat".
// #define M_PI 3.14159265358979323846
#define Ncoilmax 100
#define nmaxmag 300
#define Nspmagmax 3000
#define Ngroup 1
#define Nmax 5000 //9000
#define nmax 1100
#define INTNUM 20
#define Nspmax 10000
// TODO: reference additional headers your program requires here
