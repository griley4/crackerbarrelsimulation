///////////////////////////////////////////////////////////////
//Name: NabPrimaryGeneratorMessenger.hh
//Description: UI Messenger for NabPrimaryGeneratorAction
///////////////////////////////////////////////////////////////

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

#ifndef NabPrimaryGeneratorMessenger_h
#define NabPrimaryGeneratorMessenger_h 1

#include "G4UImessenger.hh"
#include "globals.hh"

class NabPrimaryGeneratorAction;
class G4UIdirectory;
class G4UIcmdWithoutParameter;
class G4UIcmdWithABool;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADouble;


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

class NabPrimaryGeneratorMessenger: public G4UImessenger
{
public:
  NabPrimaryGeneratorMessenger(NabPrimaryGeneratorAction*);
  ~NabPrimaryGeneratorMessenger();
  
  void SetNewValue(G4UIcommand*, G4String);
  
private:
  NabPrimaryGeneratorAction* Action;
  
  G4UIdirectory*             gunDir;
  G4UIdirectory*             nabSimDir;

  G4UIcmdWithoutParameter*   DefaultCmd;
  G4UIcmdWithoutParameter*   ManualModeCmd;
  G4UIcmdWithABool*          RunProtonCmd;
  G4UIcmdWithABool*          RunElectronCmd;
  G4UIcmdWithoutParameter*   DiscreteSpectrumCmd;
  G4UIcmdWithoutParameter*   NeutronDecayCmd;
  G4UIcmdWithoutParameter*   MonoEnergeticCmd;
  G4UIcmdWithoutParameter*   WithCoefficientsCmd;
  G4UIcmdWithoutParameter*   ProtonResponseCmd;
  G4UIcmdWithoutParameter*   DecayBeamNoEdgeCmd;
  G4UIcmdWithADoubleAndUnit* ProtonEnergyCmd;
  G4UIcmdWithADoubleAndUnit* ElectronEnergyCmd;
  
  G4UIcmdWithADoubleAndUnit* RingRadiusCmd;
  
  G4UIcmdWithAnInteger*      ForceEventCmd;
  G4UIcmdWithADouble*        LittleACmd;
  G4UIcmdWithADouble*        LittleBCmd;
  G4UIcmdWithABool*          RunForwardProtonCmd;
  G4UIcmdWithAnInteger*      ChangeSeedCmd;

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

#endif
