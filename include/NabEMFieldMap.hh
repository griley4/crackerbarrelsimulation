///////////////////////////////////////////////////////////////
//Name: NabEMFieldMap.hh
//Description: Field Map for electro-magnetic fields
///////////////////////////////////////////////////////////////

#ifndef NabEMFieldMap_h
#define NabEMFieldMap_h 1

#include "globals.hh"
#include <fstream>

class NabEMFieldMap
{
public:

  NabEMFieldMap();//do not use
  NabEMFieldMap(G4String name, G4String FerencFile, G4double z1, G4double z2, G4double rmax, G4double Delta);
  ~NabEMFieldMap();
  
  void SetFieldMapName(G4String newName);
  void LoadFieldMap();
  void Interpolate(G4double Zpos, G4double Rpos, G4double* Bz, G4double* Br, G4double* Ez, G4double* Er);
  void SetMapDimensions(G4double R, G4double Delta, G4double Z1, G4double Z2);
  void ApproximateTop();
  void ApproximateBottom();
  
  
private:
  
  void MakeFieldMap();

private:

  G4double GridDelta;
  G4double InverseGridDelta;
  G4double GridR;
  G4double GridZ1;
  G4double GridZ2;
  G4double* BFieldMapR;
  G4double* BFieldMapZ;
  G4double* EFieldMapR;
  G4double* EFieldMapZ;
  G4double* PhiFieldMapZ;
  std::fstream FieldMapFile;
  G4String FieldMapFileName;
  G4String FerencFilename;
  G4int NbCoilMax;
  G4bool ApproximateTopFlag;
  G4bool ApproximateBottomFlag;

  G4int RMax, ZMax;
};

#endif
