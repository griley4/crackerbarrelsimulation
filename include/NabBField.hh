///////////////////////////////////////////////////////////////
//Name: NabBField.hh
//Description: Interface to ferenc's routines for BField only
///////////////////////////////////////////////////////////////

//In the current version NabBField does not call FERENC()
//May lead to improper functioning if FERENC() is not called by previous regular NabField!!!!

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

#ifndef NabBField_h
#define NabBField_h 1

#include "globals.hh"
#include "G4Field.hh"
#include "G4MagneticField.hh"

class NabBField : public G4MagneticField
{
public:

  //Constructor and Destructor
  NabBField(char* fieldfile);
  
  ~NabBField();
  
  //Field has electric component => true
  //Pure Magnetic Field => false
  G4bool DoesFieldChangeEnergy() const { return false;};
  
  //Main method of NabBField.
  //Called by the field manager to determine the field at any point in time
  void GetFieldValue(const G4double Point[4], G4double* Bfield) const;

  //this function invokes ferenc's code
  void FERENC();

private:
  char* filename;

};

  
#endif
