//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// $Id: G4SecondaryElectronEmission.hh,v 1.0 2011-08-11 12:08:26 jfw3eu Exp $
// GEANT4 tag $Name: geant4-09-04-patch-01 $
//
////////////////////////////////////////////////////////////////////////
// Secondary Electron Emission Class Definition
////////////////////////////////////////////////////////////////////////
//
// File:        G4SecondaryElectronEmission.hh
// Description: PostStep Discrete Process - Generation of Secondary Electrons
// Version:     1.0
// Created:     2011-08-11
// Author:      James Wu
// Updated:     
//
// mail:        james.jimmy.wu@gmail.com
//
////////////////////////////////////////////////////////////////////////



#ifndef G4SecondaryElectronEmission_h
#define G4SecondaryElectronEmission_h 1

#include "globals.hh"
#include "templates.hh"
#include "geomdefs.hh"
#include "Randomize.hh"

#include "G4Step.hh"
#include "G4VDiscreteProcess.hh"
#include "G4DynamicParticle.hh"
#include "G4Material.hh"
#include "G4LogicalBorderSurface.hh"
#include "G4LogicalSkinSurface.hh"
#include "G4OpticalSurface.hh"
//#include "G4OpticalPhoton.hh"
#include "G4Gamma.hh"
#include "G4Electron.hh"
#include "G4Proton.hh"

#include "G4TransportationManager.hh"

enum G4SEEStatus {  Undefined, GeneratingElectrons,
                                  Absorption, Detection, NotAtBoundary,
                                  SameMaterial, StepTooSmall };


class G4SecondaryElectronEmission : public G4VDiscreteProcess
{

public: // Without description

        ////////////////////////////////
        // Constructors and Destructor
        ////////////////////////////////

        G4SecondaryElectronEmission(const G4String& processName = "SEE",
                                     G4ProcessType type = fUserDefined);

	~G4SecondaryElectronEmission();

	////////////
	// Methods
        ////////////

public:
        /////////////////////
        // **Inline methods**
        ///////////////////////

        // returns true for charged particles, false otherwise

        G4bool IsApplicable (const G4ParticleDefinition& p);
  
        G4SEEStatus GetStatus() const;
        // Returns the current status.

	//G4double GetVerbosity() const;
	// Returns the process verbosity

        /////////////////////////////////////////////////////
	// **Methods defined in G4SecondaryElectronEmission.cc
        /////////////////////////////////////////////////////

	G4double GetMeanFreePath(const G4Track& ,
				 G4double ,
				 G4ForceCondition* condition);
        // Returns infinity; i. e. the process does not limit the step,
        // but sets the 'Forced' condition for the DoIt to be invoked at
        // every step. However, only at a boundary will any action be
        // taken.

	G4VParticleChange* PostStepDoIt(const G4Track& aTrack,
				       const G4Step&  aStep);
        // This is the method implementing boundary processes.

private:

        ////////////
        // Inline method
        //////////////
        void SEEVerbose(void) const;

private:

        ////////////////
        // Private variables
        //////////////////////

        //G4bool   isInitialized;


	G4ThreeVector OldMomentum;

	G4ThreeVector theGlobalNormal;

	G4Material* Material1;
	G4Material* Material2;

	G4SEEStatus theStatus;

        G4double kCarTolerance;

};

////////////////////
// Inline methods
////////////////////


/*inline
G4bool G4SecondaryElectronEmission::IsApplicable(const G4ParticleDefinition& 
					               aParticleType)
{
  //if(&aParticleType == G4OpticalPhoton::OpticalPhoton())
  //return false;
  if(&aParticleType == G4Gamma::Gamma())
	return false;
  else
	return true;

}
*/
inline
G4SEEStatus G4SecondaryElectronEmission::GetStatus() const
{
   return theStatus;
}

/*inline G4double G4SecondaryElectronEmission::GetVerbosity() const
{
   return verboseLevel;
}*/

#endif /* G4SecondaryElectronEmission_h */
