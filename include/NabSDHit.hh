///////////////////////////////////////////////////////////////
//Name: NabSDHit.hh
//Description: Hit class for NabSensitiveDetector
///////////////////////////////////////////////////////////////

//To add an additional variable:
// 1. Add Data member
// 2. Add Getter and Setter
// 3. Use in NabSensitiveDetector and read out in NabEventAction

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

#ifndef SDHit_h
#define SDHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

class NabSDHit : public G4VHit
{
public:
  //Constructor and Destructor
  NabSDHit();
  ~NabSDHit();
  NabSDHit(const NabSDHit&);
  const NabSDHit& operator=(const NabSDHit&);
  G4int operator==(const NabSDHit&) const;
  
  inline void* operator new(size_t);
  inline void  operator delete(void*);
  
  //mandatory functions inherited from G4VHit
  void Draw();
  void Print();
  
public:
  
  //setters
  void SetTrackID     (G4int track)      { trackID = track; };
  void SetEdep        (G4double de)      { edep = de; };
  void SetPos         (G4ThreeVector xyz){ pos = xyz; };
  void SetName        (G4String newname) { name = newname; };
  void SetTimeGlobal  (G4double nt)      { hitTimeGlobal = nt; };
  void SetTimeLocal   (G4double nt)      { hitTimeLocal = nt; };
  void SetParentID    (G4int track)      { parentID = track; };
  void SetProcessName (G4String newname) { processName = newname; };
  void SetBounceE     (G4int val)        { bounceE = val; };
  void SetBounceP     (G4int val)        { bounceP = val; };
  void SetParticleType(G4int val)        { particleType = val;};
  void SetPixelPos    (G4ThreeVector xyz){ pixelPos = xyz; };
  void SetPixelNb     (G4int Nb)         { pixelNb = Nb; };
  void SetAngle       (G4double newangle){ angle = newangle; };
  
  //getters
  G4int GetTrackID()         { return trackID; };
  G4double GetEdep()         { return edep; };      
  G4ThreeVector GetPos()     { return pos; };
  G4String GetName()         { return name; };
  G4double GetTimeGlobal()   { return hitTimeGlobal; };          
  G4double GetTimeLocal()    { return hitTimeLocal; };
  G4int GetParentID()        { return parentID; };  
  G4String GetProcessName()  { return processName; };
  G4int GetParticleType()    { return particleType;};
  G4ThreeVector GetPixelPos(){ return pixelPos; };
  G4int GetPixelNb()         { return pixelNb; };
  G4int GetBounceE()         { return bounceE; };
  G4int GetBounceP()         { return bounceP; };
  G4double GetAngle()        { return angle; };
  
private:
  
  //data members
  G4int         trackID;
  G4double      edep;
  G4ThreeVector pos;
  G4String      name;
  G4double      hitTimeGlobal;
  G4double      hitTimeLocal;
  G4int         parentID;
  G4String      processName;
  G4int         bounceE;
  G4int         bounceP;
  G4int         particleType; //using int to increase speed 1 = proton, 2 = electron
  G4ThreeVector pixelPos;
  G4int         pixelNb;
  G4double      angle;
  
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

typedef G4THitsCollection<NabSDHit> NabSDHitsCollection;

extern G4Allocator<NabSDHit> NabSDHitAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

inline void* NabSDHit::operator new(size_t)
{
  void *aHit;
  aHit = (void *) NabSDHitAllocator.MallocSingle();
  return aHit;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

inline void NabSDHit::operator delete(void *aHit)
{
  NabSDHitAllocator.FreeSingle((NabSDHit*) aHit);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

#endif
