///////////////////////////////////////////////////////////////
//Name: NabDetectorMessenger.hh
//Description: UI Messenger for CBDetectorConstruction
///////////////////////////////////////////////////////////////


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef NabDetectorMessenger_h
#define NabDetectorMessenger_h 1

#include "G4UIcmdWithAString.hh"
#include "G4UImessenger.hh"
#include "globals.hh"

class CBDetectorConstruction;
class G4UIdirectory;
class G4UIcmdWithABool;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class NabDetectorMessenger: public G4UImessenger
{
public:
  NabDetectorMessenger(CBDetectorConstruction* );
  ~NabDetectorMessenger();
    
  void SetNewValue(G4UIcommand*, G4String);
  
private:
  CBDetectorConstruction*   Detector;
  
  G4UIdirectory*             NabSimDir;
  G4UIdirectory*             detDir;
  
  G4UIcmdWithABool*          GridCmd;
  G4UIcmdWithABool*          CoilCmd;
  G4UIcmdWithABool*          ElectrodeCmd;
  G4UIcmdWithABool*          FieldCmd;  
  G4UIcmdWithABool*          DriftRegionCmd;  
  G4UIcmdWithABool*          UseFieldMapCmd;

  G4UIcmdWithAString*        ResidualGasCmd;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
