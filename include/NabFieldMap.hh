///////////////////////////////////////////////////////////////
//Name: NabFieldMap.hh
//Description: Field Map for the electric field close to the detector
///////////////////////////////////////////////////////////////

#ifndef NabFieldMap_h
#define NabFieldMap_h 1

#include "globals.hh"
#include <fstream>

class NabFieldMap
{
public:

  NabFieldMap();
  NabFieldMap(G4String name, G4double z1, G4double z2, G4double rmax, G4double Delta);
  ~NabFieldMap();
  
  void SetFieldMapName(G4String newName);
  void LoadFieldMap();
  void Interpolate(G4double Zpos, G4double Rpos, G4double* Ez, G4double* Er);
  void SetMapDimensions(G4double R, G4double Delta, G4double Z1, G4double Z2);
  
private:
  
  void MakeFieldMap();

private:

  G4double GridDelta;
  G4double InverseGridDelta;
  G4double GridR;
  G4double GridZ1;
  G4double GridZ2;
  G4double* FieldMapR;
  G4double* FieldMapZ;
  std::fstream FieldMapFile;
  G4String FieldMapFileName;

  G4int RMax, ZMax;
};

#endif
