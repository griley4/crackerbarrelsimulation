///////////////////////////////////////////////////////////////
//Name: NabSensitiveDetector.hh
//Description: Used to flag volumes as sensitive in CBDetectorConstruction
///////////////////////////////////////////////////////////////

#ifndef SensitiveDetector_h
#define SensitiveDetector_h 1

#include "G4VSensitiveDetector.hh"
#include "NabSDHit.hh"

class G4Step;
class G4HcofThisEvent;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

// Sensitive Detector class derived from G4VSensitiveDetector
class NabSensitiveDetector : public G4VSensitiveDetector
{
public:

  //constructor and destructor
  NabSensitiveDetector(G4String, G4String);
  ~NabSensitiveDetector();
  
  //mandatory functions
  void Initialize(G4HCofThisEvent*);
  G4bool ProcessHits(G4Step*, G4TouchableHistory*);
  void EndOfEvent(G4HCofThisEvent*);
  
private:
  //Hits collection pointer and its ID
  NabSDHitsCollection* trackerCollection;
  G4int HCID;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif


