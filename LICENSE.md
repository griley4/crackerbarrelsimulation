Copyright 2017 University of Virgnia.  All rights reserved.

Additionally, this program is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License as published by 
the Free Software Foundation; version 2.0 of the License. Accordingly, this 
program is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
A PARTICULAR PURPOSE. See the GNU General Public License for more details.