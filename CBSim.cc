///////////////////////////////////////////////////////////////
//Name: CBSim.cc
//Description: Main program of the CB Simulation
//Heavily derived from NabSimulation by Jason Fry Ph.D.
///////////////////////////////////////////////////////////////

#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "Randomize.hh"

#include "CBDetectorConstruction.hh"
#include "NabPhysicsList.hh"
#include "NabPrimaryGeneratorAction.hh"
#include "NabRunAction.hh"
#include "NabEventAction.hh"
#include "NabTrackingAction.hh"
#include "NabSteppingAction.hh"
#include "NabSteppingVerbose.hh"
#include "NabStackingAction.hh"

#include "G4UnitsTable.hh"
#include <iostream>
#include <fstream>
#include <string>
#include <unistd.h>

#ifdef G4VIS_USE
#include "G4VisExecutive.hh"
#endif

#ifdef G4UI_USE
#include "G4UIExecutive.hh"
#endif

//#include <stdio.h> //inlcude this and the next to make the program exit once a nan or floating point exception occurs
//#include <fenv.h>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

int main(int argc, char** argv)
{

  //feenableexcept (FE_INVALID | FE_OVERFLOW); //inlcude this to make the program exit once a nan or floating point exception occurs
  
  const int hostnameLength=128;
  char hostname[hostnameLength];
  if (!gethostname(hostname, hostnameLength-1))
    printf("Executing on: %s\n", hostname);
  else G4cout << "Couldn't retrieve hostname" << G4endl;
  G4cout << "PID: " << getpid() << G4endl;
  
  // Check Validity of user Input
  //********************************************
  
  //number of expected arguments
  const G4int NArguments = 4;

  if(argc > NArguments + 2)
    {//check for too many args
      G4cout << "Too many arguments." << G4endl;
      G4cout << "Usage: CBSim <Field sufffix> <output filename> <random gen seed>" << G4endl;
      G4cout << "See README for details" << G4endl;
      G4cout << "Exiting Program" << G4endl;
      return -1;
    }
  if(argc < NArguments)
    {//check for too few args
      G4cout << "Too few arguments." << G4endl;
      G4cout << "Usage: CBSim <Field sufffix> <output filename> <random gen seed>" << G4endl;
      G4cout << "See README for details" << G4endl;
      G4cout << "Exiting Program" << G4endl;
      return -1;
    }

 
  //initiate batch mode if there is NArguments+1 args
  //otherwise run in interactive mode
  G4bool BatchMode = false;
  if(argc == NArguments + 1 || argc == NArguments + 2)
    {
      G4cout << "Running CBSim in Batch mode:" << G4endl;
      BatchMode = true;
    }
  else if(argc == NArguments)
    {
      G4cout << "Running CBSim in Interactive mode:" << G4endl;
    }  
  else
    {
      G4cout << "Jokes on you..." <<G4endl;
      return -1;
    }

  //File strings
  G4String OutputFilePrefix  = argv[2];
  G4String FieldFileSuffix = argv[1];
  
  //check for existence of files
  std::ifstream testfile;
  G4String electrodeFileName = "inputelectrodes_" + FieldFileSuffix;
  testfile.open((char*)electrodeFileName.data(), std::ios_base::out | std::ios_base::in);
  if(!testfile.is_open())
    {
	G4cout << "Cannot find file inputelectrodes_" << FieldFileSuffix << G4endl;
	G4cout << "Exiting Program" << G4endl;
	return -1;
    }
  testfile.close();
  G4String detectorFileName = "detector_" + FieldFileSuffix;
  testfile.open((char*)detectorFileName.data(), std::ios_base::out | std::ios_base::in);
  if(!testfile.is_open())
    {
      G4cout << "Cannot find file detector_" << FieldFileSuffix << G4endl;
      G4cout << "Exiting Program" << G4endl;
      return -1;
    }
  testfile.close();

  //initialize to 0 to prevent undesired operation of resetrandomnumbergenerator() in CBPrimaryGeneratorAction
  G4int seed = 0;

  if(atol(argv[3])) 
    {
      seed = atol(argv[3]);
      G4cout << "received seed: " << seed << G4endl;
    }
  else 
    {
      G4cout << "Invalid entry for <random gen seed> \n "
	     << "please enter an integer \n"
	     << "See README for details \n" 
	     << "Exiting Program" 
	     << G4endl;
      return -1;
    }
 
  // Create necessary user classes
  //********************************************

  //choose the Random engine
  //CLHEP::HepRandom::setTheEngine(new CLHEP::RanecuEngine);

  //my Verbose output class
  G4VSteppingVerbose::SetInstance(new NabSteppingVerbose);

  // Construct the default run manager
  G4RunManager * runManager = new G4RunManager;

  //make detector
  std::ifstream detectorDefFile(detectorFileName);
  std::string stdline;
  G4int counter = 0;
  G4double lowerDetectorPosition = -1000000;
  G4double upperDetectorPosition = -1000000;
  while(getline(detectorDefFile, stdline))
    {
      G4String line(stdline.data());
      if(line.contains("#")) continue;
      if(counter == 0) 
	{
	  lowerDetectorPosition = atof(line);
	  counter++;
	  continue;
	}
      if(counter == 1)
	{
	  upperDetectorPosition = atof(line);
	  counter++;
	}

    }
  
  if(lowerDetectorPosition ==  -1000000 || upperDetectorPosition ==  -1000000)
    {
      G4cout << "no value for upper or lower detector position \n"
	     << "check detector_* file" << G4endl;
      return -1;
    }

  detectorDefFile.close();


  G4cout << "Lower Detector at: " << G4BestUnit(lowerDetectorPosition, "Length")
	 << G4endl;
  G4cout << "Upper Detector at: " << G4BestUnit(upperDetectorPosition, "Length")
	 << G4endl;
   

  CBDetectorConstruction* detector;
  detector = new CBDetectorConstruction(FieldFileSuffix);
  detector->SetLDZfront(lowerDetectorPosition);
  detector->SetUDZfront(upperDetectorPosition);

  runManager->SetUserInitialization(detector);

  //physics 
  //NabPhysicsList* physics = new NabPhysicsList;
  NabPhysicsList* physics = new NabPhysicsList;
  //physics->AddPhysicsList("empenelope");
  physics->AddPhysicsList("emlivermore");
  runManager->SetUserInitialization(physics);

  // set user action classes
  //
  //primaryGenerator
  NabPrimaryGeneratorAction* primary = new NabPrimaryGeneratorAction(detector, seed);
  runManager->SetUserAction(primary);

  //runAction
  NabRunAction* runaction = new NabRunAction(detector, primary);
  runManager->SetUserAction(runaction);

  //eventAction
  NabEventAction* eventaction = new NabEventAction(runaction);
  eventaction->SetOutputFilePrefix(OutputFilePrefix);
  eventaction->AutoOverwriteOutputFile(false);
  if (atoi(getenv("G4AUTOOVERWRITEOUTPUT"))==1) eventaction->AutoOverwriteOutputFile(true);
  if (argc == NArguments + 2) eventaction->AutoOverwriteOutputFile(true);

  runManager->SetUserAction(eventaction);

  //trackAction
  NabTrackingAction* trackingaction = new NabTrackingAction(detector, runaction,eventaction);
  runManager->SetUserAction(trackingaction);

  //stepAction
  NabSteppingAction* steppingaction = new NabSteppingAction(detector, runaction, eventaction);
  runManager->SetUserAction(steppingaction);
  
  //stackAction
  NabStackingAction* stackingaction = new NabStackingAction(runaction, eventaction);
  runManager->SetUserAction(stackingaction);

  // get the pointer to the User Interface manager 
  G4UImanager* UI = G4UImanager::GetUIpointer();  

 
  //initiate batch mode
  if(BatchMode)
    {
      UI->ApplyCommand("/control/verbose 2");
      G4String command = "/control/execute ";
      G4String fileName = argv[NArguments];
      UI->ApplyCommand(command+fileName);
    }
  //or proceed with interactive session
  else
    {
#ifdef G4VIS_USE
      G4VisManager* visManager = new G4VisExecutive;
      visManager->Initialize();
#endif    
     
#ifdef G4UI_USE
      G4UIExecutive * ui = new G4UIExecutive(argc,argv);      
      ui->SessionStart();
      delete ui;
#endif
     
#ifdef G4VIS_USE
      delete visManager;
#endif     
    }

  // job termination
  //  
  delete runManager;
  
  return 0;
}
