#include "TROOT.h"
#include "TStyle.h"
#include "TTree.h"
#include "TLatex.h"
#include "TGraphErrors.h"
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <math.h>
#include <TArrayD.h>
#include "TH2D.h"
#include "TH1D.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "TMath.h"
#include "TRint.h"
#include "TCanvas.h"
#include "TTreeFormula.h"
#include "TLegend.h"
#include "TFile.h"
#include "TCut.h"
#include "TImage.h"
#include "TSystem.h"
#include "TPaveText.h"
#include "TPaveStats.h"
#include <sys/time.h>
#include <sys/resource.h>
#include "TColor.h"
using namespace std;


void Nab_sim_analyzer_v1()
{
  char* st1 = NULL;

  //cout << "What file would you like to use? \n";
  //cin >> st1;
	
  st1 = "../09052017_10m_forward_1DE_ferencB_witha_withgrid"; //change file name here (without .root)
	
  TString baseline = (Form("%s.root", st1));
					  
  TFile *f1 = new TFile(baseline);
  TTree *tbaseline = (TTree*)f1->Get("dynamicTree");
  Int_t palette[20];
  for (int i=0;i<20;i++) palette[i] = 100-2*(19-i);
  gStyle->SetPalette(20,palette);

  gStyle->SetStatX(0.95); 
  gStyle->SetStatY(1); 
  gStyle->SetStatW(0.12);
  gStyle->SetStatH(0.15);

  ////////////////////////////////////////////////////
  //start to draw can11 (Analyzer Pad 1)
	
  TCanvas *can11 = new TCanvas("can11", "Diagnostic Pad 1", 1100, 800);
  gStyle->SetOptStat(1111);
  can11->Divide(3, 3, 0.005, 0.005);
		
  can11->cd(1)->SetGrid();
  can11->cd(1)->SetLogy();
  tbaseline->Draw("Hit_time>>h1(300, 0, 500)", "TimeOfFlight>0 && Hit_particleType==2 && Hit_detectorNb==1 && Hit_parentID==0", "");
  //TGraph *h1 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *h1 = (TH1D*) gDirectory->Get("h1");
	
  h1->SetMarkerColor(4);
  h1->SetLineColor(4);
  h1->SetMarkerStyle(20);
  h1->SetXTitle("Hit time (ns)");
  h1->GetXaxis()->CenterTitle();
  h1->SetTitle("Hit time e-, top detector");
  can11->Modified();
	
  can11->Update();
  can11->Modified();
		
  can11->cd(2)->SetGrid();
  can11->cd(2)->SetLogy();
  tbaseline->Draw("Hit_time>>h2(500, -50, 850)", "TimeOfFlight>0 && Hit_particleType==2 && Hit_detectorNb==-1 && Hit_parentID==0", "");
  //TGraph *h2 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *h2 = (TH1D*) gDirectory->Get("h2");
	
  h2->SetMarkerColor(4);
  h2->SetLineColor(4);
  h2->SetMarkerStyle(20);
  h2->SetXTitle("Hit time (ns)");
  h2->GetXaxis()->CenterTitle();
  h2->SetTitle("Hit time e-, bottom detector");
  can11->Modified();
	
  can11->Modified();
  can11->Update();
	
  can11->cd(3)->SetGrid();
  can11->cd(3)->SetLogy();
  tbaseline->Draw("TimeOfFlight>>h3(300, 0, 50)", "TimeOfFlight>1 && PFirstHitDetNb==1", "");
  //TGraph *h3 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *h3 = (TH1D*) gDirectory->Get("h3");
	
  h3->SetMarkerColor(4);
  h3->SetLineColor(4);
  h3->SetMarkerStyle(20);
  h3->SetXTitle("TOF (#mu s)");
  h3->GetXaxis()->CenterTitle();
  h3->SetTitle("TOF p, top detector");
  can11->Modified();
	
  can11->Modified();
  can11->Update();
	
  can11->cd(4)->SetGrid();
  can11->cd(4)->SetLogy();
  tbaseline->Draw("TimeOfFlight>>h4(300, 0, 50)", "TimeOfFlight>1 && PFirstHitDetNb==-1", "");
  //TGraph *h4 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *h4 = (TH1D*) gDirectory->Get("h4");
	
  h4->SetMarkerColor(4);
  h4->SetLineColor(4);
  h4->SetMarkerStyle(20);
  h4->SetXTitle("TOF (#mu second)");
  h4->GetXaxis()->CenterTitle();
  h4->SetTitle("TOF p, bottom detector");
  can11->Modified();
	
  can11->Modified();
  can11->Update();
	
  can11->cd(5)->SetGrid();
  tbaseline->Draw("EDepElectron>>h5()", "TimeOfFlight>0 && Hit_particleType==2 && Hit_detectorNb==1", "");
  //TGraph *h5 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *h5 = (TH1D*) gDirectory->Get("h5");
	
  h5->SetMarkerColor(4);
  h5->SetLineColor(4);
  h5->SetMarkerStyle(20);
  h5->SetXTitle("Deposited electron energy (keV)");
  h5->GetXaxis()->CenterTitle();
  h5->SetTitle("Deopsited electron energy, top detector");
  can11->Modified();
	
  can11->Modified();
  can11->Update();
	
  can11->cd(6)->SetGrid();
  tbaseline->Draw("EDepElectron>>h6()", "TimeOfFlight>0 && Hit_particleType==2 && Hit_detectorNb==-1", "");
  //TGraph *h6 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *h6 = (TH1D*) gDirectory->Get("h6");
	
  h6->SetMarkerColor(4);
  h6->SetLineColor(4);
  h6->SetMarkerStyle(20);
  h6->SetXTitle("Deposited electron energy (keV)");
  h6->GetXaxis()->CenterTitle();
  h6->SetTitle("Deopsited electron energy, bottom detector");
  can11->Modified();
	
  can11->Modified();
  can11->Update();
	
  can11->cd(7)->SetGrid();
  can11->cd(7)->SetLogx();
  tbaseline->Draw("EDepElectron:Hit_time>>h7(1000, 0, 1000, 800, 0, 800)", "TimeOfFlight>0 && Hit_particleType==2", "COLZ");
  //TGraph *h7 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *h7 = (TH1D*) gDirectory->Get("h7");
	
  h7->SetMarkerColor(4);
  h7->SetLineColor(4);
  h7->SetMarkerStyle(20);
  h7->SetXTitle("Hit Time (ns)");
  h7->GetXaxis()->CenterTitle();
  h7->SetYTitle("Deposited Electron Energy (keV)");
  h7->GetYaxis()->CenterTitle();
  h7->SetTitle("Electron Energy deposited vs trigger time");
  can11->Modified();
	
  can11->Modified();
  can11->Update();
	
  can11->cd(8)->SetGrid();
  tbaseline->Draw("pP0z>>h8()", "TimeOfFlight>0 && PmidPreZ>0", ""); //TimeOfFlight>0
  //TGraph *h8 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *h8 = (TH1D*) gDirectory->Get("h8");
	
  h8->SetMarkerColor(4);
  h8->SetLineColor(4);
  h8->SetMarkerStyle(20);
  h8->SetXTitle("Proton p_{z} (cos(#theta_{p}))");
  h8->GetXaxis()->CenterTitle();
  h8->SetTitle("Proton p_{z} (cos(#theta_{p}))");
  can11->Modified();
	
  can11->Modified();
  can11->Update();
	
  can11->cd(9)->SetGrid();
  tbaseline->Draw("pE0>>h9()", "PFirstHitDetNb!=0", "");
  //TGraph *h9 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *h9 = (TH1D*) gDirectory->Get("h9");
	
  h9->SetMarkerColor(4);
  h9->SetLineColor(4);
  h9->SetMarkerStyle(20);
  h9->SetXTitle("Proton Energy");
  h9->GetXaxis()->CenterTitle();
  h9->SetTitle("Proton Energy");
  can11->Modified();
					  
  can11->Modified();
  can11->Update();
					  
  //end drawing can11 (Residual Pad 1)
  ////////////////////////////////////////////////////
	
  ////////////////////////////////////////////////////
  //start to draw can12 (Analyzer Pad 2)
	
  TCanvas *can12 = new TCanvas("can12", "Diagnostic Pad 2", 1100, 800);
  gStyle->SetOptStat(1111);
  can12->Divide(3, 3, 0.005, 0.005);
	
  can12->cd(1)->SetGrid();
  can12->cd(1)->SetLogy();
  tbaseline->Draw("(EenergyBeforeDetector*1000-eE0)>>hh1()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]<0 && eP0z<0", "");
  //TGraph *h1 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hh1 = (TH1D*) gDirectory->Get("hh1");
	
  hh1->SetMarkerColor(4);
  hh1->SetLineColor(4);
  hh1->SetMarkerStyle(20);
  hh1->SetXTitle("#Delta E (keV)");
  hh1->GetXaxis()->CenterTitle();
  hh1->SetTitle("Energy Non-conservation, bottom detector, cos(#theta_{e})<0");
	
  can12->Update();
  can12->Modified();
	
  can12->cd(2)->SetGrid();
  can12->cd(2)->SetLogy();
  tbaseline->Draw("(EenergyBeforeDetector*1000-eE0)>>hh2()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]<0 && eP0z>0 && eP0z<0.7", "");
  //TGraph *h2 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hh2 = (TH1D*) gDirectory->Get("hh2");
	
  hh2->SetMarkerColor(4);
  hh2->SetLineColor(4);
  hh2->SetMarkerStyle(20);
  hh2->SetXTitle("#Delta E (keV)");
  hh2->GetXaxis()->CenterTitle();
  hh2->SetTitle("Energy Non-conservation, bottom detector, 0<cos(#theta_{e})<0.7");
  can12->Modified();
	
  can12->Modified();
  can12->Update();
	
  can12->cd(3)->SetGrid();
  can12->cd(3)->SetLogy();
  tbaseline->Draw("(EenergyBeforeDetector*1000-eE0)>>hh3()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]<0 && eP0z>0.7 && eP0z<0.77", "");
  //TGraph *h3 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hh3 = (TH1D*) gDirectory->Get("hh3");
	
  hh3->SetMarkerColor(4);
  hh3->SetLineColor(4);
  hh3->SetMarkerStyle(20);
  hh3->SetXTitle("#Delta E (keV)");
  hh3->GetXaxis()->CenterTitle();
  hh3->SetTitle("Energy Non-conservation, bottom detector, 0.7<cos(#theta_{e})<0.77");
	
  can12->Modified();
	
  can12->Modified();
  can12->Update();
 
  can12->cd(4)->SetGrid();
  can12->cd(4)->SetLogy();
  tbaseline->Draw("(EenergyBeforeDetector*1000-eE0)>>hh4()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]<0 && eP0z>0.77", "");
  //TGraph *h3 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hh4 = (TH1D*) gDirectory->Get("hh4");
	
  hh4->SetMarkerColor(4);
  hh4->SetLineColor(4);
  hh4->SetMarkerStyle(20);
  hh4->SetXTitle("#Delta E (keV)");
  hh4->GetXaxis()->CenterTitle();
  hh4->SetTitle("Energy Non-conservation, bottom detector, cos(#theta_{e})>0.77");
	
  can12->Modified();
	
  can12->Modified();
  can12->Update(); 

  can12->cd(5)->SetGrid();
  can12->cd(5)->SetLogy();
  tbaseline->Draw("(EenergyBeforeDetector*1000-eE0)>>hh5()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]>0 && eP0z<0.7", "");
  //TGraph *h1 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hh5 = (TH1D*) gDirectory->Get("hh5");
	
  hh5->SetMarkerColor(4);
  hh5->SetLineColor(4);
  hh5->SetMarkerStyle(20);
  hh5->SetXTitle("#Delta E (keV)");
  hh5->GetXaxis()->CenterTitle();
  hh5->SetTitle("Energy Non-conservation, top detector, cos(#theta_{e})<0.7");
	
  can12->Update();
  can12->Modified();
	
  can12->cd(6)->SetGrid();
  can12->cd(6)->SetLogy();
  tbaseline->Draw("(EenergyBeforeDetector*1000-eE0)>>hh6()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]>0 && eP0z>0.7 && eP0z<0.77", "");
  //TGraph *h2 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hh6 = (TH1D*) gDirectory->Get("hh6");
	
  hh6->SetMarkerColor(4);
  hh6->SetLineColor(4);
  hh6->SetMarkerStyle(20);
  hh6->SetXTitle("#Delta E (keV)");
  hh6->GetXaxis()->CenterTitle();
  hh6->SetTitle("Energy Non-conservation, top detector, 0.7<cos(#theta_{e})<0.77");
  can12->Modified();
	
  can12->Modified();
  can12->Update();
	
  can12->cd(7)->SetGrid();
  can12->cd(7)->SetLogy();
  tbaseline->Draw("(EenergyBeforeDetector*1000-eE0)>>hh7()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]>0 && eP0z>0.77", "");
  //TGraph *h3 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hh7 = (TH1D*) gDirectory->Get("hh7");
	
  hh7->SetMarkerColor(4);
  hh7->SetLineColor(4);
  hh7->SetMarkerStyle(20);
  hh7->SetXTitle("#Delta E (keV)");
  hh7->GetXaxis()->CenterTitle();
  hh7->SetTitle("Energy Non-conservation, top detector, cos(#theta_{e})>0.77");
	
  can12->Modified();
	
  can12->Modified();
  can12->Update();
			  
  //end drawing can12 (Residual Pad 2)
  ////////////////////////////////////////////////////
/*	
  ////////////////////////////////////////////////////
  //start to draw can13 (Analyzer Pad 3)
	
  TCanvas *can13 = new TCanvas("can13", "Diagnostic Pad 3", 1100, 800);
  gStyle->SetOptStat(1111);
  can13->Divide(3, 3, 0.005, 0.005);

  //gStyle->SetStatW(0.13);
  //gStyle->SetStatH(0.13);

  can13->cd(1)->SetGrid();
  tbaseline->Draw("ElastZ[9]:ElastR[9]>>hhh1(80, 0, 170, 200,-1500, 5500)", "EenergyBeforeDetector==-10^6 && @KillStatus.size()==0", "COLZ");
  //TGraph *h4 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hhh1 = (TH1D*) gDirectory->Get("hhh1");
	
  hhh1->SetMarkerColor(4);
  hhh1->SetLineColor(4);
  hhh1->SetMarkerStyle(20);
  //hh4->SetContour(50);
  hhh1->SetYTitle("Last z coordinate before impact (mm)");
  hhh1->GetYaxis()->CenterTitle();
  hhh1->GetYaxis()->SetTitleOffset(1.4);
  hhh1->GetYaxis()->SetTitleOffset(1.4);
  hhh1->SetXTitle("Last R coordinate before impact (mm)");
  hhh1->GetXaxis()->CenterTitle();
  hhh1->SetTitle("Last R-z for non-trapped, non-detected electrons");
  //hh4->SetStats(kFALSE);
  can13->Modified();
	
  can13->Modified();
  can13->Update();

  can13->cd(2)->SetGrid();
  tbaseline->Draw("PlastZ[9]:PlastR[9]>>hhh2(80, 0, 170, 200,-1500, 5500)", "PenergyBeforeDetector==-10^6 && @KillStatus.size()==0", "COLZ");
  //TGraph *h5 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hhh2 = (TH1D*) gDirectory->Get("hhh2");
	
  hhh2->SetMarkerColor(4);
  hhh2->SetLineColor(4);
  hhh2->SetMarkerStyle(20);
  hhh2->SetYTitle("Last z coordinate before impact (mm)");
  hhh2->GetYaxis()->CenterTitle();
  hhh2->GetYaxis()->SetTitleOffset(1.4);
  hhh2->SetXTitle("Last R coordinate before impact (mm)");
  hhh2->GetXaxis()->CenterTitle();
  hhh2->SetTitle("Last R-z for non-trapped, non-detected protons");
  //hh5->SetStats(kFALSE);
  can13->Modified();
	
  can13->Modified();
  can13->Update();

  gStyle->SetOptFit();
  TF1 *f2 = new TF1("f2","pol1", 0.001, 0.0048);

  can13->cd(3)->SetGrid();
  tbaseline->Draw("1/(TimeOfFlight*TimeOfFlight)>>hhh3()", "TimeOfFlight>0 && eE0>279 && eE0<321 && PFirstHitDetNb==1 && EFirstHitDetNb==-1 && 1/(TimeOfFlight*TimeOfFlight)<0.01", "");
  //TGraph *hh7 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hhh3 = (TH1D*) gDirectory->Get("hhh3");
	
  hhh3->SetMarkerColor(4);
  hhh3->SetLineColor(4);
  hhh3->SetMarkerStyle(20);
  hhh3->SetXTitle("1/TOF^{2} (s^{-2})");
  hhh3->GetXaxis()->CenterTitle();
  hhh3->SetTitle("Fit for a, electron energy = 280 - 320keV");
  hhh3->Fit(f2,"VR");
	
  can13->Modified();

  can13->Modified();
  can13->Update();

  can13->cd(4)->SetGrid();
  can13->cd(4)->SetLogy();
  tbaseline->Draw("(EmidPreZ)>>hhh4()", "EmidPreZ!=0", "");
  //TGraph *h8 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hhh4 = (TH1D*) gDirectory->Get("hhh4");
	
  hhh4->SetMarkerColor(4);
  hhh4->SetLineColor(4);
  hhh4->SetMarkerStyle(20);
  hhh4->SetXTitle("Mid plane z coordinate of electrons (mm)");
  hhh4->GetXaxis()->CenterTitle();
  hhh4->SetTitle("Mid z electrons");
  can13->Modified();
	
  can13->Modified();
  can13->Update();
	
  can13->cd(5)->SetGrid();
  can13->cd(5)->SetLogy();
  tbaseline->Draw("(PmidPreZ)>>hhh5()", "PmidPreZ!=0", "");
  //TGraph *h9 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hhh5 = (TH1D*) gDirectory->Get("hhh5");
	
  hhh5->SetMarkerColor(4);
  hhh5->SetLineColor(4);
  hhh5->SetMarkerStyle(20);
  hhh5->SetXTitle("Mid plane z coordinate of protons (mm)");
  hhh5->GetXaxis()->CenterTitle();
  hhh5->SetTitle("Mid z protons");
  can13->Modified();
	
  can13->Modified();
  can13->Update();
	
  //end drawing can13 (Residual Pad 3)
  ////////////////////////////////////////////////////
*/	
  //string st14, st1test;
	
  //cout << "\n\nHello, testing... \nPress any key to continue! \n"; //if not included, this function thinks the last enter is this cin
  //getline (cin, st1test);
	
  //cout << "Would you like to save the residual pad .pdf? (y or n): ";
  //getline (cin, st14);
  //if (st14 == "y")
  //  {
      //save a ps!
      can11->Print(Form("%s_diagnostics.pdf(", st1));

      can12->Print(Form("%s_diagnostics.pdf)", st1));

      //can13->Print(Form("%s_diagnostics.pdf)", st1));

      //can11->Print(Form("%s_diagnostics.ps(", st1));

      //can12->Print(Form("%s_diagnostics.ps)", st1));

      printf("Finished saving files \n");
	       
   // }
  //else 
   // {
   //   printf("pad not saved \n");
   // }
	
  return;
	
}
