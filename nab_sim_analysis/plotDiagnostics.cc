//Jason Fry, January 2016
//Plots different diagnostics, chosen by the user, for different tests of the simulation. When the program is run, a prompt will come up for the user to choose which diagnostic is to be plotted. The user must input the files to compare in the first strings.

#include "TROOT.h"
#include "TStyle.h"
#include "TTree.h"
#include "TLatex.h"
#include "TGraphErrors.h"
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <math.h>
#include <TArrayD.h>
#include "TH2D.h"
#include "TH1D.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "TMath.h"
#include "TRint.h"
#include "TCanvas.h"
#include "TTreeFormula.h"
#include "TLegend.h"
#include "TFile.h"
#include "TCut.h"
#include "TImage.h"
#include "TSystem.h"
#include "TPaveText.h"
#include "TPaveStats.h"
#include <sys/time.h>
#include <sys/resource.h>
#include "TColor.h"
using namespace std;


void plotDiagnostics()
{
  char* st1 = NULL;
  char* st2 = NULL;
  char* st3 = NULL;
  char* st4 = NULL;
  char* st5 = NULL;
  char* st6 = NULL;
  char* st7 = NULL;
  char* st8 = NULL;
  char* st9 = NULL;
  /*char* st10 = NULL;
  char* st11 = NULL;
  char* st12 = NULL;*/
  
	
  st1 = "01262016_monoE_forward_3stepper_100k_merge";
  st2 = "02162016_monoE_forward_3stepper_eps-4-6_100k_merge";
  st3 = "02162016_monoE_forward_3stepper_eps-4-5_100k_merge";
  st4 = "02162016_monoE_forward_3stepper_eps-3-5_100k_merge";
  st5 = "01262016_monoE_forward_3stepper_eps-6-7_100k_merge";
  st6 = "02162016_monoE_forward_3stepper_maxloop1000_100k_merge";
  /*st7 = "01262016_monoE_forward_3stepper_hmin10nm_1M_merge";
  st8 = "01262016_monoE_forward_3stepper_hmin0p1nm_1M_merge";
  st9 = "01272016_monoE_forward_oldStepper_merge";
  st10 = "01262016_monoE_forward_45stepper_1M_merge";
  st11 = "01262016_monoE_forward_4stepper_1M_merge";
  st12 = "01262016_monoE_forward_3stepper_delta100nm_1M_merge";*/
					  
  TFile *f1 = new TFile(Form("%s.root", st1));
  TTree *tbaseline1 = (TTree*)f1->Get("dynamicTree"); 

  TFile *f2 = new TFile(Form("%s.root", st2));
  TTree *tbaseline2 = (TTree*)f2->Get("dynamicTree");

  TFile *f3 = new TFile(Form("%s.root", st3));
  TTree *tbaseline3 = (TTree*)f3->Get("dynamicTree");

  TFile *f4 = new TFile(Form("%s.root", st4));
  TTree *tbaseline4 = (TTree*)f4->Get("dynamicTree");

  TFile *f5 = new TFile(Form("%s.root", st5));
  TTree *tbaseline5 = (TTree*)f5->Get("dynamicTree");

  TFile *f6 = new TFile(Form("%s.root", st6));
  TTree *tbaseline6 = (TTree*)f6->Get("dynamicTree");

  /*TFile *f7 = new TFile(Form("%s.root", st7));
  TTree *tbaseline7 = (TTree*)f7->Get("dynamicTree");

  TFile *f8 = new TFile(Form("%s.root", st8));
  TTree *tbaseline8 = (TTree*)f8->Get("dynamicTree");

  TFile *f9 = new TFile(Form("%s.root", st9));
  TTree *tbaseline9 = (TTree*)f9->Get("dynamicTree");

  TFile *f10 = new TFile(Form("%s.root", st10));
  TTree *tbaseline10 = (TTree*)f10->Get("dynamicTree");

  TFile *f11 = new TFile(Form("%s.root", st11));
  TTree *tbaseline11 = (TTree*)f11->Get("dynamicTree");

  TFile *f12 = new TFile(Form("%s.root", st12));
  TTree *tbaseline12 = (TTree*)f12->Get("dynamicTree");*/

  string stplot;
  cout << "What do you want to plot? Options are \n\n(1) TOFtop \n(2) TOFbot \n(3) trigetop \n(4) trigebot \n(5) DepEetop \n(6) DepEebot \n(7) Ppz \n(8) DepEp \n(9) DeltaE \n(10) DeltaEpad \n(11) 1/TOF2 \n\n";
  cin >> stplot;

  if (stplot == "1")//TOF top
    {
      printf("Plotting TOF, top detector \n");
      TCanvas *can1 = new TCanvas("can1", "TOF, top detector", 200, 10, 700, 500);
      can1->cd() ->SetLogy();
      can1->cd() ->SetGrid();
      tbaseline1->Draw("TimeOfFlight>>h1(300, 0, 50)", "TimeOfFlight>1 && PFirstHitDetNb==1", "");
      TH1D *h1 = (TH1D*) gDirectory->Get("h1");

      tbaseline4->Draw("TimeOfFlight>>h2(300, 0, 50)", "TimeOfFlight>1 && PFirstHitDetNb==1", "same");
      TH1D *h2 = (TH1D*) gDirectory->Get("h2");

      //tbaseline9->Draw("TimeOfFlight>>h3(300, 0, 50)", "TimeOfFlight>1 && PFirstHitDetNb==1", "same");
      TH1D *h3 = (TH1D*) gDirectory->Get("h3");

      //tbaseline8->Draw("TimeOfFlight>>h4(300, 0, 50)", "TimeOfFlight>1 && PFirstHitDetNb==1", "");
      //TH1D *h4 = (TH1D*) gDirectory->Get("h4");

      
      h1->SetLineColor(1);
      h2->SetLineColor(2);
      h3->SetLineColor(3);
      //h4->SetLineColor(4);
      h1->SetXTitle("Trigger time (ns)");
      h1->GetXaxis()->CenterTitle();
      h1->SetTitle("Trigger time e-, top detector");
      can1->Modified();

      can1->Update();
      TLegend *leg4 = new TLegend(0.70,0.5,0.9,0.82);
      leg4->SetHeader("");
      leg4->SetFillColor(0);
      leg4->SetTextSize(0.03);
      leg4->AddEntry(h1,"3 stepper delta = 10 nm", "l");
      //leg4->AddEntry((TObject*)0, "", "");
      leg4->AddEntry(h2,"3 stepper delta = 100 nm", "l");
      //leg4->AddEntry((TObject*)0, "", "");
      leg4->AddEntry(h3,"old Stepper", "l");
      //leg4->AddEntry((TObject*)0, "", "");
      //leg4->AddEntry(h4,"3 stepper hmin = 0.1 nm", "l");
      leg4->Draw();
      can1->Modified();
    }

  else if (stplot == "4")//trigebot
    {
      printf("Plotting Trigger time e-, bot detector \n");
      TCanvas *can1 = new TCanvas("can1", "Trigger time e-, bot detector", 200, 10, 700, 500);
      can1->cd() ->SetLogy();
      can1->cd() ->SetGrid();
      tbaseline1->Draw("Trigger_time>>h1(300, 0, 500)", "TimeOfFlight>0 && Trigger_particleType==2 && Trigger_detectorNb==-1 && Trigger_parentID==0", "");
      TH1D *h1 = (TH1D*) gDirectory->Get("h1");
      
      tbaseline2->Draw("Trigger_time>>h2(300, 0, 500)", "TimeOfFlight>0 && Trigger_particleType==2 && Trigger_detectorNb==-1 && Trigger_parentID==0", "same");
      TH1D *h2 = (TH1D*) gDirectory->Get("h2");

      tbaseline3->Draw("Trigger_time>>h3(300, 0, 500)", "TimeOfFlight>0 && Trigger_particleType==2 && Trigger_detectorNb==-1 && Trigger_parentID==0", "same");
      TH1D *h3 = (TH1D*) gDirectory->Get("h3");

      tbaseline4->Draw("Trigger_time>>h4(300, 0, 500)", "TimeOfFlight>0 && Trigger_particleType==2 && Trigger_detectorNb==-1 && Trigger_parentID==0", "same");
      TH1D *h4 = (TH1D*) gDirectory->Get("h4");

      tbaseline5->Draw("Trigger_time>>h5(300, 0, 500)", "TimeOfFlight>0 && Trigger_particleType==2 && Trigger_detectorNb==-1 && Trigger_parentID==0", "");
      TH1D *h5 = (TH1D*) gDirectory->Get("h5");
      
      tbaseline6->Draw("Trigger_time>>h6(300, 0, 500)", "TimeOfFlight>0 && Trigger_particleType==2 && Trigger_detectorNb==-1 && Trigger_parentID==0", "same");
      TH1D *h6 = (TH1D*) gDirectory->Get("h6");

      //tbaseline7->Draw("Trigger_time>>h7(300, 0, 500)", "TimeOfFlight>0 && Trigger_particleType==2 && Trigger_detectorNb==-1 && Trigger_parentID==0", "same");
      TH1D *h7 = (TH1D*) gDirectory->Get("h7");

      //tbaseline8->Draw("Trigger_time>>h8(300, 0, 500)", "TimeOfFlight>0 && Trigger_particleType==2 && Trigger_detectorNb==-1 && Trigger_parentID==0", "same");
      TH1D *h8 = (TH1D*) gDirectory->Get("h8");

      h1->SetLineColor(1);
      h2->SetLineColor(2);
      h3->SetLineColor(3);
      h4->SetLineColor(4);
      h5->SetLineColor(5);
      h6->SetLineColor(6);
      h7->SetLineColor(7);
      h8->SetLineColor(8);
      h1->SetXTitle("Trigger time (ns)");
      h1->GetXaxis()->CenterTitle();
      h1->SetTitle("Trigger time e-, bot detector");
      can1->Modified();

      can1->Update();
      TLegend *leg4 = new TLegend(0.70,0.5,0.9,0.82);
      leg4->SetHeader("");
      leg4->SetFillColor(0);
      leg4->SetTextSize(0.03);
      leg4->AddEntry(h1,"3 stepper", "l");
      //leg4->AddEntry((TObject*)0, "", "");
      leg4->AddEntry(h2,"4/5 stepper", "l");
      //leg4->AddEntry((TObject*)0, "", "");
      leg4->AddEntry(h3,"4 stepper", "l");
      //leg4->AddEntry((TObject*)0, "", "");
      leg4->AddEntry(h4,"delta = 100nm", "l");
      leg4->Draw();
      can1->Modified();
    }

  else if (stplot == "9") //DeltaE
    {
      printf("Plotting DeltaE, bot detector \n");
      TCanvas *can1 = new TCanvas("can1", "Delta E for costheta<0.7", 200, 10, 700, 500);
      can1->cd() ->SetLogy();
      can1->cd() ->SetGrid();
      tbaseline1->Draw("(EdetE*1000-eE0)>>h1(1000, -0.997, -0.999)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z<0.7", "");
      TH1D *h1 = (TH1D*) gDirectory->Get("h1");
      printf("test1");
      
      tbaseline2->Draw("(EdetE*1000-eE0)>>h2(1000, -0.997, -0.999)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z<0.7", "same");
      TH1D *h2 = (TH1D*) gDirectory->Get("h2");

      tbaseline3->Draw("(EdetE*1000-eE0)>>h3(1000, -0.997, -0.999)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z<0.7", "same");
      TH1D *h3 = (TH1D*) gDirectory->Get("h3");

      tbaseline4->Draw("(EdetE*1000-eE0)>>h4(1000, -0.997, -0.999)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z<0.7", "same");
      TH1D *h4 = (TH1D*) gDirectory->Get("h4");

      tbaseline5->Draw("(EdetE*1000-eE0)>>h5(1000, -0.997, -0.999)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z<0.7", "");
      TH1D *h5 = (TH1D*) gDirectory->Get("h5");
      
      tbaseline6->Draw("(EdetE*1000-eE0)>>h6(1000, -0.997, -0.999)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z<0.7", "same");
      TH1D *h6 = (TH1D*) gDirectory->Get("h6");

      //tbaseline7->Draw("(EdetE*1000-eE0)>>h7(1000, -0.997, -0.999)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z<0.7", "same");
      //TH1D *h7 = (TH1D*) gDirectory->Get("h7");

      //tbaseline8->Draw("(EdetE*1000-eE0)>>h8(1000, -0.997, -0.999)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z<0.7", "same");
      //TH1D *h8 = (TH1D*) gDirectory->Get("h8");
      
      //tbaseline9->Draw("(EdetE*1000-eE0)>>h9(1000, -0.997, -0.999)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z<0.7", "same");
      TH1D *h9 = (TH1D*) gDirectory->Get("h9");
      printf("test1");
      
      h1->SetLineColor(1);
      h2->SetLineColor(2);
      h3->SetLineColor(3);
      h4->SetLineColor(4);
      h5->SetLineColor(5);
      h6->SetLineColor(6);
      //h7->SetLineColor(7);
      //h8->SetLineColor(8);
      //h9->SetLineColor(9);
      h1->SetXTitle("#Delta E (keV)");
      h1->GetXaxis()->CenterTitle();
      h1->SetTitle("Energy Non-conservation, bottom detector, cos(#theta_{e})<0.7");
      can1->Modified();

      can1->Update();
      TLegend *leg4 = new TLegend(0.70,0.5,0.9,0.82);
      leg4->SetHeader("");
      leg4->SetFillColor(0);
      leg4->SetTextSize(0.03);
      leg4->AddEntry(h1,"3 stepper delta = 10 nm (1M)", "l");
      //leg4->AddEntry((TObject*)0, "", "");
      leg4->AddEntry(h2,"3 stepper delta = 1 nm (1M)", "l");
      //leg4->AddEntry((TObject*)0, "", "");
      leg4->AddEntry(h3,"old Stepper (2400 events)", "l");
      //leg4->AddEntry((TObject*)0, "", "");
      //leg4->AddEntry(h6,"delta = 100nm", "l");
      leg4->Draw();
      can1->Modified();
    }


  else if (stplot == "10")//DeltaEpad
    {
      printf("Plotting Delta pad \n");

      TCanvas *can12 = new TCanvas("can12", "Diagnostic Pad 2", 1100, 800);
      gStyle->SetOptStat(1111);
      can12->Divide(3, 3, 0.005, 0.005);
	
      can12->cd(1)->SetGrid();
      can12->cd(1)->SetLogy();
      tbaseline1->Draw("(EdetE*1000-eE0)>>hh1()", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z<0.7", "");
      //TGraph *h1 = (TGraph*) gPad->GetPrimitive("Graph");
      TH1D *hh1 = (TH1D*) gDirectory->Get("hh1");
	
      hh1->SetMarkerColor(4);
      hh1->SetLineColor(4);
      hh1->SetMarkerStyle(20);
      hh1->SetXTitle("#Delta E (keV), bottom detector, cos(#theta_{e})<0.7");
      hh1->GetXaxis()->CenterTitle();
      hh1->SetTitle("#Delta E, 3stepper delta = 10nm (default)");

      can12->Update();
      can12->Modified();
	
      can12->cd(2)->SetGrid();
      can12->cd(2)->SetLogy();
      tbaseline2->Draw("(EdetE*1000-eE0)>>hh2()", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z<0.7", "");
      //TGraph *h2 = (TGraph*) gPad->GetPrimitive("Graph");
      TH1D *hh2 = (TH1D*) gDirectory->Get("hh2");
	
      hh2->SetMarkerColor(4);
      hh2->SetLineColor(4);
      hh2->SetMarkerStyle(20);
      hh2->SetXTitle("#Delta E (keV), bottom detector, cos(#theta_{e})<0.7");
      hh2->GetXaxis()->CenterTitle();
      hh2->SetTitle("#Delta E, eps 10^{-4}, 10^{-6}");
      can12->Modified();
	
      can12->Modified();
      can12->Update();
      
      can12->cd(3)->SetGrid();
      can12->cd(3)->SetLogy();
      tbaseline3->Draw("(EdetE*1000-eE0)>>hh3()", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z<0.7", "");
      //TGraph *h2 = (TGraph*) gPad->GetPrimitive("Graph");
      TH1D *hh3 = (TH1D*) gDirectory->Get("hh3");
	
      hh3->SetMarkerColor(4);
      hh3->SetLineColor(4);
      hh3->SetMarkerStyle(20);
      hh3->SetXTitle("#Delta E (keV), bottom detector, cos(#theta_{e})<0.7");
      hh3->GetXaxis()->CenterTitle();
      hh3->SetTitle("#Delta E, eps 10^{-4}, 10^{-5}");
      can12->Modified();
	
      can12->Modified();
      can12->Update();

      can12->cd(4)->SetGrid();
      can12->cd(4)->SetLogy();
      tbaseline4->Draw("(EdetE*1000-eE0)>>hh4()", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z<0.7", "");
      TH1D *hh4 = (TH1D*) gDirectory->Get("hh4");
	
      hh4->SetMarkerColor(4);
      hh4->SetLineColor(4);
      hh4->SetMarkerStyle(20);
      hh4->SetXTitle("#Delta E (keV), bottom detector, cos(#theta_{e})<0.7");
      hh4->GetXaxis()->CenterTitle();
      hh4->SetTitle("#Delta E, eps 10^{-3}, 10^{-5}");

      can12->Modified();
	
      can12->Modified();
      can12->Update();

      can12->cd(5)->SetGrid();
      can12->cd(5)->SetLogy();
      tbaseline5->Draw("(EdetE*1000-eE0)>>hh5()", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z<0.7", "");
      TH1D *hh5 = (TH1D*) gDirectory->Get("hh5");
	
      hh5->SetMarkerColor(4);
      hh5->SetLineColor(4);
      hh5->SetMarkerStyle(20);
      hh5->SetXTitle("#Delta E (keV), bottom detector, cos(#theta_{e})<0.7");
      hh5->GetXaxis()->CenterTitle();
      hh5->SetTitle("#Delta E, eps 10^{-6}, 10^{-7}");

      can12->Modified();
	
      can12->Modified();
      can12->Update();

      can12->cd(6)->SetGrid();
      can12->cd(6)->SetLogy();
      tbaseline6->Draw("(EdetE*1000-eE0)>>hh6()", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z<0.7", "");
      TH1D *hh6 = (TH1D*) gDirectory->Get("hh6");
	
      hh6->SetMarkerColor(4);
      hh6->SetLineColor(4);
      hh6->SetMarkerStyle(20);
      hh6->SetXTitle("#Delta E (keV), bottom detector, cos(#theta_{e})<0.7");
      hh6->GetXaxis()->CenterTitle();
      hh6->SetTitle("#Delta E, max loop 10^{3}");

      can12->Modified();
	
      can12->Modified();
      can12->Update();

      can12->cd(5)->SetGrid();
      can12->cd(5)->SetLogy();
      /*tbaseline7->Draw("(EdetE*1000-eE0)>>hh7()", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z<0.7", "");
      //TH1D *hh7 = (TH1D*) gDirectory->Get("hh7");
	
      hh7->SetMarkerColor(4);
      hh7->SetLineColor(4);
      hh7->SetMarkerStyle(20);
      hh7->SetXTitle("#Delta E (keV), bottom detector, cos(#theta_{e})<0.7");
      hh7->GetXaxis()->CenterTitle();
      hh7->SetTitle("#Delta E, 3stepper hmin = 10nm");*/

      can12->Modified();
	
      can12->Modified();
      can12->Update();

      can12->cd(6)->SetGrid();
      can12->cd(6)->SetLogy();
      /*tbaseline8->Draw("(EdetE*1000-eE0)>>hh8()", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z<0.7", "");
      TH1D *hh8 = (TH1D*) gDirectory->Get("hh8");
	
      hh8->SetMarkerColor(4);
      hh8->SetLineColor(4);
      hh8->SetMarkerStyle(20);
      hh8->SetXTitle("#Delta E (keV), bottom detector, cos(#theta_{e})<0.7");
      hh8->GetXaxis()->CenterTitle();
      hh8->SetTitle("#Delta E, 3stepper hmin = 0.1nm");*/

      can12->Modified();
	
      can12->Modified();
      can12->Update();

    }


  else
    {
        printf("Did not recognize input, exiting... \n");
        return;
    } 

  /*
  ////////////////////////////////////////////////////
  //start to draw can11 (Analyzer Pad 1)
	
  TCanvas *can11 = new TCanvas("can11", "Diagnostic Pad 1", 1200, 600);
  gStyle->SetOptStat(1111);
  can11->Divide(2, 1, 0.005, 0.005);
		
  can11->cd(1)->SetGrid();
  //can11->cd(1)->SetLogy();
  tbaseline1->Draw("TimeOfFlight>>h1(300, 0, 41)", "TimeOfFlight>1 && PFirstHitDetNb==1", "");
  TH1D *h1 = (TH1D*) gDirectory->Get("h1");

  tbaseline2->Draw("TimeOfFlight>>h2(300, 0, 41)", "TimeOfFlight>1 && PFirstHitDetNb==1", "same");
  TH1D *h2 = (TH1D*) gDirectory->Get("h2");

  tbaseline3->Draw("TimeOfFlight>>h3(300, 0, 41)", "TimeOfFlight>1 && PFirstHitDetNb==1", "same");
  TH1D *h3 = (TH1D*) gDirectory->Get("h3");

  tbaseline4->Draw("TimeOfFlight>>h4(300, 0, 41)", "TimeOfFlight>1 && PFirstHitDetNb==1", "same");
  TH1D *h4 = (TH1D*) gDirectory->Get("h4");
	
  
  h1->SetMarkerColor(4);
  h1->SetLineColor(1);
  h1->SetMarkerStyle(20);
  h2->SetMarkerColor(4);
  h2->SetLineColor(2);
  h2->SetMarkerStyle(20);
  h3->SetMarkerColor(4);
  h3->SetLineColor(3);
  h3->SetMarkerStyle(20);
  h4->SetMarkerColor(4);
  h4->SetLineColor(4);
  h4->SetMarkerStyle(20);
  h1->SetXTitle("TOF (#mu s)");
  h1->GetXaxis()->CenterTitle();
  h1->SetTitle("TOF top detector");
	
  can11->Update();
  can11->cd(1);
  TLegend *leg4 = new TLegend(0.70,0.5,0.9,0.82);
  leg4->SetHeader("");
  leg4->SetFillColor(0);
  leg4->SetTextSize(0.03);
  leg4->AddEntry(h1,"1#mu step", "l");
  //leg4->AddEntry((TObject*)0, "", "");
  leg4->AddEntry(h2,"10#mu step", "l");
  //leg4->AddEntry((TObject*)0, "", "");
  leg4->AddEntry(h3,"25#mu step", "l");
  //leg4->AddEntry((TObject*)0, "", "");
  leg4->AddEntry(h4,"50#mu step", "l");
  leg4->Draw();
  can11->Modified();
		
  can11->cd(2)->SetGrid();
  //can11->cd(2)->SetLogy();
  tbaseline1->Draw("TimeOfFlight>>h5(300, 0, 41)", "TimeOfFlight>1 && PFirstHitDetNb==-1", "");
  TH1D *h5 = (TH1D*) gDirectory->Get("h5");

  tbaseline2->Draw("TimeOfFlight>>h6(300, 0, 41)", "TimeOfFlight>1 && PFirstHitDetNb==-1", "same");
  TH1D *h6 = (TH1D*) gDirectory->Get("h6");

  tbaseline3->Draw("TimeOfFlight>>h7(300, 0, 41)", "TimeOfFlight>1 && PFirstHitDetNb==-1", "same");
  TH1D *h7 = (TH1D*) gDirectory->Get("h7");

  tbaseline4->Draw("TimeOfFlight>>h8(300, 0, 41)", "TimeOfFlight>1 && PFirstHitDetNb==-1", "same");
  TH1D *h8 = (TH1D*) gDirectory->Get("h8");
	
  
  h5->SetMarkerColor(4);
  h5->SetLineColor(1);
  h5->SetMarkerStyle(20);
  h6->SetMarkerColor(4);
  h6->SetLineColor(2);
  h6->SetMarkerStyle(20);
  h7->SetMarkerColor(4);
  h7->SetLineColor(3);
  h7->SetMarkerStyle(20);
  h8->SetMarkerColor(4);
  h8->SetLineColor(4);
  h8->SetMarkerStyle(20);
  h5->SetXTitle("TOF (#mu s)");
  h5->GetXaxis()->CenterTitle();
  h5->SetTitle("TOF bottom detector");
  can11->Modified();
	
  can11->Update();
  can11->cd(2);
  TLegend *leg5 = new TLegend(0.70,0.5,0.9,0.82);
  leg5->SetHeader("");
  leg5->SetFillColor(0);
  leg5->SetTextSize(0.03);
  leg5->AddEntry(h5,"1#mu step", "l");
  //leg5->AddEntry((TObject*)0, "", "");
  leg5->AddEntry(h6,"10#mu step", "l");
  //leg5->AddEntry((TObject*)0, "", "");
  leg5->AddEntry(h7,"25#mu step", "l");
  //leg5->AddEntry((TObject*)0, "", "");
  leg5->AddEntry(h8,"50#mu step", "l");
  leg5->Draw();
  can11->Modified();
	
  //end drawing can11 (Residual Pad 1)
  ////////////////////////////////////////////////////

  ////////////////////////////////////////////////////
  //start to draw can12 (Analyzer Pad 2)
	
  TCanvas *can12 = new TCanvas("can12", "Diagnostic Pad 2", 1100, 800);
  gStyle->SetOptStat(1111);
  can12->Divide(3, 3, 0.005, 0.005);
	
  can12->cd(1)->SetGrid();
  can12->cd(1)->SetLogy();
  tbaseline4->Draw("(EdetE*1000-eE0)>>hh4(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z<0", "");
  TH1D *hh4 = (TH1D*) gDirectory->Get("hh4");
  tbaseline1->Draw("(EdetE*1000-eE0)>>hh1(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z<0", "same");
  TH1D *hh1 = (TH1D*) gDirectory->Get("hh1");
  tbaseline2->Draw("(EdetE*1000-eE0)>>hh2(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z<0", "same");
  TH1D *hh2 = (TH1D*) gDirectory->Get("hh2");
  tbaseline3->Draw("(EdetE*1000-eE0)>>hh3(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z<0", "same");
  TH1D *hh3 = (TH1D*) gDirectory->Get("hh3");
	
  hh1->SetLineColor(1);
  hh2->SetLineColor(2);
  hh3->SetLineColor(3);
  hh4->SetLineColor(4);
  hh4->SetXTitle("#Delta E (keV)");
  hh4->GetXaxis()->CenterTitle();
  hh4->SetTitle("Energy Non-conservation, bottom detector, cos(#theta_{e})<0");

  can12->Update();
  can12->cd(1);
  TLegend *leg6 = new TLegend(0.75,0.5,0.9,0.82);
  leg6->SetHeader("");
  leg6->SetFillColor(0);
  leg6->SetTextSize(0.03);
  leg6->AddEntry(hh1,"1#mu step", "l");
  //leg6->AddEntry((TObject*)0, "", "");
  leg6->AddEntry(hh2,"10#mu step", "l");
  //leg6->AddEntry((TObject*)0, "", "");
  leg6->AddEntry(hh3,"25#mu step", "l");
  //leg6->AddEntry((TObject*)0, "", "");
  leg6->AddEntry(hh4,"50#mu step", "l");
  leg6->Draw();
  can12->Modified();

	
  can12->cd(2)->SetGrid();
  can12->cd(2)->SetLogy();
  tbaseline1->Draw("(EdetE*1000-eE0)>>hh5(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z>0 && eP0z<0.7", "");
  TH1D *hh5 = (TH1D*) gDirectory->Get("hh5");
  tbaseline2->Draw("(EdetE*1000-eE0)>>hh6(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z>0 && eP0z<0.7", "same");
  TH1D *hh6 = (TH1D*) gDirectory->Get("hh6");
  tbaseline3->Draw("(EdetE*1000-eE0)>>hh7(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z>0 && eP0z<0.7", "same");
  TH1D *hh7 = (TH1D*) gDirectory->Get("hh7");
  tbaseline4->Draw("(EdetE*1000-eE0)>>hh8(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z>0 && eP0z<0.7", "same");
  TH1D *hh8 = (TH1D*) gDirectory->Get("hh8");

  hh5->SetLineColor(1);
  hh6->SetLineColor(2);
  hh7->SetLineColor(3);
  hh8->SetLineColor(4);
  hh5->SetXTitle("#Delta E (keV)");
  hh5->GetXaxis()->CenterTitle();
  hh5->SetTitle("Energy Non-conservation, bottom detector, 0<cos(#theta_{e})<0.7");
  can12->Modified();
	
  can12->Modified();
  TLegend *leg7 = new TLegend(0.75,0.5,0.9,0.82);
  leg7->SetHeader("");
  leg7->SetFillColor(0);
  leg7->SetTextSize(0.03);
  leg7->AddEntry(hh5,"1#mu step", "l");
  //leg7->AddEntry((TObject*)0, "", "");
  leg7->AddEntry(hh6,"10#mu step", "l");
  //leg7->AddEntry((TObject*)0, "", "");
  leg7->AddEntry(hh7,"25#mu step", "l");
  //leg7->AddEntry((TObject*)0, "", "");
  leg7->AddEntry(hh8,"50#mu step", "l");
  leg7->Draw();
  can12->Update();

	
  can12->cd(3)->SetGrid();
  can12->cd(3)->SetLogy();
  tbaseline1->Draw("(EdetE*1000-eE0)>>hj5(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z>0.7 && eP0z<0.77", "");
  TH1D *hj5 = (TH1D*) gDirectory->Get("hj5");
  tbaseline2->Draw("(EdetE*1000-eE0)>>hj6(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z>0.7 && eP0z<0.77", "same");
  TH1D *hj6 = (TH1D*) gDirectory->Get("hj6");
  tbaseline3->Draw("(EdetE*1000-eE0)>>hj7(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z>0.7 && eP0z<0.77", "same");
  TH1D *hj7 = (TH1D*) gDirectory->Get("hj7");
  tbaseline4->Draw("(EdetE*1000-eE0)>>hj8(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z>0.7 && eP0z<0.77", "same");
  TH1D *hj8 = (TH1D*) gDirectory->Get("hj8");

  hj5->SetLineColor(1);
  hj6->SetLineColor(2);
  hj7->SetLineColor(3);
  hj8->SetLineColor(4);
  hj5->SetXTitle("#Delta E (keV)");
  hj5->GetXaxis()->CenterTitle();
  hj5->SetTitle("Energy Non-conservation, bottom detector, 0.7<cos(#theta_{e})<0.77");
  can12->Modified();
	
  can12->Modified();
  TLegend *leg8 = new TLegend(0.75,0.5,0.9,0.82);
  leg8->SetHeader("");
  leg8->SetFillColor(0);
  leg8->SetTextSize(0.03);
  leg8->AddEntry(hj5,"1#mu step", "l");
  //leg8->AddEntry((TObject*)0, "", "");
  leg8->AddEntry(hj6,"10#mu step", "l");
  //leg8->AddEntry((TObject*)0, "", "");
  leg8->AddEntry(hj7,"25#mu step", "l");
  //leg8->AddEntry((TObject*)0, "", "");
  leg8->AddEntry(hj8,"50#mu step", "l");
  leg8->Draw();
  can12->Update();

 
  can12->cd(4)->SetGrid();
  can12->cd(4)->SetLogy();
  tbaseline1->Draw("(EdetE*1000-eE0)>>hk5(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z>0.77", "");
  TH1D *hk5 = (TH1D*) gDirectory->Get("hk5");
  tbaseline2->Draw("(EdetE*1000-eE0)>>hk6(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z>0.77", "same");
  TH1D *hk6 = (TH1D*) gDirectory->Get("hk6");
  tbaseline3->Draw("(EdetE*1000-eE0)>>hk7(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z>0.77", "same");
  TH1D *hk7 = (TH1D*) gDirectory->Get("hk7");
  tbaseline4->Draw("(EdetE*1000-eE0)>>hk8(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z>0.77", "same");
  TH1D *hk8 = (TH1D*) gDirectory->Get("hk8");

  hk5->SetLineColor(1);
  hk6->SetLineColor(2);
  hk7->SetLineColor(3);
  hk8->SetLineColor(4);
  hk5->SetXTitle("#Delta E (keV)");
  hk5->GetXaxis()->CenterTitle();
  hk5->SetTitle("Energy Non-conservation, bottom detector, cos(#theta_{e})>0.77");
  can12->Modified();
	
  can12->Modified();
  TLegend *leg9 = new TLegend(0.75,0.5,0.9,0.82);
  leg9->SetHeader("");
  leg9->SetFillColor(0);
  leg9->SetTextSize(0.03);
  leg9->AddEntry(hk5,"1#mu step", "l");
  //leg9->AddEntry((TObject*)0, "", "");
  leg9->AddEntry(hk6,"10#mu step", "l");
  //leg9->AddEntry((TObject*)0, "", "");
  leg9->AddEntry(hk7,"25#mu step", "l");
  //leg9->AddEntry((TObject*)0, "", "");
  leg9->AddEntry(hk8,"50#mu step", "l");
  leg9->Draw();
  can12->Update();
 

  can12->cd(5)->SetGrid();
  can12->cd(5)->SetLogy();
  tbaseline1->Draw("(EdetE*1000-eE0)>>hl5(300,-30.01, -29.998)", "EdetE!=-10^6 && EimpactPosition[2]>0 && eP0z<0.7", "");
  TH1D *hl5 = (TH1D*) gDirectory->Get("hl5");
  tbaseline2->Draw("(EdetE*1000-eE0)>>hl6(300,-30.08, -30)", "EdetE!=-10^6 && EimpactPosition[2]>0 && eP0z<0.7", "same");
  TH1D *hl6 = (TH1D*) gDirectory->Get("hl6");
  tbaseline3->Draw("(EdetE*1000-eE0)>>hl7(300,-30.08, -30)", "EdetE!=-10^6 && EimpactPosition[2]>0 && eP0z<0.7", "same");
  TH1D *hl7 = (TH1D*) gDirectory->Get("hl7");
  tbaseline4->Draw("(EdetE*1000-eE0)>>hl8(300,-30.08, -30)", "EdetE!=-10^6 && EimpactPosition[2]>0 && eP0z<0.7", "same");
  TH1D *hl8 = (TH1D*) gDirectory->Get("hl8");

  hl5->SetLineColor(1);
  hl6->SetLineColor(2);
  hl7->SetLineColor(3);
  hl8->SetLineColor(4);
  hl5->SetXTitle("#Delta E (keV)");
  hl5->GetXaxis()->CenterTitle();
  hl5->SetTitle("Energy Non-conservation, top detector, cos(#theta_{e})<0.7");
  can12->Modified();

	
  can12->cd(6)->SetGrid();
  can12->cd(6)->SetLogy();
  tbaseline1->Draw("(EdetE*1000-eE0)>>hm5(300,-30.01, -29.998)", "EdetE!=-10^6 && EimpactPosition[2]>0 && eP0z>0.7 && eP0z<0.77", "");
  TH1D *hm5 = (TH1D*) gDirectory->Get("hm5");
  tbaseline2->Draw("(EdetE*1000-eE0)>>hm6(300,-30.08, -30)", "EdetE!=-10^6 && EimpactPosition[2]>0 && eP0z>0.7 && eP0z<0.77", "same");
  TH1D *hm6 = (TH1D*) gDirectory->Get("hm6");
  tbaseline3->Draw("(EdetE*1000-eE0)>>hm7(300-,-30.08, -30)", "EdetE!=-10^6 && EimpactPosition[2]>0 && eP0z>0.7 && eP0z<0.77", "same");
  TH1D *hm7 = (TH1D*) gDirectory->Get("hm7");
  tbaseline4->Draw("(EdetE*1000-eE0)>>hm8(300,-30.08, -30)", "EdetE!=-10^6 && EimpactPosition[2]>0 && eP0z>0.7 && eP0z<0.77", "same");
  TH1D *hm8 = (TH1D*) gDirectory->Get("hm8");

  hm5->SetLineColor(1);
  hm6->SetLineColor(2);
  hm7->SetLineColor(3);
  hm8->SetLineColor(4);
  hm5->SetXTitle("#Delta E (keV)");
  hm5->GetXaxis()->CenterTitle();
  hm5->SetTitle("Energy Non-conservation, top detector, 0.7<cos(#theta_{e})<0.77");
  can12->Modified();
	
  can12->Modified();
  can12->Update();
	
	
  can12->cd(7)->SetGrid();
  can12->cd(7)->SetLogy();
  tbaseline1->Draw("(EdetE*1000-eE0)>>hn5(300,-30.01, -29.998)", "EdetE!=-10^6 && EimpactPosition[2]>0 && eP0z>0.77", "");
  TH1D *hn5 = (TH1D*) gDirectory->Get("hn5");
  tbaseline2->Draw("(EdetE*1000-eE0)>>hn6(300,-30.08, -29.9)", "EdetE!=-10^6 && EimpactPosition[2]>0 && eP0z>0.77", "same");
  TH1D *hn6 = (TH1D*) gDirectory->Get("hn6");
  tbaseline3->Draw("(EdetE*1000-eE0)>>hn7(300,-30.08, -29.9)", "EdetE!=-10^6 && EimpactPosition[2]>0 && eP0z>0.77", "same");
  TH1D *hn7 = (TH1D*) gDirectory->Get("hn7");
  tbaseline4->Draw("(EdetE*1000-eE0)>>hn8(300,-30.08, -29.9)", "EdetE!=-10^6 && EimpactPosition[2]>0 && eP0z>0.77", "same");
  TH1D *hn8 = (TH1D*) gDirectory->Get("hn8");

  hn5->SetLineColor(1);
  hn6->SetLineColor(2);
  hn7->SetLineColor(3);
  hn8->SetLineColor(4);
  hn5->SetXTitle("#Delta E (keV)");
  hn5->GetXaxis()->CenterTitle();
  hn5->SetTitle("Energy Non-conservation, top detector, cos(#theta_{e})>0.77");
  can12->Modified();
	
  can12->Modified();
  TLegend *leg12 = new TLegend(0.75,0.5,0.9,0.82);
  can12->Update();


  //end drawing can12 (Residual Pad 2)
  ////////////////////////////////////////////////////
	
  string st14, st1test;
	
  cout << "\n\nHello, testing... \nPress any key to continue! \n"; //if not included, this function thinks the last enter is this cin
  getline (cin, st1test);
	
  cout << "Would you like to save the residual pad .pdf? (y or n): ";
  getline (cin, st14);
  if (st14 == "y")
    {
      //save a ps!
      can11->Print("diffStepSize.pdf(");

      can12->Print("diffStepSize.pdf)");

      printf("Finished saving files \n");
	       
    }
  else 
    {
      printf("pad not saved \n");
    }
  */	
  
  return;
	
}
