//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon May  2 14:30:02 2016 by ROOT version 5.34/03
// from TTree dynamicTree/dynamicTree
// found on file: 04282016_monoE_forward_ferenc_fieldmap_newsetting_10k.root
//////////////////////////////////////////////////////////

#ifndef Event_comparison_h
#define Event_comparison_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include <vector>
#include <vector>

// Fixed size dimensions of array or collections stored in the TTree if any.
const Int_t kMaxKillStatus = 1;

class Event_comparison {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Int_t           eventID;
   Double_t        x0;
   Double_t        y0;
   Double_t        z0;
   Double_t        pP0x;
   Double_t        pP0y;
   Double_t        pP0z;
   Double_t        pE0;
   Double_t        pE;
   Double_t        eP0x;
   Double_t        eP0y;
   Double_t        eP0z;
   Double_t        eE0;
   Double_t        eE;
   Int_t           bouncesE;
   Int_t           bouncesP;
   Int_t           PFirstHitDetNb;
   Int_t           EFirstHitDetNb;
   Double_t        PFirstHitDetTime;
   Double_t        EFirstHitDetTime;
   Int_t           DeadParticles;
   Int_t           NbOfGamma;
   Int_t           NbOfSE;
   Double_t        GammaEscape;
   Double_t        GammaLoss;
   Double_t        DLLoss;
   Double_t        SELoss;
   Double_t        SEDeposit;
   vector<double>  *Trigger_time;
   vector<double>  *Trigger_x;
   vector<double>  *Trigger_y;
   vector<double>  *Trigger_energy;
   vector<int>     *Trigger_particleType;
   vector<int>     *Trigger_parentID;
   vector<int>     *Trigger_detectorNb;
   Double_t        GammaDeposit;
   vector<int>     *Trigger_bouncesEAtTrigger;
   vector<int>     *Trigger_bouncesPAtTrigger;
   Int_t           PixelStatus;
   Double_t        TimeOfFlight;
   Double_t        TimeOfFlight_proton_cm;
   Double_t        EDepElectron;
   Int_t           KillStatus_;
   Double_t        KillStatus_curr_x[kMaxKillStatus];   //[KillStatus_]
   Double_t        KillStatus_curr_y[kMaxKillStatus];   //[KillStatus_]
   Double_t        KillStatus_curr_z[kMaxKillStatus];   //[KillStatus_]
   Double_t        KillStatus_curr_px[kMaxKillStatus];   //[KillStatus_]
   Double_t        KillStatus_curr_py[kMaxKillStatus];   //[KillStatus_]
   Double_t        KillStatus_curr_pz[kMaxKillStatus];   //[KillStatus_]
   Double_t        KillStatus_curr_E[kMaxKillStatus];   //[KillStatus_]
   Int_t           KillStatus_status[kMaxKillStatus];   //[KillStatus_]
   Int_t           KillStatus_trackID[kMaxKillStatus];   //[KillStatus_]
   Int_t           KillStatus_parentID[kMaxKillStatus];   //[KillStatus_]
   Int_t           KillStatus_particleID[kMaxKillStatus];   //[KillStatus_]
   Double_t        KillStatus_px0[kMaxKillStatus];   //[KillStatus_]
   Double_t        KillStatus_py0[kMaxKillStatus];   //[KillStatus_]
   Double_t        KillStatus_pz0[kMaxKillStatus];   //[KillStatus_]
   Double_t        KillStatus_E0[kMaxKillStatus];   //[KillStatus_]
   Double_t        KillStatus_x0[kMaxKillStatus];   //[KillStatus_]
   Double_t        KillStatus_y0[kMaxKillStatus];   //[KillStatus_]
   Double_t        KillStatus_z0[kMaxKillStatus];   //[KillStatus_]
   Double_t        elapsedTime;
   Double_t        ElastZ[10];
   Double_t        ElastR[10];
   Double_t        ElastE[10];
   Double_t        ElastV[10];
   Double_t        ElastS[10];
   Double_t        ElastDeltaT[10];
   Double_t        EimpactAngleViaPos;
   Double_t        EimpactAngleViaMomentum;
   Double_t        EenergyBeforeDetector;
   Double_t        EimpactPosition[3];
   Double_t        PlastZ[10];
   Double_t        PlastR[10];
   Double_t        PlastE[10];
   Double_t        PlastV[10];
   Double_t        PlastS[10];
   Double_t        PlastDeltaT[10];
   Double_t        PimpactAngleViaPos;
   Double_t        PimpactAngleViaMomentum;
   Double_t        PenergyBeforeDetector;
   Double_t        PimpactPosition[3];
   Double_t        EmidPreX;
   Double_t        EmidPreY;
   Double_t        EmidPreZ;
   Double_t        EmidPostX;
   Double_t        EmidPostY;
   Double_t        EmidPostZ;
   Double_t        EmidE;
   Double_t        EmidT;
   Double_t        EmidImpactAngle;
   Double_t        PmidPreX;
   Double_t        PmidPreY;
   Double_t        PmidPreZ;
   Double_t        PmidPostX;
   Double_t        PmidPostY;
   Double_t        PmidPostZ;
   Double_t        PmidE;
   Double_t        PmidT;
   Double_t        PmidImpactAngle;
   Double_t        EdetPreX;
   Double_t        EdetPreY;
   Double_t        EdetPreZ;
   Double_t        EdetPx;
   Double_t        EdetPy;
   Double_t        EdetPz;
   Double_t        EdetE;
   Double_t        EdetT;
   Double_t        PdetPreX;
   Double_t        PdetPreY;
   Double_t        PdetPreZ;
   Double_t        PdetPx;
   Double_t        PdetPy;
   Double_t        PdetPz;
   Double_t        PdetE;
   Double_t        PdetT;
   Double_t        EdetCMPreX;
   Double_t        EdetCMPreY;
   Double_t        EdetCMPreZ;
   Double_t        EdetCMPostX;
   Double_t        EdetCMPostY;
   Double_t        EdetCMPostZ;
   Double_t        EdetCMPx;
   Double_t        EdetCMPy;
   Double_t        EdetCMPz;
   Double_t        EdetCME;
   Double_t        EdetCMT;
   Double_t        EdetCMPostT;
   Double_t        PdetCMPreX;
   Double_t        PdetCMPreY;
   Double_t        PdetCMPreZ;
   Double_t        PdetCMPostX;
   Double_t        PdetCMPostY;
   Double_t        PdetCMPostZ;
   Double_t        PdetCMPx;
   Double_t        PdetCMPy;
   Double_t        PdetCMPz;
   Double_t        PdetCME;
   Double_t        PdetCMT;
   Double_t        PdetCMPostT;

   // List of branches
   TBranch        *b_eventID;   //!
   TBranch        *b_x0;   //!
   TBranch        *b_y0;   //!
   TBranch        *b_z0;   //!
   TBranch        *b_P0xproton;   //!
   TBranch        *b_P0yproton;   //!
   TBranch        *b_P0zproton;   //!
   TBranch        *b_E0proton;   //!
   TBranch        *b_Eproton;   //!
   TBranch        *b_P0xelectron;   //!
   TBranch        *b_P0yelectron;   //!
   TBranch        *b_P0zelectron;   //!
   TBranch        *b_E0electron;   //!
   TBranch        *b_Eelectron;   //!
   TBranch        *b_bouncesE;   //!
   TBranch        *b_bouncesP;   //!
   TBranch        *b_PFirstHitDetNb;   //!
   TBranch        *b_EFirstHitDetNb;   //!
   TBranch        *b_PFirstHitDetTime;   //!
   TBranch        *b_EFirstHitDetTime;   //!
   TBranch        *b_deadParticles;   //!
   TBranch        *b_NbOfGammas;   //!
   TBranch        *b_NbOfSE;   //!
   TBranch        *b_gammaEscape;   //!
   TBranch        *b_gammaLoss;   //!
   TBranch        *b_DLLoss;   //!
   TBranch        *b_SELoss;   //!
   TBranch        *b_SEDeposit;   //!
   TBranch        *b_Trigger_time;   //!
   TBranch        *b_Trigger_x;   //!
   TBranch        *b_Trigger_y;   //!
   TBranch        *b_Trigger_energy;   //!
   TBranch        *b_Trigger_particleType;   //!
   TBranch        *b_Trigger_parentID;   //!
   TBranch        *b_Trigger_detectorNb;   //!
   TBranch        *b_gammaDeposit;   //!
   TBranch        *b_Trigger_bouncesEAtTrigger;   //!
   TBranch        *b_Trigger_bouncesPAtTrigger;   //!
   TBranch        *b_PixelStatus;   //!
   TBranch        *b_TimeOfFlight;   //!
   TBranch        *b_TimeOfFlight_proton_cm;   //!
   TBranch        *b_EDepElectron;   //!
   TBranch        *b_KillStatus_;   //!
   TBranch        *b_KillStatus_curr_x;   //!
   TBranch        *b_KillStatus_curr_y;   //!
   TBranch        *b_KillStatus_curr_z;   //!
   TBranch        *b_KillStatus_curr_px;   //!
   TBranch        *b_KillStatus_curr_py;   //!
   TBranch        *b_KillStatus_curr_pz;   //!
   TBranch        *b_KillStatus_curr_E;   //!
   TBranch        *b_KillStatus_status;   //!
   TBranch        *b_KillStatus_trackID;   //!
   TBranch        *b_KillStatus_parentID;   //!
   TBranch        *b_KillStatus_particleID;   //!
   TBranch        *b_KillStatus_px0;   //!
   TBranch        *b_KillStatus_py0;   //!
   TBranch        *b_KillStatus_pz0;   //!
   TBranch        *b_KillStatus_E0;   //!
   TBranch        *b_KillStatus_x0;   //!
   TBranch        *b_KillStatus_y0;   //!
   TBranch        *b_KillStatus_z0;   //!
   TBranch        *b_elapsedTime;   //!
   TBranch        *b_ElastZ;   //!
   TBranch        *b_ElastR;   //!
   TBranch        *b_ElastE;   //!
   TBranch        *b_ElastV;   //!
   TBranch        *b_ElastS;   //!
   TBranch        *b_ElastDeltaT;   //!
   TBranch        *b_EimpactAngleViaPos;   //!
   TBranch        *b_EimpactAngleViaMomentum;   //!
   TBranch        *b_EenergyBeforeDetector;   //!
   TBranch        *b_EimpactPosition;   //!
   TBranch        *b_PlastZ;   //!
   TBranch        *b_PlastR;   //!
   TBranch        *b_PlastE;   //!
   TBranch        *b_PlastV;   //!
   TBranch        *b_PlastS;   //!
   TBranch        *b_PlastDeltaT;   //!
   TBranch        *b_PimpactAngleViaPos;   //!
   TBranch        *b_PimpactAngleViaMomentum;   //!
   TBranch        *b_PenergyBeforeDetector;   //!
   TBranch        *b_PimpactPosition;   //!
   TBranch        *b_EmidPreX;   //!
   TBranch        *b_EmidPreY;   //!
   TBranch        *b_EmidPreZ;   //!
   TBranch        *b_EmidPostX;   //!
   TBranch        *b_EmidPostY;   //!
   TBranch        *b_EmidPostZ;   //!
   TBranch        *b_EmidE;   //!
   TBranch        *b_EmidT;   //!
   TBranch        *b_EmidImpactAngle;   //!
   TBranch        *b_PmidPreX;   //!
   TBranch        *b_PmidPreY;   //!
   TBranch        *b_PmidPreZ;   //!
   TBranch        *b_PmidPostX;   //!
   TBranch        *b_PmidPostY;   //!
   TBranch        *b_PmidPostZ;   //!
   TBranch        *b_PmidE;   //!
   TBranch        *b_PmidT;   //!
   TBranch        *b_PmidImpactAngle;   //!
   TBranch        *b_EdetPreX;   //!
   TBranch        *b_EdetPreY;   //!
   TBranch        *b_EdetPreZ;   //!
   TBranch        *b_EdetPx;   //!
   TBranch        *b_EdetPy;   //!
   TBranch        *b_EdetPz;   //!
   TBranch        *b_EdetE;   //!
   TBranch        *b_EdetT;   //!
   TBranch        *b_PdetPreX;   //!
   TBranch        *b_PdetPreY;   //!
   TBranch        *b_PdetPreZ;   //!
   TBranch        *b_PdetPx;   //!
   TBranch        *b_PdetPy;   //!
   TBranch        *b_PdetPz;   //!
   TBranch        *b_PdetE;   //!
   TBranch        *b_PdetT;   //!
   TBranch        *b_EdetCMPreX;   //!
   TBranch        *b_EdetCMPreY;   //!
   TBranch        *b_EdetCMPreZ;   //!
   TBranch        *b_EdetCMPostX;   //!
   TBranch        *b_EdetCMPostY;   //!
   TBranch        *b_EdetCMPostZ;   //!
   TBranch        *b_EdetCMPx;   //!
   TBranch        *b_EdetCMPy;   //!
   TBranch        *b_EdetCMPz;   //!
   TBranch        *b_EdetCME;   //!
   TBranch        *b_EdetCMT;   //!
   TBranch        *b_EdetCMPostT;   //!
   TBranch        *b_PdetCMPreX;   //!
   TBranch        *b_PdetCMPreY;   //!
   TBranch        *b_PdetCMPreZ;   //!
   TBranch        *b_PdetCMPostX;   //!
   TBranch        *b_PdetCMPostY;   //!
   TBranch        *b_PdetCMPostZ;   //!
   TBranch        *b_PdetCMPx;   //!
   TBranch        *b_PdetCMPy;   //!
   TBranch        *b_PdetCMPz;   //!
   TBranch        *b_PdetCME;   //!
   TBranch        *b_PdetCMT;   //!
   TBranch        *b_PdetCMPostT;   //!

   Event_comparison(TTree *tree=0);
   virtual ~Event_comparison();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef Event_comparison_cxx
Event_comparison::Event_comparison(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(Form("%s.root", FILE_NAME));
      if (!f || !f->IsOpen()) {
         f = new TFile(Form("%s.root", FILE_NAME));
      }
      f->GetObject("dynamicTree",tree);

   }
   Init(tree);
}

Event_comparison::~Event_comparison()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t Event_comparison::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t Event_comparison::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void Event_comparison::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   Trigger_time = 0;
   Trigger_x = 0;
   Trigger_y = 0;
   Trigger_energy = 0;
   Trigger_particleType = 0;
   Trigger_parentID = 0;
   Trigger_detectorNb = 0;
   Trigger_bouncesEAtTrigger = 0;
   Trigger_bouncesPAtTrigger = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("eventID", &eventID, &b_eventID);
   fChain->SetBranchAddress("x0", &x0, &b_x0);
   fChain->SetBranchAddress("y0", &y0, &b_y0);
   fChain->SetBranchAddress("z0", &z0, &b_z0);
   fChain->SetBranchAddress("pP0x", &pP0x, &b_P0xproton);
   fChain->SetBranchAddress("pP0y", &pP0y, &b_P0yproton);
   fChain->SetBranchAddress("pP0z", &pP0z, &b_P0zproton);
   fChain->SetBranchAddress("pE0", &pE0, &b_E0proton);
   fChain->SetBranchAddress("pE", &pE, &b_Eproton);
   fChain->SetBranchAddress("eP0x", &eP0x, &b_P0xelectron);
   fChain->SetBranchAddress("eP0y", &eP0y, &b_P0yelectron);
   fChain->SetBranchAddress("eP0z", &eP0z, &b_P0zelectron);
   fChain->SetBranchAddress("eE0", &eE0, &b_E0electron);
   fChain->SetBranchAddress("eE", &eE, &b_Eelectron);
   fChain->SetBranchAddress("bouncesE", &bouncesE, &b_bouncesE);
   fChain->SetBranchAddress("bouncesP", &bouncesP, &b_bouncesP);
   fChain->SetBranchAddress("PFirstHitDetNb", &PFirstHitDetNb, &b_PFirstHitDetNb);
   fChain->SetBranchAddress("EFirstHitDetNb", &EFirstHitDetNb, &b_EFirstHitDetNb);
   fChain->SetBranchAddress("PFirstHitDetTime", &PFirstHitDetTime, &b_PFirstHitDetTime);
   fChain->SetBranchAddress("EFirstHitDetTime", &EFirstHitDetTime, &b_EFirstHitDetTime);
   fChain->SetBranchAddress("DeadParticles", &DeadParticles, &b_deadParticles);
   fChain->SetBranchAddress("NbOfGamma", &NbOfGamma, &b_NbOfGammas);
   fChain->SetBranchAddress("NbOfSE", &NbOfSE, &b_NbOfSE);
   fChain->SetBranchAddress("GammaEscape", &GammaEscape, &b_gammaEscape);
   fChain->SetBranchAddress("GammaLoss", &GammaLoss, &b_gammaLoss);
   fChain->SetBranchAddress("DLLoss", &DLLoss, &b_DLLoss);
   fChain->SetBranchAddress("SELoss", &SELoss, &b_SELoss);
   fChain->SetBranchAddress("SEDeposit", &SEDeposit, &b_SEDeposit);
   fChain->SetBranchAddress("Trigger_time", &Trigger_time, &b_Trigger_time);
   fChain->SetBranchAddress("Trigger_x", &Trigger_x, &b_Trigger_x);
   fChain->SetBranchAddress("Trigger_y", &Trigger_y, &b_Trigger_y);
   fChain->SetBranchAddress("Trigger_energy", &Trigger_energy, &b_Trigger_energy);
   fChain->SetBranchAddress("Trigger_particleType", &Trigger_particleType, &b_Trigger_particleType);
   fChain->SetBranchAddress("Trigger_parentID", &Trigger_parentID, &b_Trigger_parentID);
   fChain->SetBranchAddress("Trigger_detectorNb", &Trigger_detectorNb, &b_Trigger_detectorNb);
   fChain->SetBranchAddress("GammaDeposit", &GammaDeposit, &b_gammaDeposit);
   fChain->SetBranchAddress("Trigger_bouncesEAtTrigger", &Trigger_bouncesEAtTrigger, &b_Trigger_bouncesEAtTrigger);
   fChain->SetBranchAddress("Trigger_bouncesPAtTrigger", &Trigger_bouncesPAtTrigger, &b_Trigger_bouncesPAtTrigger);
   fChain->SetBranchAddress("PixelStatus", &PixelStatus, &b_PixelStatus);
   fChain->SetBranchAddress("TimeOfFlight", &TimeOfFlight, &b_TimeOfFlight);
   fChain->SetBranchAddress("TimeOfFlight_proton_cm", &TimeOfFlight_proton_cm, &b_TimeOfFlight_proton_cm);
   fChain->SetBranchAddress("EDepElectron", &EDepElectron, &b_EDepElectron);
   fChain->SetBranchAddress("KillStatus", &KillStatus_, &b_KillStatus_);
   fChain->SetBranchAddress("KillStatus.curr_x", &KillStatus_curr_x, &b_KillStatus_curr_x);
   fChain->SetBranchAddress("KillStatus.curr_y", &KillStatus_curr_y, &b_KillStatus_curr_y);
   fChain->SetBranchAddress("KillStatus.curr_z", &KillStatus_curr_z, &b_KillStatus_curr_z);
   fChain->SetBranchAddress("KillStatus.curr_px", &KillStatus_curr_px, &b_KillStatus_curr_px);
   fChain->SetBranchAddress("KillStatus.curr_py", &KillStatus_curr_py, &b_KillStatus_curr_py);
   fChain->SetBranchAddress("KillStatus.curr_pz", &KillStatus_curr_pz, &b_KillStatus_curr_pz);
   fChain->SetBranchAddress("KillStatus.curr_E", &KillStatus_curr_E, &b_KillStatus_curr_E);
   fChain->SetBranchAddress("KillStatus.status", &KillStatus_status, &b_KillStatus_status);
   fChain->SetBranchAddress("KillStatus.trackID", &KillStatus_trackID, &b_KillStatus_trackID);
   fChain->SetBranchAddress("KillStatus.parentID", &KillStatus_parentID, &b_KillStatus_parentID);
   fChain->SetBranchAddress("KillStatus.particleID", &KillStatus_particleID, &b_KillStatus_particleID);
   fChain->SetBranchAddress("KillStatus.px0", &KillStatus_px0, &b_KillStatus_px0);
   fChain->SetBranchAddress("KillStatus.py0", &KillStatus_py0, &b_KillStatus_py0);
   fChain->SetBranchAddress("KillStatus.pz0", &KillStatus_pz0, &b_KillStatus_pz0);
   fChain->SetBranchAddress("KillStatus.E0", &KillStatus_E0, &b_KillStatus_E0);
   fChain->SetBranchAddress("KillStatus.x0", &KillStatus_x0, &b_KillStatus_x0);
   fChain->SetBranchAddress("KillStatus.y0", &KillStatus_y0, &b_KillStatus_y0);
   fChain->SetBranchAddress("KillStatus.z0", &KillStatus_z0, &b_KillStatus_z0);
   fChain->SetBranchAddress("elapsedTime", &elapsedTime, &b_elapsedTime);
   fChain->SetBranchAddress("ElastZ[10]", ElastZ, &b_ElastZ);
   fChain->SetBranchAddress("ElastR[10]", ElastR, &b_ElastR);
   fChain->SetBranchAddress("ElastE[10]", ElastE, &b_ElastE);
   fChain->SetBranchAddress("ElastV[10]", ElastV, &b_ElastV);
   fChain->SetBranchAddress("ElastS[10]", ElastS, &b_ElastS);
   fChain->SetBranchAddress("ElastDeltaT[10]", ElastDeltaT, &b_ElastDeltaT);
   fChain->SetBranchAddress("EimpactAngleViaPos", &EimpactAngleViaPos, &b_EimpactAngleViaPos);
   fChain->SetBranchAddress("EimpactAngleViaMomentum", &EimpactAngleViaMomentum, &b_EimpactAngleViaMomentum);
   fChain->SetBranchAddress("EenergyBeforeDetector", &EenergyBeforeDetector, &b_EenergyBeforeDetector);
   fChain->SetBranchAddress("EimpactPosition[3]", EimpactPosition, &b_EimpactPosition);
   fChain->SetBranchAddress("PlastZ[10]", PlastZ, &b_PlastZ);
   fChain->SetBranchAddress("PlastR[10]", PlastR, &b_PlastR);
   fChain->SetBranchAddress("PlastE[10]", PlastE, &b_PlastE);
   fChain->SetBranchAddress("PlastV[10]", PlastV, &b_PlastV);
   fChain->SetBranchAddress("PlastS[10]", PlastS, &b_PlastS);
   fChain->SetBranchAddress("PlastDeltaT[10]", PlastDeltaT, &b_PlastDeltaT);
   fChain->SetBranchAddress("PimpactAngleViaPos", &PimpactAngleViaPos, &b_PimpactAngleViaPos);
   fChain->SetBranchAddress("PimpactAngleViaMomentum", &PimpactAngleViaMomentum, &b_PimpactAngleViaMomentum);
   fChain->SetBranchAddress("PenergyBeforeDetector", &PenergyBeforeDetector, &b_PenergyBeforeDetector);
   fChain->SetBranchAddress("PimpactPosition[3]", PimpactPosition, &b_PimpactPosition);
   fChain->SetBranchAddress("EmidPreX", &EmidPreX, &b_EmidPreX);
   fChain->SetBranchAddress("EmidPreY", &EmidPreY, &b_EmidPreY);
   fChain->SetBranchAddress("EmidPreZ", &EmidPreZ, &b_EmidPreZ);
   fChain->SetBranchAddress("EmidPostX", &EmidPostX, &b_EmidPostX);
   fChain->SetBranchAddress("EmidPostY", &EmidPostY, &b_EmidPostY);
   fChain->SetBranchAddress("EmidPostZ", &EmidPostZ, &b_EmidPostZ);
   fChain->SetBranchAddress("EmidE", &EmidE, &b_EmidE);
   fChain->SetBranchAddress("EmidT", &EmidT, &b_EmidT);
   fChain->SetBranchAddress("EmidImpactAngle", &EmidImpactAngle, &b_EmidImpactAngle);
   fChain->SetBranchAddress("PmidPreX", &PmidPreX, &b_PmidPreX);
   fChain->SetBranchAddress("PmidPreY", &PmidPreY, &b_PmidPreY);
   fChain->SetBranchAddress("PmidPreZ", &PmidPreZ, &b_PmidPreZ);
   fChain->SetBranchAddress("PmidPostX", &PmidPostX, &b_PmidPostX);
   fChain->SetBranchAddress("PmidPostY", &PmidPostY, &b_PmidPostY);
   fChain->SetBranchAddress("PmidPostZ", &PmidPostZ, &b_PmidPostZ);
   fChain->SetBranchAddress("PmidE", &PmidE, &b_PmidE);
   fChain->SetBranchAddress("PmidT", &PmidT, &b_PmidT);
   fChain->SetBranchAddress("PmidImpactAngle", &PmidImpactAngle, &b_PmidImpactAngle);
   fChain->SetBranchAddress("EdetPreX", &EdetPreX, &b_EdetPreX);
   fChain->SetBranchAddress("EdetPreY", &EdetPreY, &b_EdetPreY);
   fChain->SetBranchAddress("EdetPreZ", &EdetPreZ, &b_EdetPreZ);
   fChain->SetBranchAddress("EdetPx", &EdetPx, &b_EdetPx);
   fChain->SetBranchAddress("EdetPy", &EdetPy, &b_EdetPy);
   fChain->SetBranchAddress("EdetPz", &EdetPz, &b_EdetPz);
   fChain->SetBranchAddress("EdetE", &EdetE, &b_EdetE);
   fChain->SetBranchAddress("EdetT", &EdetT, &b_EdetT);
   fChain->SetBranchAddress("PdetPreX", &PdetPreX, &b_PdetPreX);
   fChain->SetBranchAddress("PdetPreY", &PdetPreY, &b_PdetPreY);
   fChain->SetBranchAddress("PdetPreZ", &PdetPreZ, &b_PdetPreZ);
   fChain->SetBranchAddress("PdetPx", &PdetPx, &b_PdetPx);
   fChain->SetBranchAddress("PdetPy", &PdetPy, &b_PdetPy);
   fChain->SetBranchAddress("PdetPz", &PdetPz, &b_PdetPz);
   fChain->SetBranchAddress("PdetE", &PdetE, &b_PdetE);
   fChain->SetBranchAddress("PdetT", &PdetT, &b_PdetT);
   fChain->SetBranchAddress("EdetCMPreX", &EdetCMPreX, &b_EdetCMPreX);
   fChain->SetBranchAddress("EdetCMPreY", &EdetCMPreY, &b_EdetCMPreY);
   fChain->SetBranchAddress("EdetCMPreZ", &EdetCMPreZ, &b_EdetCMPreZ);
   fChain->SetBranchAddress("EdetCMPostX", &EdetCMPostX, &b_EdetCMPostX);
   fChain->SetBranchAddress("EdetCMPostY", &EdetCMPostY, &b_EdetCMPostY);
   fChain->SetBranchAddress("EdetCMPostZ", &EdetCMPostZ, &b_EdetCMPostZ);
   fChain->SetBranchAddress("EdetCMPx", &EdetCMPx, &b_EdetCMPx);
   fChain->SetBranchAddress("EdetCMPy", &EdetCMPy, &b_EdetCMPy);
   fChain->SetBranchAddress("EdetCMPz", &EdetCMPz, &b_EdetCMPz);
   fChain->SetBranchAddress("EdetCME", &EdetCME, &b_EdetCME);
   fChain->SetBranchAddress("EdetCMT", &EdetCMT, &b_EdetCMT);
   fChain->SetBranchAddress("EdetCMPostT", &EdetCMPostT, &b_EdetCMPostT);
   fChain->SetBranchAddress("PdetCMPreX", &PdetCMPreX, &b_PdetCMPreX);
   fChain->SetBranchAddress("PdetCMPreY", &PdetCMPreY, &b_PdetCMPreY);
   fChain->SetBranchAddress("PdetCMPreZ", &PdetCMPreZ, &b_PdetCMPreZ);
   fChain->SetBranchAddress("PdetCMPostX", &PdetCMPostX, &b_PdetCMPostX);
   fChain->SetBranchAddress("PdetCMPostY", &PdetCMPostY, &b_PdetCMPostY);
   fChain->SetBranchAddress("PdetCMPostZ", &PdetCMPostZ, &b_PdetCMPostZ);
   fChain->SetBranchAddress("PdetCMPx", &PdetCMPx, &b_PdetCMPx);
   fChain->SetBranchAddress("PdetCMPy", &PdetCMPy, &b_PdetCMPy);
   fChain->SetBranchAddress("PdetCMPz", &PdetCMPz, &b_PdetCMPz);
   fChain->SetBranchAddress("PdetCME", &PdetCME, &b_PdetCME);
   fChain->SetBranchAddress("PdetCMT", &PdetCMT, &b_PdetCMT);
   fChain->SetBranchAddress("PdetCMPostT", &PdetCMPostT, &b_PdetCMPostT);
   Notify();
}

Bool_t Event_comparison::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void Event_comparison::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t Event_comparison::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef Event_comparison_cxx
