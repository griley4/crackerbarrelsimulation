#include "TROOT.h"
#include "TStyle.h"
#include "TTree.h"
#include "TLatex.h"
#include "TGraphErrors.h"
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <math.h>
#include <TArrayD.h>
#include "TH2D.h"
#include "TH1D.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "TMath.h"
#include "TRint.h"
#include "TCanvas.h"
#include "TTreeFormula.h"
#include "TLegend.h"
#include "TFile.h"
#include "TCut.h"
#include "TImage.h"
#include "TSystem.h"
#include "TPaveText.h"
#include "TPaveStats.h"
#include <sys/time.h>
#include <sys/resource.h>
#include "TColor.h"
using namespace std;


void TOF_plot()
{

  TFile *f = new TFile("07142016_monoE_forward_1DFieldmap_testinputcoil_10k_data_tree.root"); 
  TTree *MyTree = (TTree*)f->Get("t");

  //TTree *MyTree1 = new TTree("MyTree1", "MyTree1");	
  //MyTree1->ReadFile("../FieldProfile_particle4604008_ferenc_1D.dat", "Z:R:Bz:Br:Bz1:Br1");
  //TTree *MyTree2 = new TTree("MyTree2", "MyTree2");	
  //MyTree2->ReadFile("../FieldProfile_particle4604008_ferenc_1D.dat", "Z:R:Bz:Br:Bz1:Br1");
  //Int_t palette[100];
  //for (int i=0;i<100;i++) palette[i] = 100-0.8*(100-i);
  //gStyle->SetPalette(100,palette);

  ////////////////////////////////////////////////////
  //start to draw can11 (Analyzer Pad 1)
	
  TCanvas *can11 = new TCanvas("can11", "Diagnostic Pad 1", 1100, 800);
  gStyle->SetOptStat(0);
  can11->Divide(1, 1, 0.005, 0.005);
		
  can11->cd(1)->SetGrid();
  MyTree->Draw("pP0z_1:R_1>>h1(100, 0, 22, 100,0.7, 1)","TOF_diff_10*(TOF_diff_10>0.3)","COLZ", 10000, 0);//abs(diffBz/Bz)>10^-6
  TH2F *h1 = (TH2F*) gDirectory->Get("h1");
  //h1->SetXTitle("R/mm");
  //h1->GetXaxis()->CenterTitle();
  //h1->SetYTitle("cos(#theta)");
  //h1->GetYaxis()->CenterTitle();
  //h1->GetZaxis()->SetLabelSize(0.025); 
  //h1->SetTitle("TOF diff");
  can11->Modified();	
  can11->Update();

  //can11->cd(2)->SetGrid();
  MyTree->Draw("pP0z_1:R_1>>h2(100, 0, 22, 100,0.7, 1)","TOF_diff_10>0.3","COLZ SAME", 10000, 0);//abs(diffBz/Bz)>10^-6
  TH2F *h2 = (TH2F*) gDirectory->Get("h2");
  //h1->SetXTitle("R/mm");
  //h1->GetXaxis()->CenterTitle();
  //h1->SetYTitle("cos(#theta)");
  //h1->GetYaxis()->CenterTitle();
  //h1->GetZaxis()->SetLabelSize(0.025); 
  //h1->SetTitle("TOF diff");
  can11->Modified();	
  can11->Update();

  //can11->cd(3)->SetGrid();
  h1->Divide(h2);
  h1->Draw("COLZ");
  //MyTree->Draw("pP0z_1:R_1>>h3(200, 0, 22, 200,0.7, 1.1)","TOF_diff_10*(TOF_diff_10>0.3)","COLZ", 10000, 0);//abs(diffBz/Bz)>10^-6
  //TH2F *h3 = (TH2F*) gDirectory->Get("h3");
  h1->SetXTitle("R (mm)");
  h1->GetXaxis()->CenterTitle();
  h1->SetYTitle("cos(#theta)");
  h1->GetYaxis()->CenterTitle();
  //h1->GetZaxis()->SetLabelSize(0.025); 
  h1->SetTitle("TOF diff");
  can11->Modified();	
  can11->Update();

  can11->Print("TOF_Testinputcoil_100.pdf");
  //can11->Print("TOF_plot.png");
		
}
