#include "TROOT.h"
#include "TStyle.h"
#include "TTree.h"
#include "TLatex.h"
#include "TGraphErrors.h"
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <math.h>
#include <TArrayD.h>
#include "TH2D.h"
#include "TH1D.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "TMath.h"
#include "TRint.h"
#include "TCanvas.h"
#include "TTreeFormula.h"
#include "TLegend.h"
#include "TFile.h"
#include "TCut.h"
#include "TImage.h"
#include "TSystem.h"
#include "TPaveText.h"
#include "TPaveStats.h"
#include <sys/time.h>
#include <sys/resource.h>
#include "TColor.h"
using namespace std;


void plotDiffSteps()
{
  char* st1 = NULL;
  char* st2 = NULL;
  char* st3 = NULL;
  char* st4 = NULL;
  
	
  st1 = "ContEe_100k_RunForward_1um_new";
  st2 = "ContEe_100k_RunForward_10um_new";
  st3 = "ContEe_100k_RunForward_25um_new";
  st4 = "ContEe_100k_RunForward_50um_new";
					  
  TFile *f1 = new TFile(Form("%s.root", st1));
  TTree *tbaseline1 = (TTree*)f1->Get("dynamicTree");

  TFile *f2 = new TFile(Form("%s.root", st2));
  TTree *tbaseline2 = (TTree*)f2->Get("dynamicTree");

  TFile *f3 = new TFile(Form("%s.root", st3));
  TTree *tbaseline3 = (TTree*)f3->Get("dynamicTree");

  TFile *f4 = new TFile(Form("%s.root", st4));
  TTree *tbaseline4 = (TTree*)f4->Get("dynamicTree");


  gStyle->SetStatX(0.95); 
  gStyle->SetStatY(1); 
  gStyle->SetStatW(0.12);
  gStyle->SetStatH(0.15);

  ////////////////////////////////////////////////////
  //start to draw can11 (Analyzer Pad 1)
	
  TCanvas *can11 = new TCanvas("can11", "Diagnostic Pad 1", 1200, 600);
  gStyle->SetOptStat(1111);
  can11->Divide(2, 1, 0.005, 0.005);
		
  can11->cd(1)->SetGrid();
  //can11->cd(1)->SetLogy();
  tbaseline1->Draw("TimeOfFlight>>h1(300, 10, 41)", "TimeOfFlight>1 && PFirstHitDetNb==1", "");
  TH1D *h1 = (TH1D*) gDirectory->Get("h1");

  tbaseline2->Draw("TimeOfFlight>>h2(300, 10, 41)", "TimeOfFlight>1 && PFirstHitDetNb==1", "same");
  TH1D *h2 = (TH1D*) gDirectory->Get("h2");

  tbaseline3->Draw("TimeOfFlight>>h3(300, 10, 41)", "TimeOfFlight>1 && PFirstHitDetNb==1", "same");
  TH1D *h3 = (TH1D*) gDirectory->Get("h3");

  tbaseline4->Draw("TimeOfFlight>>h4(300, 10, 41)", "TimeOfFlight>1 && PFirstHitDetNb==1", "same");
  TH1D *h4 = (TH1D*) gDirectory->Get("h4");
	
  
  h1->SetMarkerColor(4);
  h1->SetLineColor(1);
  h1->SetMarkerStyle(20);
  h2->SetMarkerColor(4);
  h2->SetLineColor(2);
  h2->SetMarkerStyle(20);
  h3->SetMarkerColor(4);
  h3->SetLineColor(3);
  h3->SetMarkerStyle(20);
  h4->SetMarkerColor(4);
  h4->SetLineColor(4);
  h4->SetMarkerStyle(20);
  h1->SetXTitle("TOF (#mus)");
  h1->GetXaxis()->CenterTitle();
  h1->SetTitle("TOF top detector");
	
  can11->Update();
  can11->cd(1);
  TLegend *leg4 = new TLegend(0.78,0.5,0.99,0.78);
  leg4->SetHeader("");
  leg4->SetFillColor(0);
  leg4->SetTextSize(0.03);
  leg4->AddEntry(h1,"1#mu stepsize", "l");
  //leg4->AddEntry((TObject*)0, "", "");
  leg4->AddEntry(h2,"10#mu stepsize", "l");
  //leg4->AddEntry((TObject*)0, "", "");
  leg4->AddEntry(h3,"25#mu stepsize", "l");
  //leg4->AddEntry((TObject*)0, "", "");
  leg4->AddEntry(h4,"50#mu stepsize", "l");
  leg4->Draw();
  can11->Modified();
		
  can11->cd(2)->SetGrid();
  //can11->cd(2)->SetLogy();
  tbaseline1->Draw("TimeOfFlight>>h5(300, 0, 41)", "TimeOfFlight>1 && PFirstHitDetNb==-1", "");
  TH1D *h5 = (TH1D*) gDirectory->Get("h5");

  tbaseline2->Draw("TimeOfFlight>>h6(300, 0, 41)", "TimeOfFlight>1 && PFirstHitDetNb==-1", "same");
  TH1D *h6 = (TH1D*) gDirectory->Get("h6");

  tbaseline3->Draw("TimeOfFlight>>h7(300, 0, 41)", "TimeOfFlight>1 && PFirstHitDetNb==-1", "same");
  TH1D *h7 = (TH1D*) gDirectory->Get("h7");

  tbaseline4->Draw("TimeOfFlight>>h8(300, 0, 41)", "TimeOfFlight>1 && PFirstHitDetNb==-1", "same");
  TH1D *h8 = (TH1D*) gDirectory->Get("h8");
	
  
  h5->SetMarkerColor(4);
  h5->SetLineColor(1);
  h5->SetMarkerStyle(20);
  h6->SetMarkerColor(4);
  h6->SetLineColor(2);
  h6->SetMarkerStyle(20);
  h7->SetMarkerColor(4);
  h7->SetLineColor(3);
  h7->SetMarkerStyle(20);
  h8->SetMarkerColor(4);
  h8->SetLineColor(4);
  h8->SetMarkerStyle(20);
  h5->SetXTitle("TOF (#mu s)");
  h5->GetXaxis()->CenterTitle();
  h5->SetTitle("TOF bottom detector");
  can11->Modified();
	
  can11->Update();
  can11->cd(2);
  TLegend *leg5 = new TLegend(0.78,0.5,0.99,0.78);
  leg5->SetHeader("");
  leg5->SetFillColor(0);
  leg5->SetTextSize(0.03);
  leg5->AddEntry(h5,"1#mu stepsize", "l");
  //leg5->AddEntry((TObject*)0, "", "");
  leg5->AddEntry(h6,"10#mu stepsize", "l");
  //leg5->AddEntry((TObject*)0, "", "");
  leg5->AddEntry(h7,"25#mu stepsize", "l");
  //leg5->AddEntry((TObject*)0, "", "");
  leg5->AddEntry(h8,"50#mu stepsize", "l");
  leg5->Draw();
  can11->Modified();
  can11->Update();
	
  //end drawing can11 (Residual Pad 1)
  ////////////////////////////////////////////////////

  ////////////////////////////////////////////////////
  //start to draw can12 (Analyzer Pad 2)
	
  TCanvas *can12 = new TCanvas("can12", "Diagnostic Pad 2", 1100, 800);
  gStyle->SetOptStat(1111);
  can12->Divide(3, 3, 0.005, 0.005);
	
  can12->cd(1)->SetGrid();
  can12->cd(1)->SetLogy();
  tbaseline4->Draw("(EdetE*1000-eE0)>>hh4(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z<0", "");
  TH1D *hh4 = (TH1D*) gDirectory->Get("hh4");
  tbaseline1->Draw("(EdetE*1000-eE0)>>hh1(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z<0", "same");
  TH1D *hh1 = (TH1D*) gDirectory->Get("hh1");
  tbaseline2->Draw("(EdetE*1000-eE0)>>hh2(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z<0", "same");
  TH1D *hh2 = (TH1D*) gDirectory->Get("hh2");
  tbaseline3->Draw("(EdetE*1000-eE0)>>hh3(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z<0", "same");
  TH1D *hh3 = (TH1D*) gDirectory->Get("hh3");
	
  hh1->SetLineColor(1);
  hh2->SetLineColor(2);
  hh3->SetLineColor(3);
  hh4->SetLineColor(4);
  hh4->SetXTitle("#Delta E (keV)");
  hh4->GetXaxis()->CenterTitle();
  hh4->SetTitle("Energy Non-conservation, bottom detector, cos(#theta_{e})<0");

  can12->Update();
  can12->cd(1);
  TLegend *leg6 = new TLegend(0.82,0.5,0.99,0.78);
  leg6->SetHeader("");
  leg6->SetFillColor(0);
  leg6->SetTextSize(0.03);
  leg6->AddEntry(hh1,"1#mu stepsize", "l");
  //leg6->AddEntry((TObject*)0, "", "");
  leg6->AddEntry(hh2,"10#mu stepsize", "l");
  //leg6->AddEntry((TObject*)0, "", "");
  leg6->AddEntry(hh3,"25#mu stepsize", "l");
  //leg6->AddEntry((TObject*)0, "", "");
  leg6->AddEntry(hh4,"50#mu stepsize", "l");
  leg6->Draw();
  can12->Modified();

	
  can12->cd(2)->SetGrid();
  can12->cd(2)->SetLogy();
  tbaseline1->Draw("(EdetE*1000-eE0)>>hh5(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z>0 && eP0z<0.7", "");
  TH1D *hh5 = (TH1D*) gDirectory->Get("hh5");
  tbaseline2->Draw("(EdetE*1000-eE0)>>hh6(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z>0 && eP0z<0.7", "same");
  TH1D *hh6 = (TH1D*) gDirectory->Get("hh6");
  tbaseline3->Draw("(EdetE*1000-eE0)>>hh7(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z>0 && eP0z<0.7", "same");
  TH1D *hh7 = (TH1D*) gDirectory->Get("hh7");
  tbaseline4->Draw("(EdetE*1000-eE0)>>hh8(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z>0 && eP0z<0.7", "same");
  TH1D *hh8 = (TH1D*) gDirectory->Get("hh8");

  hh5->SetLineColor(1);
  hh6->SetLineColor(2);
  hh7->SetLineColor(3);
  hh8->SetLineColor(4);
  hh5->SetXTitle("#Delta E (keV)");
  hh5->GetXaxis()->CenterTitle();
  hh5->SetTitle("Energy Non-conservation, bottom detector, 0<cos(#theta_{e})<0.7");
  can12->Modified();
	
  can12->Modified();
  TLegend *leg7 = new TLegend(0.82,0.5,0.99,0.78);
  leg7->SetHeader("");
  leg7->SetFillColor(0);
  leg7->SetTextSize(0.03);
  leg7->AddEntry(hh5,"1#mu stepsize", "l");
  //leg7->AddEntry((TObject*)0, "", "");
  leg7->AddEntry(hh6,"10#mu stepsize", "l");
  //leg7->AddEntry((TObject*)0, "", "");
  leg7->AddEntry(hh7,"25#mu stepsize", "l");
  //leg7->AddEntry((TObject*)0, "", "");
  leg7->AddEntry(hh8,"50#mu stepsize", "l");
  leg7->Draw();
  can12->Update();

	
  can12->cd(3)->SetGrid();
  can12->cd(3)->SetLogy();
  tbaseline1->Draw("(EdetE*1000-eE0)>>hj5(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z>0.7 && eP0z<0.77", "");
  TH1D *hj5 = (TH1D*) gDirectory->Get("hj5");
  tbaseline2->Draw("(EdetE*1000-eE0)>>hj6(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z>0.7 && eP0z<0.77", "same");
  TH1D *hj6 = (TH1D*) gDirectory->Get("hj6");
  tbaseline3->Draw("(EdetE*1000-eE0)>>hj7(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z>0.7 && eP0z<0.77", "same");
  TH1D *hj7 = (TH1D*) gDirectory->Get("hj7");
  tbaseline4->Draw("(EdetE*1000-eE0)>>hj8(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z>0.7 && eP0z<0.77", "same");
  TH1D *hj8 = (TH1D*) gDirectory->Get("hj8");

  hj5->SetLineColor(1);
  hj6->SetLineColor(2);
  hj7->SetLineColor(3);
  hj8->SetLineColor(4);
  hj5->SetXTitle("#Delta E (keV)");
  hj5->GetXaxis()->CenterTitle();
  hj5->SetTitle("Energy Non-conservation, bottom detector, 0.7<cos(#theta_{e})<0.77");
  can12->Modified();
	
  can12->Modified();
  TLegend *leg8 = new TLegend(0.82,0.5,0.99,0.78);
  leg8->SetHeader("");
  leg8->SetFillColor(0);
  leg8->SetTextSize(0.03);
  leg8->AddEntry(hj5,"1#mu stepsize", "l");
  //leg8->AddEntry((TObject*)0, "", "");
  leg8->AddEntry(hj6,"10#mu stepsize", "l");
  //leg8->AddEntry((TObject*)0, "", "");
  leg8->AddEntry(hj7,"25#mu stepsize", "l");
  //leg8->AddEntry((TObject*)0, "", "");
  leg8->AddEntry(hj8,"50#mu stepsize", "l");
  leg8->Draw();
  can12->Update();

 
  can12->cd(4)->SetGrid();
  can12->cd(4)->SetLogy();
  tbaseline1->Draw("(EdetE*1000-eE0)>>hk5(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z>0.77", "");
  TH1D *hk5 = (TH1D*) gDirectory->Get("hk5");
  tbaseline2->Draw("(EdetE*1000-eE0)>>hk6(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z>0.77", "same");
  TH1D *hk6 = (TH1D*) gDirectory->Get("hk6");
  tbaseline3->Draw("(EdetE*1000-eE0)>>hk7(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z>0.77", "same");
  TH1D *hk7 = (TH1D*) gDirectory->Get("hk7");
  tbaseline4->Draw("(EdetE*1000-eE0)>>hk8(300,-1.002, -0.994)", "EdetE!=-10^6 && EimpactPosition[2]<0 && eP0z>0.77", "same");
  TH1D *hk8 = (TH1D*) gDirectory->Get("hk8");

  hk5->SetLineColor(1);
  hk6->SetLineColor(2);
  hk7->SetLineColor(3);
  hk8->SetLineColor(4);
  hk5->SetXTitle("#Delta E (keV)");
  hk5->GetXaxis()->CenterTitle();
  hk5->SetTitle("Energy Non-conservation, bottom detector, cos(#theta_{e})>0.77");
  can12->Modified();
	
  can12->Modified();
  TLegend *leg9 = new TLegend(0.82,0.5,0.99,0.78);
  leg9->SetHeader("");
  leg9->SetFillColor(0);
  leg9->SetTextSize(0.03);
  leg9->AddEntry(hk5,"1#mu stepsize", "l");
  //leg9->AddEntry((TObject*)0, "", "");
  leg9->AddEntry(hk6,"10#mu stepsize", "l");
  //leg9->AddEntry((TObject*)0, "", "");
  leg9->AddEntry(hk7,"25#mu stepsize", "l");
  //leg9->AddEntry((TObject*)0, "", "");
  leg9->AddEntry(hk8,"50#mu stepsize", "l");
  leg9->Draw();
  can12->Update();
 

  can12->cd(5)->SetGrid();
  can12->cd(5)->SetLogy();
  tbaseline1->Draw("(EdetE*1000-eE0)>>hl5(300,-30.01, -29.998)", "EdetE!=-10^6 && EimpactPosition[2]>0 && eP0z<0.7", "");
  TH1D *hl5 = (TH1D*) gDirectory->Get("hl5");
  tbaseline2->Draw("(EdetE*1000-eE0)>>hl6(300,-30.08, -30)", "EdetE!=-10^6 && EimpactPosition[2]>0 && eP0z<0.7", "same");
  TH1D *hl6 = (TH1D*) gDirectory->Get("hl6");
  tbaseline3->Draw("(EdetE*1000-eE0)>>hl7(300,-30.08, -30)", "EdetE!=-10^6 && EimpactPosition[2]>0 && eP0z<0.7", "same");
  TH1D *hl7 = (TH1D*) gDirectory->Get("hl7");
  tbaseline4->Draw("(EdetE*1000-eE0)>>hl8(300,-30.08, -30)", "EdetE!=-10^6 && EimpactPosition[2]>0 && eP0z<0.7", "same");
  TH1D *hl8 = (TH1D*) gDirectory->Get("hl8");

  hl5->SetLineColor(1);
  hl6->SetLineColor(2);
  hl7->SetLineColor(3);
  hl8->SetLineColor(4);
  hl5->SetXTitle("#Delta E (keV)");
  hl5->GetXaxis()->CenterTitle();
  hl5->SetTitle("Energy Non-conservation, top detector, cos(#theta_{e})<0.7");
  can12->Modified();
	
  TLegend *leg10 = new TLegend(0.82,0.5,0.99,0.78);
  leg10->SetHeader("");
  leg10->SetFillColor(0);
  leg10->SetTextSize(0.03);
  leg10->AddEntry(hl5,"1#mu stepsize", "l");
  //leg10->AddEntry((TObject*)0, "", "");
  leg10->AddEntry(hl6,"10#mu stepsize", "l");
  //leg10->AddEntry((TObject*)0, "", "");
  leg10->AddEntry(hl7,"25#mu stepsize", "l");
  //leg10->AddEntry((TObject*)0, "", "");
  leg10->AddEntry(hl8,"50#mu stepsize", "l");
  leg10->Draw();
  can12->Modified();
  can12->Update();

	
  can12->cd(6)->SetGrid();
  can12->cd(6)->SetLogy();
  tbaseline1->Draw("(EdetE*1000-eE0)>>hm5(300,-30.01, -29.998)", "EdetE!=-10^6 && EimpactPosition[2]>0 && eP0z>0.7 && eP0z<0.77", "");
  TH1D *hm5 = (TH1D*) gDirectory->Get("hm5");
  tbaseline2->Draw("(EdetE*1000-eE0)>>hm6(300,-30.08, -30)", "EdetE!=-10^6 && EimpactPosition[2]>0 && eP0z>0.7 && eP0z<0.77", "same");
  TH1D *hm6 = (TH1D*) gDirectory->Get("hm6");
  tbaseline3->Draw("(EdetE*1000-eE0)>>hm7(300-,-30.08, -30)", "EdetE!=-10^6 && EimpactPosition[2]>0 && eP0z>0.7 && eP0z<0.77", "same");
  TH1D *hm7 = (TH1D*) gDirectory->Get("hm7");
  tbaseline4->Draw("(EdetE*1000-eE0)>>hm8(300,-30.08, -30)", "EdetE!=-10^6 && EimpactPosition[2]>0 && eP0z>0.7 && eP0z<0.77", "same");
  TH1D *hm8 = (TH1D*) gDirectory->Get("hm8");

  hm5->SetLineColor(1);
  hm6->SetLineColor(2);
  hm7->SetLineColor(3);
  hm8->SetLineColor(4);
  hm5->SetXTitle("#Delta E (keV)");
  hm5->GetXaxis()->CenterTitle();
  hm5->SetTitle("Energy Non-conservation, top detector, 0.7<cos(#theta_{e})<0.77");
  can12->Modified();
	
  TLegend *leg11 = new TLegend(0.82,0.5,0.99,0.78);
  leg11->SetHeader("");
  leg11->SetFillColor(0);
  leg11->SetTextSize(0.03);
  leg11->AddEntry(hm5,"1#mu stepsize", "l");
  //leg11->AddEntry((TObject*)0, "", "");
  leg11->AddEntry(hm6,"10#mu stepsize", "l");
  //leg11->AddEntry((TObject*)0, "", "");
  leg11->AddEntry(hm7,"25#mu stepsize", "l");
  //leg11->AddEntry((TObject*)0, "", "");
  leg11->AddEntry(hm8,"50#mu stepsize", "l");
  leg11->Draw();
  can12->Modified();
  can12->Update();
	
	
  can12->cd(7)->SetGrid();
  can12->cd(7)->SetLogy();
  tbaseline1->Draw("(EdetE*1000-eE0)>>hn5(300,-30.01, -29.998)", "EdetE!=-10^6 && EimpactPosition[2]>0 && eP0z>0.77", "");
  TH1D *hn5 = (TH1D*) gDirectory->Get("hn5");
  tbaseline2->Draw("(EdetE*1000-eE0)>>hn6(300,-30.08, -29.9)", "EdetE!=-10^6 && EimpactPosition[2]>0 && eP0z>0.77", "same");
  TH1D *hn6 = (TH1D*) gDirectory->Get("hn6");
  tbaseline3->Draw("(EdetE*1000-eE0)>>hn7(300,-30.08, -29.9)", "EdetE!=-10^6 && EimpactPosition[2]>0 && eP0z>0.77", "same");
  TH1D *hn7 = (TH1D*) gDirectory->Get("hn7");
  tbaseline4->Draw("(EdetE*1000-eE0)>>hn8(300,-30.08, -29.9)", "EdetE!=-10^6 && EimpactPosition[2]>0 && eP0z>0.77", "same");
  TH1D *hn8 = (TH1D*) gDirectory->Get("hn8");

  hn5->SetLineColor(1);
  hn6->SetLineColor(2);
  hn7->SetLineColor(3);
  hn8->SetLineColor(4);
  hn5->SetXTitle("#Delta E (keV)");
  hn5->GetXaxis()->CenterTitle();
  hn5->SetTitle("Energy Non-conservation, top detector, cos(#theta_{e})>0.77");
  can12->Modified();
	
  TLegend *leg12 = new TLegend(0.82,0.5,0.99,0.78);
  leg12->SetHeader("");
  leg12->SetFillColor(0);
  leg12->SetTextSize(0.03);
  leg12->AddEntry(hn5,"1#mu stepsize", "l");
  //leg12->AddEntry((TObject*)0, "", "");
  leg12->AddEntry(hn6,"10#mu stepsize", "l");
  //leg12->AddEntry((TObject*)0, "", "");
  leg12->AddEntry(hn7,"25#mu stepsize", "l");
  //leg12->AddEntry((TObject*)0, "", "");
  leg12->AddEntry(hn8,"50#mu stepsize", "l");
  leg12->Draw();
  can12->Modified();
  can12->Update();


  //end drawing can12 (Residual Pad 2)
  ////////////////////////////////////////////////////
	
  string st14, st1test;
	
  cout << "\n\nHello, testing... \nPress any key to continue! \n"; //if not included, this function thinks the last enter is this cin
  getline (cin, st1test);
	
  cout << "Would you like to save the residual pad .pdf? (y or n): ";
  getline (cin, st14);
  if (st14 == "y")
    {
      //save a ps!
      can11->Print(Form("%s_diagnostics.pdf(", st1));

      can12->Print(Form("%s_diagnostics.pdf)", st1));

      printf("Finished saving files \n");
	       
    }
  else 
    {
      printf("pad not saved \n");
    }
	
  return;
	
}
