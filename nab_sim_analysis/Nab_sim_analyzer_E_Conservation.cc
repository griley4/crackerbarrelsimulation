#include "TROOT.h"
#include "TStyle.h"
#include "TTree.h"
#include "TLatex.h"
#include "TGraphErrors.h"
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <math.h>
#include <TArrayD.h>
#include "TH2D.h"
#include "TH1D.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "TMath.h"
#include "TRint.h"
#include "TCanvas.h"
#include "TTreeFormula.h"
#include "TLegend.h"
#include "TFile.h"
#include "TCut.h"
#include "TImage.h"
#include "TSystem.h"
#include "TPaveText.h"
#include "TPaveStats.h"
#include <sys/time.h>
#include <sys/resource.h>
#include "TColor.h"
using namespace std;


void Nab_sim_analyzer_E_Conservation()
{
  Int_t palette[20];
  for (int i=0;i<20;i++) palette[i] = 100-2*(19-i);


  char* st1 = NULL;

  //cout << "What file would you like to use? \n";
  //cin >> st1;
	
  st1 = "test_1um_0"; //change file name here (without .root)
	
  TString baseline1 = (Form("%s.root", st1));
					  
  TFile *f1 = new TFile(baseline1);
  TTree *tbaseline1 = (TTree*)f1->Get("dynamicTree");
  gStyle->SetPalette(20,palette);

  gStyle->SetStatX(0.95); 
  gStyle->SetStatY(1); 
  gStyle->SetStatW(0.12);
  gStyle->SetStatH(0.15);

  ////////////////////////////////////////////////////
  //start to draw can11 (Analyzer Pad 1)

	
  TCanvas *can11 = new TCanvas("can11", "Diagnostic Pad 1", 1100, 800);
  gStyle->SetOptStat(1111);
  can11->Divide(3, 3, 0.005, 0.005);
	
  can11->cd(1)->SetGrid();
  can11->cd(1)->SetLogy();
  tbaseline1->Draw("(EenergyBeforeDetector*1000-eE0)>>h1()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]<0 && eP0z<0", "");
  //TGraph *h1 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *h1 = (TH1D*) gDirectory->Get("h1");
	
  h1->SetMarkerColor(4);
  h1->SetLineColor(4);
  h1->SetMarkerStyle(20);
  h1->SetXTitle("#Delta E (keV)");
  h1->GetXaxis()->CenterTitle();
  h1->SetTitle("Energy Non-conservation, bottom detector, cos(#theta_{e})<0");
	
  can11->Update();
  can11->Modified();
	
  can11->cd(2)->SetGrid();
  can11->cd(2)->SetLogy();
  tbaseline1->Draw("(EenergyBeforeDetector*1000-eE0)>>h2()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]<0 && eP0z>0 && eP0z<0.7", "");
  //TGraph *h2 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *h2 = (TH1D*) gDirectory->Get("h2");
	
  h2->SetMarkerColor(4);
  h2->SetLineColor(4);
  h2->SetMarkerStyle(20);
  h2->SetXTitle("#Delta E (keV)");
  h2->GetXaxis()->CenterTitle();
  h2->SetTitle("Energy Non-conservation, bottom detector, 0<cos(#theta_{e})<0.7");
	
  can11->Modified();
  can11->Update();
	
  can11->cd(3)->SetGrid();
  can11->cd(3)->SetLogy();
  tbaseline1->Draw("(EenergyBeforeDetector*1000-eE0)>>h3()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]<0 && eP0z>0.7 && eP0z<0.77", "");
  //TGraph *h3 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *h3 = (TH1D*) gDirectory->Get("h3");
	
  h3->SetMarkerColor(4);
  h3->SetLineColor(4);
  h3->SetMarkerStyle(20);
  h3->SetXTitle("#Delta E (keV)");
  h3->GetXaxis()->CenterTitle();
  h3->SetTitle("Energy Non-conservation, bottom detector, 0.7<cos(#theta_{e})<0.77");
	
  can11->Modified();
  can11->Update();
 
  can11->cd(4)->SetGrid();
  can11->cd(4)->SetLogy();
  tbaseline1->Draw("(EenergyBeforeDetector*1000-eE0)>>h4()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]<0 && eP0z>0.77", "");
  //TGraph *h3 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *h4 = (TH1D*) gDirectory->Get("h4");
	
  h4->SetMarkerColor(4);
  h4->SetLineColor(4);
  h4->SetMarkerStyle(20);
  h4->SetXTitle("#Delta E (keV)");
  h4->GetXaxis()->CenterTitle();
  h4->SetTitle("Energy Non-conservation, bottom detector, cos(#theta_{e})>0.77");
	
  can11->Modified();
  can11->Update(); 

  can11->cd(5)->SetGrid();
  can11->cd(5)->SetLogy();
  tbaseline1->Draw("(EenergyBeforeDetector*1000-eE0)>>h5()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]>0 && eP0z<0.7", "");
  //TGraph *h1 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *h5 = (TH1D*) gDirectory->Get("h5");
	
  h5->SetMarkerColor(4);
  h5->SetLineColor(4);
  h5->SetMarkerStyle(20);
  h5->SetXTitle("#Delta E (keV)");
  h5->GetXaxis()->CenterTitle();
  h5->SetTitle("Energy Non-conservation, top detector, cos(#theta_{e})<0.7");
	
  can11->Update();
  can11->Modified();
	
  can11->cd(6)->SetGrid();
  can11->cd(6)->SetLogy();
  tbaseline1->Draw("(EenergyBeforeDetector*1000-eE0)>>h6()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]>0 && eP0z>0.7 && eP0z<0.77", "");
  //TGraph *h2 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *h6 = (TH1D*) gDirectory->Get("h6");
	
  h6->SetMarkerColor(4);
  h6->SetLineColor(4);
  h6->SetMarkerStyle(20);
  h6->SetXTitle("#Delta E (keV)");
  h6->GetXaxis()->CenterTitle();
  h6->SetTitle("Energy Non-conservation, top detector, 0.7<cos(#theta_{e})<0.77");
	
  can11->Modified();
  can11->Update();
	
  can11->cd(7)->SetGrid();
  can11->cd(7)->SetLogy();
  tbaseline1->Draw("(EenergyBeforeDetector*1000-eE0)>>h7()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]>0 && eP0z>0.77", "");
  //TGraph *h3 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *h7 = (TH1D*) gDirectory->Get("h7");
	
  h7->SetMarkerColor(4);
  h7->SetLineColor(4);
  h7->SetMarkerStyle(20);
  h7->SetXTitle("#Delta E (keV)");
  h7->GetXaxis()->CenterTitle();
  h7->SetTitle("Energy Non-conservation, top detector, cos(#theta_{e})>0.77");
	
  can11->Modified();
  can11->Update();
			  
  //end drawing can11 (Residual Pad 1)
  ////////////////////////////////////////////////////


  char* st2 = NULL;

  //cout << "What file would you like to use? \n";
  //cin >> st1;
	
  st2 = "test_10um_0"; //change file name here (without .root)
	
  TString baseline2 = (Form("%s.root", st2));
					  
  TFile *f2 = new TFile(baseline2);
  TTree *tbaseline2 = (TTree*)f2->Get("dynamicTree");

  gStyle->SetPalette(20,palette);

  gStyle->SetStatX(0.95); 
  gStyle->SetStatY(1); 
  gStyle->SetStatW(0.12);
  gStyle->SetStatH(0.15);

	
  ////////////////////////////////////////////////////
  //start to draw can12 (Analyzer Pad 2)
	
  TCanvas *can12 = new TCanvas("can12", "Diagnostic Pad 2", 1100, 800);
  gStyle->SetOptStat(1111);
  can12->Divide(3, 3, 0.005, 0.005);
	
  can12->cd(1)->SetGrid();
  can12->cd(1)->SetLogy();
  tbaseline2->Draw("(EenergyBeforeDetector*1000-eE0)>>hh1()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]<0 && eP0z<0", "");
  //TGraph *h1 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hh1 = (TH1D*) gDirectory->Get("hh1");
	
  hh1->SetMarkerColor(4);
  hh1->SetLineColor(4);
  hh1->SetMarkerStyle(20);
  hh1->SetXTitle("#Delta E (keV)");
  hh1->GetXaxis()->CenterTitle();
  hh1->SetTitle("Energy Non-conservation, bottom detector, cos(#theta_{e})<0");
	
  can12->Update();
  can12->Modified();
	
  can12->cd(2)->SetGrid();
  can12->cd(2)->SetLogy();
  tbaseline2->Draw("(EenergyBeforeDetector*1000-eE0)>>hh2()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]<0 && eP0z>0 && eP0z<0.7", "");
  //TGraph *h2 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hh2 = (TH1D*) gDirectory->Get("hh2");
	
  hh2->SetMarkerColor(4);
  hh2->SetLineColor(4);
  hh2->SetMarkerStyle(20);
  hh2->SetXTitle("#Delta E (keV)");
  hh2->GetXaxis()->CenterTitle();
  hh2->SetTitle("Energy Non-conservation, bottom detector, 0<cos(#theta_{e})<0.7");
  can12->Modified();
	
  can12->Modified();
  can12->Update();
	
  can12->cd(3)->SetGrid();
  can12->cd(3)->SetLogy();
  tbaseline2->Draw("(EenergyBeforeDetector*1000-eE0)>>hh3()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]<0 && eP0z>0.7 && eP0z<0.77", "");
  //TGraph *h3 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hh3 = (TH1D*) gDirectory->Get("hh3");
	
  hh3->SetMarkerColor(4);
  hh3->SetLineColor(4);
  hh3->SetMarkerStyle(20);
  hh3->SetXTitle("#Delta E (keV)");
  hh3->GetXaxis()->CenterTitle();
  hh3->SetTitle("Energy Non-conservation, bottom detector, 0.7<cos(#theta_{e})<0.77");
	
  can12->Modified();
	
  can12->Modified();
  can12->Update();
 
  can12->cd(4)->SetGrid();
  can12->cd(4)->SetLogy();
  tbaseline2->Draw("(EenergyBeforeDetector*1000-eE0)>>hh4()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]<0 && eP0z>0.77", "");
  //TGraph *h3 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hh4 = (TH1D*) gDirectory->Get("hh4");
	
  hh4->SetMarkerColor(4);
  hh4->SetLineColor(4);
  hh4->SetMarkerStyle(20);
  hh4->SetXTitle("#Delta E (keV)");
  hh4->GetXaxis()->CenterTitle();
  hh4->SetTitle("Energy Non-conservation, bottom detector, cos(#theta_{e})>0.77");
	
  can12->Modified();
	
  can12->Modified();
  can12->Update(); 

  can12->cd(5)->SetGrid();
  can12->cd(5)->SetLogy();
  tbaseline2->Draw("(EenergyBeforeDetector*1000-eE0)>>hh5()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]>0 && eP0z<0.7", "");
  //TGraph *h1 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hh5 = (TH1D*) gDirectory->Get("hh5");
	
  hh5->SetMarkerColor(4);
  hh5->SetLineColor(4);
  hh5->SetMarkerStyle(20);
  hh5->SetXTitle("#Delta E (keV)");
  hh5->GetXaxis()->CenterTitle();
  hh5->SetTitle("Energy Non-conservation, top detector, cos(#theta_{e})<0.7");
	
  can12->Update();
  can12->Modified();
	
  can12->cd(6)->SetGrid();
  can12->cd(6)->SetLogy();
  tbaseline2->Draw("(EenergyBeforeDetector*1000-eE0)>>hh6()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]>0 && eP0z>0.7 && eP0z<0.77", "");
  //TGraph *h2 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hh6 = (TH1D*) gDirectory->Get("hh6");
	
  hh6->SetMarkerColor(4);
  hh6->SetLineColor(4);
  hh6->SetMarkerStyle(20);
  hh6->SetXTitle("#Delta E (keV)");
  hh6->GetXaxis()->CenterTitle();
  hh6->SetTitle("Energy Non-conservation, top detector, 0.7<cos(#theta_{e})<0.77");
  can12->Modified();
	
  can12->Modified();
  can12->Update();
	
  can12->cd(7)->SetGrid();
  can12->cd(7)->SetLogy();
  tbaseline2->Draw("(EenergyBeforeDetector*1000-eE0)>>hh7()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]>0 && eP0z>0.77", "");
  //TGraph *h3 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hh7 = (TH1D*) gDirectory->Get("hh7");
	
  hh7->SetMarkerColor(4);
  hh7->SetLineColor(4);
  hh7->SetMarkerStyle(20);
  hh7->SetXTitle("#Delta E (keV)");
  hh7->GetXaxis()->CenterTitle();
  hh7->SetTitle("Energy Non-conservation, top detector, cos(#theta_{e})>0.77");
	
  can12->Modified();
	
  can12->Modified();
  can12->Update();
			  
  //end drawing can12 (Residual Pad 2)
  ////////////////////////////////////////////////////



  char* st3 = NULL;

  //cout << "What file would you like to use? \n";
  //cin >> st1;
	
  st3 = "test_100um_0"; //change file name here (without .root)
	
  TString baseline3 = (Form("%s.root", st3));
					  
  TFile *f3 = new TFile(baseline3);
  TTree *tbaseline3 = (TTree*)f3->Get("dynamicTree");

  gStyle->SetPalette(20,palette);

  gStyle->SetStatX(0.95); 
  gStyle->SetStatY(1); 
  gStyle->SetStatW(0.12);
  gStyle->SetStatH(0.15);

	
  ////////////////////////////////////////////////////
  //start to draw can13 (Analyzer Pad 3)
	
  TCanvas *can13 = new TCanvas("can13", "Diagnostic Pad 3", 1100, 800);
  gStyle->SetOptStat(1111);
  can13->Divide(3, 3, 0.005, 0.005);
	
  can13->cd(1)->SetGrid();
  can13->cd(1)->SetLogy();
  tbaseline3->Draw("(EenergyBeforeDetector*1000-eE0)>>hhh1()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]<0 && eP0z<0", "");
  //TGraph *h1 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hhh1 = (TH1D*) gDirectory->Get("hhh1");
	
  hhh1->SetMarkerColor(4);
  hhh1->SetLineColor(4);
  hhh1->SetMarkerStyle(20);
  hhh1->SetXTitle("#Delta E (keV)");
  hhh1->GetXaxis()->CenterTitle();
  hhh1->SetTitle("Energy Non-conservation, bottom detector, cos(#theta_{e})<0");
	
  can13->Update();
  can13->Modified();
	
  can13->cd(2)->SetGrid();
  can13->cd(2)->SetLogy();
  tbaseline3->Draw("(EenergyBeforeDetector*1000-eE0)>>hhh2()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]<0 && eP0z>0 && eP0z<0.7", "");
  //TGraph *h2 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hhh2 = (TH1D*) gDirectory->Get("hhh2");
	
  hhh2->SetMarkerColor(4);
  hhh2->SetLineColor(4);
  hhh2->SetMarkerStyle(20);
  hhh2->SetXTitle("#Delta E (keV)");
  hhh2->GetXaxis()->CenterTitle();
  hhh2->SetTitle("Energy Non-conservation, bottom detector, 0<cos(#theta_{e})<0.7");

  can13->Modified();
  can13->Update();
	
  can13->cd(3)->SetGrid();
  can13->cd(3)->SetLogy();
  tbaseline3->Draw("(EenergyBeforeDetector*1000-eE0)>>hhh3()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]<0 && eP0z>0.7 && eP0z<0.77", "");
  //TGraph *h3 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hhh3 = (TH1D*) gDirectory->Get("hhh3");
	
  hhh3->SetMarkerColor(4);
  hhh3->SetLineColor(4);
  hhh3->SetMarkerStyle(20);
  hhh3->SetXTitle("#Delta E (keV)");
  hhh3->GetXaxis()->CenterTitle();
  hhh3->SetTitle("Energy Non-conservation, bottom detector, 0.7<cos(#theta_{e})<0.77");
	
  can13->Modified();
  can13->Update();
 
  can13->cd(4)->SetGrid();
  can13->cd(4)->SetLogy();
  tbaseline3->Draw("(EenergyBeforeDetector*1000-eE0)>>hhh4()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]<0 && eP0z>0.77", "");
  //TGraph *h3 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hhh4 = (TH1D*) gDirectory->Get("hhh4");
	
  hhh4->SetMarkerColor(4);
  hhh4->SetLineColor(4);
  hhh4->SetMarkerStyle(20);
  hhh4->SetXTitle("#Delta E (keV)");
  hhh4->GetXaxis()->CenterTitle();
  hhh4->SetTitle("Energy Non-conservation, bottom detector, cos(#theta_{e})>0.77");
	
  can13->Modified();
  can13->Update(); 

  can13->cd(5)->SetGrid();
  can13->cd(5)->SetLogy();
  tbaseline3->Draw("(EenergyBeforeDetector*1000-eE0)>>hhh5()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]>0 && eP0z<0.7", "");
  //TGraph *h1 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hhh5 = (TH1D*) gDirectory->Get("hhh5");
	
  hhh5->SetMarkerColor(4);
  hhh5->SetLineColor(4);
  hhh5->SetMarkerStyle(20);
  hhh5->SetXTitle("#Delta E (keV)");
  hhh5->GetXaxis()->CenterTitle();
  hhh5->SetTitle("Energy Non-conservation, top detector, cos(#theta_{e})<0.7");
	
  can13->Update();
  can13->Modified();
	
  can13->cd(6)->SetGrid();
  can13->cd(6)->SetLogy();
  tbaseline3->Draw("(EenergyBeforeDetector*1000-eE0)>>hhh6()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]>0 && eP0z>0.7 && eP0z<0.77", "");
  //TGraph *h2 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hhh6 = (TH1D*) gDirectory->Get("hhh6");
	
  hhh6->SetMarkerColor(4);
  hhh6->SetLineColor(4);
  hhh6->SetMarkerStyle(20);
  hhh6->SetXTitle("#Delta E (keV)");
  hhh6->GetXaxis()->CenterTitle();
  hhh6->SetTitle("Energy Non-conservation, top detector, 0.7<cos(#theta_{e})<0.77");
	
  can13->Modified();
  can13->Update();
	
  can13->cd(7)->SetGrid();
  can13->cd(7)->SetLogy();
  tbaseline3->Draw("(EenergyBeforeDetector*1000-eE0)>>hhh7()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]>0 && eP0z>0.77", "");
  //TGraph *h3 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hhh7 = (TH1D*) gDirectory->Get("hhh7");
	
  hhh7->SetMarkerColor(4);
  hhh7->SetLineColor(4);
  hhh7->SetMarkerStyle(20);
  hhh7->SetXTitle("#Delta E (keV)");
  hhh7->GetXaxis()->CenterTitle();
  hhh7->SetTitle("Energy Non-conservation, top detector, cos(#theta_{e})>0.77");
	
  can13->Modified();
  can13->Update();
			  
  //end drawing can13 (Residual Pad 3)
  ////////////////////////////////////////////////////



  char* st4 = NULL;

  //cout << "What file would you like to use? \n";
  //cin >> st1;
	
  st4 = "test_1mm_0"; //change file name here (without .root)
	
  TString baseline4 = (Form("%s.root", st4));
					  
  TFile *f4 = new TFile(baseline4);
  TTree *tbaseline4 = (TTree*)f4->Get("dynamicTree");

  gStyle->SetPalette(20,palette);

  gStyle->SetStatX(0.95); 
  gStyle->SetStatY(1); 
  gStyle->SetStatW(0.12);
  gStyle->SetStatH(0.15);

	
  ////////////////////////////////////////////////////
  //start to draw can14 (Analyzer Pad 4)
	
  TCanvas *can14 = new TCanvas("can14", "Diagnostic Pad 4", 1100, 800);
  gStyle->SetOptStat(1111);
  can14->Divide(3, 3, 0.005, 0.005);
	
  can14->cd(1)->SetGrid();
  can14->cd(1)->SetLogy();
  tbaseline4->Draw("(EenergyBeforeDetector*1000-eE0)>>hhhh1()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]<0 && eP0z<0", "");
  //TGraph *h1 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hhhh1 = (TH1D*) gDirectory->Get("hhhh1");
	
  hhhh1->SetMarkerColor(4);
  hhhh1->SetLineColor(4);
  hhhh1->SetMarkerStyle(20);
  hhhh1->SetXTitle("#Delta E (keV)");
  hhhh1->GetXaxis()->CenterTitle();
  hhhh1->SetTitle("Energy Non-conservation, bottom detector, cos(#theta_{e})<0");
	
  can14->Update();
  can14->Modified();
	
  can14->cd(2)->SetGrid();
  can14->cd(2)->SetLogy();
  tbaseline4->Draw("(EenergyBeforeDetector*1000-eE0)>>hhhh2()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]<0 && eP0z>0 && eP0z<0.7", "");
  //TGraph *h2 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hhhh2 = (TH1D*) gDirectory->Get("hhhh2");
	
  hhhh2->SetMarkerColor(4);
  hhhh2->SetLineColor(4);
  hhhh2->SetMarkerStyle(20);
  hhhh2->SetXTitle("#Delta E (keV)");
  hhhh2->GetXaxis()->CenterTitle();
  hhhh2->SetTitle("Energy Non-conservation, bottom detector, 0<cos(#theta_{e})<0.7");

  can14->Modified();
  can14->Update();
	
  can14->cd(3)->SetGrid();
  can14->cd(3)->SetLogy();
  tbaseline4->Draw("(EenergyBeforeDetector*1000-eE0)>>hhhh3()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]<0 && eP0z>0.7 && eP0z<0.77", "");
  //TGraph *h3 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hhhh3 = (TH1D*) gDirectory->Get("hhhh3");
	
  hhhh3->SetMarkerColor(4);
  hhhh3->SetLineColor(4);
  hhhh3->SetMarkerStyle(20);
  hhhh3->SetXTitle("#Delta E (keV)");
  hhhh3->GetXaxis()->CenterTitle();
  hhhh3->SetTitle("Energy Non-conservation, bottom detector, 0.7<cos(#theta_{e})<0.77");
	
  can14->Modified();
  can14->Update();
 
  can14->cd(4)->SetGrid();
  can14->cd(4)->SetLogy();
  tbaseline4->Draw("(EenergyBeforeDetector*1000-eE0)>>hhhh4()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]<0 && eP0z>0.77", "");
  //TGraph *h3 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hhhh4 = (TH1D*) gDirectory->Get("hhhh4");
	
  hhhh4->SetMarkerColor(4);
  hhhh4->SetLineColor(4);
  hhhh4->SetMarkerStyle(20);
  hhhh4->SetXTitle("#Delta E (keV)");
  hhhh4->GetXaxis()->CenterTitle();
  hhhh4->SetTitle("Energy Non-conservation, bottom detector, cos(#theta_{e})>0.77");
	
  can14->Modified();
  can14->Update(); 

  can14->cd(5)->SetGrid();
  can14->cd(5)->SetLogy();
  tbaseline4->Draw("(EenergyBeforeDetector*1000-eE0)>>hhhh5()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]>0 && eP0z<0.7", "");
  //TGraph *h1 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hhhh5 = (TH1D*) gDirectory->Get("hhhh5");
	
  hhhh5->SetMarkerColor(4);
  hhhh5->SetLineColor(4);
  hhhh5->SetMarkerStyle(20);
  hhhh5->SetXTitle("#Delta E (keV)");
  hhhh5->GetXaxis()->CenterTitle();
  hhhh5->SetTitle("Energy Non-conservation, top detector, cos(#theta_{e})<0.7");
	
  can14->Update();
  can14->Modified();
	
  can14->cd(6)->SetGrid();
  can14->cd(6)->SetLogy();
  tbaseline4->Draw("(EenergyBeforeDetector*1000-eE0)>>hhhh6()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]>0 && eP0z>0.7 && eP0z<0.77", "");
  //TGraph *h2 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hhhh6 = (TH1D*) gDirectory->Get("hhhh6");
	
  hhhh6->SetMarkerColor(4);
  hhhh6->SetLineColor(4);
  hhhh6->SetMarkerStyle(20);
  hhhh6->SetXTitle("#Delta E (keV)");
  hhhh6->GetXaxis()->CenterTitle();
  hhhh6->SetTitle("Energy Non-conservation, top detector, 0.7<cos(#theta_{e})<0.77");
	
  can14->Modified();
  can14->Update();
	
  can14->cd(7)->SetGrid();
  can14->cd(7)->SetLogy();
  tbaseline4->Draw("(EenergyBeforeDetector*1000-eE0)>>hhhh7()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]>0 && eP0z>0.77", "");
  //TGraph *h3 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hhhh7 = (TH1D*) gDirectory->Get("hhhh7");
	
  hhhh7->SetMarkerColor(4);
  hhhh7->SetLineColor(4);
  hhhh7->SetMarkerStyle(20);
  hhhh7->SetXTitle("#Delta E (keV)");
  hhhh7->GetXaxis()->CenterTitle();
  hhhh7->SetTitle("Energy Non-conservation, top detector, cos(#theta_{e})>0.77");
	
  can14->Modified();
  can14->Update();
			  
  //end drawing can14 (Residual Pad 4)
  ////////////////////////////////////////////////////



  char* st5 = NULL;

  //cout << "What file would you like to use? \n";
  //cin >> st1;
	
  st5 = "test_500um_0"; //change file name here (without .root)
	
  TString baseline5 = (Form("%s.root", st5));
					  
  TFile *f5 = new TFile(baseline5);
  TTree *tbaseline5 = (TTree*)f5->Get("dynamicTree");

  gStyle->SetPalette(20,palette);

  gStyle->SetStatX(0.95); 
  gStyle->SetStatY(1); 
  gStyle->SetStatW(0.12);
  gStyle->SetStatH(0.15);

	
  ////////////////////////////////////////////////////
  //start to draw can15 (Analyzer Pad 5)
	
  TCanvas *can15 = new TCanvas("can15", "Diagnostic Pad 5", 1100, 800);
  gStyle->SetOptStat(1111);
  can15->Divide(3, 3, 0.005, 0.005);
	
  can15->cd(1)->SetGrid();
  can15->cd(1)->SetLogy();
  tbaseline5->Draw("(EenergyBeforeDetector*1000-eE0)>>hhhhh1()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]<0 && eP0z<0", "");
  //TGraph *h1 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hhhhh1 = (TH1D*) gDirectory->Get("hhhhh1");
	
  hhhhh1->SetMarkerColor(4);
  hhhhh1->SetLineColor(4);
  hhhhh1->SetMarkerStyle(20);
  hhhhh1->SetXTitle("#Delta E (keV)");
  hhhhh1->GetXaxis()->CenterTitle();
  hhhhh1->SetTitle("Energy Non-conservation, bottom detector, cos(#theta_{e})<0");
	
  can15->Update();
  can15->Modified();
	
  can15->cd(2)->SetGrid();
  can15->cd(2)->SetLogy();
  tbaseline5->Draw("(EenergyBeforeDetector*1000-eE0)>>hhhhh2()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]<0 && eP0z>0 && eP0z<0.7", "");
  //TGraph *h2 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hhhhh2 = (TH1D*) gDirectory->Get("hhhhh2");
	
  hhhhh2->SetMarkerColor(4);
  hhhhh2->SetLineColor(4);
  hhhhh2->SetMarkerStyle(20);
  hhhhh2->SetXTitle("#Delta E (keV)");
  hhhhh2->GetXaxis()->CenterTitle();
  hhhhh2->SetTitle("Energy Non-conservation, bottom detector, 0<cos(#theta_{e})<0.7");

  can15->Modified();
  can15->Update();
	
  can15->cd(3)->SetGrid();
  can15->cd(3)->SetLogy();
  tbaseline5->Draw("(EenergyBeforeDetector*1000-eE0)>>hhhhh3()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]<0 && eP0z>0.7 && eP0z<0.77", "");
  //TGraph *h3 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hhhhh3 = (TH1D*) gDirectory->Get("hhhhh3");
	
  hhhhh3->SetMarkerColor(4);
  hhhhh3->SetLineColor(4);
  hhhhh3->SetMarkerStyle(20);
  hhhhh3->SetXTitle("#Delta E (keV)");
  hhhhh3->GetXaxis()->CenterTitle();
  hhhhh3->SetTitle("Energy Non-conservation, bottom detector, 0.7<cos(#theta_{e})<0.77");
	
  can15->Modified();
  can15->Update();
 
  can15->cd(4)->SetGrid();
  can15->cd(4)->SetLogy();
  tbaseline5->Draw("(EenergyBeforeDetector*1000-eE0)>>hhhhh4()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]<0 && eP0z>0.77", "");
  //TGraph *h3 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hhhhh4 = (TH1D*) gDirectory->Get("hhhhh4");
	
  hhhhh4->SetMarkerColor(4);
  hhhhh4->SetLineColor(4);
  hhhhh4->SetMarkerStyle(20);
  hhhhh4->SetXTitle("#Delta E (keV)");
  hhhhh4->GetXaxis()->CenterTitle();
  hhhhh4->SetTitle("Energy Non-conservation, bottom detector, cos(#theta_{e})>0.77");
	
  can15->Modified();
  can15->Update(); 

  can15->cd(5)->SetGrid();
  can15->cd(5)->SetLogy();
  tbaseline5->Draw("(EenergyBeforeDetector*1000-eE0)>>hhhhh5()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]>0 && eP0z<0.7", "");
  //TGraph *h1 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hhhhh5 = (TH1D*) gDirectory->Get("hhhhh5");
	
  hhhhh5->SetMarkerColor(4);
  hhhhh5->SetLineColor(4);
  hhhhh5->SetMarkerStyle(20);
  hhhhh5->SetXTitle("#Delta E (keV)");
  hhhhh5->GetXaxis()->CenterTitle();
  hhhhh5->SetTitle("Energy Non-conservation, top detector, cos(#theta_{e})<0.7");
	
  can15->Update();
  can15->Modified();
	
  can15->cd(6)->SetGrid();
  can15->cd(6)->SetLogy();
  tbaseline5->Draw("(EenergyBeforeDetector*1000-eE0)>>hhhhh6()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]>0 && eP0z>0.7 && eP0z<0.77", "");
  //TGraph *h2 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hhhhh6 = (TH1D*) gDirectory->Get("hhhhh6");
	
  hhhhh6->SetMarkerColor(4);
  hhhhh6->SetLineColor(4);
  hhhhh6->SetMarkerStyle(20);
  hhhhh6->SetXTitle("#Delta E (keV)");
  hhhhh6->GetXaxis()->CenterTitle();
  hhhhh6->SetTitle("Energy Non-conservation, top detector, 0.7<cos(#theta_{e})<0.77");
	
  can15->Modified();
  can15->Update();
	
  can15->cd(7)->SetGrid();
  can15->cd(7)->SetLogy();
  tbaseline5->Draw("(EenergyBeforeDetector*1000-eE0)>>hhhhh7()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]>0 && eP0z>0.77", "");
  //TGraph *h3 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hhhhh7 = (TH1D*) gDirectory->Get("hhhhh7");
	
  hhhhh7->SetMarkerColor(4);
  hhhhh7->SetLineColor(4);
  hhhhh7->SetMarkerStyle(20);
  hhhhh7->SetXTitle("#Delta E (keV)");
  hhhhh7->GetXaxis()->CenterTitle();
  hhhhh7->SetTitle("Energy Non-conservation, top detector, cos(#theta_{e})>0.77");
	
  can15->Modified();
  can15->Update();
			  
  //end drawing can15 (Residual Pad 5)
  ////////////////////////////////////////////////////

		
  string st14, st1test;
	
  cout << "\n\nHello, testing... \nPress any key to continue! \n"; //if not included, this function thinks the last enter is this cin
  getline (cin, st1test);
	
  cout << "Would you like to save the residual pad .pdf? (y or n): ";
  getline (cin, st14);
  if (st14 == "y")
    {
      //save a ps!
      can11->Print(Form("%s_diagnostics.pdf(", st1));

      can12->Print(Form("%s_diagnostics.pdf(", st1));

      can15->Print(Form("%s_diagnostics.pdf(", st1));

      can13->Print(Form("%s_diagnostics.pdf(", st1));

      can14->Print(Form("%s_diagnostics.pdf)", st1));

      //can11->Print(Form("%s_diagnostics.ps(", st1));

      //can12->Print(Form("%s_diagnostics.ps)", st1));

      printf("Finished saving files \n");
	       
    }
  else 
    {
      printf("pad not saved \n");
    }
	
  return;
	
}
