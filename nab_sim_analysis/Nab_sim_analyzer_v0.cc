#include "TROOT.h"
#include "TStyle.h"
#include "TTree.h"
#include "TLatex.h"
#include "TGraphErrors.h"
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <math.h>
#include <TArrayD.h>
#include "TH2D.h"
#include "TH1D.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "TMath.h"
#include "TRint.h"
#include "TCanvas.h"
#include "TTreeFormula.h"
#include "TLegend.h"
#include "TFile.h"
#include "TCut.h"
#include "TImage.h"
#include "TSystem.h"
#include "TPaveText.h"
#include <sys/time.h>
#include <sys/resource.h>
#include "TColor.h"
using namespace std;


void Nab_sim_analyzer_v0()
{
  char* st1 = NULL;
	
  //cout << "What file would you like to use? \n";
  //cin >> st1;
	
  st1 = "ContEe_1m_all_dir"; //change file name here (without .root)
	
  TString baseline = (Form("%s.root", st1));
					  
  TFile *f1 = new TFile(baseline);
  TTree *tbaseline = (TTree*)f1->Get("dynamicTree");
	
  ////////////////////////////////////////////////////
  //start to draw can11 (Analyzer Pad 1)
	
  TCanvas *can11 = new TCanvas("can11", "Diagnostic Pad 1", 1100, 800);
  gStyle->SetOptStat(1111);
  can11->Divide(3, 3, 0.005, 0.005);
		
  can11->cd(1)->SetGrid();
  can11->cd(1)->SetLogy();
  tbaseline->Draw("Trigger_time>>h1()", "TimeOfFlight>0 && Trigger_particleType==2 && Trigger_detectorNb==1 && Trigger_parentID==0", "");
  //TGraph *h1 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *h1 = (TH1D*) gDirectory->Get("h1");
	
  h1->SetMarkerColor(4);
  h1->SetLineColor(4);
  h1->SetMarkerStyle(20);
  h1->SetXTitle("Trigger time (ns)");
  h1->GetXaxis()->CenterTitle();
  h1->SetTitle("Trigger time e-, upper detector");
  can11->Modified();

  gStyle->SetStatX(0.95); 
  gStyle->SetStatY(1); 
  gStyle->SetStatW(0.12);
  gStyle->SetStatH(0.15); 

  can11->Update();
  can11->Modified();
		
  can11->cd(2)->SetGrid();
  can11->cd(2)->SetLogy();
  tbaseline->Draw("Trigger_time>>h2()", "TimeOfFlight>0 && Trigger_particleType==2 && Trigger_detectorNb==-1 && Trigger_parentID==0", "");
  //TGraph *h2 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *h2 = (TH1D*) gDirectory->Get("h2");
	
  h2->SetMarkerColor(4);
  h2->SetLineColor(4);
  h2->SetMarkerStyle(20);
  h2->SetXTitle("Trigger time (ns)");
  h2->GetXaxis()->CenterTitle();
  h2->SetTitle("Trigger time e-, lower detector");
  can11->Modified();
	
  can11->Modified();
  can11->Update();
	
  can11->cd(3)->SetGrid();
  can11->cd(3)->SetLogy();
  tbaseline->Draw("TimeOfFlight>>h3()", "TimeOfFlight>0 && PFirstHitDetNb==1", "");
  //TGraph *h3 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *h3 = (TH1D*) gDirectory->Get("h3");
	
  h3->SetMarkerColor(4);
  h3->SetLineColor(4);
  h3->SetMarkerStyle(20);
  h3->SetXTitle("TOF (#mus)");
  h3->GetXaxis()->CenterTitle();
  h3->SetTitle("TOF p, upper detector");
  can11->Modified();
	
  can11->Modified();
  can11->Update();
	
  can11->cd(4)->SetGrid();
  can11->cd(4)->SetLogy();
  tbaseline->Draw("TimeOfFlight>>h4()", "TimeOfFlight>0 && PFirstHitDetNb==-1", "");
  //TGraph *h4 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *h4 = (TH1D*) gDirectory->Get("h4");
	
  h4->SetMarkerColor(4);
  h4->SetLineColor(4);
  h4->SetMarkerStyle(20);
  h4->SetXTitle("TOF (#mus)");
  h4->GetXaxis()->CenterTitle();
  h4->SetTitle("TOF p, lower detector");
  can11->Modified();
	
  can11->Modified();
  can11->Update();
	
  can11->cd(5)->SetGrid();
  can11->cd(5)->SetLogy();
  tbaseline->Draw("EDepElectron>>h5()", "TimeOfFlight>0 && Trigger_particleType==2 && Trigger_detectorNb==1", "");
  //TGraph *h5 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *h5 = (TH1D*) gDirectory->Get("h5");
	
  h5->SetMarkerColor(4);
  h5->SetLineColor(4);
  h5->SetMarkerStyle(20);
  h5->SetXTitle("Deposited electron energy (keV)");
  h5->GetXaxis()->CenterTitle();
  h5->SetTitle("Deposited electron energy, upper detector");
  can11->Modified();
	
  can11->Modified();
  can11->Update();
	
  can11->cd(6)->SetGrid();
  can11->cd(6)->SetLogy();
  tbaseline->Draw("EDepElectron>>h6()", "TimeOfFlight>0 && Trigger_particleType==2 && Trigger_detectorNb==-1", "");
  //TGraph *h6 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *h6 = (TH1D*) gDirectory->Get("h6");
	
  h6->SetMarkerColor(4);
  h6->SetLineColor(4);
  h6->SetMarkerStyle(20);
  h6->SetXTitle("Deposited electron energy (keV)");
  h6->GetXaxis()->CenterTitle();
  h6->SetTitle("Deposited electron energy, lower detector");
  can11->Modified();
	
  can11->Modified();
  can11->Update();
	
  gStyle->SetOptFit();
  TF1 *f2 = new TF1("f2","pol1", 0.0012, 0.004);

  can11->cd(7)->SetGrid();
  tbaseline->Draw("1/(TimeOfFlight*TimeOfFlight)>>h7()", "TimeOfFlight>0 && eE0>220 && eE0<240 && PFirstHitDetNb==1 && 1/(TimeOfFlight*TimeOfFlight)>0 && 1/(TimeOfFlight*TimeOfFlight)<0.005", "");
  //TGraph *h7 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *h7 = (TH1D*) gDirectory->Get("h7");
	
  h7->SetMarkerColor(4);
  h7->SetLineColor(4);
  h7->SetMarkerStyle(20);
  h7->SetXTitle("1/TOF^{2} (s^{-2})");
  h7->GetXaxis()->CenterTitle();
  h7->SetTitle("Fit for a");
  h7->Fit(f2,"VR");

  can11->Modified();

  can11->Modified();
  can11->Update();
	
  can11->cd(8)->SetGrid();
  can11->cd(8)->SetLogy();
  tbaseline->Draw("pP0z>>h8()", "TimeOfFlight>0", "");
  //TGraph *h8 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *h8 = (TH1D*) gDirectory->Get("h8");
	
  h8->SetMarkerColor(4);
  h8->SetLineColor(4);
  h8->SetMarkerStyle(20);
  h8->SetXTitle("Proton cos(#theta_{0})");
  h8->GetXaxis()->CenterTitle();
  h8->SetTitle("Proton cos(#theta_{0})");
  can11->Modified();
	
  can11->Modified();
  can11->Update();
	
  can11->cd(9)->SetGrid();
  can11->cd(9)->SetLogy();
  tbaseline->Draw("pE0>>h9()", "TimeOfFlight>0", "");
  //TGraph *h9 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *h9 = (TH1D*) gDirectory->Get("h9");
	
  h9->SetMarkerColor(4);
  h9->SetLineColor(4);
  h9->SetMarkerStyle(20);
  h9->SetXTitle("Proton Energy (eV)");
  h9->GetXaxis()->CenterTitle();
  h9->SetTitle("Proton Energy");
  can11->Modified();
					  
  can11->Modified();
  can11->Update();
					  
  //end drawing can11 (Residual Pad 1)
  ////////////////////////////////////////////////////
	
  ////////////////////////////////////////////////////
  //start to draw can12 (Analyzer Pad 2)
	
  TCanvas *can12 = new TCanvas("can12", "Diagnostic Pad 2", 1100, 800);
  gStyle->SetOptStat(1111);
  can12->Divide(3, 3, 0.005, 0.005);
	
  can12->cd(1)->SetGrid();
  can12->cd(1)->SetLogy();
  tbaseline->Draw("(eE0)>>hh1()", "TimeOfFlight>0", "");
  //TGraph *h1 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hh1 = (TH1D*) gDirectory->Get("hh1");
	
  hh1->SetMarkerColor(4);
  hh1->SetLineColor(4);
  hh1->SetMarkerStyle(20);
  hh1->SetXTitle("Electron Energy (keV)");
  hh1->GetXaxis()->CenterTitle();
  hh1->SetTitle("Electron Energy");
  can12->Modified();
	
  can12->Modified();
  can12->Update();

  can12->cd(2)->SetGrid();
  can12->cd(2)->SetLogy();
  tbaseline->Draw("(EenergyBeforeDetector*1000-eE0)>>hh2()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]>0", "");
  //TGraph *h1 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hh2 = (TH1D*) gDirectory->Get("hh2");
	
  hh2->SetMarkerColor(4);
  hh2->SetLineColor(4);
  hh2->SetMarkerStyle(20);
  hh2->SetXTitle("#Delta E (keV)");
  hh2->GetXaxis()->CenterTitle();
  hh2->SetTitle("Energy Non-conservation, upper detector");
  can12->Modified();
	
  can12->Update();
  can12->Modified();
	
  can12->cd(3)->SetGrid();
  can12->cd(3)->SetLogy();
  tbaseline->Draw("(EenergyBeforeDetector*1000-eE0)>>hh3()", "EenergyBeforeDetector!=-10^6 && EimpactPosition[2]<0", "");
  //TGraph *h2 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hh3 = (TH1D*) gDirectory->Get("hh3");
	
  hh3->SetMarkerColor(4);
  hh3->SetLineColor(4);
  hh3->SetMarkerStyle(20);
  hh3->SetXTitle("#Delta E (keV)");
  hh3->GetXaxis()->CenterTitle();
  hh3->SetTitle("Energy Non-conservation, lower detector");
  can12->Modified();
	
  can12->Modified();
  can12->Update();
	
  can12->cd(4)->SetGrid();
  tbaseline->Draw("KillStatus.status>>hh4()", "", "");
  //TGraph *h4 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hh4 = (TH1D*) gDirectory->Get("hh4");
	
  hh4->SetMarkerColor(4);
  hh4->SetLineColor(4);
  hh4->SetMarkerStyle(20);
  hh4->SetXTitle("Kill Status");
  hh4->GetXaxis()->CenterTitle();
  hh4->SetTitle("1 = G4 can't converge, 2 = too many steps, 3 = trapped");
	
  can12->Modified();
	
  can12->Modified();
  can12->Update();
	
  can12->cd(5)->SetGrid();
  tbaseline->Draw("ElastZ[9]>>hh5()", "EenergyBeforeDetector==-10^6 && @KillStatus.size()==0", "");
  //TGraph *h5 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hh5 = (TH1D*) gDirectory->Get("hh5");
	
  hh5->SetMarkerColor(4);
  hh5->SetLineColor(4);
  hh5->SetMarkerStyle(20);
  hh5->SetXTitle("Last z coordinate before impact (mm)");
  hh5->GetXaxis()->CenterTitle();
  hh5->SetTitle("Last z for non-trapped, non-detected electrons");
  can12->Modified();
	
  can12->Modified();
  can12->Update();
	
  can12->cd(6)->SetGrid();
  tbaseline->Draw("PlastZ[9]>>hh6()", "PenergyBeforeDetector==-10^6 && @KillStatus.size()==0", "");
  //TGraph *h5 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hh6 = (TH1D*) gDirectory->Get("hh6");
	
  hh6->SetMarkerColor(4);
  hh6->SetLineColor(4);
  hh6->SetMarkerStyle(20);
  hh6->SetXTitle("Last z coordinate before impact (mm)");
  hh6->GetXaxis()->CenterTitle();
  hh6->SetTitle("Last z for non-trapped, non-detected protons");
  can12->Modified();
	
  can12->Modified();
  can12->Update();

  can12->cd(7)->SetGrid();
  can12->cd(7)->SetLogy();
  tbaseline->Draw("(PlastR[9])>>hh7()", "PenergyBeforeDetector==-10^6 && @KillStatus.size()==0", "");
  //TGraph *h7 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hh7 = (TH1D*) gDirectory->Get("hh7");
	
  hh7->SetMarkerColor(4);
  hh7->SetLineColor(4);
  hh7->SetMarkerStyle(20);
  hh7->SetXTitle("Last R coordinate before impact (mm)");
  hh7->GetXaxis()->CenterTitle();
  hh7->SetTitle("Last R for non-trapped, non-detected protons");
  can12->Modified();
	
  can12->Modified();
  can12->Update();
	
  can12->cd(8)->SetGrid();
  can12->cd(8)->SetLogy();
  tbaseline->Draw("(EmidPreZ)>>hh8()", "TimeOfFlight>0", "");
  //TGraph *h8 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hh8 = (TH1D*) gDirectory->Get("hh8");
	
  hh8->SetMarkerColor(4);
  hh8->SetLineColor(4);
  hh8->SetMarkerStyle(20);
  hh8->SetXTitle("Mid plane z coordinate of electrons (mm)");
  hh8->GetXaxis()->CenterTitle();
  hh8->SetTitle("Mid z for TOF>0 electrons");
  can12->Modified();
	
  can12->Modified();
  can12->Update();
	
  can12->cd(9)->SetGrid();
  can12->cd(9)->SetLogy();
  tbaseline->Draw("(PmidPreZ)>>hh9()", "TimeOfFlight>0", "");
  //TGraph *h9 = (TGraph*) gPad->GetPrimitive("Graph");
  TH1D *hh9 = (TH1D*) gDirectory->Get("hh9");
	
  hh9->SetMarkerColor(4);
  hh9->SetLineColor(4);
  hh9->SetMarkerStyle(20);
  hh9->SetXTitle("Mid plane z coordinate of protons (mm)");
  hh9->GetXaxis()->CenterTitle();
  hh9->SetTitle("Mid z for TOF>0 protons");
  can12->Modified();
	
  can12->Modified();
  can12->Update();
	
  //end drawing can12 (Residual Pad 2)
  ////////////////////////////////////////////////////
	
  string st14, st1test;
	
  cout << "Hello, testing... \nPress any key! \n"; //if not included, this function thinks the last enter is this cin
  getline (cin, st1test);
	
  cout << "Would you like to save the residual pad .ps? (y or n): ";
  getline (cin, st14);
  if (st14 == "y")
    {
      //save a ps!
      can11->Print(Form("%s_diagnostics1.ps", st1));
      printf("Saving files: \n%s1.ps \n", st1);

      can12->Print(Form("%s_diagnostics2.ps", st1));
      printf("Saving files: \n%s2.ps \n", st1);
		
    }
  else 
    {
      printf("pad not saved \n");
    }
	
  return;
	
}
