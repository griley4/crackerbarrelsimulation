#include "TROOT.h"
#include "TStyle.h"
#include "TTree.h"
#include "TLatex.h"
#include "TGraphErrors.h"
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <math.h>
#include <TArrayD.h>
#include "TH2D.h"
#include "TH1D.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "TMath.h"
#include "TRint.h"
#include "TCanvas.h"
#include "TTreeFormula.h"
#include "TLegend.h"
#include "TFile.h"
#include "TCut.h"
#include "TImage.h"
#include "TSystem.h"
#include "TPaveText.h"
#include "TPaveStats.h"
#include <sys/time.h>
#include <sys/resource.h>
#include "TColor.h"
using namespace std;


void PMagmomConservation()
{

  TTree *MyTree1 = new TTree("MyTree1", "MyTree1");	
  MyTree1->ReadFile("../../PMagmomConservation_20160812_ferenc_200um_4604008", "eventID:X:Y:Z:E:B:sin2:Radius:Gyrophase:PConserved:PMagmom");
  //Int_t palette[20];
  //for (int i=0;i<20;i++) palette[i] = 100-2*(19-i);
  //gStyle->SetPalette(20,palette);

  ////////////////////////////////////////////////////
  //start to draw can11 (Analyzer Pad 1)
	
  TCanvas *can11 = new TCanvas("can11", "Diagnostic Pad 1", 1100, 800);
  gStyle->SetOptStat(1111);
  can11->Divide(1, 1, 0.005, 0.005);
		
  can11->cd(1)->SetGrid();
  can11->cd(1)->SetLogy();
  MyTree1->Draw("E>>h1()", "Z<4.999", "");//"sqrt(X*X+Y*Y):Z>>h1(520,-0.2,5,220,0.004,0.026)", "PMagmom*(Z<4.999)", "COL"//10000,8.8E-18,9.7E-18
  TH1D *h1 = (TH1D*) gDirectory->Get("h1");
  can11->Modified();	
  can11->Update();
/*
  MyTree1->Draw("sqrt(X*X+Y*Y):Z>>h2(520,-0.2,5,220,0.004,0.026)", "Z<4.999","COL SAME");//abs(diffBz/Bz)>10^-6
  TH1D *h2 = (TH1D*) gDirectory->Get("h2");
  can11->Modified();	
  can11->Update();

  h1->Divide(h2);
  h1->Draw("COLZ");
*/	
  h1->SetMarkerSize(100);
  h1->SetXTitle("Z (m)");
  h1->GetXaxis()->CenterTitle();
  h1->SetYTitle("R (m)");
  h1->GetYaxis()->CenterTitle();
  h1->GetZaxis()->SetLabelSize(0.025); 
  h1->SetTitle("PConserved (SI unit)");
  can11->Modified();	
  can11->Update();

  //can11->Print("FieldProfile_bilinear_vs_bicubic_to_ferenc_B.pdf");
  //can11->Print("FieldProfile_2D_vs_1D_to_ferenc_EFR_B.png");
  //can12->Print("FieldProfile_2D_vs_1D_to_ferenc_EFR_E.png");
		
}
