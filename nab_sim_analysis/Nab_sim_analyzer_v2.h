//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Nov  2 12:49:42 2017 by ROOT version 5.34/21
// from TTree dynamicTree/dynamicTree
// found on file: /pool/a/pibeta/nab/NabSim_output/09052017_10m_forward_1DE_ferencB_witha.root
//////////////////////////////////////////////////////////

#ifndef Nab_sim_analyzer_v2_h
#define Nab_sim_analyzer_v2_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include <vector>

// Fixed size dimensions of array or collections stored in the TTree if any.

class Nab_sim_analyzer_v2 {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Int_t           eventID;
   Double_t        x0;
   Double_t        y0;
   Double_t        z0;
   Double_t        pP0x;
   Double_t        pP0y;
   Double_t        pP0z;
   Double_t        pE0;
   Double_t        pE;
   Double_t        eP0x;
   Double_t        eP0y;
   Double_t        eP0z;
   Double_t        eE0;
   Double_t        eE;
   Int_t           bouncesE;
   Int_t           bouncesP;
   Int_t           PFirstHitDetNb;
   Int_t           EFirstHitDetNb;
   Double_t        PFirstHitDetTime;
   Double_t        EFirstHitDetTime;
   Int_t           DeadParticles;
   Int_t           NbOfGamma;
   Int_t           NbOfSE;
   Double_t        GammaEscape;
   Double_t        GammaLoss;
   Double_t        DLLoss;
   Double_t        SELoss;
   Double_t        SEDeposit;
   Double_t        eTotalDeposit;
   Double_t        pTotalDeposit;
   vector<double>  *Hit_time;
   vector<double>  *Hit_x;
   vector<double>  *Hit_y;
   vector<double>  *Hit_energy;
   vector<int>     *Hit_particleType;
   vector<int>     *Hit_parentID;
   vector<int>     *Hit_detectorNb;
   Double_t        GammaDeposit;
   vector<int>     *Trigger_bouncesEAtTrigger;
   vector<int>     *Trigger_bouncesPAtTrigger;
   Int_t           PixelStatus;
   Double_t        TimeOfFlight;
   Double_t        TimeOfFlight_proton_mm;
   Double_t        EDepElectron;
   Double_t        elapsedTime;
   Double_t        EimpactAngleViaPos;
   Double_t        EimpactAngleViaMomentum;
   Double_t        EenergyBeforeDetector;
   Double_t        EimpactPosition[3];
   Double_t        PimpactAngleViaPos;
   Double_t        PimpactAngleViaMomentum;
   Double_t        PenergyBeforeDetector;
   Double_t        PimpactPosition[3];
   Double_t        PMagmoment_mean;
   Double_t        PMagmoment_RMS;
   Double_t        PmidPreZ;
   Double_t        EdetMMPreX;
   Double_t        EdetMMPreY;
   Double_t        EdetMMPreZ;
   Double_t        EdetMMPostX;
   Double_t        EdetMMPostY;
   Double_t        EdetMMPostZ;
   Double_t        EdetMMPx;
   Double_t        EdetMMPy;
   Double_t        EdetMMPz;
   Double_t        EdetMME;
   Double_t        EdetMMT;
   Double_t        EdetMMPostT;
   Double_t        PdetMMPreX;
   Double_t        PdetMMPreY;
   Double_t        PdetMMPreZ;
   Double_t        PdetMMPostX;
   Double_t        PdetMMPostY;
   Double_t        PdetMMPostZ;
   Double_t        PdetMMPx;
   Double_t        PdetMMPy;
   Double_t        PdetMMPz;
   Double_t        PdetMME;
   Double_t        PdetMMT;
   Double_t        PdetMMPostT;

   // List of branches
   TBranch        *b_eventID;   //!
   TBranch        *b_x0;   //!
   TBranch        *b_y0;   //!
   TBranch        *b_z0;   //!
   TBranch        *b_P0xproton;   //!
   TBranch        *b_P0yproton;   //!
   TBranch        *b_P0zproton;   //!
   TBranch        *b_E0proton;   //!
   TBranch        *b_Eproton;   //!
   TBranch        *b_P0xelectron;   //!
   TBranch        *b_P0yelectron;   //!
   TBranch        *b_P0zelectron;   //!
   TBranch        *b_E0electron;   //!
   TBranch        *b_Eelectron;   //!
   TBranch        *b_bouncesE;   //!
   TBranch        *b_bouncesP;   //!
   TBranch        *b_PFirstHitDetNb;   //!
   TBranch        *b_EFirstHitDetNb;   //!
   TBranch        *b_PFirstHitDetTime;   //!
   TBranch        *b_EFirstHitDetTime;   //!
   TBranch        *b_deadParticles;   //!
   TBranch        *b_NbOfGammas;   //!
   TBranch        *b_NbOfSE;   //!
   TBranch        *b_gammaEscape;   //!
   TBranch        *b_gammaLoss;   //!
   TBranch        *b_DLLoss;   //!
   TBranch        *b_SELoss;   //!
   TBranch        *b_SEDeposit;   //!
   TBranch        *b_eTotalDeposit;   //!
   TBranch        *b_pTotalDeposit;   //!
   TBranch        *b_Hit_time;   //!
   TBranch        *b_Hit_x;   //!
   TBranch        *b_Hit_y;   //!
   TBranch        *b_Hit_energy;   //!
   TBranch        *b_Hit_particleType;   //!
   TBranch        *b_Hit_parentID;   //!
   TBranch        *b_Hit_detectorNb;   //!
   TBranch        *b_gammaDeposit;   //!
   TBranch        *b_Trigger_bouncesEAtTrigger;   //!
   TBranch        *b_Trigger_bouncesPAtTrigger;   //!
   TBranch        *b_PixelStatus;   //!
   TBranch        *b_TimeOfFlight;   //!
   TBranch        *b_TimeOfFlight_proton_mm;   //!
   TBranch        *b_EDepElectron;   //!
   TBranch        *b_elapsedTime;   //!
   TBranch        *b_EimpactAngleViaPos;   //!
   TBranch        *b_EimpactAngleViaMomentum;   //!
   TBranch        *b_EenergyBeforeDetector;   //!
   TBranch        *b_EimpactPosition;   //!
   TBranch        *b_PimpactAngleViaPos;   //!
   TBranch        *b_PimpactAngleViaMomentum;   //!
   TBranch        *b_PenergyBeforeDetector;   //!
   TBranch        *b_PimpactPosition;   //!
   TBranch        *b_PMagmoment_mean;   //!
   TBranch        *b_PMagmoment_RMS;   //!
   TBranch        *b_PmidPreZ;   //!
   TBranch        *b_EdetMMPreX;   //!
   TBranch        *b_EdetMMPreY;   //!
   TBranch        *b_EdetMMPreZ;   //!
   TBranch        *b_EdetMMPostX;   //!
   TBranch        *b_EdetMMPostY;   //!
   TBranch        *b_EdetMMPostZ;   //!
   TBranch        *b_EdetMMPx;   //!
   TBranch        *b_EdetMMPy;   //!
   TBranch        *b_EdetMMPz;   //!
   TBranch        *b_EdetMME;   //!
   TBranch        *b_EdetMMT;   //!
   TBranch        *b_EdetMMPostT;   //!
   TBranch        *b_PdetMMPreX;   //!
   TBranch        *b_PdetMMPreY;   //!
   TBranch        *b_PdetMMPreZ;   //!
   TBranch        *b_PdetMMPostX;   //!
   TBranch        *b_PdetMMPostY;   //!
   TBranch        *b_PdetMMPostZ;   //!
   TBranch        *b_PdetMMPx;   //!
   TBranch        *b_PdetMMPy;   //!
   TBranch        *b_PdetMMPz;   //!
   TBranch        *b_PdetMME;   //!
   TBranch        *b_PdetMMT;   //!
   TBranch        *b_PdetMMPostT;   //!

   Nab_sim_analyzer_v2(TTree *tree=0);
   virtual ~Nab_sim_analyzer_v2();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef Nab_sim_analyzer_v2_cxx
Nab_sim_analyzer_v2::Nab_sim_analyzer_v2(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(FILE_NAME);
      if (!f || !f->IsOpen()) {
         f = new TFile(FILE_NAME);
      }
      f->GetObject("dynamicTree",tree);

   }
   Init(tree);
}

Nab_sim_analyzer_v2::~Nab_sim_analyzer_v2()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t Nab_sim_analyzer_v2::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t Nab_sim_analyzer_v2::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void Nab_sim_analyzer_v2::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   Hit_time = 0;
   Hit_x = 0;
   Hit_y = 0;
   Hit_energy = 0;
   Hit_particleType = 0;
   Hit_parentID = 0;
   Hit_detectorNb = 0;
   Trigger_bouncesEAtTrigger = 0;
   Trigger_bouncesPAtTrigger = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("eventID", &eventID, &b_eventID);
   fChain->SetBranchAddress("x0", &x0, &b_x0);
   fChain->SetBranchAddress("y0", &y0, &b_y0);
   fChain->SetBranchAddress("z0", &z0, &b_z0);
   fChain->SetBranchAddress("pP0x", &pP0x, &b_P0xproton);
   fChain->SetBranchAddress("pP0y", &pP0y, &b_P0yproton);
   fChain->SetBranchAddress("pP0z", &pP0z, &b_P0zproton);
   fChain->SetBranchAddress("pE0", &pE0, &b_E0proton);
   fChain->SetBranchAddress("pE", &pE, &b_Eproton);
   fChain->SetBranchAddress("eP0x", &eP0x, &b_P0xelectron);
   fChain->SetBranchAddress("eP0y", &eP0y, &b_P0yelectron);
   fChain->SetBranchAddress("eP0z", &eP0z, &b_P0zelectron);
   fChain->SetBranchAddress("eE0", &eE0, &b_E0electron);
   fChain->SetBranchAddress("eE", &eE, &b_Eelectron);
   fChain->SetBranchAddress("bouncesE", &bouncesE, &b_bouncesE);
   fChain->SetBranchAddress("bouncesP", &bouncesP, &b_bouncesP);
   fChain->SetBranchAddress("PFirstHitDetNb", &PFirstHitDetNb, &b_PFirstHitDetNb);
   fChain->SetBranchAddress("EFirstHitDetNb", &EFirstHitDetNb, &b_EFirstHitDetNb);
   fChain->SetBranchAddress("PFirstHitDetTime", &PFirstHitDetTime, &b_PFirstHitDetTime);
   fChain->SetBranchAddress("EFirstHitDetTime", &EFirstHitDetTime, &b_EFirstHitDetTime);
   fChain->SetBranchAddress("DeadParticles", &DeadParticles, &b_deadParticles);
   fChain->SetBranchAddress("NbOfGamma", &NbOfGamma, &b_NbOfGammas);
   fChain->SetBranchAddress("NbOfSE", &NbOfSE, &b_NbOfSE);
   fChain->SetBranchAddress("GammaEscape", &GammaEscape, &b_gammaEscape);
   fChain->SetBranchAddress("GammaLoss", &GammaLoss, &b_gammaLoss);
   fChain->SetBranchAddress("DLLoss", &DLLoss, &b_DLLoss);
   fChain->SetBranchAddress("SELoss", &SELoss, &b_SELoss);
   fChain->SetBranchAddress("SEDeposit", &SEDeposit, &b_SEDeposit);
   fChain->SetBranchAddress("eTotalDeposit", &eTotalDeposit, &b_eTotalDeposit);
   fChain->SetBranchAddress("pTotalDeposit", &pTotalDeposit, &b_pTotalDeposit);
   fChain->SetBranchAddress("Hit_time", &Hit_time, &b_Hit_time);
   fChain->SetBranchAddress("Hit_x", &Hit_x, &b_Hit_x);
   fChain->SetBranchAddress("Hit_y", &Hit_y, &b_Hit_y);
   fChain->SetBranchAddress("Hit_energy", &Hit_energy, &b_Hit_energy);
   fChain->SetBranchAddress("Hit_particleType", &Hit_particleType, &b_Hit_particleType);
   fChain->SetBranchAddress("Hit_parentID", &Hit_parentID, &b_Hit_parentID);
   fChain->SetBranchAddress("Hit_detectorNb", &Hit_detectorNb, &b_Hit_detectorNb);
   fChain->SetBranchAddress("GammaDeposit", &GammaDeposit, &b_gammaDeposit);
   fChain->SetBranchAddress("Trigger_bouncesEAtTrigger", &Trigger_bouncesEAtTrigger, &b_Trigger_bouncesEAtTrigger);
   fChain->SetBranchAddress("Trigger_bouncesPAtTrigger", &Trigger_bouncesPAtTrigger, &b_Trigger_bouncesPAtTrigger);
   fChain->SetBranchAddress("PixelStatus", &PixelStatus, &b_PixelStatus);
   fChain->SetBranchAddress("TimeOfFlight", &TimeOfFlight, &b_TimeOfFlight);
   fChain->SetBranchAddress("TimeOfFlight_proton_mm", &TimeOfFlight_proton_mm, &b_TimeOfFlight_proton_mm);
   fChain->SetBranchAddress("EDepElectron", &EDepElectron, &b_EDepElectron);
   fChain->SetBranchAddress("elapsedTime", &elapsedTime, &b_elapsedTime);
   fChain->SetBranchAddress("EimpactAngleViaPos", &EimpactAngleViaPos, &b_EimpactAngleViaPos);
   fChain->SetBranchAddress("EimpactAngleViaMomentum", &EimpactAngleViaMomentum, &b_EimpactAngleViaMomentum);
   fChain->SetBranchAddress("EenergyBeforeDetector", &EenergyBeforeDetector, &b_EenergyBeforeDetector);
   fChain->SetBranchAddress("EimpactPosition[3]", EimpactPosition, &b_EimpactPosition);
   fChain->SetBranchAddress("PimpactAngleViaPos", &PimpactAngleViaPos, &b_PimpactAngleViaPos);
   fChain->SetBranchAddress("PimpactAngleViaMomentum", &PimpactAngleViaMomentum, &b_PimpactAngleViaMomentum);
   fChain->SetBranchAddress("PenergyBeforeDetector", &PenergyBeforeDetector, &b_PenergyBeforeDetector);
   fChain->SetBranchAddress("PimpactPosition[3]", PimpactPosition, &b_PimpactPosition);
   fChain->SetBranchAddress("PMagmoment_mean", &PMagmoment_mean, &b_PMagmoment_mean);
   fChain->SetBranchAddress("PMagmoment_RMS", &PMagmoment_RMS, &b_PMagmoment_RMS);
   fChain->SetBranchAddress("PmidPreZ", &PmidPreZ, &b_PmidPreZ);
   fChain->SetBranchAddress("EdetMMPreX", &EdetMMPreX, &b_EdetMMPreX);
   fChain->SetBranchAddress("EdetMMPreY", &EdetMMPreY, &b_EdetMMPreY);
   fChain->SetBranchAddress("EdetMMPreZ", &EdetMMPreZ, &b_EdetMMPreZ);
   fChain->SetBranchAddress("EdetMMPostX", &EdetMMPostX, &b_EdetMMPostX);
   fChain->SetBranchAddress("EdetMMPostY", &EdetMMPostY, &b_EdetMMPostY);
   fChain->SetBranchAddress("EdetMMPostZ", &EdetMMPostZ, &b_EdetMMPostZ);
   fChain->SetBranchAddress("EdetMMPx", &EdetMMPx, &b_EdetMMPx);
   fChain->SetBranchAddress("EdetMMPy", &EdetMMPy, &b_EdetMMPy);
   fChain->SetBranchAddress("EdetMMPz", &EdetMMPz, &b_EdetMMPz);
   fChain->SetBranchAddress("EdetMME", &EdetMME, &b_EdetMME);
   fChain->SetBranchAddress("EdetMMT", &EdetMMT, &b_EdetMMT);
   fChain->SetBranchAddress("EdetMMPostT", &EdetMMPostT, &b_EdetMMPostT);
   fChain->SetBranchAddress("PdetMMPreX", &PdetMMPreX, &b_PdetMMPreX);
   fChain->SetBranchAddress("PdetMMPreY", &PdetMMPreY, &b_PdetMMPreY);
   fChain->SetBranchAddress("PdetMMPreZ", &PdetMMPreZ, &b_PdetMMPreZ);
   fChain->SetBranchAddress("PdetMMPostX", &PdetMMPostX, &b_PdetMMPostX);
   fChain->SetBranchAddress("PdetMMPostY", &PdetMMPostY, &b_PdetMMPostY);
   fChain->SetBranchAddress("PdetMMPostZ", &PdetMMPostZ, &b_PdetMMPostZ);
   fChain->SetBranchAddress("PdetMMPx", &PdetMMPx, &b_PdetMMPx);
   fChain->SetBranchAddress("PdetMMPy", &PdetMMPy, &b_PdetMMPy);
   fChain->SetBranchAddress("PdetMMPz", &PdetMMPz, &b_PdetMMPz);
   fChain->SetBranchAddress("PdetMME", &PdetMME, &b_PdetMME);
   fChain->SetBranchAddress("PdetMMT", &PdetMMT, &b_PdetMMT);
   fChain->SetBranchAddress("PdetMMPostT", &PdetMMPostT, &b_PdetMMPostT);
   Notify();
}

Bool_t Nab_sim_analyzer_v2::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void Nab_sim_analyzer_v2::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t Nab_sim_analyzer_v2::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef Nab_sim_analyzer_v2_cxx
