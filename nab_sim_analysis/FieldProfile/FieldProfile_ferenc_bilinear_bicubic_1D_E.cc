#include "TROOT.h"
#include "TStyle.h"
#include "TTree.h"
#include "TLatex.h"
#include "TGraphErrors.h"
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <math.h>
#include <TArrayD.h>
#include "TH2D.h"
#include "TH1D.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "TMath.h"
#include "TRint.h"
#include "TCanvas.h"
#include "TTreeFormula.h"
#include "TLegend.h"
#include "TFile.h"
#include "TCut.h"
#include "TImage.h"
#include "TSystem.h"
#include "TPaveText.h"
#include "TPaveStats.h"
#include <sys/time.h>
#include <sys/resource.h>
#include "TColor.h"
using namespace std;


void FieldProfile_ferenc_bilinear_bicubic_1D_E()
{

  TTree *MyTree1 = new TTree("MyTree1", "MyTree1");	
  MyTree1->ReadFile("../FieldProfile_ferenc_bilinear_z_1mm.dat", "Z:R:Bz:Br:Ez:Er:Bz1:Br1:Ez1:Er1:diffBz:diffBr:diffEz:diffEr");
  TTree *MyTree2 = new TTree("MyTree2", "MyTree2");	
  MyTree2->ReadFile("../FieldProfile_ferenc_bicubic_z_1mm.dat", "Z:R:Bz:Br:Ez:Er:Bz1:Br1:Ez1:Er1:diffBz:diffBr:diffEz:diffEr");
  //Int_t palette[20];
  //for (int i=0;i<20;i++) palette[i] = 100-2*(19-i);
  //gStyle->SetPalette(20,palette);

  ////////////////////////////////////////////////////
  //start to draw can11 (Analyzer Pad 1)
	
  TCanvas *can11 = new TCanvas("can11", "Diagnostic Pad 1", 1100, 800);
  gStyle->SetOptStat(0);
  can11->Divide(2, 3, 0.005, 0.005);
		
  can11->cd(1)->SetGrid();
  MyTree1->Draw("diffEz/Ez:Z>>h1()", "Z<5000 && Z>-1200", "*");//abs(diffBz/Bz)>10^-6
  TH1D *h1 = (TH1D*) gDirectory->Get("h1");
  h1->SetXTitle("R (mm)");
  h1->GetXaxis()->CenterTitle();
  h1->SetYTitle("Z (mm)");
  h1->GetYaxis()->CenterTitle();
  //h1->GetZaxis()->SetLabelSize(0.025); 
  h1->SetTitle("diffBz (bilinear)");
  can11->Modified();	
  can11->Update();
  can11->Modified();
		
  can11->cd(2)->SetGrid();
  MyTree2->Draw("diffEz/Ez:Z>>h2()", "Z<5000 && Z>-1200", "*");
  TH1D *h2 = (TH1D*) gDirectory->Get("h2");
  h2->GetXaxis()->SetTitle("Z (mm)");
  h2->GetXaxis()->CenterTitle();
  h2->SetYTitle("Z (mm)");
  h2->GetYaxis()->CenterTitle();
  //h2->GetZaxis()->SetLabelSize(0.025); 
  h2->SetTitle("diffBz (bicubic)");
  can11->Modified();	
  can11->Update();
  can11->Modified();
		
  can11->cd(3)->SetGrid();
  MyTree1->Draw("diffEz/Ez:Z>>h3()", "Z<-600 && Z>-1200 && abs(diffEz/Ez)<0.01", "*");
  TH1D *h3 = (TH1D*) gDirectory->Get("h3");
		
  //h3->SetMarkerStyle(20);
  //h3->SetMarkerSize(0.1);
  h3->SetXTitle("R (mm)");
  h3->GetXaxis()->CenterTitle();
  h3->SetYTitle("Z (mm)");
  h3->GetYaxis()->CenterTitle();
  //h3->GetZaxis()->SetLabelSize(0.025); 
  h3->SetTitle("diffBr (bilinear)");
  can11->Modified();	
  can11->Modified();
  can11->Update();

  can11->cd(4)->SetGrid();
  MyTree2->Draw("diffEz/Ez:Z>>h4()", "Z<-600 && Z>-1200 && abs(diffEz/Ez)<0.01", "*");
  TH1D *h4 = (TH1D*) gDirectory->Get("h4");
		
  h4->SetXTitle("R (mm)");
  h4->GetXaxis()->CenterTitle();
  h4->SetYTitle("Z (mm)");
  h4->GetYaxis()->CenterTitle();
  //h4->GetZaxis()->SetLabelSize(0.025); 
  h4->SetTitle("diffBr (bicubic)");
  can11->Modified();	
  can11->Modified();
  can11->Update();
		
  can11->cd(5)->SetGrid();
  MyTree1->Draw("diffEz/Ez:Z>>h5()", "Z<5000 && Z>4000", "*");
  TH1D *h5 = (TH1D*) gDirectory->Get("h5");
		
  //h5->SetMarkerStyle(20);
  //h5->SetMarkerSize(0.1);
  h5->SetXTitle("R (mm)");
  h5->GetXaxis()->CenterTitle();
  h5->SetYTitle("Z (mm)");
  h5->GetYaxis()->CenterTitle();
  //h5->GetZaxis()->SetLabelSize(0.025); 
  h5->SetTitle("diffBr (bilinear)");
  can11->Modified();	
  can11->Modified();
  can11->Update();

  can11->cd(6)->SetGrid();
  MyTree2->Draw("diffEz/Ez:Z>>h6()", "Z<5000 && Z>4000", "*");
  TH1D *h6 = (TH1D*) gDirectory->Get("h6");
		
  h6->SetXTitle("R (mm)");
  h6->GetXaxis()->CenterTitle();
  h6->SetYTitle("Z (mm)");
  h6->GetYaxis()->CenterTitle();
  //h6->GetZaxis()->SetLabelSize(0.025); 
  h6->SetTitle("diffBr (bicubic)");
  can11->Modified();	
  can11->Modified();
  can11->Update();

  ////////////////////////////////////////////////////
  //start to draw can13 (Analyzer Pad 3)
	
  TCanvas *can13 = new TCanvas("can13", "Diagnostic Pad 3", 1100, 800);
  gStyle->SetOptStat(0);
  can13->Divide(2, 3, 0.005, 0.005);
		
  can13->cd(1)->SetGrid();
  MyTree1->Draw("diffEr/Er:Z>>hhh1()", "Z<5000 && Z>-1200", "*");//abs(diffBz/Bz)>10^-6
  TH1D *hhh1 = (TH1D*) gDirectory->Get("hhh1");
  hhh1->SetXTitle("R (mm)");
  hhh1->GetXaxis()->CenterTitle();
  hhh1->SetYTitle("Z (mm)");
  hhh1->GetYaxis()->CenterTitle();
  //hhh1->GetZaxis()->SetLabelSize(0.025); 
  hhh1->SetTitle("diffBz (bilinear)");
  can13->Modified();	
  can13->Update();
  can13->Modified();
		
  can13->cd(2)->SetGrid();
  MyTree2->Draw("diffEr/Er:Z>>hhh2()", "Z<5000 && Z>-1200", "*");
  TH1D *hhh2 = (TH1D*) gDirectory->Get("hhh2");
  hhh2->GetXaxis()->SetTitle("Z (mm)");
  hhh2->GetXaxis()->CenterTitle();
  hhh2->SetYTitle("Z (mm)");
  hhh2->GetYaxis()->CenterTitle();
  //hhh2->GetZaxis()->SetLabelSize(0.025); 
  hhh2->SetTitle("diffBz (bicubic)");
  can13->Modified();	
  can13->Update();
  can13->Modified();
		
  can13->cd(3)->SetGrid();
  MyTree1->Draw("diffEr/Er:Z>>hhh3()", "Z<-600 && Z>-1200 && abs(diffEr/Er)<0.002", "*");
  TH1D *hhh3 = (TH1D*) gDirectory->Get("hhh3");
		
  //h3->SetMarkerStyle(20);
  //h3->SetMarkerSize(0.1);
  hhh3->SetXTitle("R (mm)");
  hhh3->GetXaxis()->CenterTitle();
  hhh3->SetYTitle("Z (mm)");
  hhh3->GetYaxis()->CenterTitle();
  //hhh3->GetZaxis()->SetLabelSize(0.025); 
  hhh3->SetTitle("diffBr (bilinear)");
  can13->Modified();	
  can13->Modified();
  can13->Update();

  can13->cd(4)->SetGrid();
  MyTree2->Draw("diffEr/Er:Z>>hhh4()", "Z<-600 && Z>-1200 && abs(diffEr/Er)<0.002", "*");
  TH1D *hhh4 = (TH1D*) gDirectory->Get("hhh4");
		
  hhh4->SetXTitle("R (mm)");
  hhh4->GetXaxis()->CenterTitle();
  hhh4->SetYTitle("Z (mm)");
  hhh4->GetYaxis()->CenterTitle();
  //hhh4->GetZaxis()->SetLabelSize(0.025); 
  hhh4->SetTitle("diffBr (bicubic)");
  can13->Modified();	
  can13->Modified();
  can13->Update();
		
  can13->cd(5)->SetGrid();
  MyTree1->Draw("diffEr/Er:Z>>hhh5()", "Z<5000 && Z>4000 && abs(diffEr/Er)<0.002", "*");
  TH1D *hhh5 = (TH1D*) gDirectory->Get("hhh5");
		
  //hhh5->SetMarkerStyle(20);
  //hhh5->SetMarkerSize(0.1);
  hhh5->SetXTitle("R (mm)");
  hhh5->GetXaxis()->CenterTitle();
  hhh5->SetYTitle("Z (mm)");
  hhh5->GetYaxis()->CenterTitle();
  //hhh5->GetZaxis()->SetLabelSize(0.025); 
  hhh5->SetTitle("diffBr (bilinear)");
  can13->Modified();	
  can13->Modified();
  can13->Update();

  can13->cd(6)->SetGrid();
  MyTree2->Draw("diffEr/Er:Z>>hhh6()", "Z<5000 && Z>4000 && abs(diffEr/Er)<0.002", "*");
  TH1D *hhh6 = (TH1D*) gDirectory->Get("hhh6");
		
  hhh6->SetXTitle("R (mm)");
  hhh6->GetXaxis()->CenterTitle();
  hhh6->SetYTitle("Z (mm)");
  hhh6->GetYaxis()->CenterTitle();
  //hhh6->GetZaxis()->SetLabelSize(0.025); 
  hhh6->SetTitle("diffBr (bicubic)");
  can13->Modified();	
  can13->Modified();
  can13->Update();

  can11->Print("FieldProfile_bilinear_vs_bicubic_to_ferenc_z_1mm_E.pdf(");

  can13->Print("FieldProfile_bilinear_vs_bicubic_to_ferenc_z_1mm_E.pdf)");

  //can11->Print("FieldProfile_bilinear_vs_bicubic_to_ferenc_B.pdf");
  //can11->Print("FieldProfile_bilinear_vs_bicubic_to_ferenc_EFR_B.png");
  //can12->Print("FieldProfile_bilinear_vs_bicubic_to_ferenc_EFR_E.png");
		
}
