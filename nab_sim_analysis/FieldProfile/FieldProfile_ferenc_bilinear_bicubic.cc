#include "TROOT.h"
#include "TStyle.h"
#include "TTree.h"
#include "TLatex.h"
#include "TGraphErrors.h"
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <math.h>
#include <TArrayD.h>
#include "TH2D.h"
#include "TH1D.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "TMath.h"
#include "TRint.h"
#include "TCanvas.h"
#include "TTreeFormula.h"
#include "TLegend.h"
#include "TFile.h"
#include "TCut.h"
#include "TImage.h"
#include "TSystem.h"
#include "TPaveText.h"
#include "TPaveStats.h"
#include <sys/time.h>
#include <sys/resource.h>
#include "TColor.h"
using namespace std;


void FieldProfile_ferenc_bilinear_bicubic()
{

  TTree *MyTree1 = new TTree("MyTree1", "MyTree1");	
  MyTree1->ReadFile("../FieldProfile_ferenc_2D_1D_EM.dat", "Z:R:Bz:Br:Ez:Er:Bz1:Br1:Ez1:Er1:Bz2:Br2:Ez2:Er2:diffBz1:diffBr1:diffEz1:diffEr1:diffBz2:diffBr2:diffEz2:diffEr2");
  TTree *MyTree2 = new TTree("MyTree2", "MyTree2");	
  MyTree2->ReadFile("../FieldProfile_ferenc_2D_1D_EM.dat", "Z:R:Bz:Br:Ez:Er:Bz1:Br1:Ez1:Er1:Bz2:Br2:Ez2:Er2:diffBz1:diffBr1:diffEz1:diffEr1:diffBz2:diffBr2:diffEz2:diffEr2");
  //Int_t palette[20];
  //for (int i=0;i<20;i++) palette[i] = 100-2*(19-i);
  //gStyle->SetPalette(20,palette);

  ////////////////////////////////////////////////////
  //start to draw can11 (Analyzer Pad 1)
	
  TCanvas *can11 = new TCanvas("can11", "Diagnostic Pad 1", 1100, 800);
  gStyle->SetOptStat(0);
  can11->Divide(2, 2, 0.005, 0.005);
		
  can11->cd(1)->SetGridx();
  MyTree1->Draw("Z:R:abs(diffBz1/Bz)>>h1()", "Bz>0", "COLZ");//abs(diffBz/Bz)>10^-6
  TH1D *h1 = (TH1D*) gDirectory->Get("h1");
	
  h1->SetMarkerSize(100);
  h1->SetXTitle("R (mm)");
  h1->GetXaxis()->CenterTitle();
  h1->SetYTitle("Z (mm)");
  h1->GetYaxis()->CenterTitle();
  h1->GetZaxis()->SetLabelSize(0.025); 
  h1->SetTitle("B");
  can11->Modified();	
  can11->Update();
  can11->Modified();
		
  can11->cd(2)->SetGridx();
  MyTree2->Draw("Z:R:abs(diffBz2/Bz)>>h2()", "Bz>0", "COLZ");
  TH1D *h2 = (TH1D*) gDirectory->Get("h2");
	
  h2->SetXTitle("R (mm)");
  h2->GetXaxis()->CenterTitle();
  h2->SetYTitle("Z (mm)");
  h2->GetYaxis()->CenterTitle();
  h2->GetZaxis()->SetLabelSize(0.025); 
  h2->SetTitle("B");
  can11->Modified();	
  can11->Update();
  can11->Modified();
		
  can11->cd(3)->SetGridx();
  MyTree1->Draw("Z:R:abs(diffBr1/Br)>>h3()", "Br>0", "COLZ");
  TH1D *h3 = (TH1D*) gDirectory->Get("h3");
		
  h3->SetXTitle("R (mm)");
  h3->GetXaxis()->CenterTitle();
  h3->SetYTitle("Z (mm)");
  h3->GetYaxis()->CenterTitle();
  h3->GetZaxis()->SetLabelSize(0.025); 
  h3->SetTitle("B");
  can11->Modified();	
  can11->Modified();
  can11->Update();

  can11->cd(4)->SetGridx();
  MyTree2->Draw("Z:R:abs(diffBr2/Br)>>h4()", "Br>0", "COLZ");
  TH1D *h4 = (TH1D*) gDirectory->Get("h4");
		
  h4->SetXTitle("R (mm)");
  h4->GetXaxis()->CenterTitle();
  h4->SetYTitle("Z (mm)");
  h4->GetYaxis()->CenterTitle();
  h4->GetZaxis()->SetLabelSize(0.025); 
  h4->SetTitle("Br");
  can11->Modified();	
  can11->Modified();
  can11->Update();

  ////////////////////////////////////////////////////
  //start to draw can12 (Analyzer Pad 2)
	
  TCanvas *can12 = new TCanvas("can12", "Diagnostic Pad 2", 1100, 800);
  gStyle->SetOptStat(0);
  can12->Divide(2, 2, 0.005, 0.005);
	
  can12->cd(1)->SetGridy();
  MyTree1->Draw("Z:R:abs(diffEz1/Ez)>>hh1()", "Ez>0", "COLZ");
  TH1D *hh1 = (TH1D*) gDirectory->Get("hh1");
	
  //hh1->SetMarkerSize(1000);
  //hh1->SetMarkerStyle(20);
  hh1->SetXTitle("R (mm)");
  hh1->GetXaxis()->CenterTitle();
  hh1->SetYTitle("Z (mm)");
  hh1->GetYaxis()->CenterTitle();
  hh1->GetZaxis()->SetLabelSize(0.025); 
  hh1->SetTitle("diffEz (bilinear)");
  can12->Modified();	
  can12->Modified();
  can12->Update();
	
  can12->cd(2)->SetGridy();
  MyTree2->Draw("Z:R:abs(diffEz2/Ez)>>hh2()", "Ez>0", "COLZ");
  TH1D *hh2 = (TH1D*) gDirectory->Get("hh2");
	
  hh2->SetXTitle("R (mm)");
  hh2->GetXaxis()->CenterTitle();
  hh2->SetYTitle("Z (mm)");
  hh2->GetYaxis()->CenterTitle();
  hh2->GetZaxis()->SetLabelSize(0.025); 
  hh2->SetTitle("diffEz (bicubic)");
  can12->Modified();	
  can12->Modified();
  can12->Update();
	
  can12->cd(3)->SetGridy();
  MyTree1->Draw("Z:R:abs(diffEr1/Er)>>hh3()", "Er>0", "COLZ");
  TH1D *hh3 = (TH1D*) gDirectory->Get("hh3");
	
  hh3->SetXTitle("R (mm)");
  hh3->GetXaxis()->CenterTitle();
  hh3->SetYTitle("Z (mm)");
  hh3->GetYaxis()->CenterTitle();
  hh3->GetZaxis()->SetLabelSize(0.025); 
  hh3->SetTitle("diffEr (bilinear)");
  can12->Modified();	
  can12->Modified();
  can12->Update();
	
  can12->cd(4)->SetGridy();
  MyTree2->Draw("Z:R:abs(diffEr2/Er)>>hh4()", "Er>0", "COLZ");
  TH1D *hh4 = (TH1D*) gDirectory->Get("hh4");
	
  hh4->SetXTitle("R (mm)");
  hh4->GetXaxis()->CenterTitle();
  hh4->SetYTitle("Z (mm)");
  hh4->GetYaxis()->CenterTitle();
  hh4->GetZaxis()->SetLabelSize(0.025); 
  hh4->SetTitle("diffEr (bicubic)");
  can12->Modified();	
  can12->Modified();
  can12->Update();

  //can11->Print("FieldProfile_bilinear_vs_bicubic_to_ferenc_B.pdf");
  //can11->Print("FieldProfile_2D_vs_1D_to_ferenc_EFR_B.png");
  //can12->Print("FieldProfile_2D_vs_1D_to_ferenc_EFR_E.png");
		
}
