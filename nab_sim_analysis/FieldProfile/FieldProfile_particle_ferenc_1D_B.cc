#include "TROOT.h"
#include "TStyle.h"
#include "TTree.h"
#include "TLatex.h"
#include "TGraphErrors.h"
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <math.h>
#include <TArrayD.h>
#include "TH2D.h"
#include "TH1D.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "TMath.h"
#include "TRint.h"
#include "TCanvas.h"
#include "TTreeFormula.h"
#include "TLegend.h"
#include "TFile.h"
#include "TCut.h"
#include "TImage.h"
#include "TSystem.h"
#include "TPaveText.h"
#include "TPaveStats.h"
#include <sys/time.h>
#include <sys/resource.h>
#include "TColor.h"
using namespace std;


void FieldProfile_particle_ferenc_1D_B()
{

  TTree *MyTree1 = new TTree("MyTree1", "MyTree1");	
  MyTree1->ReadFile("../FieldProfile_particle4604008_ferenc_1D.dat", "Z:R:Bz:Br:Bz1:Br1");
  TTree *MyTree2 = new TTree("MyTree2", "MyTree2");	
  MyTree2->ReadFile("../FieldProfile_particle4604008_ferenc_1D.dat", "Z:R:Bz:Br:Bz1:Br1");
  Int_t palette[100];
  for (int i=0;i<100;i++) palette[i] = 100-0.8*(100-i);
  gStyle->SetPalette(100,palette);

  ////////////////////////////////////////////////////
  //start to draw can11 (Analyzer Pad 1)
	
  TCanvas *can11 = new TCanvas("can11", "Diagnostic Pad 1", 1100, 800);
  gStyle->SetOptStat(0);
  can11->Divide(2, 2, 0.005, 0.005);
		
  can11->cd(1)->SetGrid();
  MyTree1->Draw("R:Z:Bz>>h1()", "", "COLZ");//abs(diffBz/Bz)>10^-6
  TH1D *h1 = (TH1D*) gDirectory->Get("h1");
  h1->SetXTitle("Z (mm)");
  h1->GetXaxis()->CenterTitle();
  h1->SetYTitle("R (mm)");
  h1->GetYaxis()->CenterTitle();
  //h1->GetZaxis()->SetLabelSize(0.025); 
  h1->SetTitle("Bz from ferenc(T)");
  can11->Modified();	
  can11->Update();
  can11->Modified();
		
  can11->cd(2)->SetGrid();
  MyTree2->Draw("R:Z:abs((Bz1-Bz)/Bz)>>h2()", "Z<4990", "COLZ");
  TH1D *h2 = (TH1D*) gDirectory->Get("h2");
  h2->GetXaxis()->SetTitle("Z (mm)");
  h2->GetXaxis()->CenterTitle();
  h2->SetYTitle("R (mm)");
  h2->GetYaxis()->CenterTitle();
  //h2->GetZaxis()->SetLabelSize(0.025); 
  h2->SetTitle("Relative diff of Bz (1D to ferenc)");
  can11->Modified();	
  can11->Update();
  can11->Modified();
		
  can11->cd(3)->SetGrid();
  MyTree1->Draw("R:Z:abs(Br)>>h3()", "", "COLZ");// && abs(diffBz/Bz)<0.1
  TH1D *h3 = (TH1D*) gDirectory->Get("h3");
		
  //h3->SetMarkerStyle(20);
  //h3->SetMarkerSize(0.1);
  h3->SetXTitle("Z (mm)");
  h3->GetXaxis()->CenterTitle();
  h3->SetYTitle("R (mm)");
  h3->GetYaxis()->CenterTitle();
  //h3->GetZaxis()->SetLabelSize(0.025); 
  h3->SetTitle("Br from ferenc(T)");
  can11->Modified();	
  can11->Modified();
  can11->Update();

  can11->cd(4)->SetGrid();
  MyTree2->Draw("R:Z:abs((Br1-Br)/Br)>>h4()", "Z<4990", "COLZ");
  TH1D *h4 = (TH1D*) gDirectory->Get("h4");
		
  h4->SetXTitle("Z (mm)");
  h4->GetXaxis()->CenterTitle();
  h4->SetYTitle("R (mm)");
  h4->GetYaxis()->CenterTitle();
  //h4->GetZaxis()->SetLabelSize(0.025); 
  h4->SetTitle("Relative diff of Br (1D to ferenc)");
  can11->Modified();	
  can11->Modified();
  can11->Update();
/*		
  can11->cd(5)->SetGrid();
  MyTree1->Draw("diffBz1/Bz:Z>>h5()", "Bz!=0 && Z<600 && Z>-600 && R==0", "*");
  TH1D *h5 = (TH1D*) gDirectory->Get("h5");
		
  //h5->SetMarkerStyle(20);
  //h5->SetMarkerSize(0.1);
  h5->SetXTitle("R (mm)");
  h5->GetXaxis()->CenterTitle();
  h5->SetYTitle("Z (mm)");
  h5->GetYaxis()->CenterTitle();
  //h5->GetZaxis()->SetLabelSize(0.025); 
  h5->SetTitle("diffBz (bilinear)");
  can11->Modified();	
  can11->Modified();
  can11->Update();

  can11->cd(6)->SetGrid();
  MyTree2->Draw("diffBz2/Bz:Z>>h6()", "Bz!=0 && Z<600 && Z>-600 && R==0", "*");
  TH1D *h6 = (TH1D*) gDirectory->Get("h6");
		
  h6->SetXTitle("R (mm)");
  h6->GetXaxis()->CenterTitle();
  h6->SetYTitle("Z (mm)");
  h6->GetYaxis()->CenterTitle();
  //h6->GetZaxis()->SetLabelSize(0.025); 
  h6->SetTitle("diffBz (bicubic)");
  can11->Modified();	
  can11->Modified();
  can11->Update();

  ////////////////////////////////////////////////////
  //start to draw can12 (Analyzer Pad 2)
	
  TCanvas *can12 = new TCanvas("can12", "Diagnostic Pad 2", 1100, 800);
  gStyle->SetOptStat(0);
  can12->Divide(2, 2, 0.005, 0.005);
	
  can12->cd(1)->SetGrid();
  MyTree1->Draw("diffBz1/Bz:Z>>hh1()", "Bz!=0 && Z<4000 && Z>600 && R==0", "*");
  TH1D *hh1 = (TH1D*) gDirectory->Get("hh1");
	
  //hh1->SetMarkerSize(1000);
  //hh1->SetMarkerStyle(20);
  hh1->SetXTitle("R (mm)");
  hh1->GetXaxis()->CenterTitle();
  hh1->SetYTitle("Z (mm)");
  hh1->GetYaxis()->CenterTitle();
  //hh1->GetZaxis()->SetLabelSize(0.025); 
  hh1->SetTitle("diffBz (bilinear)");
  can12->Modified();	
  can12->Modified();
  can12->Update();
	
  can12->cd(2)->SetGrid();
  MyTree2->Draw("diffBz2/Bz:Z>>hh2()", "Bz!=0 && Z<4000 && Z>600 && R==0", "*");
  TH1D *hh2 = (TH1D*) gDirectory->Get("hh2");
	
  hh2->SetXTitle("R (mm)");
  hh2->GetXaxis()->CenterTitle();
  hh2->SetYTitle("Z (mm)");
  hh2->GetYaxis()->CenterTitle();
  //hh2->GetZaxis()->SetLabelSize(0.025); 
  hh2->SetTitle("diffBz (bicubic)");
  can12->Modified();	
  can12->Modified();
  can12->Update();
	
  can12->cd(3)->SetGrid();
  MyTree1->Draw("diffBz1/Bz:Z>>hh3()", "Bz!=0 && Z<4995 && Z>4000 && R==0", "*");
  TH1D *hh3 = (TH1D*) gDirectory->Get("hh3");
	
  hh3->SetXTitle("R (mm)");
  hh3->GetXaxis()->CenterTitle();
  hh3->SetYTitle("Z (mm)");
  hh3->GetYaxis()->CenterTitle();
  //hh3->GetZaxis()->SetLabelSize(0.025); 
  hh3->SetTitle("diffBz (bilinear)");
  can12->Modified();	
  can12->Modified();
  can12->Update();
	
  can12->cd(4)->SetGrid();
  MyTree2->Draw("diffBz2/Bz:Z>>hh4()", "Bz!=0 && Z<4995 && Z>4000 && R==0", "*");
  TH1D *hh4 = (TH1D*) gDirectory->Get("hh4");
	
  hh4->SetXTitle("R (mm)");
  hh4->GetXaxis()->CenterTitle();
  hh4->SetYTitle("Z (mm)");
  hh4->GetYaxis()->CenterTitle();
  //hh4->GetZaxis()->SetLabelSize(0.025); 
  hh4->SetTitle("diffBz (bicubic)");
  can12->Modified();	
  can12->Modified();
  can12->Update();
      

  ////////////////////////////////////////////////////
  //start to draw can13 (Analyzer Pad 3)
	
  TCanvas *can13 = new TCanvas("can13", "Diagnostic Pad 3", 1100, 800);
  gStyle->SetOptStat(0);
  can13->Divide(2, 3, 0.005, 0.005);
		
  can13->cd(1)->SetGrid();
  MyTree1->Draw("diffBr1/Br:Z>>hhh1()", "Br!=0 && Z<5000 && Z>-1200 && R==0", "*");//abs(diffBz/Bz)>10^-6
  TH1D *hhh1 = (TH1D*) gDirectory->Get("hhh1");
  hhh1->SetXTitle("R (mm)");
  hhh1->GetXaxis()->CenterTitle();
  hhh1->SetYTitle("Z (mm)");
  hhh1->GetYaxis()->CenterTitle();
  //hhh1->GetZaxis()->SetLabelSize(0.025); 
  hhh1->SetTitle("diffBz (bilinear)");
  can13->Modified();	
  can13->Update();
  can13->Modified();
		
  can13->cd(2)->SetGrid();
  MyTree2->Draw("diffBr2/Br:Z>>hhh2()", "Br!=0 && Z<5000 && Z>-1200 && R==0", "*");
  TH1D *hhh2 = (TH1D*) gDirectory->Get("hhh2");
  hhh2->GetXaxis()->SetTitle("Z (mm)");
  hhh2->GetXaxis()->CenterTitle();
  hhh2->SetYTitle("Z (mm)");
  hhh2->GetYaxis()->CenterTitle();
  //hhh2->GetZaxis()->SetLabelSize(0.025); 
  hhh2->SetTitle("diffBz (bicubic)");
  can13->Modified();	
  can13->Update();
  can13->Modified();
		
  can13->cd(3)->SetGrid();
  MyTree1->Draw("diffBr1/Br:Z>>hhh3()", "Br!=0 && Z<-600 && Z>-1195 && R==0", "*");
  TH1D *hhh3 = (TH1D*) gDirectory->Get("hhh3");
		
  //h3->SetMarkerStyle(20);
  //h3->SetMarkerSize(0.1);
  hhh3->SetXTitle("R (mm)");
  hhh3->GetXaxis()->CenterTitle();
  hhh3->SetYTitle("Z (mm)");
  hhh3->GetYaxis()->CenterTitle();
  //hhh3->GetZaxis()->SetLabelSize(0.025); 
  hhh3->SetTitle("diffBr (bilinear)");
  can13->Modified();	
  can13->Modified();
  can13->Update();

  can13->cd(4)->SetGrid();
  MyTree2->Draw("diffBr2/Br:Z>>hhh4()", "Br!=0 && Z<-600 && Z>-1195 && R==0", "*");
  TH1D *hhh4 = (TH1D*) gDirectory->Get("hhh4");
		
  hhh4->SetXTitle("R (mm)");
  hhh4->GetXaxis()->CenterTitle();
  hhh4->SetYTitle("Z (mm)");
  hhh4->GetYaxis()->CenterTitle();
  //hhh4->GetZaxis()->SetLabelSize(0.025); 
  hhh4->SetTitle("diffBr (bicubic)");
  can13->Modified();	
  can13->Modified();
  can13->Update();
		
  can13->cd(5)->SetGrid();
  MyTree1->Draw("diffBr1/Br:Z>>hhh5()", "Br!=0 && Z<600 && Z>-600 && R==0", "*");
  TH1D *hhh5 = (TH1D*) gDirectory->Get("hhh5");
		
  //hhh5->SetMarkerStyle(20);
  //hhh5->SetMarkerSize(0.1);
  hhh5->SetXTitle("R (mm)");
  hhh5->GetXaxis()->CenterTitle();
  hhh5->SetYTitle("Z (mm)");
  hhh5->GetYaxis()->CenterTitle();
  //hhh5->GetZaxis()->SetLabelSize(0.025); 
  hhh5->SetTitle("diffBr (bilinear)");
  can13->Modified();	
  can13->Modified();
  can13->Update();

  can13->cd(6)->SetGrid();
  MyTree2->Draw("diffBr2/Br:Z>>hhh6()", "Br!=0 && Z<600 && Z>-600 && R==0", "*");
  TH1D *hhh6 = (TH1D*) gDirectory->Get("hhh6");
		
  hhh6->SetXTitle("R (mm)");
  hhh6->GetXaxis()->CenterTitle();
  hhh6->SetYTitle("Z (mm)");
  hhh6->GetYaxis()->CenterTitle();
  //hhh6->GetZaxis()->SetLabelSize(0.025); 
  hhh6->SetTitle("diffBr (bicubic)");
  can13->Modified();	
  can13->Modified();
  can13->Update();

  ////////////////////////////////////////////////////
  //start to draw can12 (Analyzer Pad 2)
	
  TCanvas *can14 = new TCanvas("can14", "Diagnostic Pad 4", 1100, 800);
  gStyle->SetOptStat(0);
  can14->Divide(2, 2, 0.005, 0.005);
	
  can14->cd(1)->SetGrid();
  MyTree1->Draw("diffBr1/Br:Z>>hhhh1()", "Br!=0 && Z<4000 && Z>600 && R==0", "*");
  TH1D *hhhh1 = (TH1D*) gDirectory->Get("hhhh1");
	
  //hhhh1->SetMarkerSize(1000);
  //hhhh1->SetMarkerStyle(20);
  hhhh1->SetXTitle("R (mm)");
  hhhh1->GetXaxis()->CenterTitle();
  hhhh1->SetYTitle("Z (mm)");
  hhhh1->GetYaxis()->CenterTitle();
  //hh1->GetZaxis()->SetLabelSize(0.025); 
  hhhh1->SetTitle("diffBz (bilinear)");
  can14->Modified();	
  can14->Modified();
  can14->Update();
	
  can14->cd(2)->SetGrid();
  MyTree2->Draw("diffBr2/Br:Z>>hhhh2()", "Br!=0 && Z<4000 && Z>600 && R==0", "*");
  TH1D *hhhh2 = (TH1D*) gDirectory->Get("hhhh2");
	
  hhhh2->SetXTitle("R (mm)");
  hhhh2->GetXaxis()->CenterTitle();
  hhhh2->SetYTitle("Z (mm)");
  hhhh2->GetYaxis()->CenterTitle();
  //hhhh2->GetZaxis()->SetLabelSize(0.025); 
  hhhh2->SetTitle("diffBz (bicubic)");
  can14->Modified();	
  can14->Modified();
  can14->Update();
	
  can14->cd(3)->SetGrid();
  MyTree1->Draw("diffBr1/Br:Z>>hhhh3()", "Br!=0 && Z<4995 && Z>4000 && R==0", "*");
  TH1D *hhhh3 = (TH1D*) gDirectory->Get("hhhh3");
	
  hhhh3->SetXTitle("R (mm)");
  hhhh3->GetXaxis()->CenterTitle();
  hhhh3->SetYTitle("Z (mm)");
  hhhh3->GetYaxis()->CenterTitle();
  //hhhh3->GetZaxis()->SetLabelSize(0.025); 
  hhhh3->SetTitle("diffBr (bilinear)");
  can14->Modified();	
  can14->Modified();
  can14->Update();
	
  can14->cd(4)->SetGrid();
  MyTree2->Draw("diffBr2/Br:Z>>hhhh4()", "Br!=0 && Z<4995 && Z>4000 && R==0", "*");
  TH1D *hhhh4 = (TH1D*) gDirectory->Get("hhhh4");
	
  hhhh4->SetXTitle("R (mm)");
  hhhh4->GetXaxis()->CenterTitle();
  hhhh4->SetYTitle("Z (mm)");
  hhhh4->GetYaxis()->CenterTitle();
  //hhhh4->GetZaxis()->SetLabelSize(0.025); 
  hhhh4->SetTitle("diffBr (bicubic)");
  can14->Modified();	
  can14->Modified();
  can14->Update();
*/

  //can11->Print("FieldProfile_particle4604008_ferenc_1D_B.pdf");
  //can11->Print("FieldProfile_particle4604008_ferenc_1D_B.png");

/*  can12->Print("FieldProfile_2D_vs_1D_to_ferenc_B_r_0.pdf(");

  can13->Print("FieldProfile_2D_vs_1D_to_ferenc_B_r_0.pdf(");

  can14->Print("FieldProfile_2D_vs_1D_to_ferenc_B_r_0.pdf)");
*/
  //can11->Print("FieldProfile_bilinear_vs_bicubic_to_ferenc_B.pdf");
  //can11->Print("FieldProfile_bilinear_vs_bicubic_to_ferenc_EFR_B.png");
  //can12->Print("FieldProfile_bilinear_vs_bicubic_to_ferenc_EFR_E.png");
		
}
