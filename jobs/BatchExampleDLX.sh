#!/bin/bash

##SBATCH --nodes=1
##SBATCH --ntasks-per-node=1
##SBATCH --cpus-per-task=1

# we expect the job to finish within 5 hours. If it takes longer than 5
# hours, SLURM can kill it:
#SBATCH --time=0:10:00
 
# we want the job to be named "myTest" rather than something generated
# from the script name. This will affect the name of the job as reported
# by squeue:
#SBATCH --job-name=LJBTest
 
## when the job ends, send me an email at this email address.
##SBATCH --mail-type=END
##SBATCH --mail-user=broussardlj@ornl.gov

# both standard output and standard error are directed to the same file.
# It will be placed in the directory I submitted the job from and will
# have a name like slurm_12345.out
#SBATCH --output=slurm_%j.out

source /etc/profile.d/modules.sh
source /home/lbr268/.bashrc

RUNDIR=$NABSIM/build
cd $RUNDIR

NabSim "Roger2.dat" "/scratch/lbr268/LJB" 1 macro/100Events.mac