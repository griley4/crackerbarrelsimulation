#! /bin/sh
#SBATCH -N 1
#SBATCH --ntasks-per-node=1
#SBATCH -c 1
#SBATCH -A pibeta
##Nab-magnet
#SBATCH -t 4:00:00
#SBATCH -p economy
##SBATCH --mem-per-cpu=20000
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jaf2qc@virginia.edu

$G4EXE/NabSim "Roger2.dat" test100 1 $NABSIM/macro/100Events.mac


